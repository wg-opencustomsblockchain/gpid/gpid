<!--
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3
-->

# GPID

GPID focuses on a blockchain-validated key data set as a trustworthy base for all steps in the customs process.

It contains data that is used recurrently in most customs processes

- Seller information
- Buyer information
- Invoice information
- Goods information

It is based on the actual invoice and is created early in the customs process.

The goal behind GPID is to create trust in data, increase transparency, speed up border processes, decrease transit times and physical inspections and increase the sustainability in international trade.

This is the modular "GPID" repository which houses the components of the GPID blockchain project.
We use the following technologies:

- Angular Frontend (Typescript)
- NestJS Backend Microservices (Typescript)
- Tendermint/Cosmos Blockchain (Golang)
- Rabbit MQ (MQP)

Further information about the software and the different services can be found in the docs.
As package manager we recommend using pnpm.
This project was generated based on the NX build system https://nx.dev/

## Documentation
Please find the documentation of this project in ./docs.

## License
Licensed under the Open Logistics Foundation License 1.3. For details on the licensing terms, see the LICENSE file.

## Licenses of third-party dependencies
For information about licenses of third-party dependencies, please refer to the README.md files of the corresponding components.


## Prerequisites

This project is tested with the following frameworks:

- Golang
- Node
- Starport or Ignite ver. v0.24.1 via ( `curl https://get.ignite.com/cli@v0.24.1! | bash`)

If required we recommend referring to the individual docker Files included in each project for detailed installation instructions.

### Installation of ignite and Golang

[Ignite](https://docs.ignite.com/) is an easy-to-use CLI tool for creating sovereign application-specific blockchains.
Blockchains created with Ignite use Cosmos SDK and Tendermint.
Ignite and the Cosmos SDK modules are written in the Go programming language.

Go is a prerequisite to use the Ignite CLI and the recommended version is 1.18.5.
To install Go, instructions from various places of the internet can be used, e.g. from [levelup.gitconnected.com](https://levelup.gitconnected.com/installing-go-on-ubuntu-b443a8f0eb55).
The recommended version of go is 1.18.5. You can download it [here](https://dl.google.com/go/go1.18.5.linux-amd64.tar.gz).

The following command will first download ignite and then move the content to "/usr/local/bin".
Therefore administrative access for this command is required.

```
curl -sL https://get.ignite.com/cli@v0.23.0! | sudo bash -
```

## Quick Start

As this project depends on the Tokenmanager base component we recommend checking out the repositories and submodules recursively via:  
`git clone --recursive`  
`git pull --recursive`

Before starting the application we need a RabbitMQ instance.
We Recommend using a docker container with following configuration:
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq`

Then, in another terminal:
Build and Initialize the Blockchain.
`yarn blockchain:init`

Start the Blockchain via the startup "init.sh" script.
`apps/blockchain/init.sh`

After pulling the software run:  
`yarn install`

To test all services run:  
`yarn test`

To start the frontend and all microservice run:  
`yarn all`

Refer to the "package.json" file in the project root for an overview of the nx based commands.

## Contact information
### Maintainer

**Schönborn, Matthias** \
Development / DevOps - Fraunhofer IML \
matthias.schoenborn@iml.fraunhofer.de

**Koller, Roman** \
Research Associate – Fraunhofer IML \
roman.koller@iml.fraunhofer.de

**Douglas, Michael** \
Senior Consultant - Customs Technology Services (CTS) \
mdouglas@als-cs.com