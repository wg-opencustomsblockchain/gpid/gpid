/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {RouterTestingModule} from '@angular/router/testing';
import {KeycloakProfile} from "keycloak-js";

describe('AppComponent', () => {
  let appComponent: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let keycloakServiceMock: any;
  const userProfileMock: KeycloakProfile = {
    id: 'id',
    username: 'username',
  }

  beforeEach(async () => {
    keycloakServiceMock = {
      isLoggedIn: jest.fn().mockResolvedValue(true),
      loadUserProfile: jest.fn().mockResolvedValue(userProfileMock),
      logout: jest.fn().mockReturnValue(undefined) // return 'void'
    }

    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [ RouterTestingModule, KeycloakAngularModule],
      providers: [
        {provide: KeycloakService, useValue: keycloakServiceMock},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    appComponent = fixture.componentInstance;
    fixture.detectChanges();

    // User hasn't logged in yet
    expect(appComponent.loggedIn).toBeFalsy();
    expect(appComponent.currentUser).toBeFalsy();
  });

  it('should login, then logout successfully', () => {
    expect(appComponent).toBeTruthy();

    // User has logged in
    expect(appComponent.loggedIn).toBeTruthy();
    expect(appComponent.currentUser).toBeTruthy();
    expect(appComponent.currentUser?.username).toBe('username');

    // User logouts
    appComponent.logout();
    expect(keycloakServiceMock.logout).toHaveBeenCalled();
  });
});

