/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './guard/auth.guard';
import {CheckDocComponent} from './_pages/check-doc/check-doc.component';
import {ErrorComponent} from './_pages/error/error.component';
import {HomeComponent} from './_pages/home/home.component';
import {ImportDocManyComponent} from './_pages/import-doc-many/import-doc-many.component';
import {ImprintComponent} from './_pages/imprint/imprint.component';
import {PrivacyPolicyComponent} from './_pages/privacy-policy/privacy-policy.component';
import {QueryIdComponent} from './_pages/query-id/query-id.component';
import {ShowIdComponent} from './_pages/show-id/show-id.component';
import {SuccessComponent} from './_pages/success/success.component';
import {ViewKeyDataSetComponent} from "./_pages/view-key-data-set/view-key-data-set.component";
import {EnterGoodsPassportIdComponent} from "./_pages/view-key-data-set/enter-goods-passport-id/enter-goods-passport-id.component";
import {AllKeyDataSetsComponent} from "./_pages/workaround/all-key-data-sets.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'docs',
    children: [
      {
        path: "add",
        component: ImportDocManyComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'hash',
        component: ImportDocManyComponent,
        canActivate: [AuthGuard]
      },
    ]
  },

  {
    path: 'docs/validate/:id',
    component: CheckDocComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'id/show',
    component: ShowIdComponent,
    canActivate: [AuthGuard],
  },
  {path: 'id/query', component: QueryIdComponent, canActivate: [AuthGuard]},
  {
    path: 'response/error',
    component: ErrorComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'response/success',
    component: SuccessComponent,
    canActivate: [AuthGuard],
  },
  {path: 'imprint', component: ImprintComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
  {
    path: 'enterGoodsPassportId',
    component: EnterGoodsPassportIdComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'viewKeyDataSet/:goodsPassportId',
    component: ViewKeyDataSetComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'enterKeyDataSet',
    component: ViewKeyDataSetComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'viewAllKeyDataSets',
    component: AllKeyDataSetsComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
