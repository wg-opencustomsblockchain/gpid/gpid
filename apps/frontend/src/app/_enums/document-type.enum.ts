/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export enum DocumentType {
  NONE = 'NONE',
  INV = 'INV',
  PLI = 'PLI',
  LIC = 'LIC',
  BOL = 'BOL',
  AWB = 'AWB',
  OTH = 'OTH',
}

export const DocumentType2LabelMapping: Record<DocumentType, string> = {
  [DocumentType.NONE]: 'Document Type',
  [DocumentType.INV]: 'Invoice',
  [DocumentType.PLI]: 'Packaging List',
  [DocumentType.LIC]: 'License',
  [DocumentType.BOL]: 'Bill of Lading',
  [DocumentType.AWB]: 'Airway Bill',
  [DocumentType.OTH]: 'Other',
};
