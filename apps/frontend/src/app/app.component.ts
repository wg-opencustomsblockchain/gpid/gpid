/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Component({
  selector: 'gpid-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  loggedIn = false;
  currentUser: KeycloakProfile | null = null;

  constructor(private keycloakService: KeycloakService) {}

  async ngOnInit(): Promise<void> {
    this.loggedIn = await this.keycloakService.isLoggedIn();
    if (this.loggedIn)
      this.currentUser = await this.keycloakService.loadUserProfile();
  }

  logout(): void {
    this.keycloakService.logout(window.location.origin);
  }
}
