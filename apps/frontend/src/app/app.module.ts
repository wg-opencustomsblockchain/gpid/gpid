/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './_pages/home/home.component';
import { ShowIdComponent } from './_pages/show-id/show-id.component';
import { QueryIdComponent } from './_pages/query-id/query-id.component';
import { CheckDocComponent } from './_pages/check-doc/check-doc.component';
import { SuccessComponent } from './_pages/success/success.component';
import { ErrorComponent } from './_pages/error/error.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializeKeycloak } from './init/keycloak-init.factory';
import { DndDirective } from './directives/dnd.directive';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { UploadComponent } from './_components/upload/upload.component';
import { ImprintComponent } from './_pages/imprint/imprint.component';
import { PrivacyPolicyComponent } from './_pages/privacy-policy/privacy-policy.component';
import { LoadingComponent } from './_components/loading/loading.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImportDocManyComponent } from './_pages/import-doc-many/import-doc-many.component';
import { EnterGoodsPassportIdComponent } from './_pages/view-key-data-set/enter-goods-passport-id/enter-goods-passport-id.component';
import { ViewKeyDataSetComponent } from './_pages/view-key-data-set/view-key-data-set.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AllKeyDataSetsComponent } from './_pages/workaround/all-key-data-sets.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { SecurityStatusComponent } from './_components/security-status/security-status.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ImportDocManyComponent,
    ShowIdComponent,
    QueryIdComponent,
    CheckDocComponent,
    SuccessComponent,
    ErrorComponent,
    DndDirective,
    UploadComponent,
    ImprintComponent,
    PrivacyPolicyComponent,
    LoadingComponent,
    ViewKeyDataSetComponent,
    EnterGoodsPassportIdComponent,
    AllKeyDataSetsComponent,
    SecurityStatusComponent,
    ViewKeyDataSetComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    KeycloakAngularModule,
    FormsModule,
    ClipboardModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSnackBarModule,
    NgxJsonViewerModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
