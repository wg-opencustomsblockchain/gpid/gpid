/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {BuyerSellerDto, GoodsItemDto, HashTokenDTO, InvoiceDto, KeyDataSetDto, ProcessDTO} from "@rhenus/api-interfaces";

export const EMPTY_GOODS_ITEM: GoodsItemDto = {
  commodityCode: {
    harmonizedSystemSubHeadingCode: "",
  },
  countryOfOrigin: "",
  countryOfPreferentialOrigin: "",
  descriptionOfGoods: "",
  netMass: "",
  invoiceAmount: "",
  invoiceCurrency: "",
  quantity: "",
  sequenceNumber: "",
}
export const EMPTY_INVOICE: InvoiceDto = {
  invoiceDate: "",
  invoiceNumber: "",
}
const EMPTY_BUYER_SELLER: BuyerSellerDto = {
  identification: {
    identificationNumber: '',
  },
  name: '',
  address: {
    streetAndNumber: '',
    postcode: '',
    country: '',
    city: '',
  }
}
export const EMPTY_DOCUMENT: HashTokenDTO = {
  creator: "",
  documentType: "",
  hash: "",
  hashFunction: "",
  id: "",
  timestamp: "",
  creatorName: "",
}

export const EMPTY_KEYDATASET_DTO: KeyDataSetDto = {
  goodsPassportId: '',
  creator: '',
  creatorName: '',
  timestamp: "",

  header: {
    seller: structuredClone(EMPTY_BUYER_SELLER),
    buyer: structuredClone(EMPTY_BUYER_SELLER),
    invoice: {...EMPTY_INVOICE},
  },
  goodsItem: [structuredClone(EMPTY_GOODS_ITEM)]
}

export const EMPTY_PROCESS_DTO: ProcessDTO = {
  processId: '',
  documents: [{...EMPTY_DOCUMENT}],
  keydataset: [structuredClone(EMPTY_KEYDATASET_DTO)]
}
