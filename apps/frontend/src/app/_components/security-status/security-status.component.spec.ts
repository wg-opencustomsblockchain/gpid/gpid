/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecurityStatusComponent } from './security-status.component';

describe('SecurityStatusComponent', () => {
  let component: SecurityStatusComponent;
  let fixture: ComponentFixture<SecurityStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SecurityStatusComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SecurityStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
