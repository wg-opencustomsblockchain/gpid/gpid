/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UploadComponent} from './upload.component';

describe('UploadComponent', () => {
  let component: UploadComponent;
  let fixture: ComponentFixture<UploadComponent>;
  const fileMock = {
    content: 'content of test-file',
    type: 'application/pdf',
  };

  const eventMock = {
    target: {files: [fileMock]},
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UploadComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should prepare file list on dropping file', () => {
    const filesMock = [new File([fileMock.content], 'test-file', {
      type: fileMock.type,
    })];
    component.onFileDropped(filesMock);

    expect(component.showError).toBeFalsy();
    expect(component.files.length).toBeGreaterThan(0);
  });

  it('should fail due to drop a non-pdf file', () => {
    const filesMock = [new File([fileMock.content], 'test-file', {
      type: 'application/zip',
    })];
    component.onFileDropped(filesMock);

    expect(component.showError).toBeTruthy();
    expect(component.files.length).toBe(0);
  });

  it('should prepare file list successfully on file selected', () => {
    component.onFileSelected(eventMock);
    expect(component.files.length).toBeGreaterThan(0);
    expect(component.fileDropEl.nativeElement.value).toBe('');
  });

  it('should delete file from the files list', () => {
    const filesMock = [new File([fileMock.content], 'test-file', {
      type: fileMock.type,
    })];

    component.onFileDropped(filesMock);
    expect(component.files.length).toBe(1);

    component.deleteFile(0);
    expect(component.files.length).toBe(0);
  });

  it('should convert bytes successfully', () => {
    expect(component.formatBytes(1000000)).toBe('976.56 KB');
    expect(component.formatBytes(0)).toBe('0 Bytes');
  });
});
