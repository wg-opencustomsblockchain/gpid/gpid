/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { KeyDataSetService } from './key-data-set.service';
import { environment } from '../../environments/environment';
import { KeyDataSetDto, ProcessDTO } from '@rhenus/api-interfaces';
import { of, throwError } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MessageNotificationService } from './message-notification.service';
import { HttpClient } from '@angular/common/http';
import { MessageNotificationLevel } from '../_enums/message-notification-level.enum';

describe('KeyDataSetService', () => {
  let keyDataSetServiceMock: KeyDataSetService;
  const BASE_URL = environment.api + '/keydataset/';
  const goodsPassportId = 'GP_TEST';
  const processDtoRequestMock: ProcessDTO = {
    documents: [
      {
        creator: '',
        documentType: '',
        hash: '',
        hashFunction: '',
        id: '',
        timestamp: '',
        creatorName: '',
      },
    ],
    processId: 'P_TEST',
    keydataset: [
      {
        goodsPassportId: 'GP_TEST',
        creator: '',
        timestamp: '',
        header: {
          seller: {
            identification: {
              identificationNumber: '',
            },
            name: 'seller test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          buyer: {
            identification: {
              identificationNumber: '',
            },
            name: 'buyer test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          invoice: {
            invoiceDate: '',
            invoiceNumber: '',
          },
        },
        goodsItem: [
          {
            commodityCode: {
              harmonizedSystemSubHeadingCode: '',
            },
            countryOfOrigin: 'DE',
            countryOfPreferentialOrigin: '',
            descriptionOfGoods: 'Goods Test',
            netMass: '',
            invoiceAmount: '',
            invoiceCurrency: '',
            quantity: '10',
            sequenceNumber: '',
          },
        ],
      },
    ],
  };
  const processDtoRequestWithEmptyGoodsPassportIdMock: ProcessDTO = {
    documents: [
      {
        creator: '',
        documentType: '',
        hash: '',
        hashFunction: '',
        id: '',
        timestamp: '',
        creatorName: '',
      },
    ],
    processId: 'P_TEST',
    keydataset: [
      {
        goodsPassportId: '',
        creator: '',
        timestamp: '',
        header: {
          seller: {
            identification: {
              identificationNumber: '',
            },
            name: 'seller test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          buyer: {
            identification: {
              identificationNumber: '',
            },
            name: 'buyer test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          invoice: {
            invoiceDate: '',
            invoiceNumber: '',
          },
        },
        goodsItem: [
          {
            commodityCode: {
              harmonizedSystemSubHeadingCode: '',
            },
            countryOfOrigin: 'DE',
            countryOfPreferentialOrigin: '',
            descriptionOfGoods: 'Goods Test',
            netMass: '',
            invoiceAmount: '',
            invoiceCurrency: '',
            quantity: '10',
            sequenceNumber: '',
          },
        ],
      },
    ],
  };
  const createRequestBody = {
    keyDataSetDto: processDtoRequestMock.keydataset[0],
    processId: processDtoRequestMock.processId,
    mnemonic: '',
  };
  const updateRequestBody = {
    keyDataSetDto: processDtoRequestMock.keydataset[0],
    goodsPassportId: processDtoRequestMock.keydataset[0].goodsPassportId,
    mnemonic: '',
  };

  let messageNotificationServiceMock: any;
  let httpClientMock: any;

  beforeEach(() => {
    httpClientMock = {
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
    };

    messageNotificationServiceMock = {
      notifyMessage: jest.fn(),
      closeMatSnackBar: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: MessageNotificationService,
          useValue: messageNotificationServiceMock,
        },
        { provide: HttpClient, useValue: httpClientMock },
      ],
    });

    keyDataSetServiceMock = TestBed.inject(KeyDataSetService);
  });

  it('should be created', () => {
    expect(keyDataSetServiceMock).toBeTruthy();
  });

  describe('when GET key data set is called', () => {
    it('should return process dto', () => {
      const processResponseMockData: ProcessDTO = {
        documents: [
          {
            creator: '',
            documentType: '',
            hash: '',
            hashFunction: '',
            id: '',
            timestamp: '',
            creatorName: '',
          },
        ],
        processId: 'P_TEST',
        keydataset: [
          {
            goodsPassportId: 'GP_TEST',
            creator: '',
            timestamp: '',
            header: {
              seller: {
                identification: {
                  identificationNumber: '',
                },
                name: 'seller test',
                address: {
                  streetAndNumber: '',
                  postcode: '',
                  country: '',
                  city: '',
                },
              },
              buyer: {
                identification: {
                  identificationNumber: '',
                },
                name: 'buyer test',
                address: {
                  streetAndNumber: '',
                  postcode: '',
                  country: '',
                  city: '',
                },
              },
              invoice: {
                invoiceDate: '',
                invoiceNumber: '',
              },
            },
            goodsItem: [
              {
                commodityCode: {
                  harmonizedSystemSubHeadingCode: '',
                },
                countryOfOrigin: 'DE',
                countryOfPreferentialOrigin: '',
                descriptionOfGoods: 'Goods Test',
                netMass: '',
                invoiceAmount: '',
                invoiceCurrency: '',
                quantity: '10',
                sequenceNumber: '',
              },
            ],
          },
        ],
      };
      const url = `${BASE_URL}${goodsPassportId}`;

      jest
        .spyOn(httpClientMock, 'get')
        .mockReturnValue(of(processResponseMockData));

      keyDataSetServiceMock.getData(goodsPassportId);

      expect(httpClientMock.get).toBeCalledTimes(1);
      expect(httpClientMock.get).toBeCalledWith(url);
    });

    it('should throw an error', () => {
      jest
        .spyOn(httpClientMock, 'get')
        .mockReturnValue(
          throwError(() => new Error('Can not retrieve a process dto'))
        );
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetServiceMock.getData(goodsPassportId).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(httpClientMock.get).toBeCalledTimes(1);
    });
  });

  describe('when SEND key data set is called', () => {
    it('should return process dto', () => {
      jest
        .spyOn(httpClientMock, 'post')
        .mockReturnValue(of(processDtoRequestMock));

      keyDataSetServiceMock.sendData(processDtoRequestMock);

      expect(httpClientMock.post).toBeCalledTimes(1);
      expect(httpClientMock.post).toHaveBeenLastCalledWith(
        BASE_URL,
        createRequestBody
      );
    });

    it('should return an error', () => {
      jest
        .spyOn(httpClientMock, 'post')
        .mockReturnValue(
          throwError(() => new Error('Can not submit the new process dto'))
        );
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetServiceMock.sendData(processDtoRequestMock).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(httpClientMock.post).toHaveBeenLastCalledWith(
        BASE_URL,
        createRequestBody
      );
    });
  });

  describe('when UPDATE key data set is called', () => {
    it('should return process dto', () => {
      jest
        .spyOn(httpClientMock, 'put')
        .mockReturnValue(of(processDtoRequestMock));

      keyDataSetServiceMock.updateData(processDtoRequestMock);

      expect(httpClientMock.put).toBeCalledTimes(1);
      expect(httpClientMock.put).toHaveBeenLastCalledWith(
        BASE_URL + 'GP_TEST',
        updateRequestBody
      );
    });

    it('should return process dto with empty goods passport id', () => {
      jest
        .spyOn(httpClientMock, 'put')
        .mockReturnValue(of(processDtoRequestWithEmptyGoodsPassportIdMock));

      keyDataSetServiceMock.updateData(
        processDtoRequestWithEmptyGoodsPassportIdMock
      );

      expect(httpClientMock.put).toBeCalledTimes(1);
    });

    it('should return an error', () => {
      jest
        .spyOn(httpClientMock, 'put')
        .mockReturnValue(
          throwError(() => new Error('Can not update the process dto'))
        );
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetServiceMock.updateData(processDtoRequestMock).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(httpClientMock.put).toHaveBeenLastCalledWith(
        BASE_URL + 'GP_TEST',
        updateRequestBody
      );
    });
  });

  describe('when SHOW HISTORY is called', () => {
    it('should return an array of key data sets', () => {
      const keyDataSetsMock: KeyDataSetDto[] = [
        {
          header: {
            seller: {
              identification: {
                identificationNumber: '',
              },
              name: '',
              address: {
                streetAndNumber: '',
                postcode: '',
                country: '',
                city: '',
              },
            },
            buyer: {
              identification: {
                identificationNumber: '',
              },
              name: '',
              address: {
                streetAndNumber: '',
                postcode: '',
                country: '',
                city: '',
              },
            },
            invoice: {
              invoiceDate: '',
              invoiceNumber: '',
            },
          },
          goodsItem: [
            {
              commodityCode: {
                harmonizedSystemSubHeadingCode: '',
              },
              countryOfOrigin: '',
              countryOfPreferentialOrigin: '',
              descriptionOfGoods: '',
              netMass: '',
              invoiceAmount: '',
              invoiceCurrency: '',
              quantity: '',
              sequenceNumber: '',
            },
          ],
        },
      ];

      jest.spyOn(httpClientMock, 'get').mockReturnValue(of(keyDataSetsMock));
      keyDataSetServiceMock.showHistory(goodsPassportId);

      expect(httpClientMock.get).toBeCalledTimes(1);
    });

    it('should return an error', () => {
      const errMessage = 'Can not show history';
      jest
        .spyOn(httpClientMock, 'get')
        .mockReturnValue(throwError(() => new Error(errMessage)));
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetServiceMock.showHistory(goodsPassportId).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalledWith(
          MessageNotificationLevel.ERROR,
          errMessage
        );
      });
    });
  });

  describe('when GET all key data sets is called', () => {
    it('should return all key data sets successfully', () => {
      const allKeyDataSetsMock: KeyDataSetDto[] = [
        {
          goodsPassportId: '',
          creator: '',
          timestamp: '',
          header: {
            seller: {
              identification: {
                identificationNumber: '',
              },
              name: 'Seller 1',
              address: {
                streetAndNumber: '',
                postcode: '',
                country: '',
                city: '',
              },
            },
            buyer: {
              identification: {
                identificationNumber: '',
              },
              name: 'Buyer 1',
              address: {
                streetAndNumber: '',
                postcode: '',
                country: '',
                city: '',
              },
            },
            invoice: {
              invoiceDate: '',
              invoiceNumber: '',
            },
          },
          goodsItem: [
            {
              commodityCode: {
                harmonizedSystemSubHeadingCode: '',
              },
              countryOfOrigin: 'US',
              countryOfPreferentialOrigin: '',
              descriptionOfGoods: 'Goods Test A',
              netMass: '',
              invoiceAmount: '',
              invoiceCurrency: '',
              quantity: '20',
              sequenceNumber: '',
            },
          ],
        },
      ];
      jest.spyOn(httpClientMock, 'get').mockReturnValue(of(allKeyDataSetsMock));

      keyDataSetServiceMock.getAllKeyDataSets();

      expect(httpClientMock.get).toHaveBeenCalledWith(BASE_URL);
    });

    it('should return all key data sets unsuccessfully', () => {
      const errMes = 'Can not retrieve all key data sets';
      jest
        .spyOn(httpClientMock, 'get')
        .mockReturnValue(throwError(() => new Error(errMes)));
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetServiceMock.getAllKeyDataSets().subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(triggerErrorSpy).toHaveBeenCalledWith(
        MessageNotificationLevel.ERROR,
        errMes
      );
    });

    it('getTokenHistory', () => {
      jest.spyOn(httpClientMock, 'get').mockReturnValue(of([]));

      keyDataSetServiceMock.getTokenHistory('').subscribe((res) => {
        expect(res).toEqual([]);
      });
    });
  });
});
