/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import {
  CreateKeyDataSetDto,
  FieldInformationDto,
  HTTPEndpoint,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { environment } from '../../environments/environment';
import { MessageNotificationService } from './message-notification.service';
import { MessageNotificationLevel } from '../_enums/message-notification-level.enum';

@Injectable({
  providedIn: 'root',
})
export class KeyDataSetService {
  readonly BASE_URL =
    environment.api + '/' + HTTPEndpoint.KEYDATASET_CONTROLLER + '/';

  constructor(
    private http: HttpClient,
    private messageNotificationService: MessageNotificationService
  ) {}

  getData(goodsPassportId: string): Observable<ProcessDTO> {
    const url = (this.BASE_URL + HTTPEndpoint.KEYDATASET_FIND).replace(
      HTTPEndpoint.KEYDATASET_PARAM_ID,
      goodsPassportId
    );
    return this.http.get<ProcessDTO>(url).pipe(
      catchError((err) => {
        this.messageNotificationService.notifyMessage(
          MessageNotificationLevel.ERROR,
          err.message
        );
        return throwError(() => err);
      })
    );
  }

  sendData(processDTO: ProcessDTO): Observable<ProcessDTO> {
    const kds = processDTO.keydataset[processDTO.keydataset.length - 1];
    const dto: CreateKeyDataSetDto = {
      processId: processDTO.processId,
      keyDataSetDto: kds,
      mnemonic: '',
    };
    const url = this.BASE_URL + HTTPEndpoint.KEYDATASET_CREATE;
    return this.http.post<ProcessDTO>(url, dto).pipe(
      catchError((err) => {
        this.messageNotificationService.notifyMessage(
          MessageNotificationLevel.ERROR,
          err.message
        );
        return throwError(() => err);
      })
    );
  }

  updateData(updateKeyDataSetDto: ProcessDTO): Observable<ProcessDTO> {
    const kds =
      updateKeyDataSetDto.keydataset[updateKeyDataSetDto.keydataset.length - 1];
    const goodsPassportId = kds.goodsPassportId || '';
    const dto: UpdateKeyDataSetDto = {
      keyDataSetDto: kds,
      goodsPassportId: goodsPassportId,
      mnemonic: '',
    };
    const url = (this.BASE_URL + HTTPEndpoint.KEYDATASET_UPDATE).replace(
      HTTPEndpoint.KEYDATASET_PARAM_ID,
      goodsPassportId
    );
    return this.http.put<ProcessDTO>(url, dto).pipe(
      catchError((err) => {
        this.messageNotificationService.notifyMessage(
          MessageNotificationLevel.ERROR,
          err.message
        );
        return throwError(() => err);
      })
    );
  }

  showHistory(goodPassportId: string): Observable<KeyDataSetDto[]> {
    const url = (this.BASE_URL + HTTPEndpoint.KEYDATASET_HISTORY).replace(
      HTTPEndpoint.KEYDATASET_PARAM_ID,
      goodPassportId
    );
    return this.http.get<KeyDataSetDto[]>(url).pipe(
      catchError((err) => {
        this.messageNotificationService.notifyMessage(
          MessageNotificationLevel.ERROR,
          err.message
        );
        return throwError(() => err);
      })
    );
  }

  getAllKeyDataSets(): Observable<KeyDataSetDto[]> {
    const url = this.BASE_URL + HTTPEndpoint.KEYDATASET_FIND_ALL;
    return this.http.get<KeyDataSetDto[]>(url).pipe(
      catchError((err) => {
        this.messageNotificationService.notifyMessage(
          MessageNotificationLevel.ERROR,
          err.message
        );
        return throwError(() => err);
      })
    );
  }

  getTokenHistory(id: string): Observable<FieldInformationDto[]> {
    return this.http
      .get<FieldInformationDto[]>(`${this.BASE_URL}token/history/${id}`)
      .pipe(
        catchError((err) => {
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.ERROR,
            err.message
          );
          return throwError(() => err);
        })
      );
  }
}
