/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {MessageNotificationLevel} from "../_enums/message-notification-level.enum";

@Injectable({
  providedIn: 'root'
})
export class MessageNotificationService {

  constructor(private _matSnackBar: MatSnackBar) {
  }

  get matSnackBar(): MatSnackBar {
    return this._matSnackBar;
  }

  /**
   * To display a snackbar with a message notification.
   * @param notificationLevel
   * @param mesContent
   */
  notifyMessage(notificationLevel: MessageNotificationLevel, mesContent: string) {
    this.matSnackBar.open(mesContent, '',
      {duration: 5000, panelClass: [`snackbar-${notificationLevel}`]});
  }


  /**
   * To close snackbar. It must be called on destroying the component which uses the snackbar.
   */
  closeMatSnackBar(): void {
    this.matSnackBar.dismiss();
  }
}
