/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {DocumentType} from '../_enums/document-type.enum';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {HashTokenDTO, HTTPEndpoint, ProcessDTO} from '@rhenus/api-interfaces';
import {catchError, Observable, throwError} from 'rxjs';
import {MessageNotificationService} from "./message-notification.service";
import {MessageNotificationLevel} from "../_enums/message-notification-level.enum";

const {api} = environment;

@Injectable({
  providedIn: 'root',
})
export class HashService {
  readonly BASE_URL = api + '/' + HTTPEndpoint.HASH_CONTROLLER + '/';
  lastProcessDTO: ProcessDTO | undefined;

  constructor(
    private http: HttpClient,
    private messageNotificationService: MessageNotificationService
  ) {
  }

  uploadDocument(
    file: File,
    docType: DocumentType,
    processId?: string
  ): Observable<ProcessDTO> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('documentType', docType);

    let url : string;
    if (processId) {
      formData.append('processId', processId);
      url = (this.BASE_URL + HTTPEndpoint.HASH_ADD).replace(HTTPEndpoint.HASH_PARAM_ID, processId);
    } else {
      url = this.BASE_URL + HTTPEndpoint.HASH_UPLOAD;
    }

    return this.http.post<ProcessDTO>(url, formData).pipe(
      catchError(err => {
        this.messageNotificationService.notifyMessage(MessageNotificationLevel.ERROR, err.message);
        return throwError(() => err);
      })
    );
  }

  validateDocument(id: string, file: File): Observable<HashTokenDTO> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('id', id);

    const url = (this.BASE_URL + HTTPEndpoint.HASH_VERIFY).replace(HTTPEndpoint.HASH_PARAM_ID, id);
    return this.http.post<HashTokenDTO>(url, formData).pipe(
      catchError(err => {
        this.messageNotificationService.notifyMessage(MessageNotificationLevel.ERROR, err.message);
        return throwError(() => err);
      })
    );
  }
}
