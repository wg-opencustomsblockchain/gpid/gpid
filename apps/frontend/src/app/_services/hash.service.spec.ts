/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';

import {HashService} from './hash.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {of, throwError} from "rxjs";
import {environment} from "../../environments/environment";
import {HashTokenDTO, ProcessDTO} from "@rhenus/api-interfaces";
import {DocumentType} from "../_enums/document-type.enum";
import {MessageNotificationService} from "./message-notification.service";
import {HttpClient} from "@angular/common/http";

const {api} = environment;
describe('HashService', () => {
  let hashServiceMock: HashService;
  let messageNotificationServiceMock: any;

  let httpClientMock: any;
  const fileMock = new File(['content of test-file'], 'test-file');

  beforeEach(() => {
    httpClientMock = {
      post: jest.fn()
    };

    messageNotificationServiceMock = {
      notifyMessage: jest.fn(),
      closeMatSnackBar: jest.fn()
    };

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: MessageNotificationService, useValue: messageNotificationServiceMock},
        {provide: HttpClient, useValue: httpClientMock}
      ]
    });
    hashServiceMock = TestBed.inject(HashService);
  });

  it('should be created', () => {
    expect(hashServiceMock).toBeTruthy();
  });

  describe('when UPLOAD a document', () => {
    const processResponseMockData: ProcessDTO = {
      documents: [{
        creator: "",
        documentType: "",
        hash: "",
        hashFunction: "",
        id: "",
        timestamp: "",
        creatorName: "",
      }],
      processId: 'P_TEST',
      keydataset: [{
        goodsPassportId: 'GP_TEST',
        creator: '',
        timestamp: "",
        header: {
          seller: {
            identification: {
              identificationNumber: '',
            },
            name: 'seller test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          buyer: {
            identification: {
              identificationNumber: '',
            },
            name: 'buyer test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          invoice: {
            invoiceDate: "",
            invoiceNumber: "",
          },
        },
        goodsItem: [{
          commodityCode: {
            harmonizedSystemSubHeadingCode: "",
          },
          countryOfOrigin: "DE",
          countryOfPreferentialOrigin: "",
          descriptionOfGoods: "Goods Test",
          netMass: "",
          invoiceAmount: "",
          invoiceCurrency: "",
          quantity: "10",
          sequenceNumber: "",
        }],
      }]
    };
    const formDataMock = new FormData();
    formDataMock.append('file', fileMock);
    formDataMock.append('documentType', DocumentType.BOL);

    it('should send request und receive a new processDTO', () => {
      const postRequestSpy = jest.spyOn(httpClientMock, 'post').mockReturnValue(of(processResponseMockData));

      hashServiceMock.uploadDocument(fileMock, DocumentType.BOL);

      expect(postRequestSpy).toHaveBeenCalledWith(`${api}/hash/`, formDataMock);
    });

    it('should send request und update the existing processDTO', () => {
      formDataMock.append('processId', 'Process_ID');
      const postRequestSpy = jest.spyOn(httpClientMock, 'post').mockReturnValue(of(processResponseMockData));

      hashServiceMock.uploadDocument(fileMock, DocumentType.BOL, 'Process_ID');

      expect(postRequestSpy).toHaveBeenCalledWith(`${api}/hash/Process_ID`, formDataMock);
    });

    it('should send request unsuccessfully', () => {
      jest.spyOn(httpClientMock, 'post')
        .mockReturnValue(throwError(() => new Error('Can not upload document')));
      const triggerErrorSpy = jest.spyOn(messageNotificationServiceMock, 'notifyMessage');

      hashServiceMock.uploadDocument(fileMock, DocumentType.BOL).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(httpClientMock.post).toBeCalledTimes(1);
    });
  });

  describe('when VALIDATE a document', () => {
    it('should receive a new HashTokenDTO after validating document', () => {
      const formDataMock = new FormData();
      formDataMock.append('file', fileMock);
      formDataMock.append('id', 'id_test');

      const hashTokenMock: HashTokenDTO = {
        id: 'id_hashToken',
        creator: 'creator',
        timestamp: 'timestamp',
        documentType: 'documentType',
        hash: 'hash',
        hashFunction: 'hashFunction'
      };

      const postRequestSpy = jest.spyOn(httpClientMock, 'post').mockReturnValue(of(hashTokenMock));

      hashServiceMock.validateDocument('id_test', fileMock);

      expect(postRequestSpy).toHaveBeenCalledWith(`${api}/hash/id_test/verify`, formDataMock);
    });

    it('should validate document unsuccessfully', () => {
      const formDataMock = new FormData();
      formDataMock.append('file', fileMock);
      formDataMock.append('id', 'id_test');

      jest.spyOn(httpClientMock, 'post')
        .mockReturnValue(throwError(() => new Error('Can not upload document')));
      const triggerErrorSpy = jest.spyOn(messageNotificationServiceMock, 'notifyMessage');

      hashServiceMock.validateDocument('id_test', fileMock).subscribe(() => {
        expect(triggerErrorSpy).toHaveBeenCalled();
      });

      expect(httpClientMock.post).toBeCalledTimes(1);
    });
  });
});
