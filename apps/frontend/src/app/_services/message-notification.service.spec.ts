/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';

import {MessageNotificationService} from './message-notification.service';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {MessageNotificationLevel} from "../_enums/message-notification-level.enum";

describe('Message Notification Service', () => {
  let messageNotificationService: MessageNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule, NoopAnimationsModule]
    });
    messageNotificationService = TestBed.inject(MessageNotificationService);
    jest.spyOn(console, 'error').mockImplementation(); // ignore console.error inside unit tests
  });

  it('should be created', () => {
    expect(messageNotificationService).toBeTruthy();
  });

  describe('notify an error message', () => {
    it('should open & dismiss snackbar', () => {
      const mes = 'test snackbar';

      const openSnackbarSpy = jest.spyOn(messageNotificationService.matSnackBar, 'open');
      messageNotificationService.notifyMessage(MessageNotificationLevel.ERROR, mes);
      expect(openSnackbarSpy).toHaveBeenCalled();

      const closeSnackbarSpy = jest.spyOn(messageNotificationService.matSnackBar, 'dismiss');
      messageNotificationService.closeMatSnackBar();
      expect(closeSnackbarSpy).toHaveBeenCalled();
    });
  })
});
