/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  DocumentType,
  DocumentType2LabelMapping,
} from '../../_enums/document-type.enum';

@Component({
  selector: 'gpid-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  id = '';
  DocumentType = DocumentType;
  docType: DocumentType = DocumentType.NONE;
  DocumentType2LabelMapping = DocumentType2LabelMapping;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    const queryId = this.route.snapshot.queryParams['id'];

    if (queryId) {
      const queryIdArray = queryId.split('_');
      this.id = queryIdArray[0];

      if (queryIdArray.length > 1)
        this.docType =
          DocumentType[queryIdArray[1] as keyof typeof DocumentType];
    }
  }
}
