/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HashTokenDTO } from '@rhenus/api-interfaces';
import {
  DocumentType,
  DocumentType2LabelMapping,
} from '../../_enums/document-type.enum';

@Component({
  selector: 'gpid-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit {
  id = '';
  docType: DocumentType = DocumentType.NONE;
  token!: HashTokenDTO;
  DocumentType2LabelMapping = DocumentType2LabelMapping;
  DocumentType = DocumentType;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    const stringToken = this.route.snapshot.queryParams['token'];
    if (!stringToken) return;

    const token: HashTokenDTO = JSON.parse(stringToken);
    this.token = token;
    this.docType =
      DocumentType[token.documentType as keyof typeof DocumentType];
    this.id = [token.id, token.documentType].join('_');
  }
}
