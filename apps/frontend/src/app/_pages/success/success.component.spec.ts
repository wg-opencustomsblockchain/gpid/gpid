/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {SuccessComponent} from './success.component';
import {ActivatedRoute} from "@angular/router";
import {DocumentType} from "../../_enums/document-type.enum";

describe('SuccessComponent', () => {
  let component: SuccessComponent;
  let fixture: ComponentFixture<SuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [SuccessComponent]
    }).compileComponents();
  });

  it('should create', () => {
    const activatedRouteMock = {
      snapshot: {
        queryParams: {
          token: "{\"id\":\"id_hashtoken\",\"creator\":\"creator\",\"creatorName\":\"creatorname\",\"timestamp\":\"2023-03-01\",\"documentType\":\"LIC\",\"hash\":\"hash\",\"hashFunction\":\"hashfunction\"}"
        }
      }
    };
    TestBed.overrideProvider(ActivatedRoute, {useValue: activatedRouteMock});
    fixture = TestBed.createComponent(SuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(activatedRouteMock.snapshot).toBeTruthy();
    expect(component).toBeTruthy();
    expect(component.id).toBe('id_hashtoken_LIC');
    expect(component.DocumentType2LabelMapping[component.docType]).toBe('License');
  });

  it('should fail', () => {
    const falsyActivatedRouteMock = {
      snapshot: {
        queryParams: {
          token: null
        }
      }
    };

    TestBed.overrideProvider(ActivatedRoute, {useValue: falsyActivatedRouteMock});
    fixture = TestBed.createComponent(SuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component.token).toBeFalsy();
    expect(component.docType).toBe(DocumentType.NONE);
    expect(component.id).toBeFalsy();
  });
});

