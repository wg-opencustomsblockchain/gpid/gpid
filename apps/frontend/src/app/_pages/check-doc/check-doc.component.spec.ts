/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClientModule} from '@angular/common/http';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';
import {RouterTestingModule} from '@angular/router/testing';
import {UploadComponent} from '../../_components/upload/upload.component';

import {CheckDocComponent} from './check-doc.component';
import {ActivatedRoute, Router} from "@angular/router";
import {HashTokenDTO} from "@rhenus/api-interfaces";
import {HashService} from "../../_services/hash.service";
import {LoadingComponent} from "../../_components/loading/loading.component";
import {BrowserAnimationsModule, NoopAnimationsModule} from "@angular/platform-browser/animations";
import {of, throwError} from "rxjs";
import {SuccessComponent} from "../success/success.component";
import {ErrorComponent} from "../error/error.component";

describe('CheckDocComponent', () => {
  let component: CheckDocComponent;
  let fixture: ComponentFixture<CheckDocComponent>;
  let router: Router;
  let hashServiceMock: any;
  const filesMock = [new Blob(['content of test file'])];

  const dialogData = {
    data: {
      heading: 'Writing to the blockchain',
      text: 'Writing to the blockchain...',
    },
    width: '80vw',
    disableClose: true
  }
  const hashTokenMock: HashTokenDTO = {
    id: 'id_test',
    creator: 'creator_test',
    creatorName: 'creatorName_test',
    timestamp: 'timestamp_test',
    documentType: 'documentType_test',
    hash: 'hash_test',
    hashFunction: 'hashFunction_test'
  }

  beforeEach(async () => {
    hashServiceMock = {validateDocument: jest.fn()}

    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatDialogModule,
        FormsModule,
        NoopAnimationsModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes([
          {path: 'response/success', component: SuccessComponent},
          {path: 'response/error', component: ErrorComponent}
        ]),
      ],
      providers: [
        {provide: HashService, useValue: hashServiceMock},
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: 'ID_TEST'},
              url: [{path: 'docs/validate/:id'}]
            }
          }
        },
        {provide: MAT_DIALOG_DATA, useValue: dialogData}
      ],
      declarations: [CheckDocComponent, UploadComponent, LoadingComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CheckDocComponent);
    router = TestBed.inject(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.id).toBe('ID_TEST');
  });

  it('should validate successfully', waitForAsync((() => {
    component.files = filesMock;

    const validateDocumentSpy = jest.spyOn(hashServiceMock, 'validateDocument')
      .mockReturnValue(of(hashTokenMock));
    const navigateSpy = jest.spyOn(router, 'navigateByUrl');

    component.validate();

    expect(component.files.length).toBeGreaterThan(0);
    expect(validateDocumentSpy).toBeCalledWith('ID_TEST', filesMock[0]);
    expect(navigateSpy).toHaveBeenCalledWith(`response/success?token=${JSON.stringify(hashTokenMock)}`);
  })));

  it('should validate unsuccessfully with an empty hash token as the result', waitForAsync((() => {
    component.files = filesMock;

    jest.spyOn(hashServiceMock, 'validateDocument').mockReturnValue(of(null));
    const navigateSpy = jest.spyOn(router, 'navigateByUrl');

    component.validate();

    expect(navigateSpy).toHaveBeenCalledWith(`response/error?id=${component.id}`);
  })));

  it('should validate unsuccessfully with an error', waitForAsync((() => {
    component.files = filesMock;

    jest.spyOn(hashServiceMock, 'validateDocument')
      .mockReturnValue(throwError(() => new Error("An error message")));
    const navigateSpy = jest.spyOn(router, 'navigateByUrl');

    component.validate();

    expect(navigateSpy).toHaveBeenCalledWith(`response/error?id=${component.id}`);
  })));
});

