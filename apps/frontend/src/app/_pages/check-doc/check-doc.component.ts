/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HashTokenDTO } from '@rhenus/api-interfaces';
import { HashService } from '../../_services/hash.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from '../../_components/loading/loading.component';

@Component({
  selector: 'gpid-check-doc',
  templateUrl: './check-doc.component.html',
  styleUrls: ['./check-doc.component.scss'],
})
export class CheckDocComponent implements OnInit {
  files: any[] = [];
  id = '';

  constructor(
    private readonly hashService: HashService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
  }

  async validate() {
    if (this.files.length > 0) {
      // Display Loading Popup
      this.dialog.open(LoadingComponent, {
        data: {
          heading: 'Uploading document',
          text: 'The document is being hashed',
        },
        width: '80vw',
        disableClose: true,
      });
      this.hashService.validateDocument(this.id, this.files[0]).subscribe({
        next: (res: HashTokenDTO) => {
          this.dialog.closeAll();
          if (res)
            this.router.navigateByUrl(
              `response/success?token=${JSON.stringify(res)}`
            );
          else this.router.navigateByUrl(`response/error?id=${this.id}`);
        },
        error: () => {
          this.dialog.closeAll();
          this.router.navigateByUrl(`response/error?id=${this.id}`);
        },
      });
    }
  }
}
