/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  DocumentType,
  DocumentType2LabelMapping,
} from '../../_enums/document-type.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { HashService } from '../../_services/hash.service';
import { MatDialog } from '@angular/material/dialog';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { ProcessDTO } from '@rhenus/api-interfaces';
import { finalize, Subscription } from 'rxjs';

@Component({
  selector: 'gpid-import-doc-many',
  templateUrl: './import-doc-many.component.html',
  styleUrls: ['./import-doc-many.component.scss'],
})
export class ImportDocManyComponent implements OnInit, OnDestroy {
  @ViewChild('fileDropRef', { static: false }) fileDropEl!: ElementRef;
  files: any[] = [];
  DocumentType = DocumentType;
  docType: DocumentType = DocumentType.NONE;
  DocumentType2LabelMapping = DocumentType2LabelMapping;
  fileTypes = Object.values(DocumentType);
  processId = '';
  doesAddDocument = false;

  private subs = new Subscription();

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly hashService: HashService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.doesAddDocument = this.activatedRoute.snapshot.url[0].path === 'add';
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  hashFile() {
    if (this.files.length > 0) {
      // Display Loading Popup
      this.dialog.open(LoadingComponent, {
        data: {
          heading: 'Uploading document',
          text: 'The document is being hashed',
        },
        width: '80vw',
        disableClose: true,
      });

      if (this.processId) {
        this.subs = this.hashService
          .uploadDocument(this.files[0], this.docType, this.processId)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe({
            next: (res: ProcessDTO) => {
              this.hashService.lastProcessDTO = res;
              this.router.navigateByUrl(`id/show`);
            },
            error: () => {
              // If we must handle an error. The place to do is here
            },
          });
      } else {
        this.subs = this.hashService
          .uploadDocument(this.files[0], this.docType)
          .pipe(finalize(() => this.dialog.closeAll()))
          .subscribe({
            next: (res: ProcessDTO) => {
              this.hashService.lastProcessDTO = res;
              this.router.navigateByUrl(`id/show`);
            },
            error: () => {
              // If we must handle an error. The place to do is here
            },
          });
      }
    }
  }
}
