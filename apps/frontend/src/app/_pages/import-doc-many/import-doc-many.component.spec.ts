/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClientModule} from '@angular/common/http';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {RouterTestingModule} from '@angular/router/testing';
import {UploadComponent} from '../../_components/upload/upload.component';

import {ImportDocManyComponent} from './import-doc-many.component';
import {ActivatedRoute, Router} from "@angular/router";
import {ProcessDTO} from "@rhenus/api-interfaces";
import {HashService} from "../../_services/hash.service";
import {of} from "rxjs";
import {DocumentType} from "../../_enums/document-type.enum";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {By} from "@angular/platform-browser";
import {ShowIdComponent} from "../show-id/show-id.component";

describe('ImportDocManyComponent', () => {
  let component: ImportDocManyComponent;
  let fixture: ComponentFixture<ImportDocManyComponent>;
  let hashServiceMock: any;
  let routerMock: Router;
  const activatedRouteHashMock = {
    snapshot: {url: [{path: 'hash'}]}
  };
  const activatedRouteAddMock = {
    snapshot: {url: [{path: 'add'}]}
  };

  const filesMock = [new Blob(['content of test file'])];
  const processDTOResponseMock: ProcessDTO = {
    documents: [{
      creator: "",
      documentType: "",
      hash: "",
      hashFunction: "",
      id: "",
      timestamp: "",
      creatorName: "",
    }],
    processId: 'P_TEST',
    keydataset: [{
      goodsPassportId: 'GP_TEST',
      creator: '',
      timestamp: "",
      header: {
        seller: {
          identification: {
            identificationNumber: '',
          },
          name: 'seller test',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        buyer: {
          identification: {
            identificationNumber: '',
          },
          name: 'buyer test',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        invoice: {
          invoiceDate: "",
          invoiceNumber: "",
        },
      },
      goodsItem: [{
        commodityCode: {
          harmonizedSystemSubHeadingCode: "",
        },
        countryOfOrigin: "DE",
        countryOfPreferentialOrigin: "",
        descriptionOfGoods: "Goods Test",
        netMass: "",
        invoiceAmount: "",
        invoiceCurrency: "",
        quantity: "10",
        sequenceNumber: "",
      }],
    }],
  };

  beforeEach(async () => {
    hashServiceMock = {
      uploadDocument: jest.fn()
    }

    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatDialogModule,
        FormsModule,
        NoopAnimationsModule,
        RouterTestingModule.withRoutes([
          {path: 'id/show', component: ShowIdComponent}
        ]),
      ],
      declarations: [ImportDocManyComponent, UploadComponent],
      providers: [{provide: HashService, useValue: hashServiceMock}]
    }).compileComponents();
  });

  describe('hash document', () => {
    it('should show DIV for inputting process ID', () => {
      TestBed.overrideProvider(ActivatedRoute, {useValue: activatedRouteHashMock});
      routerMock = TestBed.inject(Router);
      fixture = TestBed.createComponent(ImportDocManyComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.files = filesMock;
      component.docType = DocumentType.AWB;
      component.processId = 'processID';
      const uploadDocumentSpy = jest.spyOn(hashServiceMock, 'uploadDocument')
        .mockReturnValue(of(processDTOResponseMock));
      const navigateSpy = jest.spyOn(routerMock, 'navigateByUrl');

      component.hashFile();

      const processIdInput = fixture.debugElement.query(By.css('#processIdInput'));
      expect(component).toBeTruthy();
      expect(processIdInput).toBeDefined();
      expect(uploadDocumentSpy).toHaveBeenCalledWith(filesMock[0], DocumentType.AWB, 'processID');
      expect(navigateSpy).toHaveBeenCalledWith('id/show');
    });

  });

  describe('add document', () => {
    it("should show DIV for inputting process ID", () => {
      TestBed.overrideProvider(ActivatedRoute, {useValue: activatedRouteAddMock});
      routerMock = TestBed.inject(Router);
      fixture = TestBed.createComponent(ImportDocManyComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.files = filesMock;
      component.docType = DocumentType.PLI;
      const uploadDocumentSpy = jest.spyOn(hashServiceMock, 'uploadDocument')
        .mockReturnValue(of(processDTOResponseMock));
      const navigateSpy = jest.spyOn(routerMock, 'navigateByUrl');

      component.hashFile();

      const processIdInput = fixture.debugElement.query(By.css('#processIdInput'));

      expect(processIdInput).toBeTruthy();
      expect(uploadDocumentSpy).toHaveBeenCalledWith(filesMock[0], DocumentType.PLI);
      expect(navigateSpy).toHaveBeenCalledWith('id/show');
    });
  });
});

