/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'gpid-query-id',
  templateUrl: './query-id.component.html',
  styleUrls: ['./query-id.component.scss'],
})
export class QueryIdComponent {
  id = '';
  constructor(private readonly router: Router) {}

  query() {
    if (this.id !== '') this.router.navigateByUrl(`/docs/validate/${this.id}`);
  }
}
