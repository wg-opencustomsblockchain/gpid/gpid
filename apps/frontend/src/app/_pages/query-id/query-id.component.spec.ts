/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

import {QueryIdComponent} from './query-id.component';
import {Router} from "@angular/router";
import {CheckDocComponent} from "../check-doc/check-doc.component";

describe('QueryIdComponent', () => {
  let component: QueryIdComponent;
  let fixture: ComponentFixture<QueryIdComponent>;
  let router: Router;
  const queryId = 'ID_TEST';

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'docs/validate/:id', component: CheckDocComponent}
        ]),
        FormsModule],
      declarations: [QueryIdComponent],
      providers:[]
    }).compileComponents();

    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(QueryIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate successfully after querying', () => {
    component.id = queryId;
    const navigateSpy = jest.spyOn(router,'navigateByUrl');
    component.query();

    expect(navigateSpy).toHaveBeenCalledWith('/docs/validate/' + component.id);
  });
});
