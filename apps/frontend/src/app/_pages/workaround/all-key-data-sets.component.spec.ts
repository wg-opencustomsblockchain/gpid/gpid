/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AllKeyDataSetsComponent} from './all-key-data-sets.component';
import {KeyDataSetService} from "../../_services/key-data-set.service";
import {KeyDataSetDto} from "@rhenus/api-interfaces";
import {of} from "rxjs";
import {NgxJsonViewerModule} from "ngx-json-viewer";

describe('AllKeyDataSetsComponent', () => {
  let component: AllKeyDataSetsComponent;
  let fixture: ComponentFixture<AllKeyDataSetsComponent>;
  let keyDataSetServiceMock: any;

  const keyDataSetsMock: KeyDataSetDto[] = [
    {
      goodsPassportId: '',
      creator: '',
      timestamp: '',
      header: {
        seller: {
          identification: {
            identificationNumber: '',
          },
          name: 'seller 1',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        buyer: {
          identification: {
            identificationNumber: '',
          },
          name: 'buyer 1',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        invoice: {
          invoiceDate: '',
          invoiceNumber: '',
        },
      },
      goodsItem: [{
        commodityCode: {
          harmonizedSystemSubHeadingCode: '',
        },
        countryOfOrigin: 'US',
        countryOfPreferentialOrigin: '',
        descriptionOfGoods: 'Goods Test A',
        netMass: '',
        invoiceAmount: '',
        invoiceCurrency: '',
        quantity: '20',
        sequenceNumber: '',
      }],
    },
    {
      goodsPassportId: '',
      creator: '',
      timestamp: '',
      header: {
        seller: {
          identification: {
            identificationNumber: '',
          },
          name: 'seller 2',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        buyer: {
          identification: {
            identificationNumber: '',
          },
          name: 'buyer 2',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        invoice: {
          invoiceDate: '',
          invoiceNumber: '',
        },
      },
      goodsItem: [{
        commodityCode: {
          harmonizedSystemSubHeadingCode: '',
        },
        countryOfOrigin: 'DE',
        countryOfPreferentialOrigin: '',
        descriptionOfGoods: 'Goods Test B',
        netMass: '',
        invoiceAmount: '',
        invoiceCurrency: '',
        quantity: '15',
        sequenceNumber: '',
      }],
    },
  ];

  beforeEach(async () => {
    keyDataSetServiceMock = {
      getAllKeyDataSets: jest.fn()
    };

    await TestBed.configureTestingModule({
      imports: [NgxJsonViewerModule],
      declarations: [AllKeyDataSetsComponent],
      providers: [
        {provide: KeyDataSetService, useValue: keyDataSetServiceMock}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AllKeyDataSetsComponent);
    component = fixture.componentInstance;
    jest.spyOn(keyDataSetServiceMock, 'getAllKeyDataSets').mockReturnValue(of(keyDataSetsMock));
  });

  it('should create & destroy component', () => {
    expect(component.subs).toBeUndefined();
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.subs).toBeDefined();
    expect(component.keyDataSets.length).toBe(2);
    expect(component.subs.closed).toBeTruthy();
  });

  it('should unsubscribe unsuccessfully', () => {
    expect(component.subs).toBeUndefined();
  });
});
