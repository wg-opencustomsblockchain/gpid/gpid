/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { KeyDataSetService } from '../../_services/key-data-set.service';
import { KeyDataSetDto } from '@rhenus/api-interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gpid-all-keydatasets',
  templateUrl: './all-key-data-sets.component.html',
  styleUrls: ['./all-key-data-sets.component.scss'],
})
export class AllKeyDataSetsComponent implements OnInit, OnDestroy {
  keyDataSets: KeyDataSetDto[] = [];
  subs!: Subscription;

  constructor(private keyDataSetService: KeyDataSetService) {}

  ngOnInit(): void {
    this.subs = this.keyDataSetService
      .getAllKeyDataSets()
      .subscribe((keyDataSets: KeyDataSetDto[]) => {
        this.keyDataSets = keyDataSets;
      });
  }

  ngOnDestroy(): void {
    this.subs?.unsubscribe();
  }
}
