/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {HttpClientModule} from '@angular/common/http';
import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {ShowIdComponent} from './show-id.component';
import {HashService} from "../../_services/hash.service";
import {ProcessDTO} from "@rhenus/api-interfaces";
import {ClipboardModule} from "@angular/cdk/clipboard";

describe('ShowIdComponent', () => {
  let component: ShowIdComponent;
  let fixture: ComponentFixture<ShowIdComponent>;
  let hashServiceMock: any;
  const processDTOMock: ProcessDTO = {
    documents: [{
      creator: "",
      documentType: "",
      hash: "",
      hashFunction: "",
      id: "",
      timestamp: "",
      creatorName: "",
    }],
    processId: 'P_TEST',
    keydataset: [{
      goodsPassportId: 'GP_TEST',
      creator: '',
      timestamp: "",
      header: {
        seller: {
          identification: {
            identificationNumber: '',
          },
          name: 'seller test',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        buyer: {
          identification: {
            identificationNumber: '',
          },
          name: 'buyer test',
          address: {
            streetAndNumber: '',
            postcode: '',
            country: '',
            city: '',
          },
        },
        invoice: {
          invoiceDate: "",
          invoiceNumber: "",
        },
      },
      goodsItem: [{
        commodityCode: {
          harmonizedSystemSubHeadingCode: "",
        },
        countryOfOrigin: "DE",
        countryOfPreferentialOrigin: "",
        descriptionOfGoods: "Goods Test",
        netMass: "",
        invoiceAmount: "",
        invoiceCurrency: "",
        quantity: "10",
        sequenceNumber: "",
      }],
    }],
  };

  beforeEach(async () => {
    hashServiceMock = {
      lastProcessDTO: processDTOMock
    }

    await TestBed.configureTestingModule({
      imports: [HttpClientModule, ClipboardModule],
      declarations: [ShowIdComponent],
      providers: [{provide: HashService, useValue: hashServiceMock}]
    }).compileComponents();

    fixture = TestBed.createComponent(ShowIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', waitForAsync(() => {
    expect(component).toBeTruthy();
    expect(component.processId).toBe('P_TEST');
  }));
});
