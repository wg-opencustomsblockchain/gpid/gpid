/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { ProcessDTO } from '@rhenus/api-interfaces';
import { HashService } from '../../_services/hash.service';

@Component({
  selector: 'gpid-show-id',
  templateUrl: './show-id.component.html',
  styleUrls: ['./show-id.component.scss'],
})
export class ShowIdComponent implements OnInit {
  processDTO: ProcessDTO | undefined;
  processId = '';
  constructor(private hashService: HashService) {}

  async ngOnInit(): Promise<void> {
    this.processDTO = this.hashService.lastProcessDTO;
    if (this.processDTO) {
      this.processId = this.processDTO.processId;
    }
  }
}
