/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';

@Component({
  selector: 'gpid-enter-goods-passport-id',
  templateUrl: './enter-goods-passport-id.component.html',
  styleUrls: ['./enter-goods-passport-id.component.scss'],
})
export class EnterGoodsPassportIdComponent {
  goodsPassportId = '';
}
