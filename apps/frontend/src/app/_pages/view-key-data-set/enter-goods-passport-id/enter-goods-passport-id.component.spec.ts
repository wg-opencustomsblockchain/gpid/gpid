/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EnterGoodsPassportIdComponent} from './enter-goods-passport-id.component';
import {FormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe('EnterGoodsPassportIdComponent', () => {
  let component: EnterGoodsPassportIdComponent;
  let fixture: ComponentFixture<EnterGoodsPassportIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EnterGoodsPassportIdComponent],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes([])]
    }).compileComponents();

    fixture = TestBed.createComponent(EnterGoodsPassportIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
