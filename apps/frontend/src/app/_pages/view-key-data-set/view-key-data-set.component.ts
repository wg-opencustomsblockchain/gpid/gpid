/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KeyDataSetService } from '../../_services/key-data-set.service';
import {
  FieldInformationDto,
  KeyDataSetDto,
  ProcessDTO,
} from '@rhenus/api-interfaces';
import { finalize, Subscription } from 'rxjs';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { MatDialog } from '@angular/material/dialog';
import {
  EMPTY_GOODS_ITEM,
  EMPTY_PROCESS_DTO,
} from '../../_constants/empty-process.dto';
import { MessageNotificationService } from '../../_services/message-notification.service';
import { MessageNotificationLevel } from '../../_enums/message-notification-level.enum';
import countries from 'world-countries';

@Component({
  selector: 'gpid-view-key-data-set',
  templateUrl: './view-key-data-set.component.html',
  styleUrls: ['./view-key-data-set.component.scss'],
})
export class ViewKeyDataSetComponent implements OnInit, OnDestroy {
  goodsPassportId = '';
  public processDTO!: ProcessDTO;
  processDtoTmp!: string;
  isInputMask = true;
  showEntryData = true;
  isEditFirstStep = true;
  fontWeight = '';
  buttonState = true;
  readOnlyFlag = false;
  sellerCountryName = '';
  buyerCountryName = '';
  keyDataSet!: KeyDataSetDto;
  keydatasetOrigin!: KeyDataSetDto;
  keyDataSetHistoryLength = -1;
  keyDataSetHistoryPosition = -1;
  keyDataSets: KeyDataSetDto[] = [];
  isPreviousDisabled = false;
  isNextDisabled = false;
  isHistoryMode = false;
  newRecord = true;
  fieldInformation: FieldInformationDto[] = [];

  private subs!: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private keyDataSetService: KeyDataSetService,
    private dialog: MatDialog,
    private messageNotificationService: MessageNotificationService
  ) {}

  ngOnInit(): void {
    this.goodsPassportId = this.route.snapshot.params['goodsPassportId'];

    if (this.goodsPassportId) {
      console.log(this.goodsPassportId);
      this.keyDataSetService
        .getTokenHistory(this.goodsPassportId)
        .subscribe((res) => (this.fieldInformation = res));
    }

    this.isInputMask =
      sessionStorage.getItem('InitialChoice') === 'Enter' ||
      this.route.snapshot.url[0].path === 'enterKeyDataSet';
    sessionStorage.removeItem('InitialChoice');

    if (!this.isInputMask) {
      this.loadProcessDTO();
      this.newRecord = false;
      this.fontWeight = 'font-bold text-lg';
      this.readOnlyFlag = true;
    } else {
      this.initKeyDataSet();
      this.newRecord = true;
      this.readOnlyFlag = false;
    }
  }

  ngOnDestroy(): void {
    this.messageNotificationService.closeMatSnackBar();
    this.subs?.unsubscribe();
  }

  change() {
    this.buttonState = false;
    const tmp: string = JSON.stringify(this.processDTO);
    this.buttonState = this.processDtoTmp === tmp;
  }

  private loadProcessDTO(): void {
    this.subs = this.keyDataSetService
      .getData(this.goodsPassportId)
      .subscribe((processDTO: ProcessDTO) => {
        console.log('Process DTO:', processDTO);
        this.processDTO = processDTO;
        this.keyDataSet = processDTO.keydataset[0];

        console.log(this.keyDataSet.header.seller);
        // load 2-letter code
        this.onChangingSellerCountry(
          this.keyDataSet.header.seller.address.country
        );
        this.onChangingBuyerCountry(
          this.keyDataSet.header.buyer.address.country
        );
      });
  }

  initKeyDataSet(): void {
    console.log('Initialize processDTO...');
    this.processDTO = structuredClone(EMPTY_PROCESS_DTO);
    this.keyDataSet =
      this.processDTO.keydataset[this.processDTO.keydataset.length - 1];
  }

  submit() {
    this.openDialogForLoading();

    this.subs = this.keyDataSetService
      .sendData(this.processDTO)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: (processDTO: ProcessDTO) => {
          console.log('Process DTO:', this.processDTO);
          this.processDTO = processDTO;
          this.keyDataSet =
            processDTO.keydataset[processDTO.keydataset.length - 1];
          this.isInputMask = false;
          this.readOnlyFlag = true;
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.SUCCESS,
            'Created successfully'
          );
        },
        error: (err) => {
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.ERROR,
            err.message
          );
        },
      });
  }

  save() {
    this.openDialogForLoading();

    this.subs = this.keyDataSetService
      .updateData(this.processDTO)
      .pipe(finalize(() => this.dialog.closeAll()))
      .subscribe({
        next: (res: ProcessDTO) => {
          console.log('Updated!: ', res);
          this.processDTO = res;
          this.keyDataSet = res.keydataset[0];
          this.isInputMask = false;
          this.readOnlyFlag = true;
          this.showEntryData = false;
          this.isEditFirstStep = true;
          this.buttonState = true;
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.SUCCESS,
            'Updated successfully'
          );

          this.fieldInformation = [];

          setTimeout(() => {
            if (this.goodsPassportId) {
              this.keyDataSetService
                .getTokenHistory(this.goodsPassportId)
                .subscribe((res) => (this.fieldInformation = res));
            }
          }, 500);
        },
        error: (err) => {
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.ERROR,
            err.message
          );
        },
      });
  }

  edit() {
    this.readOnlyFlag = !this.readOnlyFlag;
    this.showEntryData = true;
    this.isEditFirstStep = false;
    this.processDtoTmp = JSON.stringify(this.processDTO);
    this.keydatasetOrigin = structuredClone(this.keyDataSet);
  }

  cancelEdit() {
    this.readOnlyFlag = !this.readOnlyFlag;
    this.showEntryData = false;
    this.isEditFirstStep = true;
    this.buttonState = true;
    this.keyDataSet = this.keydatasetOrigin;
    this.processDTO.keydataset[0] = this.keydatasetOrigin;
  }

  addGoodsItem() {
    console.log('Process DTO:', this.processDTO);
    this.keyDataSet.goodsItem.push(structuredClone(EMPTY_GOODS_ITEM));
    this.change();
  }

  removeGoodsItem(index: number): void {
    this.keyDataSet.goodsItem.splice(index, 1);
    this.change();
  }

  getProcessDTO(): ProcessDTO {
    return this.processDTO;
  }

  private openDialogForLoading(): void {
    this.dialog.open(LoadingComponent, {
      data: {
        heading: 'Writing to the blockchain',
        text: 'Writing to the blockchain...',
      },
      width: '80vw',
      disableClose: true,
    });
  }

  onChangingSellerCountry(input: string): void {
    this.sellerCountryName = this.getCountyName(input) || '';
  }

  onChangingBuyerCountry(input: string): void {
    this.buyerCountryName = this.getCountyName(input) || '';
  }

  /**
   * Mapping of country code onto common name
   * @param countryName
   */
  private getCountyName(countryCode: string): string | undefined {
    return countries.find((c) => c.cca2 === countryCode)?.name.common;
  }

  showHistory(): void {
    this.isHistoryMode = true;
    this.subs = this.keyDataSetService
      .showHistory(this.goodsPassportId)
      .subscribe({
        next: (keyDataSets: KeyDataSetDto[]) => {
          keyDataSets.push(this.processDTO.keydataset[0]);
          this.keyDataSets = keyDataSets;
          this.keydatasetOrigin = this.keyDataSet;
          this.keyDataSetHistoryLength = keyDataSets.length;
          this.keyDataSetHistoryPosition = keyDataSets.length;
          this.keyDataSet = keyDataSets[keyDataSets.length - 1];
          this.isNextDisabled = true;
          this.isPreviousDisabled = keyDataSets.length <= 1;
          console.log('Key data sets:', this.keyDataSets);
        },
        error: (err) => {
          this.messageNotificationService.notifyMessage(
            MessageNotificationLevel.ERROR,
            err.message
          );
        },
      });
  }

  hideHistory(): void {
    this.isHistoryMode = false;
    this.keyDataSet = this.keydatasetOrigin;
  }

  /**
   * Go to the previous key data set
   */
  showPrevious(): void {
    if (this.keyDataSetHistoryLength > 1) {
      this.isNextDisabled = false;
    }
    if (this.keyDataSetHistoryPosition > 1) {
      this.keyDataSetHistoryPosition--;
      this.keyDataSet = this.keyDataSets[this.keyDataSetHistoryPosition - 1];
    }
    if (this.keyDataSetHistoryPosition === 1) {
      this.isPreviousDisabled = true;
    }
  }

  /**
   * Go to the next key data set
   */
  showNext(): void {
    if (this.keyDataSetHistoryLength > 1) {
      this.isPreviousDisabled = false;
    }
    if (this.keyDataSetHistoryPosition < this.keyDataSetHistoryLength) {
      this.keyDataSetHistoryPosition++;
      this.keyDataSet = this.keyDataSets[this.keyDataSetHistoryPosition - 1];
    }

    if (this.keyDataSetHistoryPosition === this.keyDataSetHistoryLength) {
      this.isNextDisabled = true;
    }
  }

  getFieldInformationStatus(
    fieldName: string
  ): 'loading' | 'secure' | 'edited' {
    if (this.fieldInformation.length === 0) return 'loading';

    const isEdited = this.fieldInformation.find(
      (fi) => fi.fieldName === fieldName
    )?.isEdited;

    return isEdited ? 'edited' : 'secure';
  }

  getFieldInformationEditor(fieldName: string): string {
    if (this.fieldInformation.length === 0) return 'Loading';

    return (
      this.fieldInformation.find((fi) => fi.fieldName === fieldName)?.editor ||
      'Loading'
    );
  }
}
