/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewKeyDataSetComponent } from './view-key-data-set.component';
import { KeyDataSetService } from '../../_services/key-data-set.service';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
} from '@angular/material/dialog';
import { LoadingComponent } from '../../_components/loading/loading.component';
import { of, throwError } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ProcessDTO } from '@rhenus/api-interfaces';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MessageNotificationService } from '../../_services/message-notification.service';
import { MessageNotificationLevel } from '../../_enums/message-notification-level.enum';

describe('ViewKeyDataSetComponent', () => {
  let keyDataSetComponent: ViewKeyDataSetComponent;
  let keyDataSetFixture: ComponentFixture<ViewKeyDataSetComponent>;
  let keyDataSetServiceMock: any;
  let activatedRouteMock: ActivatedRoute;
  let messageNotificationServiceMock: any;
  let dialogMock: any;

  const dialogData = {
    data: {
      heading: 'Writing to the blockchain',
      text: 'Writing to the blockchain...',
    },
    width: '80vw',
    disableClose: true,
  };

  const processDTOMock: ProcessDTO = {
    documents: [
      {
        creator: '',
        documentType: '',
        hash: '',
        hashFunction: '',
        id: '',
        timestamp: '',
        creatorName: '',
      },
    ],
    processId: 'P_TEST',
    keydataset: [
      {
        goodsPassportId: 'GP_TEST',
        creator: '',
        timestamp: '',
        header: {
          seller: {
            identification: {
              identificationNumber: '',
            },
            name: 'seller test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          buyer: {
            identification: {
              identificationNumber: '',
            },
            name: 'buyer test',
            address: {
              streetAndNumber: '',
              postcode: '',
              country: '',
              city: '',
            },
          },
          invoice: {
            invoiceDate: '',
            invoiceNumber: '',
          },
        },
        goodsItem: [
          {
            commodityCode: {
              harmonizedSystemSubHeadingCode: '',
            },
            countryOfOrigin: 'DE',
            countryOfPreferentialOrigin: '',
            descriptionOfGoods: 'Goods Test',
            netMass: '',
            invoiceAmount: '',
            invoiceCurrency: '',
            quantity: '10',
            sequenceNumber: '',
          },
        ],
      },
    ],
  };

  beforeEach(async () => {
    keyDataSetServiceMock = {
      getData: jest.fn(),
      sendData: jest.fn(),
      updateData: jest.fn(),
      showHistory: jest.fn(),
      hideHistory: jest.fn(),
      showPrevious: jest.fn(),
      showNext: jest.fn(),
      getTokenHistory: jest.fn(() => {
        return of([]);
      }),
    };

    messageNotificationServiceMock = {
      notifyMessage: jest.fn(),
      closeMatSnackBar: jest.fn(),
    };

    dialogMock = {
      open: jest.fn(),
      closeAll: jest.fn(),
    };

    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        FormsModule,
        NoopAnimationsModule,
        MatSnackBarModule,
      ],
      declarations: [ViewKeyDataSetComponent, LoadingComponent],
      providers: [
        { provide: KeyDataSetService, useValue: keyDataSetServiceMock },
        {
          provide: MessageNotificationService,
          useValue: messageNotificationServiceMock,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: { goodsPassportId: 'GP_TEST' },
              url: [{ path: 'viewKeyDataSet' }],
            },
          },
        },
        { provide: MAT_DIALOG_DATA, useValue: dialogData },
        { provide: MatDialog, useValue: dialogMock },
      ],
    }).compileComponents();

    keyDataSetFixture = TestBed.createComponent(ViewKeyDataSetComponent);
    // dialogFixture = TestBed.createComponent(LoadingComponent);
    keyDataSetComponent = keyDataSetFixture.componentInstance;
    // keyDataSetFixture.detectChanges();
  });

  afterEach(() => {
    keyDataSetFixture.destroy();
  });

  describe('ngOnInit', () => {
    it('should load ProcessDTO when loading page VIEW KEYDATASET', () => {
      expect(keyDataSetComponent.processDTO).toBeUndefined();
      jest
        .spyOn(keyDataSetServiceMock, 'getData')
        .mockReturnValue(of(processDTOMock));

      keyDataSetComponent.ngOnInit();
      keyDataSetFixture.detectChanges();

      expect(keyDataSetComponent.processDTO).toBeTruthy();
      expect(
        keyDataSetComponent.processDTO.keydataset[0].goodsPassportId
      ).toMatch('GP_TEST');
    });

    it('should empty ProcessDTO when loading page ENTER KEYDATASET', () => {
      expect(keyDataSetComponent.processDTO).toBeUndefined();
      activatedRouteMock = TestBed.inject(ActivatedRoute); // reinitialize activatedRouteMock
      activatedRouteMock.snapshot.url[0].path = 'enterKeyDataSet';
      expect(keyDataSetServiceMock.getData).not.toHaveBeenCalled();

      keyDataSetComponent.ngOnInit();
      keyDataSetFixture.detectChanges();

      expect(keyDataSetComponent.processDTO).toBeTruthy();
      expect(keyDataSetComponent.processDTO.keydataset[0].goodsPassportId).toBe(
        ''
      );
    });
  });

  describe('submit button', () => {
    it('should submit data successfully', () => {
      // init processDTO
      activatedRouteMock = TestBed.inject(ActivatedRoute);
      activatedRouteMock.snapshot.url[0].path = 'enterKeyDataSet';
      keyDataSetFixture.detectChanges();

      jest
        .spyOn(keyDataSetServiceMock, 'sendData')
        .mockReturnValue(of(processDTOMock));
      const openDialogSpy = jest.spyOn(dialogMock, 'open').mockImplementation();
      const closeDialogSpy = jest
        .spyOn(dialogMock, 'closeAll')
        .mockImplementation();

      keyDataSetComponent.submit();

      expect(keyDataSetComponent.processDTO.keydataset[0].goodsPassportId).toBe(
        'GP_TEST'
      );
      expect(keyDataSetComponent.isInputMask).toBeFalsy();
      expect(openDialogSpy).toHaveBeenCalled();
      expect(closeDialogSpy).toHaveBeenCalled();
    });

    it('should submit data unsuccessfully', () => {
      // init processDTO
      activatedRouteMock = TestBed.inject(ActivatedRoute);
      activatedRouteMock.snapshot.url[0].path = 'enterKeyDataSet';
      keyDataSetFixture.detectChanges();

      jest
        .spyOn(keyDataSetServiceMock, 'sendData')
        .mockReturnValue(throwError(() => new Error('An error message')));
      const openDialogSpy = jest.spyOn(dialogMock, 'open').mockImplementation();
      const closeDialogSpy = jest
        .spyOn(dialogMock, 'closeAll')
        .mockImplementation();
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetComponent.submit();

      expect(keyDataSetComponent.isInputMask).toBeTruthy();
      expect(triggerErrorSpy).toHaveBeenCalled();
      expect(openDialogSpy).toHaveBeenCalled();
      expect(closeDialogSpy).toHaveBeenCalled();
    });
  });

  describe('save button', () => {
    it('should save key data set successfully', () => {
      // init processDTO
      activatedRouteMock = TestBed.inject(ActivatedRoute);
      activatedRouteMock.snapshot.url[0].path = 'enterKeyDataSet';
      keyDataSetFixture.detectChanges();

      jest
        .spyOn(keyDataSetServiceMock, 'updateData')
        .mockReturnValue(of(processDTOMock));
      const triggerSuccessSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetComponent.save();

      expect(keyDataSetComponent.processDTO).not.toBeNull();
      expect(triggerSuccessSpy).toHaveBeenCalledWith(
        MessageNotificationLevel.SUCCESS,
        'Updated successfully'
      );
    });

    it('should save key data set unsuccessfully', () => {
      // init processDTO
      activatedRouteMock = TestBed.inject(ActivatedRoute);
      activatedRouteMock.snapshot.url[0].path = 'enterKeyDataSet';
      keyDataSetFixture.detectChanges();

      jest
        .spyOn(keyDataSetServiceMock, 'updateData')
        .mockReturnValue(
          throwError(() => new Error('Can not save the key data set'))
        );
      const triggerErrorSpy = jest.spyOn(
        messageNotificationServiceMock,
        'notifyMessage'
      );

      keyDataSetComponent.save();

      expect(triggerErrorSpy).toHaveBeenCalledWith(
        MessageNotificationLevel.ERROR,
        'Can not save the key data set'
      );
    });
  });

  it('should go into editing mode successfully', () => {
    jest
      .spyOn(keyDataSetServiceMock, 'getData')
      .mockReturnValue(of(processDTOMock));
    keyDataSetComponent.ngOnInit();

    expect(keyDataSetComponent.isEditFirstStep).toBeTruthy();
    expect(keyDataSetComponent.processDtoTmp).toBeFalsy();

    keyDataSetComponent.edit();

    expect(keyDataSetComponent.isEditFirstStep).toBeFalsy();
    expect(keyDataSetComponent.processDtoTmp).toBeTruthy();
  });

  describe('add & remove a goods item', () => {
    let processDTO: ProcessDTO;
    it('should add a goods item successfully', () => {
      keyDataSetComponent.initKeyDataSet();
      processDTO = keyDataSetComponent.getProcessDTO();
      const length1 = processDTO.keydataset[0].goodsItem.length;
      keyDataSetComponent.addGoodsItem();
      const length2 = processDTO.keydataset[0].goodsItem.length;
      expect(length1 + 1 == length2).toBeTruthy();
    });

    it('should remove a goods item successfully from the list', () => {
      keyDataSetComponent.initKeyDataSet();
      processDTO = keyDataSetComponent.getProcessDTO();
      const length1 = processDTO.keydataset[0].goodsItem.length;
      keyDataSetComponent.removeGoodsItem(0);
      const length2 = processDTO.keydataset[0].goodsItem.length;
      expect(length1 - 1 == length2).toBeTruthy();
    });
  });

  describe('when clicking the Show history button', () => {
    it('should show & hide history of key data sets successfully', () => {
      jest
        .spyOn(keyDataSetServiceMock, 'getData')
        .mockReturnValue(of(processDTOMock));
      keyDataSetComponent.ngOnInit();
      expect(keyDataSetComponent.keyDataSets.length).toBe(0);

      jest
        .spyOn(keyDataSetServiceMock, 'showHistory')
        .mockReturnValue(of(processDTOMock.keydataset));

      // Click 'Show history' button
      keyDataSetComponent.showHistory();
      expect(keyDataSetComponent.keyDataSets.length).toBe(2);
      expect(keyDataSetComponent.isHistoryMode).toBeTruthy();

      // Click 'Back to the latest version' button
      keyDataSetComponent.hideHistory();
      expect(keyDataSetComponent.isHistoryMode).toBeFalsy();
    });

    it('should show history of key data set unsuccessfully', () => {
      jest
        .spyOn(keyDataSetServiceMock, 'getData')
        .mockReturnValue(of(processDTOMock));
      keyDataSetComponent.ngOnInit();

      expect(keyDataSetComponent.keyDataSets.length).toBe(0);

      const errMessage = 'History could not be shown';
      jest
        .spyOn(keyDataSetServiceMock, 'showHistory')
        .mockReturnValue(throwError(() => new Error(errMessage)));

      // Click 'Show history' button
      keyDataSetComponent.showHistory();
      expect(keyDataSetComponent.keyDataSets.length).toBe(0);
    });
  });

  describe('when browsing the history of key data sets (2 elements)', () => {
    it('prev & next button should work', () => {
      jest
        .spyOn(keyDataSetServiceMock, 'getData')
        .mockReturnValue(of(processDTOMock));
      keyDataSetComponent.ngOnInit();

      jest
        .spyOn(keyDataSetServiceMock, 'showHistory')
        .mockReturnValue(of(processDTOMock.keydataset));

      // Click 'Show history' button
      keyDataSetComponent.showHistory();
      expect(keyDataSetComponent.keyDataSet).toBe(
        keyDataSetComponent.keyDataSets[1]
      );

      // Click 'prev' button
      keyDataSetComponent.showPrevious();
      expect(keyDataSetComponent.keyDataSet).toBe(
        keyDataSetComponent.keyDataSets[0]
      );
      expect(keyDataSetComponent.isPreviousDisabled).toBeFalsy();

      keyDataSetComponent.showPrevious();
      expect(keyDataSetComponent.keyDataSetHistoryPosition).toBe(1);
      expect(keyDataSetComponent.isPreviousDisabled).toBeTruthy();

      // Click 'next' button
      keyDataSetComponent.showNext();
      expect(keyDataSetComponent.keyDataSet).toBe(
        keyDataSetComponent.keyDataSets[1]
      );

      // Click 'next' button
      keyDataSetComponent.showNext();
      expect(keyDataSetComponent.isNextDisabled).toBeTruthy();
    });
  });
});
