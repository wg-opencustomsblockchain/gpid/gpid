/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HomeComponent} from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    sessionStorage.clear();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('view key data set button should be chosen', () => {
    expect(sessionStorage.getItem("InitialChoice")).toBeNull();
    const viewButtonSpy = jest.spyOn(component, 'buttonIsPressedView');

    const button = fixture.debugElement.nativeElement.querySelector('#viewKeyDataSet');
    button.click();

    expect(viewButtonSpy).toHaveBeenCalled();
    expect(sessionStorage.getItem("InitialChoice")).toBe("View");
  })

  it('enter key data set button should be chosen', () => {
    expect(sessionStorage.getItem("InitialChoice")).toBeNull();
    const enterButtonSpy = jest.spyOn(component, 'buttonIsPressedEnter');

    const button = fixture.debugElement.nativeElement.querySelector('#enterKeyDataSet');
    button.click();

    expect(enterButtonSpy).toHaveBeenCalled();
    expect(sessionStorage.getItem("InitialChoice")).toBe("Enter");
  })
});
