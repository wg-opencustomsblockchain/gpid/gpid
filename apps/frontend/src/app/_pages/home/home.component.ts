/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';

@Component({
  selector: 'gpid-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  buttonIsPressedView() {
    sessionStorage.setItem('InitialChoice', 'View');
  }
  buttonIsPressedEnter() {
    sessionStorage.setItem('InitialChoice', 'Enter');
  }
}
