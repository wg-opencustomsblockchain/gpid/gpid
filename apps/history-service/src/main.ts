/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger, LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.RMQ,
      options: {
        urls: [process.env.RMQ_URL],
        queue: 'rhenus_history',
        queueOptions: {
          durable: false,
        },
      },
    }
  );

  const logger = new Logger('HISTORY-SERVICE');
  app
    .listen()
    .then(() =>
      logger.log(
        `🚀 - History Service started with RMQ: ${process.env.RMQ_URL}`
      )
    );
}

bootstrap();
