/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { HistoryService } from './history.service';
import {
  FieldInformationDto,
  HistoryMqPattern,
  ProcessDTO,
} from '@rhenus/api-interfaces';

@Controller()
export class HistoryController {
  private logger: Logger = new Logger(HistoryController.name);

  constructor(private readonly historyService: HistoryService) {}

  @MessagePattern(HistoryMqPattern.PREFIX + HistoryMqPattern.GENERATE_HISTORY)
  public async generateHistory(
    @Payload() payload: { token: ProcessDTO }
  ): Promise<boolean> {
    this.logger.debug(payload);
    for (const keyDataSet of payload.token.keydataset) {
      await this.historyService.generateHistory(keyDataSet);
    }
    return true;
  }

  @MessagePattern(HistoryMqPattern.PREFIX + HistoryMqPattern.GET_HISTORY)
  public getHistory(
    @Payload() payload: { id: string }
  ): Promise<FieldInformationDto[]> {
    return this.historyService.getTokenHistory(payload.id);
  }
}
