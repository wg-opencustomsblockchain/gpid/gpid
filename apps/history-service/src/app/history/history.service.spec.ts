/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { ClientProxy, ClientsModule, Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { HistoryService } from './history.service';
import { PrismaService } from '../prisma.service';
import { prismaMock } from './test/singleton';

describe('HistoryService', () => {
  let service: HistoryService;
  let companyService: ClientProxy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'KEYDATASET_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [''],
              queue: 'rhenus_data',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      providers: [HistoryService, PrismaService],
    }).compile();

    service = module.get<HistoryService>(HistoryService);
    companyService = module.get('KEYDATASET_SERVICE');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return the correct string representation of an object', () => {
    const obj = {
      name: 'test',
      obj1: {
        name: 'test1',
        objInObj: {
          name: 'test objInObj',
        },
      },
      array1: [
        {
          name: 'array test 1',
        },
      ],
    };

    expect(service.generateTokenStringRepresentation(obj)).toEqual([
      'name',
      'obj1.name',
      'obj1.objInObj.name',
      'array1.0.name',
    ]);
  });

  it('should create a history object', async () => {
    const history = {
      id: 'id',
      fieldInformation: [],
      tokenId: 'testID',
    };

    prismaMock.history.create.mockResolvedValue(history);

    await expect(
      service.createHistory({ tokenId: 'testID' })
    ).resolves.toMatchObject({
      id: expect.any(String),
      tokenId: 'testID',
      fieldInformation: [],
    });
  });

  it('should return a FieldInformationDto[]', async () => {
    await expect(service.getTokenHistory('')).resolves.toEqual([]);
  });
});
