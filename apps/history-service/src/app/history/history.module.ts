/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { HistoryService } from './history.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { HistoryController } from './history.controller';
import { PrismaService } from '../prisma.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'KEYDATASET_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_data',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  providers: [HistoryService, PrismaService],
  controllers: [HistoryController],
})
export class HistoryModule {}
