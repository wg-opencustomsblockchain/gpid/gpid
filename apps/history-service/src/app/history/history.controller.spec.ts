/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Test, TestingModule } from '@nestjs/testing';
import { HistoryController } from './history.controller';
import { HistoryService } from './history.service';
import { FieldInformationDto, ProcessDTO } from '@rhenus/api-interfaces';

describe('HistoryController', () => {
  let controller: HistoryController;
  let historyService: HistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HistoryController],
      providers: [
        {
          provide: HistoryService,
          useValue: {
            generateHistory(token: ProcessDTO): Promise<boolean> {
              return Promise.resolve(true);
            },
            getTokenHistory(id: string): Promise<FieldInformationDto[]> {
              return Promise.resolve([]);
            },
          },
        },
      ],
    }).compile();

    controller = module.get<HistoryController>(HistoryController);
    historyService = module.get<HistoryService>(HistoryService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should generate a history', () => {
    const spy = jest.spyOn(historyService, 'generateHistory');
    controller.generateHistory({ token: { keydataset: [{}] } as ProcessDTO });
    expect(spy).toBeCalled();
  });

  it('should return the history', async () => {
    expect(await controller.getHistory({ id: '' })).toEqual([]);
  });
});
