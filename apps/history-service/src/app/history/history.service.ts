/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { ClientProxy } from '@nestjs/microservices';
import { FieldInformation, History, Prisma } from '@prisma/client';
import { MsgFetchTokenHistoryResponse } from '@rhenus/generated';
import { firstValueFrom } from 'rxjs';
import { GetTokenHistoryDto } from './dtos';
import { AMQPPatterns, FieldInformationDto } from '@rhenus/api-interfaces';

@Injectable()
export class HistoryService {
  private logger: Logger = new Logger(HistoryService.name);
  private loadingList: Map<string, boolean> = new Map<string, boolean>();

  constructor(
    private prismaService: PrismaService,
    @Inject('KEYDATASET_SERVICE') private blockchainService: ClientProxy
  ) {}

  /**
   * Returns the history of all fields from a token
   * @param id Token ID
   * @param user Executing user
   */
  async getTokenHistory(id: string): Promise<FieldInformationDto[]> {
    if (this.loadingList.has(id) && this.loadingList.get(id)) {
      setTimeout(() => {
        return this.getTokenHistory(id);
      }, 1000);
    }

    const history: History & { fieldInformation: FieldInformation[] } =
      await this.prismaService.history.findFirst({
        where: {
          tokenId: id,
        },
        include: {
          fieldInformation: true,
        },
      });

    if (!history) return [];

    const fieldInformationDto: FieldInformationDto[] = [];

    history.fieldInformation.forEach((element) => {
      const info: FieldInformationDto = {
        fieldName: element.fieldName,
        isEdited: element.isEdited,
        editor: element.isEdited ? element.editor : element.creator,
      };
      fieldInformationDto.push(info);
    });

    return fieldInformationDto;
  }

  /**
   * Generate the history for a token
   * @param token
   */
  async generateHistory<T>(token: T): Promise<void> {
    this.loadingList.set(token['goodsPassportId'], true);
    let history: History & { fieldInformation: FieldInformation[] } =
      await this.prismaService.history.findFirst({
        where: {
          tokenId: token['goodsPassportId'],
        },
        include: {
          fieldInformation: true,
        },
      });

    if (!history)
      history = await this.createHistory({
        tokenId: token['goodsPassportId'],
      });

    const dto: GetTokenHistoryDto = {
      id: history.tokenId,
      mnemonic: process.env.WALLET_ADDRESS_MNENMONIC,
    };

    const tokenHistory: MsgFetchTokenHistoryResponse = await firstValueFrom(
      this.blockchainService.send(
        AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_HISTORY,
        dto
      )
    ).catch(() => null);

    this.logger.debug(tokenHistory.TokenHistory);

    if (
      !tokenHistory.TokenHistory ||
      !tokenHistory.TokenHistory.history ||
      tokenHistory.TokenHistory.history.length <= 1 ||
      history.fieldInformation.length === 0
    ) {
      const stringRepresentation =
        this.generateTokenStringRepresentation(token);

      for (const s of stringRepresentation) {
        if (history.fieldInformation.findIndex((fi) => fi.fieldName === s) >= 0)
          return;
        await this.prismaService.fieldInformation.create({
          data: {
            fieldName: s,
            isEdited: false,
            creator: token['creator'],
            History: {
              connect: {
                id: history.id,
              },
            },
          },
        });
      }
      return;
    }

    const originalToken: T = JSON.parse(
      tokenHistory.TokenHistory.history[1].info.data
    );

    const stringRepresentation =
      this.generateTokenStringRepresentation(originalToken);

    await this.compareTokens<T>(
      stringRepresentation,
      originalToken,
      token,
      history
    );

    this.loadingList.set(token['goodsPassportId'], false);
  }

  /**
   * Create a DB history object
   * @param history
   */
  async createHistory(
    history: Prisma.HistoryCreateInput
  ): Promise<History & { fieldInformation: FieldInformation[] }> {
    return this.prismaService.history.create({
      data: {
        ...history,
      },
      include: {
        fieldInformation: true,
      },
    });
  }

  /**
   * Compare the tokens
   * @param stringRepresentation
   * @param originalToken
   * @param token
   * @param history
   * @param lastEditorUser
   * @param originalUser
   * @param lastEditorCompany
   * @param originalCompany
   * @private
   */
  async compareTokens<T>(
    stringRepresentation: string[],
    originalToken: T,
    token: T,
    history: History & { fieldInformation: FieldInformation[] }
  ) {
    for (const o of stringRepresentation) {
      //this.logger.debug(o);
      const splitted = o.split('.');

      let fieldValueOriginal = originalToken;
      let fieldValueEdited = token;

      splitted.forEach((f) => {
        if (fieldValueEdited && fieldValueOriginal) {
          fieldValueOriginal = fieldValueOriginal[f]
            ? fieldValueOriginal[f]
            : null;
          fieldValueEdited = fieldValueEdited[f] ? fieldValueEdited[f] : null;
        }
      });

      const currentHistory = history.fieldInformation.findIndex(
        (fi) => fi.fieldName === o
      );

      // If no field information exists, create one
      if (currentHistory === -1)
        this.prismaService.fieldInformation.create({
          data: {
            fieldName: o,
            isEdited: fieldValueOriginal !== fieldValueEdited,
            creator: originalToken['creator'],
            History: {
              connect: {
                id: history.id,
              },
            },
          },
        });

      // If the field value is edited, update the fieldInformation db object
      if (fieldValueOriginal !== fieldValueEdited)
        await this.prismaService.fieldInformation.update({
          where: {
            id: history.fieldInformation[currentHistory].id,
          },
          data: {
            isEdited: true,
            editor: token['creator'],
          },
        });
    }
  }

  /**
   * Generate a string representation for all token fields (eg. exporter.address.city)
   * @param token
   * @private
   */
  generateTokenStringRepresentation<T>(token: T) {
    const fields = Object.keys(token);
    let fieldArray: string[] = [];
    for (const field of fields) {
      fieldArray = fieldArray.concat(
        this.getStringRepresentationOfObject(token, field)
      );
      this.logger.debug(fieldArray);
    }
    return fieldArray;
  }

  private getStringRepresentationOfObject<Type>(
    object: Type,
    currentFieldName: string
  ) {
    const array = [];
    if (object[currentFieldName] instanceof Object) {
      const fields = Object.keys(object[currentFieldName]);
      for (const field of fields) {
        const temp = this.getStringRepresentationOfObject(
          object[currentFieldName],
          field
        );
        temp.forEach((e) => array.push(`${currentFieldName}.${e}`));
      }
      return array;
    }
    return [currentFieldName];
  }
}
