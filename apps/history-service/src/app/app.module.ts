/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { HistoryModule } from './history/history.module';

@Module({
  imports: [HistoryModule],
  providers: [],
})
export class AppModule {}
