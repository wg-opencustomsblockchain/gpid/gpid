/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';

import { AppModule } from './app/app.module';
import { Swagger } from './app/constants/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  /** Swagger Documentation Setup */
  const config: Pick<
    OpenAPIObject,
    'openapi' | 'info' | 'servers' | 'security' | 'tags' | 'externalDocs'
  > = new DocumentBuilder()
    .setTitle(Swagger.APPLICATION_TITLE)
    .setDescription(Swagger.APPLICATION_DESCRIPTION)
    .setVersion(Swagger.APPLICATION_VERSION)
    .addBearerAuth()
    .build();
  const document: OpenAPIObject = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(Swagger.SWAGGER_PATH, app, document);
  app.enableCors();

  const port = process.env.PORT || 3333;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
