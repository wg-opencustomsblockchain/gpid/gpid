/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Test, TestingModule} from '@nestjs/testing';
import {ServiceProviderLogicService} from "./service-provider-logic.service";
import {HttpModule, HttpService} from "@nestjs/axios";
import {firstValueFrom, of, throwError} from "rxjs";
import {AxiosResponse} from 'axios';

describe('ServiceProviderLogicService', () => {
  let serviceProviderLogicServiceMock: ServiceProviderLogicService;
  let httpServiceMock: HttpService;
  let messageNotificationServiceMock: any;


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({

      imports: [
        HttpModule
      ],
      providers: [
        ServiceProviderLogicService
      ]

    }).compile();

    serviceProviderLogicServiceMock = module.get<ServiceProviderLogicService>(ServiceProviderLogicService);
    httpServiceMock = module.get<HttpService>(HttpService);

    messageNotificationServiceMock = {
      notifyMessage: jest.fn()
    };

  });

  it('should be defined', () => {
    expect(serviceProviderLogicServiceMock).toBeDefined();
  });


  it('should pass thru users that have no sid (in the header)', () => {

    const user = {mnemonic: 'Secret'}

    const mnemonic = firstValueFrom(serviceProviderLogicServiceMock.handleServiceProviderMnemonic(undefined, user));

    expect(mnemonic).resolves.toBe('Secret');
  })

  it('should give the mnemonic from the sid-user', () => {

    const user = {mnemonic: 'Secret', service_provider: true}
    const postResult: AxiosResponse = {
      data: {
        access_token: 'access_token',
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };
    const getResult: AxiosResponse = {
      data: {
        attributes: {
          mnemonic: ['More secret'],
        },
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));
    jest.spyOn(httpServiceMock, 'get').mockImplementationOnce(() => of(getResult));

    const mnemonic = firstValueFrom(serviceProviderLogicServiceMock.handleServiceProviderMnemonic('sid', user));

    expect(mnemonic).resolves.toBe('More secret');
  })

  it('should handle post error', () => {

    const user = {mnemonic: 'Secret', service_provider: true}

    jest.spyOn(httpServiceMock, 'post')
      .mockReturnValue(throwError(() => new Error('An error has occurred')));
    const triggerErrorSpy = jest.spyOn(messageNotificationServiceMock, 'notifyMessage');

    serviceProviderLogicServiceMock.handleServiceProviderMnemonic('sid', user).subscribe(() => {
      expect(triggerErrorSpy).toHaveBeenCalled();
    });

    expect(httpServiceMock.post).toBeCalledTimes(1);
  })

  it('should handle get error', () => {

    const user = {mnemonic: 'Secret', service_provider: true}
    const postResult: AxiosResponse = {
      data: {
        access_token: 'access_token',
      },
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));

    jest.spyOn(httpServiceMock, 'get')
      .mockReturnValue(throwError(() => new Error('An error has occurred')));
    const triggerErrorSpy = jest.spyOn(messageNotificationServiceMock, 'notifyMessage');

    serviceProviderLogicServiceMock.handleServiceProviderMnemonic('sid', user).subscribe(() => {
      expect(triggerErrorSpy).toHaveBeenCalled();
    });

    expect(httpServiceMock.post).toBeCalledTimes(1);
  })

});
