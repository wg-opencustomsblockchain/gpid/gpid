/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Test, TestingModule} from '@nestjs/testing';
import {HttpModule, HttpService} from "@nestjs/axios";
import {of, throwError} from "rxjs";
import {AxiosResponse} from 'axios';
import {CosmosAddressService} from "./cosmos-address.service";
import {Constants, KeyDataSetDto, ProcessDTO} from "@rhenus/api-interfaces";

describe('CosmosAddressService', () => {
  let cosmosAddressServiceMock: CosmosAddressService;
  let httpServiceMock: HttpService;

  const postResult: AxiosResponse = {
    data: {
      access_token: 'access_token',
    },
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
  };

  const getResultWithUser: AxiosResponse = {
    data: [
      {
        username: 'cct-service-provider@iml.fhg.de',
        email: 'cct-service-provider@iml.fhg.de',
        attributes: {}
      },
      {
        username: 'rhenus-lab@iml.fhg.de',
        email: 'rhenus-lab@iml.fhg.de',
        attributes: {
          mnemonic: ['picture quick swap disorder promote dentist palm top group unaware change cruise food worth baby wedding auction company nuclear tornado lecture lawn ramp filter'],
        }
      }
    ],
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
  };

  const getResultWithOutUser: AxiosResponse = {
    data: [],
    status: 200,
    statusText: 'OK',
    headers: {},
    config: {},
  };

  const fakeKeyDataSetDto: KeyDataSetDto =
    {
      goodsPassportId: 'goodsPassportId',
      creator: 'baumann',
      timestamp: 'just now',

      header: {
        seller: {
          identification: {
            identificationNumber: 'DE537400371045831'
          },
          name: 'Fraunhofer IML',
          address: {
            streetAndNumber: 'Jospeh von Fraunhofer Straße 2-4',
            city: 'Dortmund',
            postcode: '44227',
            country: 'DE'
          }
        },
        buyer: {
          identification: {
            identificationNumber: 'UK53742341045831'
          },
          name: 'Rhenus/ALS',
          address: {
            streetAndNumber: 'Rhenus Street 1',
            city: 'London',
            postcode: '12345',
            country: 'UK'
          }
        },
        invoice: {
          invoiceNumber: '123123',
          invoiceDate: '2021-02-17T12:45:00'
        }
      },
      goodsItem: [{
        sequenceNumber: '1',
        commodityCode: {
          harmonizedSystemSubHeadingCode: '940190'
        },
        descriptionOfGoods: 'Rohre aus Aluminium; hier: aus nicht legiertem Aluminium',
        countryOfOrigin: 'DE',
        countryOfPreferentialOrigin: 'DE',
        invoiceAmount: '2.00',
        invoiceCurrency: 'EUR',
        quantity: '23123',
        netMass: '7025'
      }]
    };

  const fakeKeyDataSetDtos: KeyDataSetDto[] =
    [{
      goodsPassportId: 'goodsPassportId',
      creator: 'baumann',
      timestamp: 'just now',

      header: {
        seller: {
          identification: {
            identificationNumber: 'DE537400371045831'
          },
          name: 'Fraunhofer IML',
          address: {
            streetAndNumber: 'Jospeh von Fraunhofer Straße 2-4',
            city: 'Dortmund',
            postcode: '44227',
            country: 'DE'
          }
        },
        buyer: {
          identification: {
            identificationNumber: 'UK53742341045831'
          },
          name: 'Rhenus/ALS',
          address: {
            streetAndNumber: 'Rhenus Street 1',
            city: 'London',
            postcode: '12345',
            country: 'UK'
          }
        },
        invoice: {
          invoiceNumber: '123123',
          invoiceDate: '2021-02-17T12:45:00'
        }
      },
      goodsItem: [{
        sequenceNumber: '1',
        commodityCode: {
          harmonizedSystemSubHeadingCode: '940190'
        },
        descriptionOfGoods: 'Rohre aus Aluminium; hier: aus nicht legiertem Aluminium',
        countryOfOrigin: 'DE',
        countryOfPreferentialOrigin: 'DE',
        invoiceAmount: '2.00',
        invoiceCurrency: 'EUR',
        quantity: '23123',
        netMass: '7025'
      }]
    }];

  const fakeProcessDto: ProcessDTO = {
    processId: 'processId',
    documents: [
      {
        id: 'id',
        creator: 'baumann',
        creatorName: '',
        timestamp: '',
        documentType: 'type',
        hash: 'hash',
        hashFunction: '',
      }
    ],
    keydataset: [{
      goodsPassportId: 'goodsPassportId',
      creator: 'baumann',
      timestamp: 'just now',

      header: {
        seller: {
          identification: {
            identificationNumber: 'DE537400371045831'
          },
          name: 'Fraunhofer IML',
          address: {
            streetAndNumber: 'Jospeh von Fraunhofer Straße 2-4',
            city: 'Dortmund',
            postcode: '44227',
            country: 'DE'
          }
        },
        buyer: {
          identification: {
            identificationNumber: 'UK53742341045831'
          },
          name: 'Rhenus/ALS',
          address: {
            streetAndNumber: 'Rhenus Street 1',
            city: 'London',
            postcode: '12345',
            country: 'UK'
          }
        },
        invoice: {
          invoiceNumber: '123123',
          invoiceDate: '2021-02-17T12:45:00'
        }
      },
      goodsItem: [{
        sequenceNumber: '1',
        commodityCode: {
          harmonizedSystemSubHeadingCode: '940190'
        },
        descriptionOfGoods: 'Rohre aus Aluminium; hier: aus nicht legiertem Aluminium',
        countryOfOrigin: 'DE',
        countryOfPreferentialOrigin: 'DE',
        invoiceAmount: '2.00',
        invoiceCurrency: 'EUR',
        quantity: '23123',
        netMass: '7025'
      }]
    }]
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({

      imports: [
        HttpModule
      ],
      providers: [
        CosmosAddressService
      ]

    }).compile();

    cosmosAddressServiceMock = module.get<CosmosAddressService>(CosmosAddressService);
    httpServiceMock = module.get<HttpService>(HttpService);

  });

  it('should be defined', () => {
    expect(cosmosAddressServiceMock).toBeDefined();
  });


  it('should get a email from a cosmic address', async () => {

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));
    jest.spyOn(httpServiceMock, 'get').mockImplementationOnce(() => of(getResultWithUser));

    const email = await cosmosAddressServiceMock.getCosmosName('cosmos1u9ur6xrejjxanxz9rurwc2f4425tcs8v2nyj35');

    expect(email).toBe('rhenus-lab@iml.fhg.de');
  })

  it('should should extend a KeyDataSetDto with the cosmos name', async () => {

    jest.spyOn(cosmosAddressServiceMock, 'getCosmosName').mockImplementationOnce(async () => 'rhenus-lab@iml.fhg.de');

    const keyDataSetDto = await cosmosAddressServiceMock.extendKeyDataSetDtoWithCosmosName(fakeKeyDataSetDto);

    expect(keyDataSetDto.creatorName).toBe('rhenus-lab@iml.fhg.de');
  })

  it('should should extend an Array of KeyDataSetDtos with the cosmos name', async () => {

    jest.spyOn(cosmosAddressServiceMock, 'getCosmosName').mockImplementationOnce(async () => 'rhenus-lab@iml.fhg.de');

    const keyDataSetDtos = await cosmosAddressServiceMock.extendKeyDataSetDtoArrayWithCosmosName(fakeKeyDataSetDtos);

    expect(keyDataSetDtos[0].creatorName).toBe('rhenus-lab@iml.fhg.de');
  })

  it('should should extend a ProcessDto with the cosmos name', async () => {

    jest.spyOn(cosmosAddressServiceMock, 'getCosmosName').mockImplementation(async () => 'rhenus-lab@iml.fhg.de');

    const processDto = await cosmosAddressServiceMock.extendProcessDtoWithCosmosName(fakeProcessDto);

    expect(processDto.keydataset[0].creatorName).toBe('rhenus-lab@iml.fhg.de');
  })

  it('should handle post error', async () => {

    jest.spyOn(httpServiceMock, 'post')
      .mockReturnValue(throwError(() => new Error('An error has occurred')));

    await expect(cosmosAddressServiceMock.getCosmosName('sid')).rejects.toEqual(Error('An error has occurred'))
  })

  it('should handle get error', async () => {

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));

    jest.spyOn(httpServiceMock, 'get')
      .mockReturnValue(throwError(() => new Error('An error has occurred')));

    await expect(cosmosAddressServiceMock.getCosmosName('sid')).rejects.toEqual(Error('An error has occurred'))
  })

  it('should get the email after the second call from the internal map', async () => {

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));
    jest.spyOn(httpServiceMock, 'get').mockImplementationOnce(() => of(getResultWithUser));

    // First call to fill the internal map
    await cosmosAddressServiceMock.getCosmosName('cosmos1u9ur6xrejjxanxz9rurwc2f4425tcs8v2nyj35');
    // Second call to get the result from the internal map
    const email = await cosmosAddressServiceMock.getCosmosName('cosmos1u9ur6xrejjxanxz9rurwc2f4425tcs8v2nyj35');

    expect(email).toBe('rhenus-lab@iml.fhg.de');
  })

  it('should get the email after the second call from the internal map', async () => {

    jest.spyOn(httpServiceMock, 'post').mockImplementationOnce(() => of(postResult));
    jest.spyOn(httpServiceMock, 'get').mockImplementationOnce(() => of(getResultWithOutUser));

    // First call to fill the internal map
    await cosmosAddressServiceMock.getCosmosName('cosmos1u9ur6xrejjxanxz9rurwc2f4425tcs8v2nyj35');
    // Second call to get the result from the internal map
    const email = await cosmosAddressServiceMock.getCosmosName('cosmos1u9ur6xrejjxanxz9rurwc2f4425tcs8v2nyj35');

    expect(email).toBe(Constants.UNDEFINED_COSMIC_ADDRESS);
  })

});
