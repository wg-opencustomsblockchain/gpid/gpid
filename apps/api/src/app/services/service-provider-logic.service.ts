/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from "@nestjs/common";
import {HttpService} from "@nestjs/axios";
import {catchError, map, Observable, of, switchMap} from "rxjs";
import {rethrow} from "@nestjs/core/helpers/rethrow";

/***
 * This service handles the logic around the user of a service provider.
 */
@Injectable()
export class ServiceProviderLogicService {

  constructor(private readonly httpService: HttpService) {
  }

  private tokenUrl = process.env.KEYCLOAK_TOKEN_URL;
  private userUrl = process.env.KEYCLOAK_USERINFO_URL;

  /***
   * Service Provider use a different mechanism to look for the mnemonic. All service provider of a company use the
   * same login to the api and provide the 'real' user with the header sid (keycloak user id).
   * With the given sid the mnemonic of the representing sid-keycloak-user will be returned .
   * If there is no given sid or the user is not a service provider, the user mnemonic will be returned.
   *
   * @param sid The unique keycloak user id
   * @param user The user object given by keycloak
   */
  public handleServiceProviderMnemonic(sid: string, user: any): Observable<string> {

    // Check if the user is a service_provider and send a sid (keycloak userid)
    if (sid !== undefined && user.service_provider) {

      // Get the user information of user 'sid' from keycloak
      console.log('Use service_provider logic for keycloak user:', sid);

      // Params for the token post message
      const urlSearchParams = new URLSearchParams();
      urlSearchParams.append('client_id', process.env.KEYCLOAK_CLIENTID);
      urlSearchParams.append('client_secret', process.env.KEYCLOAK_SECRET);
      urlSearchParams.append('grant_type', 'client_credentials');

      // Get an admin token to call with the token the sid-user information
      return this.httpService.post(this.tokenUrl, urlSearchParams).pipe(
        catchError(e => {
          console.log(e);
          rethrow(e);
        }),
        map((resp) => resp.data.access_token),
        switchMap((adminToken: string) => {

//          console.log('=> Token: ', adminToken);
          // Add adminToken to the config for the HTTP get call
          const config = {
            headers: {Authorization: `Bearer ${adminToken}`}
          };

          // Get the mnemonic from keycloak with the given token
          return this.httpService.get(this.userUrl + '/' + sid, config).pipe(
            catchError(e => {
              console.log(e);
              rethrow(e);
            }),
            map((resp) => resp.data.attributes.mnemonic.toString())
          );
        })
      );

    } else {
      return of(user.mnemonic);
    }
  }
}
