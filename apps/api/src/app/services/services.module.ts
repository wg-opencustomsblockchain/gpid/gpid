/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Module} from '@nestjs/common';
import {ServiceProviderLogicService} from "../services/service-provider-logic.service";
import {HttpModule} from "@nestjs/axios";
import {CosmosAddressService} from "./cosmos-address.service";

@Module({
  imports: [HttpModule],
  providers: [ServiceProviderLogicService, CosmosAddressService],
})
export class ServicesModule {
}
