/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { catchError, firstValueFrom, map, Observable, switchMap } from 'rxjs';
import { rethrow } from '@nestjs/core/helpers/rethrow';
import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import {
  KeyDataSetDto,
  ProcessDTO,
  Constants,
  FieldInformationDto,
} from '@rhenus/api-interfaces';

/***
 * This service handles the logic around the translation of a cosmos-address to the cosmos name (email-address).
 */
@Injectable()
export class CosmosAddressService {
  constructor(private readonly httpService: HttpService) {}

  private undefinedCosmicAddress = Constants.UNDEFINED_COSMIC_ADDRESS;
  private tokenUrl = process.env.KEYCLOAK_TOKEN_URL;
  private userUrl = process.env.KEYCLOAK_USERINFO_URL;

  private cosmosMap: Map<string, string> = new Map<string, string>();

  public async extendKeyDataSetDtoWithCosmosName(
    keyDataSetDto: KeyDataSetDto
  ): Promise<KeyDataSetDto> {
    keyDataSetDto.creatorName = await this.getCosmosName(keyDataSetDto.creator);
    return keyDataSetDto;
  }

  public async extendKeyDataSetDtoArrayWithCosmosName(
    keyDataSetDtoArray: KeyDataSetDto[]
  ): Promise<KeyDataSetDto[]> {
    for (const keyDataSetDto of keyDataSetDtoArray) {
      keyDataSetDto.creatorName = await this.getCosmosName(
        keyDataSetDto.creator
      );
    }
    return keyDataSetDtoArray;
  }

  public async extendTokenHistoryArrayWithCosmosName(
    tokenHistoryDto: FieldInformationDto[]
  ): Promise<FieldInformationDto[]> {
    for (const field of tokenHistoryDto) {
      field.editor = await this.getCosmosName(field.editor);
    }
    return tokenHistoryDto;
  }

  public async extendProcessDtoWithCosmosName(
    processDTO: ProcessDTO
  ): Promise<ProcessDTO> {
    if (processDTO.documents) {
      for (const hashTokenDto of processDTO.documents) {
        hashTokenDto.creatorName = await this.getCosmosName(
          hashTokenDto.creator
        );
      }
    }
    if (processDTO.keydataset) {
      for (const keyDataSetDto of processDTO.keydataset) {
        keyDataSetDto.creatorName = await this.getCosmosName(
          keyDataSetDto.creator
        );
      }
    }
    return processDTO;
  }

  /***
   * Converts a given cosmos-address to the name (email)
   *
   * @param cosmosAddressToSearchFor The cosmos-address to search for
   */
  public async getCosmosName(cosmosAddressToSearchFor: string) {
    // Do we already have the cosmos address in the map?
    let cosmosEmail = this.cosmosMap.get(cosmosAddressToSearchFor);
    if (cosmosEmail !== undefined) {
      return cosmosEmail;
    } else {
      console.log('Cosmos-address not found: ', cosmosAddressToSearchFor);
      // clear the old map of all cosmos-addresses
      this.cosmosMap.clear();
      // Get all user mnemonic and user email as a map
      const usersMap = await firstValueFrom(this.getUsersMapFromKeycloak());

      // Replace the user.mnemonic with the cosmos-address and store it in the cosmos map
      for (const [key, value] of usersMap.entries()) {
        const cosmosAddress = await this.getCosmosAddress(key.toString());
        this.cosmosMap.set(cosmosAddress, value);
        // Is this the address to search for?
        if (cosmosAddressToSearchFor == cosmosAddress) {
          cosmosEmail = value;
        }
      }

      if (cosmosEmail !== undefined) {
        console.log(
          'Translated cosmos-address ',
          cosmosAddressToSearchFor,
          'to',
          cosmosEmail
        );
        return cosmosEmail;
      } else {
        console.log(
          'Translated cosmos-address ',
          cosmosAddressToSearchFor,
          'to undefined'
        );
        // The 'user' was deleted in keycloak. Store this result also in the map.
        this.cosmosMap.set(
          cosmosAddressToSearchFor,
          this.undefinedCosmicAddress
        );
        return this.undefinedCosmicAddress;
      }
    }
  }

  /**
   * Gets a map of mnemonic and email from keycloak.
   *
   * @private
   */
  private getUsersMapFromKeycloak(): Observable<Map<string, string>> {
    // Params for the token post message
    const urlSearchParams = new URLSearchParams();
    urlSearchParams.append('client_id', process.env.KEYCLOAK_CLIENTID);
    urlSearchParams.append('client_secret', process.env.KEYCLOAK_SECRET);
    urlSearchParams.append('grant_type', 'client_credentials');

    // Get an admin token to call with the token the sid-user information
    return this.httpService.post(this.tokenUrl, urlSearchParams).pipe(
      catchError((e) => {
        console.log(e);
        rethrow(e);
      }),
      map((resp) => resp.data.access_token),
      switchMap((adminToken: string) => {
        // Add adminToken to the config for the HTTP get call
        const config = {
          headers: { Authorization: `Bearer ${adminToken}` },
        };

        // Get the mnemonic from keycloak with the given token
        return this.httpService.get(this.userUrl, config).pipe(
          catchError((e) => {
            console.log(e);
            rethrow(e);
          }),
          map((resp) => resp.data),
          map((users) => {
            // New map for all user mnemonics and emails
            const usersMap = new Map<string, string>();
            // fill the map with the new user data
            for (const element of users) {
              const user = element;
              // service provider don't have a mnemonic, so they are not a creator. Skip them
              if (
                user.attributes !== undefined &&
                user.attributes.mnemonic !== undefined
              ) {
                usersMap.set(user.attributes.mnemonic, user.email);
              }
            }
            return usersMap;
          })
        );
      })
    );
  }

  /**
   * Translate the given mnemonic to a cosmos address
   *
   * @param mnemonic The mnemonic to translate
   * @private
   */
  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(
        mnemonic,
        undefined,
        process.env.WALLET_ADDRESS_PREFIX
      )
    ).getAccounts();

    return creatorAddress.address;
  }
}
