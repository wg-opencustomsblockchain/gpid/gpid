/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Basic Configuration settings. */
export enum Swagger {
  // Application configuration
  APPLICATION_TITLE = 'rhenus.api',
  APPLICATION_DESCRIPTION = 'Rhenus',
  APPLICATION_VERSION = '1.0',
  SWAGGER_PATH = 'api',
}
