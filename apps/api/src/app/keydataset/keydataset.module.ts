/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { KeyDataSetController } from './keydataset.controller';
import { KeyDataSetService } from './keydataset.service';
import { ServiceProviderLogicService } from '../services/service-provider-logic.service';
import { HttpModule } from '@nestjs/axios';
import { CosmosAddressService } from '../services/cosmos-address.service';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'KEYDATASET_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_data',
          queueOptions: {
            durable: false,
          },
        },
      },
      {
        name: 'HISTORY_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_history',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
    HttpModule,
  ],
  controllers: [KeyDataSetController],
  providers: [
    KeyDataSetService,
    ServiceProviderLogicService,
    CosmosAddressService,
  ],
})
export class KeyDataSetModule {}
