/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ClientProxy, ClientsModule, Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { KeyDataSetService } from './keydataset.service';
import {
  CreateKeyDataSetDto,
  FindKeyDataSetDto,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { of } from 'rxjs';
import { CosmosAddressService } from '../services/cosmos-address.service';

describe('KeyDataSetService', () => {
  const fakeKeyDatasetDtoArray: KeyDataSetDto[] = [
    {
      goodsPassportId: 'goodsPassportId',
      creator: 'baumann',
      timestamp: 'just now',

      header: {
        seller: {
          identification: {
            identificationNumber: 'DE537400371045831',
          },
          name: 'Fraunhofer IML',
          address: {
            streetAndNumber: 'Jospeh von Fraunhofer Straße 2-4',
            city: 'Dortmund',
            postcode: '44227',
            country: 'DE',
          },
        },
        buyer: {
          identification: {
            identificationNumber: 'UK53742341045831',
          },
          name: 'Rhenus/ALS',
          address: {
            streetAndNumber: 'Rhenus Street 1',
            city: 'London',
            postcode: '12345',
            country: 'UK',
          },
        },
        invoice: {
          invoiceNumber: '123123',
          invoiceDate: '2021-02-17T12:45:00',
        },
      },
      goodsItem: [
        {
          sequenceNumber: '1',
          commodityCode: {
            harmonizedSystemSubHeadingCode: '940190',
          },
          descriptionOfGoods:
            'Rohre aus Aluminium; hier: aus nicht legiertem Aluminium',
          countryOfOrigin: 'DE',
          countryOfPreferentialOrigin: 'DE',
          invoiceAmount: '2.00',
          invoiceCurrency: 'EUR',
          quantity: '23123',
          netMass: '7025',
        },
      ],
    },
  ];
  let service: KeyDataSetService;
  let clientProxy: ClientProxy;
  let historyService: ClientProxy;
  let fakeCosmosAddressService: Partial<CosmosAddressService>;

  const fakeKeyDataSetDtos: KeyDataSetDto[] = [
    {
      goodsPassportId: 'goodsPassportId',
      creator: 'baumann',
      timestamp: 'just now',
      header: {
        seller: {
          identification: {
            identificationNumber: 'referenceNumber',
          },
          name: 'seller',
          address: {
            streetAndNumber: 'line',
            city: 'city',
            postcode: 'postcode',
            country: 'country',
          },
        },
        buyer: {
          identification: {
            identificationNumber: 'referenceNumber',
          },
          name: 'buyer',
          address: {
            streetAndNumber: 'line',
            city: 'city',
            postcode: 'postcode',
            country: 'country',
          },
        },
        invoice: {
          invoiceNumber: 'invoiceNumber',
          invoiceDate: 'invoiceDate',
        },
      },
      goodsItem: [
        {
          sequenceNumber: 'sequenceNumber',
          commodityCode: {
            harmonizedSystemSubHeadingCode: 'DE',
          },
          descriptionOfGoods: 'goodsDescription',
          countryOfOrigin: 'countryOfOrigin',
          countryOfPreferentialOrigin: 'countryOfPreferentialOrigin',
          invoiceAmount: 'invoiceAmount',
          invoiceCurrency: 'invoiceCurrency',
          quantity: 'quantity',
          netMass: 'grossMass',
        },
      ],
    },
  ];

  beforeEach(async () => {
    fakeCosmosAddressService = {
      extendKeyDataSetDtoWithCosmosName: (fakeKeyDataSetDto) =>
        Promise.resolve(fakeKeyDataSetDto),
      extendKeyDataSetDtoArrayWithCosmosName: (any) =>
        Promise.resolve(fakeKeyDatasetDtoArray),
      extendProcessDtoWithCosmosName: (fakeProcessDTO) =>
        Promise.resolve(fakeProcessDTO),
      extendTokenHistoryArrayWithCosmosName: (tokenHistoryDto) =>
        Promise.resolve(tokenHistoryDto),
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'KEYDATASET_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [process.env.RMQ_URL],
              queue: 'rhenus_data',
              queueOptions: {
                durable: false,
              },
            },
          },
          {
            name: 'HISTORY_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [''],
              queue: 'rhenus_history',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      providers: [
        KeyDataSetService,
        {
          provide: CosmosAddressService,
          useValue: fakeCosmosAddressService,
        },
      ],
    }).compile();

    service = module.get<KeyDataSetService>(KeyDataSetService);
    clientProxy = module.get<ClientProxy>('KEYDATASET_SERVICE');
    historyService = module.get<ClientProxy>('HISTORY_SERVICE');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('findKeyDataSet', async () => {
    const findKeyDataSetDto: FindKeyDataSetDto = {
      goodsPassportId: '1234',
    };
    const processDTO: ProcessDTO = {
      processId: '',
      documents: [],
      keydataset: [],
    };

    const spy = jest.spyOn(clientProxy, 'send').mockReturnValue(of(processDTO));

    expect(await service.findKeyDataSet(findKeyDataSetDto)).toBe(processDTO);
  });

  it('createKeyDataSet', async () => {
    const createKeyDataSetDto: CreateKeyDataSetDto = {
      keyDataSetDto: undefined,
      processId: '1234',
    };
    const processDTO: ProcessDTO = {
      processId: '',
      documents: [],
      keydataset: [],
    };

    const spy = jest.spyOn(clientProxy, 'send').mockReturnValue(of(processDTO));

    expect(await service.createKeyDataSet(createKeyDataSetDto)).toBe(
      processDTO
    );
  });

  it('updateKeyDataSet', async () => {
    const updateKeyDataSetDto: UpdateKeyDataSetDto = {
      keyDataSetDto: undefined,
      goodsPassportId: '1234',
    };
    const processDTO: ProcessDTO = {
      processId: '',
      documents: [],
      keydataset: [],
    };

    const spy = jest.spyOn(clientProxy, 'send').mockReturnValue(of(processDTO));

    expect(await service.updateKeyDataSet(updateKeyDataSetDto)).toBe(
      processDTO
    );
  });

  it('findKeyDataSetHistory', async () => {
    const findKeyDataSetDto: FindKeyDataSetDto = {
      goodsPassportId: '1234',
    };

    const spy = jest
      .spyOn(clientProxy, 'send')
      .mockReturnValue(of(fakeKeyDatasetDtoArray));

    expect(await service.findKeyDataSetHistory(findKeyDataSetDto)).toBe(
      fakeKeyDatasetDtoArray
    );
  });

  it('findAllKeyDataSets', async () => {
    const spy = jest
      .spyOn(clientProxy, 'send')
      .mockReturnValue(of(fakeKeyDatasetDtoArray));

    expect(await service.findAllKeyDataSets('user-mnemonic')).toBe(
      fakeKeyDatasetDtoArray
    );
  });

  it('getHistoryOfDataset', async () => {
    const spy = jest.spyOn(historyService, 'send').mockReturnValue(of([]));

    await expect(service.getHistoryOfDataset('')).resolves.toStrictEqual([]);
  });
});
