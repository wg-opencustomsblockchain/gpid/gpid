/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, Injectable } from '@nestjs/common';
import {
  AMQPPatterns,
  CreateKeyDataSetDto,
  FieldInformationDto,
  FindKeyDataSetDto,
  HistoryMqPattern,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';
import { CosmosAddressService } from '../services/cosmos-address.service';

/**
 * This class forwards the frontend REST requests to the blockchain connector. It rebuilds the requests to the RabbitMQ format.
 */
@Injectable()
export class KeyDataSetService {
  constructor(
    @Inject('KEYDATASET_SERVICE') public readonly client: ClientProxy,
    @Inject('HISTORY_SERVICE') private readonly historyService: ClientProxy,
    private cosmosAddressService: CosmosAddressService
  ) {}

  /**
   * Calls via RabbitMQ (AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_FIND) the blockchain connector to find the related
   * ProcessDTO with the given goodsPassportId.
   *
   * @param findKeyDataSetDto The goodsPassportId and the mnemonic
   */
  async findKeyDataSet(
    findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<ProcessDTO> {
    return this.cosmosAddressService.extendProcessDtoWithCosmosName(
      await firstValueFrom(
        this.client.send<ProcessDTO>(
          AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_FIND,
          findKeyDataSetDto
        )
      )
    );
  }

  /**
   * Calls via RabbitMQ (AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_CREATE) the blockchain connector to store the given
   * KeyDataSet.
   *
   * @param createKeyDataSetDto The KeyDataSet to store in the blockchain together with the processId and mnemonic
   */
  async createKeyDataSet(
    createKeyDataSetDto: CreateKeyDataSetDto
  ): Promise<ProcessDTO> {
    const keyDataSetDto = this.client.send<ProcessDTO>(
      AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_CREATE,
      createKeyDataSetDto
    );
    return this.cosmosAddressService.extendProcessDtoWithCosmosName(
      await firstValueFrom(keyDataSetDto)
    );
  }

  /**
   * Calls via RabbitMQ (AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_UPDATE) the blockchain connector to store the given
   * KeyDataSet.
   *
   * @param updateKeyDataSetDto The KeyDataSet to update in the blockchain together with the processId and mnemonic
   */
  async updateKeyDataSet(
    updateKeyDataSetDto: UpdateKeyDataSetDto
  ): Promise<ProcessDTO> {
    const keyDataSetDto = this.client.send<ProcessDTO>(
      AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_UPDATE,
      updateKeyDataSetDto
    );
    return this.cosmosAddressService.extendProcessDtoWithCosmosName(
      await firstValueFrom(keyDataSetDto)
    );
  }

  /**
   * Calls via RabbitMQ (AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_HISTORY) the blockchain connector to find the related
   * history of a KeyDataSet with the given goodsPassportId.
   *
   * @param findKeyDataSetDto The goodsPassportId and the mnemonic
   * @returns An Array of KeyDataSets that contains the history of the KeyDataSet, referenced by the goodsPassportId.
   */
  async findKeyDataSetHistory(
    findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<KeyDataSetDto[]> {
    const keyDataSetDtos = await firstValueFrom(
      this.client.send<KeyDataSetDto[]>(
        AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_HISTORY,
        findKeyDataSetDto
      )
    );

    return await this.cosmosAddressService.extendKeyDataSetDtoArrayWithCosmosName(
      keyDataSetDtos
    );
  }

  /**
   * Calls via RabbitMQ (AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_ALL) the blockchain connector to find all
   * KeyDataSets in the blockchain.
   * @param mnemonic The mnemonic of the user who calls this method
   * @returns An Array of all KeyDataSets in the blockchain.
   */
  async findAllKeyDataSets(mnemonic: string): Promise<KeyDataSetDto[]> {
    const keyDataSetDtos = await firstValueFrom(
      this.client.send<KeyDataSetDto[]>(
        AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_ALL,
        mnemonic
      )
    );

    return await this.cosmosAddressService.extendKeyDataSetDtoArrayWithCosmosName(
      keyDataSetDtos
    );
  }

  async getHistoryOfDataset(
    goodsPassportId: string
  ): Promise<FieldInformationDto[]> {
    const fields = await firstValueFrom(
      this.historyService.send<FieldInformationDto[]>(
        `${HistoryMqPattern.PREFIX}${HistoryMqPattern.GET_HISTORY}`,
        { id: goodsPassportId }
      )
    );

    return await this.cosmosAddressService.extendTokenHistoryArrayWithCosmosName(
      fields
    );
  }
}
