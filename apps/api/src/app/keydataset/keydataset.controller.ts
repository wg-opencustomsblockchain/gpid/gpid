/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  Body,
  Controller,
  Get,
  Headers,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { KeyDataSetService } from './keydataset.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  CreateKeyDataSetDto,
  FieldInformationDto,
  HTTPEndpoint,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { AuthenticatedUser } from 'nest-keycloak-connect';
import { ServiceProviderLogicService } from '../services/service-provider-logic.service';
import { firstValueFrom } from 'rxjs';

/***
 * This class handles all incoming REST requests around a KeyDataSet from the frontend.
 */
@ApiTags('KeyDataSet Controller')
@Controller(HTTPEndpoint.KEYDATASET_CONTROLLER)
export class KeyDataSetController {
  constructor(
    private keyDataSetService: KeyDataSetService,
    private serviceProviderLogicService: ServiceProviderLogicService
  ) {}

  /**
   * Creates a new KeyDataSet inside the chain with the given processId. Creates a new processId, if there is no processId given.
   *
   * @param createKeyDataSetDto The KeyDataSet to store together with the processId and mnemonic.
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiBearerAuth()
  @Post(HTTPEndpoint.KEYDATASET_CREATE)
  async createKeyDataSet(
    @Body() createKeyDataSetDto: CreateKeyDataSetDto,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<ProcessDTO> {
    createKeyDataSetDto.mnemonic = await firstValueFrom(
      this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user)
    );

    return await this.keyDataSetService.createKeyDataSet(createKeyDataSetDto);
  }

  /**
   * Calls the KeyDataSetService to get all KeyDataSets in the blockchain
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   * @returns An Array of all KeyDataSets in the blockchain.
   */
  @ApiBearerAuth()
  @Get(HTTPEndpoint.KEYDATASET_FIND_ALL)
  async findAllKeyDataSets(
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<KeyDataSetDto[]> {
    const mnemonic = await firstValueFrom(
      this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user)
    );

    return this.keyDataSetService.findAllKeyDataSets(mnemonic);
  }

  /**
   * Calls the KeyDataSetService to get the history of a KeyDataSet with a given goodsPassportId
   *
   * @param goodsPassportId The goodPassportId to get the history from
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   * @returns An Array of KeyDataSets that contains the history of the KeyDataSet, referenced by the goodsPassportId.
   */
  @ApiBearerAuth()
  @Get(HTTPEndpoint.KEYDATASET_HISTORY)
  async findKeyDataSetHistory(
    @Param('goodspassportid') goodsPassportId: string,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<KeyDataSetDto[]> {
    const mnemonic = await firstValueFrom(
      this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user)
    );

    return await this.keyDataSetService.findKeyDataSetHistory({
      goodsPassportId: goodsPassportId,
      mnemonic: mnemonic,
    });
  }

  /**
   * Calls the KeyDataSetService to get the complete ProcessDTO with a given goodsPassportId
   *
   * @param goodsPassportId The goodPassportId to search for
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiBearerAuth()
  @Get(HTTPEndpoint.KEYDATASET_FIND)
  async findKeyDataSet(
    @Param('goodspassportid') goodsPassportId: string,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<ProcessDTO> {
    const mnemonic = await firstValueFrom(
      this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user)
    );

    return await this.keyDataSetService.findKeyDataSet({
      goodsPassportId: goodsPassportId,
      mnemonic: mnemonic,
    });
  }

  /**
   * Updates a KeyDataSet inside the chain with the given processId.
   *
   * @param updateKeyDataSetDto The KeyDataSet to update.
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiBearerAuth()
  @Put(HTTPEndpoint.KEYDATASET_UPDATE)
  async updateKeyDataSet(
    @Param('goodspassportid') goodsPassportId: string,
    @Body() updateKeyDataSetDto: UpdateKeyDataSetDto,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<ProcessDTO> {
    updateKeyDataSetDto.mnemonic = await firstValueFrom(
      this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user)
    );

    return await this.keyDataSetService.updateKeyDataSet(updateKeyDataSetDto);
  }

  /**
   * Calls the KeyDataSetService to get all KeyDataSets in the blockchain
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   * @returns An Array of all KeyDataSets in the blockchain.
   */
  @ApiBearerAuth()
  @Get('/token/history/:id')
  async getTokenHistory(
    @Param('id') id: string,
    @AuthenticatedUser() user: any
  ): Promise<FieldInformationDto[]> {
    return this.keyDataSetService.getHistoryOfDataset(id);
  }
}
