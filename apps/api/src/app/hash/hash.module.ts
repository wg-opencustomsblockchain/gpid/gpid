/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Module} from '@nestjs/common';
import {HashController} from './hash.controller';
import {HashService} from './hash.service';
import {ClientsModule, Transport} from '@nestjs/microservices';
import {ServiceProviderLogicService} from "../services/service-provider-logic.service";
import {HttpModule} from "@nestjs/axios";
import {CosmosAddressService} from "../services/cosmos-address.service";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'HASH_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_data',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
    HttpModule,
  ],
  controllers: [HashController],
  providers: [HashService, ServiceProviderLogicService, CosmosAddressService],
})
export class HashModule {
}
