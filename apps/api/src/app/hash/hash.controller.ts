/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Body, Controller, Get, Headers, Post, Query, UploadedFile, UseInterceptors,} from '@nestjs/common';
import {FileInterceptor} from '@nestjs/platform-express';
import {Express} from 'express';
import {ApiBearerAuth, ApiBody, ApiConsumes, ApiTags} from '@nestjs/swagger';
import {HashService} from './hash.service';
import {AuthenticatedUser} from 'nest-keycloak-connect';
import {HashTokenDTO, HTTPEndpoint, ProcessDTO} from '@rhenus/api-interfaces';
import {Multer} from 'multer';
import {ServiceProviderLogicService} from "../services/service-provider-logic.service";
import {firstValueFrom} from "rxjs";
import {CosmosAddressService} from "../services/cosmos-address.service";

@ApiTags('Hash Controller')
@Controller(HTTPEndpoint.HASH_CONTROLLER)
export class HashController {
  constructor(private hashService: HashService,
              private serviceProviderLogicService: ServiceProviderLogicService,
              private cosmosAddressService: CosmosAddressService)
  {}

  /**
   * Upload a new file and compare it to an existing HashToken.
   *
   * @param file The file to be hashed
   * @param id The id of an existing HashToken
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        id: {type: 'string'},
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiBearerAuth()
  @Post(HTTPEndpoint.HASH_VERIFY)
  @UseInterceptors(FileInterceptor('file'))
  verifyFile(
    @UploadedFile() file: Express.Multer.File,
    @Body('id') id: string,
    @Headers('sid') sid: string
  ): Promise<HashTokenDTO> {
    return this.hashService.fetchHashToken(id, file);
  }


  /**
   * Upload a new file to be hashed. The resulting hash will be stored on the blockchain as a HashToken.
   *
   * @param file The file to be hashed
   * @param docType The document type
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        documentType: {type: 'string'},
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiBearerAuth()
  @Post(HTTPEndpoint.HASH_UPLOAD)
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body('documentType') docType: string,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<ProcessDTO> {
    const mnemonic = await firstValueFrom(this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user));

    return this.hashService.createHashToken(file, docType, mnemonic);
  }

  /**
   * Upload a new file to be hashed and add it to an existing process . The resulting hash will be stored on the blockchain as a HashToken.
   *
   * @param file The file to be hashed
   * @param docType The document type
   * @param processId Existing process the file will be added to.
   * @param sid The keycloak user-id to use for the service provider
   * @param user The logged-in user (via @ApiBearerAuth and @AuthenticatedUser)
   */
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        documentType: {type: 'string'},
        file: {
          type: 'string',
          format: 'binary',
        },
        processId: {type: 'string'},
      },
    },
  })
  @ApiBearerAuth()
  @Post(HTTPEndpoint.HASH_ADD)
  @UseInterceptors(FileInterceptor('file'))
  async addFile(
    @UploadedFile() file: Express.Multer.File,
    @Body('documentType') docType: string,
    @Body('processId') processId: string,
    @Headers('sid') sid: string,
    @AuthenticatedUser() user: any
  ): Promise<ProcessDTO> {
    const mnemonic = await firstValueFrom(this.serviceProviderLogicService.handleServiceProviderMnemonic(sid, user));

    return this.hashService.addHashToken(
      file,
      docType,
      mnemonic,
      processId
    );
  }

  /**
   * Resolve a cosmos address to a user name.
   *
   * @param creatorId The address to be resolved.
   */
  @ApiBearerAuth()
  @Get(HTTPEndpoint.CREATOR_NAME)
  getCreatorName(@Query('creatorid') creatorId: string): Promise<string> {
    return this.cosmosAddressService.getCosmosName(creatorId);
  }
}
