/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Test, TestingModule} from '@nestjs/testing';
import {HashService} from './hash.service';
import {ClientProxy, ClientsModule, Transport} from '@nestjs/microservices';
import {HashTokenDTO, KeyDataSetDto, ProcessDTO} from '@rhenus/api-interfaces';
import {of} from 'rxjs';
import {Multer} from 'multer';
import * as express from 'express';
import {CosmosAddressService} from "../services/cosmos-address.service";


describe('HashService', () => {
  let service: HashService;
  let client: ClientProxy;
  let fakeCosmosAddressService: Partial<CosmosAddressService>

  beforeEach(async () => {
    fakeCosmosAddressService = {
      extendProcessDtoWithCosmosName: (fakeProcessDTO) => Promise<ProcessDTO>.resolve(fakeProcessDTO),
      getCosmosName:(cosmosAddressToSearchFor) => Promise<string>.resolve('rhenus')
    }

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'HASH_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [process.env.RMQ_URL],
              queue: 'rhenus_data',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      providers: [
        HashService,
        {
          provide: CosmosAddressService,
          useValue: fakeCosmosAddressService
        }
      ]
    }).compile();

    service = module.get<HashService>(HashService);
    client = module.get<ClientProxy>('HASH_SERVICE');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createHashToken', () => {
    it('creates a new token', async () => {
      const process: ProcessDTO = {
        processId: '',
        documents: [],
        keydataset: [],
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const spy = jest.spyOn(client, 'send').mockReturnValue(of([process]));
      await service.createHashToken(file, 'type', 'mnemonic');
      expect(spy).toBeCalled();
    });
  });

  describe('addHashToken', () => {
    it('adds a new token', async () => {
      const process: ProcessDTO = {
        processId: '',
        documents: [],
        keydataset: [],
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const spy = jest.spyOn(client, 'send').mockReturnValue(of([process]));
      await service.addHashToken(file, 'type', 'mnemonic', 'P_123');
      expect(spy).toBeCalled();
    });
  });

  describe('fetchHashToken', () => {
    it('fetches a token', async () => {
      const hashDto: HashTokenDTO = {
        id: '',
        creator: '',
        timestamp: '',
        documentType: '',
        hash: '',
        hashFunction: ''
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const spy = jest.spyOn(client, 'send').mockReturnValue(of([hashDto]));
      await service.fetchHashToken('123_INV', file);
      expect(spy).toBeCalled();
    });
  });
});
