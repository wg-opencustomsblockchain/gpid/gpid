/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ClientsModule, Transport} from '@nestjs/microservices';
import {Test, TestingModule} from '@nestjs/testing';
import {HashTokenDTO, ProcessDTO} from '@rhenus/api-interfaces';
import {Observable, of} from 'rxjs';
import {HashController} from './hash.controller';
import {HashService} from './hash.service';
import {ServiceProviderLogicService} from "../services/service-provider-logic.service";
import {CosmosAddressService} from "../services/cosmos-address.service";

describe('HashController', () => {
  let controller: HashController;
  let service: HashService;
  let fakeServiceProviderLogicService: Partial<ServiceProviderLogicService>;
  let fakeCosmosAddressService: Partial<CosmosAddressService>

  beforeEach(async () => {    // Create the fake ServiceProviderLogicService
    fakeServiceProviderLogicService = {
      handleServiceProviderMnemonic: (sid, user): Observable<string> => (of('mnemonic'))
    };
    fakeCosmosAddressService = {
      getCosmosName: (cosmosAddressToSearchFor) => Promise<ProcessDTO>.resolve('rhenus-lab@iml.fhg.de')
    }

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'HASH_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [process.env.RMQ_URL],
              queue: 'rhenus_data',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      controllers: [HashController],
      providers: [
        {
          provide: HashService,
          useClass: HashService,
        },
        {
          provide: ServiceProviderLogicService,
          useValue: fakeServiceProviderLogicService
        },
        {
          provide: CosmosAddressService,
          useValue: fakeCosmosAddressService
        }
      ]
    }).compile();

    controller = module.get<HashController>(HashController);
    service = module.get<HashService>(HashService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('uploadFile', () => {
    it('fetches a token', async () => {
      const process: ProcessDTO = {
        processId: '',
        documents: [],
        keydataset: [],
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const user = {
        mnemonic: 'MNEMONIC',
      }
      const spy = jest.spyOn(service, 'createHashToken').mockReturnValue(Promise.resolve(process));
      expect(await controller.uploadFile(file, 'docType', 'sid', user)).toEqual(process);
    });
  });

  describe('addFile', () => {
    it('adds a token', async () => {
      const process: ProcessDTO = {
        processId: '',
        documents: [],
        keydataset: [],
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const user = {
        mnemonic: 'MNEMONIC',
      }
      const spy = jest.spyOn(service, 'addHashToken').mockReturnValue(Promise.resolve(process));
      expect(await controller.addFile(file, 'docType', 'P_123', 'sid', user)).toEqual(process);
    });
  });

  describe('verifyFile', () => {
    it('verifies a token', async () => {
      const hashDto: HashTokenDTO = {
        id: '',
        creator: '',
        timestamp: '',
        documentType: '',
        hash: '',
        hashFunction: ''
      };
      const file: Express.Multer.File = {
        originalname: 'file.csv',
        mimetype: 'text/csv',
        path: 'something',
        buffer: Buffer.from('one,two,three'),
        fieldname: '',
        encoding: '',
        size: 0,
        stream: null,
        destination: '',
        filename: ''
      };
      const spy = jest.spyOn(service, 'fetchHashToken').mockReturnValue(Promise.resolve(hashDto));
      expect(await controller.verifyFile(file, '123_INV', 'sid'));
    });
  });

  describe('getCreatorName', () => {
    it('resolves a creator', async () => {
      expect(await controller.getCreatorName('cosmos158m6tn42ems5vwxsrnr6ncdjnt3flc7hnhx0vc')).toEqual('rhenus-lab@iml.fhg.de');
    });
  });
});
