/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {BadRequestException, Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {
  AddHashTokenDTO,
  AMQPPatterns,
  CheckHashTokenDTO,
  CreateHashTokenDTO,
  HashTokenDTO,
  ProcessDTO,
} from '@rhenus/api-interfaces';
import {catchError, firstValueFrom} from 'rxjs';
import {CosmosAddressService} from "../services/cosmos-address.service";

@Injectable()
export class HashService {
  constructor(@Inject('HASH_SERVICE') public readonly client: ClientProxy,
              private cosmosAddressService: CosmosAddressService) {
  }

    /**
   * Calculate the hash of a file and store it on the blockchain as a HashToken.
   *
   * @param file The file to be hashed
   * @param documentType The document type
   * @param mnemonic users mnemonic, used for signing the blockchain transaction
   */
  async createHashToken(
    file: Express.Multer.File,
    documentType: string,
    mnemonic: string
  ): Promise<ProcessDTO> {
    const createHashTokenDTO: CreateHashTokenDTO = {
      documentType,
      fileContent: file.buffer.toString('hex'),
      mnemonic,
    };
    const process = this.client.send<ProcessDTO>(
      AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_CREATE,
      createHashTokenDTO
    );

    return this.cosmosAddressService.extendProcessDtoWithCosmosName(await firstValueFrom(process));
  }

      /**
   * Calculate the hash of a file and store it on the blockchain as a HashToken. 
   * The HashToken will be saved inside an existing process
   *
   * @param file The file to be hashed
   * @param documentType The document type
   * @param processId The process the token will be stored in
   * @param mnemonic users mnemonic, used for signing the blockchain transaction
   */
  async addHashToken(
    file: Express.Multer.File,
    documentType: string,
    mnemonic: string,
    processId: string
  ): Promise<ProcessDTO> {
    const addHashTokenDTO: AddHashTokenDTO = {
      documentType,
      fileContent: file.buffer.toString('hex'),
      mnemonic,
      processId,
    };
    const process = this.client.send<ProcessDTO>(
      AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_ADD,
      addHashTokenDTO
    );

    return await this.cosmosAddressService.extendProcessDtoWithCosmosName(await firstValueFrom(process));
  }

    /**
   * Calculate the hash of a file and compare it to an existing HashToken.
   *
   * @param file The file to be hashed
   * @param id The id of an existing HashToken
   */
  async fetchHashToken(id: string, file: Express.Multer.File): Promise<HashTokenDTO> {
    const id_type = id.split('_');
    if (id_type.length === 2) {
      const checkHashTokenDTO: CheckHashTokenDTO = {
        id: id_type[0],
        fileContent: file.buffer.toString('hex'),
        documentType: id_type[1],
      };
      const hashToken = this.client
        .send<HashTokenDTO>(
          AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_FIND,
          checkHashTokenDTO
        )
        .pipe(
          catchError(() => {
            throw new BadRequestException("Request rejected");
          })
        );
      const hashTokenDTO = await firstValueFrom(hashToken);
      hashTokenDTO.creatorName = await this.cosmosAddressService.getCosmosName(hashTokenDTO.creator);
      return hashTokenDTO;
    }
    throw new BadRequestException("Invalid format: " + id);
  }
}
