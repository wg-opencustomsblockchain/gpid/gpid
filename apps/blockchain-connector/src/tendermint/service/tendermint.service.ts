/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Coin } from '@cosmjs/launchpad';
import {
  DirectSecp256k1HdWallet,
  EncodeObject,
  Registry,
} from '@cosmjs/proto-signing';
import { DecodeObject } from '@cosmjs/proto-signing/build/registry';
import {
  BroadcastTxResponse,
  isBroadcastTxFailure,
  isBroadcastTxSuccess,
  SigningStargateClient,
} from '@cosmjs/stargate';
import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import {
  MsgAttachHashToken,
  MsgCreateHashToken,
  MsgFetchDocumentHash,
  MsgFetchDocumentHashResponse,
  MsgFetchProcessResponse,
  MsgCreateKeyDataSet,
  MsgFetchProcess,
  MsgFetchKeyDataSet,
  MsgUpdateKeyDataSet,
  MsgFetchAllToken,
  MsgFetchAllTokenResponse,
  MsgFetchTokenHistory,
  MsgFetchTokenHistoryResponse,
} from '@rhenus/generated';

/**
 * Workhorse class for all Chain interactions of this application.
 * Handles both the details of writing and reading to the chain.
 */
@Injectable()
export class TendermintService {
  /** A standard coin object - Can be set to 0 as we do not use any monetary token systems.  */
  stdCoins: Coin[] = [{ denom: 'token', amount: '0' }];

  /** How much gas a transaction may use - Set to big enough value to accept all transactions.  */
  stdGas: string = process.env.TENDERMINT_MAX_GAS;

  /** Memo that is attached to a transaction  */
  stdMemo: 'Sent From Rhenus Backend';

  /** The RPC endpoint to query/send transactions to. */
  blockchainRPCEndpoint = process.env.TENDERMINT_CHAIN_ENDPOINT_RPC;

  /** Type URL corresponsing to the Import Module int he chain. */
  businesslogic = process.env.BLOCKCHAIN_BUSINESS_TYPE_URL;

  /** CosmosWallet Address to sign with. */
  wallet: string;

  /** Registry to decode Blockchain responses. */
  registry: any;

  /** PART OF THE HOTFIX FOR VERSION 0.24 */
  private maxTimeout = 30;
  private timeoutMs = 1000;

  /** Creates a new Tendermint Service instance. Injects a Http Service. */
  constructor(private readonly httpService: HttpService) {
    const types = [
      // All Messages and Responses related to Hash Token
      [`${this.businesslogic}.MsgCreateHashToken`, MsgCreateHashToken],
      [`${this.businesslogic}.MsgAttachHashToken`, MsgAttachHashToken],
      [`${this.businesslogic}.MsgFetchDocumentHash`, MsgFetchDocumentHash],
      [
        `${this.businesslogic}.MsgFetchDocumentHashResponse`,
        MsgFetchDocumentHashResponse,
      ],
      [`${this.businesslogic}.MsgFetchProcessResponse`, MsgFetchProcessResponse],
      [`${this.businesslogic}.MsgCreateKeyDataSet`, MsgCreateKeyDataSet],
      [`${this.businesslogic}.MsgUpdateKeyDataSet`, MsgUpdateKeyDataSet],
      [`${this.businesslogic}.MsgFetchProcess`, MsgFetchProcess],
      [`${this.businesslogic}.MsgFetchKeyDataSet`, MsgFetchKeyDataSet],    

      [`${this.businesslogic}.MsgFetchAllToken`, MsgFetchAllToken],
      [`${this.businesslogic}.MsgFetchAllTokenResponse`, MsgFetchAllTokenResponse],
      [`${this.businesslogic}.MsgFetchTokenHistory`, MsgFetchTokenHistory],
      [`${this.businesslogic}.MsgFetchTokenHistoryResponse`, MsgFetchTokenHistoryResponse],
    ];

    this.registry = new Registry(<any>types);
  }

  /**
   * Generic Query generator function.
   * Queries the chain endpoints specified by the function parameters.
   * @param nodeUri The address of the Blockchain to dial into.
   * @param chainId The id of the Tendermint instance running.
   * @param path The path of the resource to query for.
   * @deprecated
   * @returns
   */
  async query(nodeUri: string, chainId: string, path: string) {
    Logger.log(`${nodeUri}/${chainId}/${path}`);
    return (
      await this.httpService.get(`${nodeUri}/${chainId}/${path}`).toPromise()
    ).data;
  }

  /**
   * Prepare an incoming transaction into a Cosmos/Tendermint message format.
   * @param wallet The Wallet address used for signing.
   * @param mnenmonic The private Key used to sign with.
   * @param txInput The transaction input to sign.
   * @param type The type of a transaction Msg. For example: "MsgCreateExportToken"
   * @param typeUrl The URL under which the API is served. For example: "/org.borderblockchain.importtoken"
   * @returns
   */
  async buildSignAndBroadcast(
    wallet: string,
    mnenmonic: string,
    txInput: any[],
    type: string,
    typeUrl: string
  ): Promise<BroadcastTxResponse> {
    const messages: EncodeObject[] = [];
    for (const txs of txInput) {
      messages.push({
        typeUrl: `${typeUrl}.${type}`,
        value: txs,
      });
    }
    return this.signAndBroadcast(wallet, mnenmonic, messages);
  }

  /**
   * This is the last step in the writing process onto the tendermint/cosmos instance.
   * It signs a pre-packaged Array of Encode Objects and broadcasts it to chain peers.
   * Use buildSignAndBroadcast if you need to build your Encode objects first.
   * @param wallet The Wallet address used for signing.
   * @param mnenmonic The private Key used to sign with.
   * @param messages The array of messages of Encode Objects.
   * @returns
   */

  async signAndBroadcast(
    wallet: string,
    mnemonic: string,
    messages: readonly EncodeObject[]
  ): Promise<BroadcastTxResponse> {
    const blockchainRPCEndpoint = `${process.env.TENDERMINT_CHAIN_ENDPOINT_RPC}`;
    const signer = await DirectSecp256k1HdWallet.fromMnemonic(mnemonic);
    const stargateClient = SigningStargateClient;
    const client = await stargateClient.connectWithSigner(
      blockchainRPCEndpoint,
      signer,
      { registry: this.registry }
    );

    // Try broadcast multiple times, if transaction fails
    for (let i = 0; i < this.maxTimeout; i++) {
      const response: BroadcastTxResponse = await client.signAndBroadcast(
        wallet,
        messages,
        { amount: this.stdCoins, gas: this.stdGas },
        this.stdMemo
      );
      if (isBroadcastTxFailure(response)) {
        // details: apps/blockchain/x/businesslogic/types/errors.go
        if ([3, 4, 5, 6, 7, 8, 9, 10].includes(response.code)) {
          throw new BadRequestException(response.rawLog);
        }
      }
      if (isBroadcastTxSuccess(response)) {
        return response;
      }
      await this.delay(this.timeoutMs);
    }
  }

  /**
   * Decodes a result. The response is specified in the tendermint/cosmos go module.
   * @param result The response given by the chain.
   * @param responseType The type of response expected. Needs to be registered in the registry of this class.
   * @returns
   */
  async decodeResult(
    result: BroadcastTxResponse,
    responseType: string
  ): Promise<any> {
    try {
      const decodedResult: DecodeObject = this.registry.decode({
        typeUrl: responseType,
        value: result.data[0].data,
      });
      return decodedResult;
    } catch {
      console.log('Transaction OK, But nothing to decode.');
      return null;
    }
  }

  //** PART OF THE HOTFIX FOR VERSION 0.24 */
  async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}
