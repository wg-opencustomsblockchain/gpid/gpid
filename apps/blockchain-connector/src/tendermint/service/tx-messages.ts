/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Collection of the Rhenus API Information.
 * TENDERMINT_TYPE_URL: The base URL under which the API is served.
 * MODULE: Name of the corresponding Module in the Chain GO Code.
 * ENDPOINTS: The available Endpoints for the Module
 */
export enum TxMessages {
  MsgCreateHashToken = 'MsgCreateHashToken',
  MsgAttachHashToken = 'MsgAttachHashToken', // TODO this is just guessed currently
  MsgFetchDocumentHash = 'MsgFetchDocumentHash',
  MsgFetchDocumentHashResponse = 'MsgFetchDocumentHashResponse',
  MsgFetchProcessResponse = 'MsgFetchProcessResponse',
  MsgCreateKeyDataSet = 'MsgCreateKeyDataSet',
  MsgUpdateKeyDataSet = 'MsgUpdateKeyDataSet',
  MsgFetchProcess = 'MsgFetchProcess',
  MsgFetchKeyDataSet = 'MsgFetchKeyDataSet',
  MsgFetchAllToken = 'MsgFetchAllToken',  
  MsgFetchAllTokenResponse = 'MsgFetchAllTokenResponse',
  MsgFetchTokenHistory = 'MsgFetchTokenHistory',
  MsgFetchTokenHistoryResponse = 'MsgFetchTokenHistoryResponse'
}
