/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { HashTokenDTO, HashTokenTypes, ProcessDTO, SystemMessage } from '@rhenus/api-interfaces';
import {
  MsgCreateHashToken,
  MsgAttachHashToken,
  MsgFetchDocumentHash,
  MsgFetchDocumentHashResponse,
} from '@rhenus/generated';
import { TxMessages } from '../tendermint/service/tx-messages';
import { TendermintService } from './../tendermint/service/tendermint.service';

/**
 * Service class to pass requests to the corresponding chain endpoints.
 * Our Blockchain API delivers a full event history alongside each Token to minimize API calls.
 */
@Injectable()
export class HashTokenService {
  /**Creates a Service instance. Injects a Tendermint Service. */
  constructor(private tendermintService: TendermintService) {}

  /** Default Mnenmonic to sign transactions with.*/
  private defaultMnenmonic = process.env.WALLET_ADDRESS_MNENMONIC;

  /** Business Logic Type URL. Used for write transactions.*/
  private businessLogic = HashTokenTypes.TENDERMINT_TYPE_URL_BUSINESSLOGIC;

  /**
   * Finds a specific export token by its identifying MRN.
   * @param mrn The process ID which is mapped internally to a token ID.
   * @returns The token with current status, history and last timestamp.
   */
  public async fetchHashToken(
    id: string,
    documentType: string
  ): Promise<HashTokenDTO> {
    const address = await this.getCosmosAddress(this.defaultMnenmonic);
    const msg: MsgFetchDocumentHash = {
      creator: address,
      id: id,
      documentType: documentType,
      timestamp: new Date().toString() + Math.random(),
    };
    const token: MsgFetchDocumentHashResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          this.defaultMnenmonic,
          [msg],
          TxMessages.MsgFetchDocumentHash,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchDocumentHashResponse}`
      );
    if (!token) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    const result: HashTokenDTO = token;

    return result;
  }

  // --- Transactions ---

  /**
   * Writes an export to the chain and creates a token.
   * @param msg DTO to be saved.
   * @returns The new token that has been written to the chain.
   */
  public async createHashToken(
    msg: MsgCreateHashToken,
    mnemonic: string
  ): Promise<ProcessDTO> {
    const address = await this.getCosmosAddress(mnemonic);
    msg.creator = address;
    const result = await this.tendermintService.decodeResult(
      await this.tendermintService.buildSignAndBroadcast(
        address,
        mnemonic,
        [msg],
        TxMessages.MsgCreateHashToken,
        this.businessLogic
      ),
      `${this.businessLogic}.${TxMessages.MsgFetchProcessResponse}`
    );
    if (result == undefined)
      throw new BadRequestException(SystemMessage.ERROR_SAVE);
    return result;
  }

  /**
   * Writes an export to the chain and creates a token.
   * @param msg DTO to be saved.
   * @returns The new token that has been written to the chain.
   */
  public async addHashToken(
    msg: MsgAttachHashToken,
    mnemonic: string
  ): Promise<ProcessDTO> {
    const address = await this.getCosmosAddress(mnemonic);
    msg.creator = address;
    const request = await this.tendermintService.buildSignAndBroadcast(
        address,
        mnemonic,
        [msg],
        TxMessages.MsgAttachHashToken,
        this.businessLogic
      );
    const result = await this.tendermintService.decodeResult(
      request,
      `${this.businessLogic}.${TxMessages.MsgFetchProcessResponse}`
    );
    if (result == undefined)
      throw new BadRequestException(SystemMessage.ERROR_SAVE);
    return result;
  }

  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(
        mnemonic,
        undefined,
        process.env.WALLET_ADDRESS_PREFIX
      )
    ).getAccounts();

    return creatorAddress.address;
  }
}
