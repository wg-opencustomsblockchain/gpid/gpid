/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  BadRequestException,
  Controller,
  Inject,
  Logger,
} from '@nestjs/common';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';
import { HashTokenService } from './hashtoken.service';
import { SHA256, enc } from 'crypto-js';
import {
  AMQPPatterns,
  CheckHashTokenDTO,
  CreateHashTokenDTO,
  AddHashTokenDTO,
  HashTokenDTO,
  ProcessDTO
} from '@rhenus/api-interfaces';
import { MsgAttachHashToken, MsgCreateHashToken } from '@rhenus/generated';

/**
 * Controller that accepts AMQP calls for the Hash Token Module.
 * Passes requests to the corresponding Blockchain Service and returns Data
 * as DTO Objects per API Definition.
 */
@Controller()
export class HashTokenController {
  private logger: Logger = new Logger(HashTokenService.name);
  constructor(
    private readonly hashTokenService: HashTokenService,
    @Inject('broker') public readonly client: ClientProxy
  ) {}

  /**
   * Writes a new Token to Blockchain.
   * @param dto The new hash to be tokenized.
   * @returns The new token that has been written to the chain.
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_CREATE)
  async createHashToken(
    @Payload() dto: CreateHashTokenDTO
  ): Promise<ProcessDTO> {
    const hash = SHA256(dto.fileContent);

    const msg: MsgCreateHashToken = {
      creator: undefined,
      changeMessage: 'Send from blockchain connector',
      documentType: dto.documentType,
      hash: hash.toString(enc.Hex),
      hashFunction: 'sha256',
    };

    return this.hashTokenService.createHashToken(msg, dto.mnemonic);
  }

  /**
   * Finds a specific hash token by its identifying id and compares it's hash to the hash of the file handed over.
   * @param dto Contains the hash tokens id and the content of a file
   * @returns The hash token, if the Comparison succeeded
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_FIND)
  async fetchHashToken(
    @Payload() dto: CheckHashTokenDTO
  ): Promise<HashTokenDTO> {
    const hash = SHA256(dto.fileContent);
    try {
      const token = await this.hashTokenService.fetchHashToken(
        dto.id,
        dto.documentType
      );
      if (hash.toString(enc.Hex) != token.hash) {
        console.log('HASH NOT MATCHING');
        throw new BadRequestException("Invalid hash");
      }
      return token;
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  /**
   * Adds a new token to existing processId and write it to blockchain
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_ADD)
  async addHashTokenToProcess(@Payload() dto: AddHashTokenDTO): Promise<ProcessDTO>{
    const hash = SHA256(dto.fileContent);

    const msg: MsgAttachHashToken = {
      creator: undefined,
      targetId: dto.processId,
      changeMessage: 'Send from blockchain connector',
      hash: hash.toString(enc.Hex),
      hashFunction: 'sha256',
      documentType: dto.documentType,
    };

    return this.hashTokenService.addHashToken(msg, dto.mnemonic);
  }
}
