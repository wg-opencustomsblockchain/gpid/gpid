/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { RMQBroker } from '../shared/mqttBroker';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { HashTokenService } from './hashtoken.service';
import { HashTokenController } from './hashtoken.controller';
 
 /**
  * This module handles all tasks related to creation, updating and reading
  * of hash token.
  */
 @Module({
   imports: [HttpModule, new RMQBroker().getRMQBroker()],
   providers: [HashTokenService, TendermintService, ConfigService],
   controllers: [HashTokenController],
 })
 export class HashTokenModule {}
