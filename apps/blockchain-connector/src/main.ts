/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

import { AppModule } from './app.module';

/**
 * Main entrypoint of the Application.
 * Sets up both AMQP communication and defines the port to listen on.
 */
async function bootstrap() {
  /** Basic Microservice options to serve an AMQP Endpoint. */
  const logger = new Logger('NestApplication');
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.RMQ_URL],
      queue: 'rhenus_data',
      queueOptions: {
        durable: false,
      },
    },
    logger: ['error', 'warn'],
  });
  app.listen().then(() => logger.log(`🚀 - Server started and listening on AMQP PORT: ${process.env.RMQ_URL}.`));
}
/**
 * Calling the startup process
 */
bootstrap();
