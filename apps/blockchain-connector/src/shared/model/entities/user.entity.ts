/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Company } from './company';
import { Role } from './role.entity';
/**
 * User Entity for the placeholder User Service.
 */

export class User {
  userId: number;
  username: string;
  password: string;
  role: Role;
  mnenmonic: string;
  wallet: string;
  pubkey: string;
  company: Company;
}
