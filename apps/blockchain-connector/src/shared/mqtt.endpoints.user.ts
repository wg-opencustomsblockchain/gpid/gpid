/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Enum for Endpoints which are shared by the Modules.
 * Parts of the application communicate with other AMQP
 * Services such as the Placeholder User Service.
 */
//
export enum SharedEndpoints {
  AMQP_PREFIX = 'usermanagement/v1/',
  AMQP_TOKEN_USER_MANAGEMENT_FIND_USER = 'users',
}
