/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DynamicModule } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';

/** The interface used to communicate to other microservices via amqp.*/
export class RMQBroker {
  /** Returns an instance of the RMQ Broker as per NestJS specifications. */
  getRMQBroker(): DynamicModule {
    return ClientsModule.register([
      {
        name: 'broker',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_data',
          queueOptions: {
            durable: false,
          },
        },
      },
      {
        name: 'HISTORY_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [process.env.RMQ_URL],
          queue: 'rhenus_history',
          queueOptions: {
            durable: false,
          },
        },
      },
    ]);
  }
}
