/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { HashTokenModule } from './hashtoken/hashtoken.module';
import { KeyDataSetModule } from './keydataset/keydataset.module';

/**
 * Base Module for the App.
 * Imports the required submodules.
 */
@Module({
  imports: [
    HashTokenModule,
    KeyDataSetModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env'],
    }),
  ],
  providers: [],
})
export class AppModule {}
