/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {createHash} from 'crypto';

export function generateGoodsPassportId(identificationNumber: string, invoiceNumber: string): string {
  if (!identificationNumber) {
    throw new Error('identificationNumber is required');
  }

  if (!invoiceNumber) {
    throw new Error('invoiceNumber is required');
  }

  const concatenatedNumbers = `${identificationNumber}_${invoiceNumber}`;
  const hashDigest = createHash('sha256').update(concatenatedNumbers).digest('hex');
  return hashDigest.slice(0, 16);
}
