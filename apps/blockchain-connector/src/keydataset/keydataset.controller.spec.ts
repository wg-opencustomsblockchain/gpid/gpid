/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ClientsModule, Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { KeyDataSetController } from './keydataset.controller';
import { KeyDataSetService } from './keydataset.service';
import {
  CreateKeyDataSetDto,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';

describe('KeyDataSetController', () => {
  // Fake constants
  const fakeKeyDataSetDto: KeyDataSetDto = {
    goodsPassportId: 'goodsPassportId',
    creator: 'baumann',
    timestamp: 'just now',
    header: {
      seller: {
        identification: {
          identificationNumber: 'DE537400371045831',
        },
        name: 'Fraunhofer IML',
        address: {
          streetAndNumber: 'Jospeh von Fraunhofer Straße 2-4',
          city: 'Dortmund',
          postcode: '44227',
          country: 'DE',
        },
      },
      buyer: {
        identification: {
          identificationNumber: 'UK53742341045831',
        },
        name: 'Rhenus/ALS',
        address: {
          streetAndNumber: 'Rhenus Street 1',
          city: 'London',
          postcode: '12345',
          country: 'UK',
        },
      },
      invoice: {
        invoiceNumber: '123123',
        invoiceDate: '2021-02-17T12:45:00',
      },
    },
    goodsItem: [
      {
        sequenceNumber: '1',
        commodityCode: {
          harmonizedSystemSubHeadingCode: '940190',
        },
        descriptionOfGoods:
          'Rohre aus Aluminium; hier: aus nicht legiertem Aluminium',
        countryOfOrigin: 'DE',
        countryOfPreferentialOrigin: 'DE',
        invoiceAmount: '2.00',
        invoiceCurrency: 'EUR',
        quantity: '23123',
        netMass: '7025',
      },
    ],
  };
  const fakeProcessDTO: ProcessDTO = {
    processId: 'processId',
    documents: [],
    keydataset: [fakeKeyDataSetDto],
  };
  const fakeCreateKeyDataSetDto: CreateKeyDataSetDto = {
    keyDataSetDto: fakeKeyDataSetDto,
    processId: 'processId',
    mnemonic: 'mnemonic',
  };
  const fakeUpdateKeyDataSetDto: UpdateKeyDataSetDto = {
    keyDataSetDto: fakeKeyDataSetDto,
    goodsPassportId: 'goodsPassportId',
    mnemonic: 'mnemonic',
  };
  const fakeFindKeyDataSetDto = {
    goodsPassportId: 'goodsPassportId',
    mnemonic: 'User',
  };

  let controller: KeyDataSetController;
  let fakeKeyDataSetService: Partial<KeyDataSetService>;

  beforeEach(async () => {
    // Create the fake KeyDataService
    fakeKeyDataSetService = {
      findKeyDataSet: (findKeyDataSetDto) =>
        Promise<ProcessDTO>.resolve(fakeProcessDTO),
      createKeyDataSet: (createKeyDataSetDto) =>
        Promise<ProcessDTO>.resolve(fakeProcessDTO),
      updateKeyDataSet: (updateKeyDataSetDto) =>
        Promise<ProcessDTO>.resolve(fakeProcessDTO),
      findKeyDataSetHistory: (findKeyDataSetDto) =>
        Promise<KeyDataSetDto[]>.resolve([fakeKeyDataSetDto]),
      findAllKeyDataSets: (mnemonic) =>
        Promise<KeyDataSetDto[]>.resolve([fakeKeyDataSetDto]),
    };

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ClientsModule.register([
          {
            name: 'KEYDATASET_SERVICE',
            transport: Transport.RMQ,
            options: {
              urls: [process.env.RMQ_URL],
              queue: 'rhenus_data',
              queueOptions: {
                durable: false,
              },
            },
          },
        ]),
      ],
      controllers: [KeyDataSetController],
      providers: [
        {
          provide: KeyDataSetService,
          useValue: fakeKeyDataSetService,
        },
        {
          provide: 'broker',
          useValue: 'broker',
        },
      ],
    }).compile();

    controller = module.get<KeyDataSetController>(KeyDataSetController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call findKeyDataSet and return a ProcessDTO', async () => {
    const processDTO = await controller.findKeyDataSet(fakeFindKeyDataSetDto);

    expect(processDTO.keydataset[0].creator).toEqual('baumann');
  });

  it('should call createKeyDataSet and return a ProcessDTO', async () => {
    const processDTO = await controller.createKeyDataSet(
      fakeCreateKeyDataSetDto
    );

    expect(processDTO.keydataset[0].creator).toEqual('baumann');
  });

  it('should call updateKeyDataSet and return a ProcessDTO', async () => {
    const processDTO = await controller.updateKeyDataSet(
      fakeUpdateKeyDataSetDto
    );

    expect(processDTO.keydataset[0].creator).toEqual('baumann');
  });

  it('should call findKeyDataSetHistory and return a KeyDataSetDto[]', async () => {
    const keyDataSetDtos = await controller.findKeyDataSetHistory(
      fakeFindKeyDataSetDto
    );

    expect(keyDataSetDtos[0].creator).toEqual('baumann');
  });

  it('should call findAllKeyDataSets and return a KeyDataSetDto[]', async () => {
    const keyDataSetDtos = await controller.findAllKeyDataSets('user-mnemonic');

    expect(keyDataSetDtos[0].creator).toEqual('baumann');
  });
});
