/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Controller, Inject } from '@nestjs/common';
import { ClientProxy, MessagePattern, Payload } from '@nestjs/microservices';
import {
  AMQPPatterns,
  CreateKeyDataSetDto,
  FindKeyDataSetDto,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { KeyDataSetService } from './keydataset.service';
import { MsgFetchTokenHistoryResponse } from '@rhenus/generated';

/**
 * Controller that accepts AMQP calls for the KeyDataSet Module.
 * Pass requests to the corresponding Blockchain Service and returns Data
 * as Dto Objects per API Definition.
 */
@Controller()
export class KeyDataSetController {
  constructor(
    private readonly keyDataSetService: KeyDataSetService,
    @Inject('broker')
    public readonly client: ClientProxy
  ) {}

  /**
   * Writes a new KeyDataSet to blockchain.
   * @param createKeyDataSetDto The new keyDataSet with the processId.
   * @returns The complete ProcessDTO, which belongs to the processId. Including the KeyDataSet.
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_CREATE)
  async createKeyDataSet(
    @Payload() createKeyDataSetDto: CreateKeyDataSetDto
  ): Promise<ProcessDTO> {
    return this.keyDataSetService.createKeyDataSet(createKeyDataSetDto);
  }

  /**
   * Finds a specific KeyDataSet by its identifying goodsPassportId and returns the complete ProcessDTO.
   * @param findKeyDataSetDto Contains the goodsPassportId
   * @returns The complete ProcessDTO, which belongs to the goodsPassportId. Including the KeyDataSet
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_FIND)
  async findKeyDataSet(
    @Payload() findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<ProcessDTO> {
    return await this.keyDataSetService.findKeyDataSet(findKeyDataSetDto);
  }

  /**
   * Updates a KeyDataSet in the blockchain.
   * @param updateKeyDataSetDto The keyDataSet to update in the blockchain.
   * @returns The complete ProcessDTO, which belongs to the processId. Including the KeyDataSet.
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_UPDATE)
  async updateKeyDataSet(
    @Payload() updateKeyDataSetDto: UpdateKeyDataSetDto
  ): Promise<ProcessDTO> {
    return this.keyDataSetService.updateKeyDataSet(updateKeyDataSetDto);
  }

  /**
   * Calls the KeyDataSetService to get the history of a KeyDataSet identified by the goodsPassportId.
   * @param findKeyDataSetDto Contains the goodsPassportId
   * @returns An Array of KeyDataSets that contains the history of the KeyDataSet, referenced by the goodsPassportId.
   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_HISTORY)
  async findKeyDataSetHistory(
    @Payload() findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<KeyDataSetDto[]> {
    return await this.keyDataSetService.findKeyDataSetHistory(
      findKeyDataSetDto
    );
  }

  /**
   * Calls the KeyDataSetService to get all KeyDataSets in the blockchain.
   * @param mnemonic The mnemonic of the user, who calls this function.
   * @returns An Array of KeyDataSets of the blockchain.   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.KEYDATASET_ALL)
  async findAllKeyDataSets(
    @Payload() mnemonic: string
  ): Promise<KeyDataSetDto[]> {
    return await this.keyDataSetService.findAllKeyDataSets(mnemonic);
  }

  /**
   * Calls the KeyDataSetService to get the tokens history from blockchain.
   * @param mnemonic The mnemonic of the user, who calls this function.
   * @param id The token id to query
   * @returns An Array of KeyDataSets of the blockchain.   */
  @MessagePattern(AMQPPatterns.PREFIX + AMQPPatterns.TOKEN_HISTORY)
  async findTokenHistory(
    @Payload() payload: { mnemonic: string; id: string }
  ): Promise<MsgFetchTokenHistoryResponse> {
    return await this.keyDataSetService.getTokenHistory(
      payload.id,
      payload.mnemonic
    );
  }
}
