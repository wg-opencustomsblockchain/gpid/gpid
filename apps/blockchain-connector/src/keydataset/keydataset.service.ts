/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DirectSecp256k1HdWallet } from '@cosmjs/proto-signing';
import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import {
  CreateKeyDataSetDto,
  FindKeyDataSetDto,
  HashTokenTypes,
  HistoryMqPattern,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import {
  MsgCreateKeyDataSet,
  MsgFetchAllToken,
  MsgFetchAllTokenResponse,
  MsgFetchKeyDataSet,
  MsgFetchProcessResponse,
  MsgFetchTokenHistory,
  MsgFetchTokenHistoryResponse,
  MsgUpdateKeyDataSet,
} from '@rhenus/generated';
import { TxMessages } from '../tendermint/service/tx-messages';
import { SystemMessage } from '@rhenus/api-interfaces';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { generateGoodsPassportId } from './util';
import { ClientProxy } from '@nestjs/microservices';
import { firstValueFrom } from 'rxjs';

/**
 * Service class to pass requests to the corresponding chain endpoints.
 * Our Blockchain API delivers a full event history alongside each Token to minimize API calls.
 */
@Injectable()
export class KeyDataSetService {
  /** Creates a Service instance. Injects a Tendermint Service. */
  constructor(
    private tendermintService: TendermintService,
    @Inject('HISTORY_SERVICE') private historyService: ClientProxy
  ) {}

  /** Default Mnemonic to sign transactions with.*/
  private defaultMnemonic = process.env.WALLET_ADDRESS_MNENMONIC;

  /** Business Logic Type URL. Used for write transactions.*/
  // ToDo: Rename "HashTokenTypes" to a better name
  private businessLogic = HashTokenTypes.TENDERMINT_TYPE_URL_BUSINESSLOGIC;

  private parseProcessDto(dto: MsgFetchProcessResponse): ProcessDTO {
    return {
      processId: dto.processId,
      documents: dto.documents,
      keydataset: dto.keyData.map((kd) => {
        const out: KeyDataSetDto = JSON.parse(kd.data);
        out.creator = kd.creator;
        out.goodsPassportId = kd.goodsPassportId;
        out.timestamp = kd.timestamp;

        return out;
      }),
    };
  }

  /**
   * Finds the related ProcessDTO by its identifying KeyDataSetDto.
   * @param findKeyDataSetDto The findKeyDataSetDto includes the goodsPassportId to search for.
   * @returns The complete ProcessDTO, which belongs to the goodsPassportId. Including the KeyDataSet
   */
  public async findKeyDataSet(
    findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<ProcessDTO> {
    const address = await this.getCosmosAddress(findKeyDataSetDto.mnemonic);
    const msg: MsgFetchKeyDataSet = {
      creator: address,
      id: findKeyDataSetDto.goodsPassportId,
      timestamp: new Date().toString() + Math.random(),
    };

    const result: MsgFetchProcessResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          findKeyDataSetDto.mnemonic,
          [msg],
          TxMessages.MsgFetchKeyDataSet,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchProcessResponse}`
      );

    if (!result) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);
    return this.parseProcessDto(result);
  }

  // --- Transactions ---

  /**
   * Writes a KeyDataSet to the chain. If a processId is given, it is stored in the same segment.
   * @param createKeyDataSetDto The KeyDataSet to store together with the processId and mnemonic.
   * @returns The complete ProcessDTO, which belongs to the processId. Or a new one if the processId was empty.
   */
  public async createKeyDataSet(
    createKeyDataSetDto: CreateKeyDataSetDto
  ): Promise<ProcessDTO> {
    const address = await this.getCosmosAddress(createKeyDataSetDto.mnemonic);
    const msg: MsgCreateKeyDataSet = {
      creator: address,
      processId: createKeyDataSetDto.processId,
      data: JSON.stringify(createKeyDataSetDto.keyDataSetDto),
      goodsPassportId: generateGoodsPassportId(
        createKeyDataSetDto.keyDataSetDto.header.seller.identification
          .identificationNumber,
        createKeyDataSetDto.keyDataSetDto.header.invoice.invoiceNumber
      ),
    };

    const result: MsgFetchProcessResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          createKeyDataSetDto.mnemonic,
          [msg],
          TxMessages.MsgCreateKeyDataSet,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchProcessResponse}`
      );
    if (result == undefined)
      throw new BadRequestException(SystemMessage.ERROR_SAVE);

    const processDto = this.parseProcessDto(result);

    this.historyService
      .send(`${HistoryMqPattern.PREFIX}${HistoryMqPattern.GENERATE_HISTORY}`, {
        token: processDto,
      })
      .subscribe();

    return processDto;
  }

  public async updateKeyDataSet(
    updateKeyDataSetDto: UpdateKeyDataSetDto
  ): Promise<ProcessDTO> {
    const address = await this.getCosmosAddress(updateKeyDataSetDto.mnemonic);

    const msg: MsgUpdateKeyDataSet = {
      creator: address,
      id: updateKeyDataSetDto.goodsPassportId,
      data: JSON.stringify(updateKeyDataSetDto.keyDataSetDto),
    };

    const response = await this.tendermintService.buildSignAndBroadcast(
      address,
      updateKeyDataSetDto.mnemonic,
      [msg],
      TxMessages.MsgUpdateKeyDataSet,
      this.businessLogic
    );
    const result = await this.tendermintService.decodeResult(
      response,
      `${this.businessLogic}.${TxMessages.MsgFetchProcessResponse}`
    );
    if (result == undefined)
      throw new BadRequestException(SystemMessage.ERROR_SAVE);

    const processDto = this.parseProcessDto(result);

    await firstValueFrom(
      this.historyService.send(
        `${HistoryMqPattern.PREFIX}${HistoryMqPattern.GENERATE_HISTORY}`,
        {
          token: processDto,
        }
      )
    );

    return processDto;
  }

  /**
   * Finds the history of the KeyDataSet referenced by its identifying KeyDataSetDto (including goodspassportid).
   * @param findKeyDataSetDto The findKeyDataSetDto includes the goodsPassportId to search for.
   * @returns An Array of KeyDataSets that contains the history of the KeyDataSet, referenced by the goodsPassportId.
   */
  public async findKeyDataSetHistory(
    findKeyDataSetDto: FindKeyDataSetDto
  ): Promise<KeyDataSetDto[]> {
    const address = await this.getCosmosAddress(findKeyDataSetDto.mnemonic);
    const msg: MsgFetchTokenHistory = {
      creator: address,
      id: findKeyDataSetDto.goodsPassportId,
    };

    const result: MsgFetchTokenHistoryResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          findKeyDataSetDto.mnemonic,
          [msg],
          TxMessages.MsgFetchTokenHistory,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchTokenHistoryResponse}`
      );

    if (!result) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);

    const keyDataSetDtos: KeyDataSetDto[] = [];

    result.TokenHistory.history.forEach((t) => {
      if (!t.info?.data) return;
      const dto: KeyDataSetDto = JSON.parse(t.info.data);
      dto.creator = t.creator;
      dto.goodsPassportId = t.id;
      dto.timestamp = t.timestamp;
      keyDataSetDtos.push(dto);
    });

    return keyDataSetDtos;
  }

  /**
   * Finds all KeyDataSets in the blockchain.
   * @param mnemonic The mnemonic of the user, who calls this function.
   * @returns An Array of KeyDataSets of the blockchain.
   */
  public async findAllKeyDataSets(mnemonic: string): Promise<KeyDataSetDto[]> {
    const address = await this.getCosmosAddress(mnemonic);
    const msg: MsgFetchAllToken = {
      creator: address,
    };

    const result: MsgFetchAllTokenResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          mnemonic,
          [msg],
          TxMessages.MsgFetchAllToken,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchAllTokenResponse}`
      );

    if (!result) throw new NotFoundException(SystemMessage.ERROR_NOT_FOUND);

    const out: KeyDataSetDto[] = result.Token.filter(
      (t) => t.tokenType == 'KeyDataSet'
    ).map((t) => {
      const out: KeyDataSetDto = JSON.parse(t.info.data);
      out.creator = t.creator;
      out.goodsPassportId = t.id;
      out.timestamp = t.timestamp;
      return out;
    });

    return out;
  }

  private async getCosmosAddress(mnemonic: string): Promise<string> {
    const [creatorAddress] = await (
      await DirectSecp256k1HdWallet.fromMnemonic(
        mnemonic,
        undefined,
        process.env.WALLET_ADDRESS_PREFIX
      )
    ).getAccounts();

    return creatorAddress.address;
  }

  public async getTokenHistory(
    id: string,
    mnemonic: string
  ): Promise<MsgFetchTokenHistoryResponse> {
    const address = await this.getCosmosAddress(mnemonic);
    const msg: MsgFetchTokenHistory = {
      creator: address,
      id,
    };

    const result: MsgFetchTokenHistoryResponse =
      await this.tendermintService.decodeResult(
        await this.tendermintService.buildSignAndBroadcast(
          address,
          mnemonic,
          [msg],
          TxMessages.MsgFetchTokenHistory,
          this.businessLogic
        ),
        `${this.businessLogic}.${TxMessages.MsgFetchTokenHistoryResponse}`
      );

    return result;
  }
}
