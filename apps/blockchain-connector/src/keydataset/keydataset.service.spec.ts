/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ClientProxy, ClientsModule, Transport } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { KeyDataSetService } from './keydataset.service';
import {
  CreateKeyDataSetDto,
  FindKeyDataSetDto,
  KeyDataSetDto,
  ProcessDTO,
  UpdateKeyDataSetDto,
} from '@rhenus/api-interfaces';
import { TendermintService } from '../tendermint/service/tendermint.service';
import { HttpModule } from '@nestjs/axios';
import {
  MsgFetchAllTokenResponse,
  MsgFetchKeyDataSetResponse,
  MsgFetchProcessResponse,
  MsgFetchTokenHistoryResponse,
} from '@rhenus/generated';
import { RMQBroker } from '../shared/mqttBroker';
import { of } from 'rxjs';
import { DynamicModule } from '@nestjs/common';

describe('KeyDataSetService', () => {
  let keyDataSetService: KeyDataSetService;
  let tendermintService: TendermintService;
  let historyService: ClientProxy;

  const keyDataSetDto: KeyDataSetDto = {
    goodsPassportId: 'goodsPassportId',
    creator: 'baumann',
    timestamp: 'just now',
    header: {
      seller: {
        identification: {
          identificationNumber: 'referenceNumber',
        },
        name: 'seller',
        address: {
          streetAndNumber: 'line',
          city: 'city',
          postcode: 'postcode',
          country: 'country',
        },
      },
      buyer: {
        identification: {
          identificationNumber: 'referenceNumber',
        },
        name: 'buyer',
        address: {
          streetAndNumber: 'line',
          city: 'city',
          postcode: 'postcode',
          country: 'country',
        },
      },
      invoice: {
        invoiceNumber: 'invoiceNumber',
        invoiceDate: 'invoiceDate',
      },
    },
    goodsItem: [
      {
        sequenceNumber: 'sequenceNumber',
        commodityCode: {
          harmonizedSystemSubHeadingCode: 'DE',
        },
        descriptionOfGoods: 'goodsDescription',
        countryOfOrigin: 'countryOfOrigin',
        countryOfPreferentialOrigin: 'countryOfPreferentialOrigin',
        invoiceAmount: 'invoiceAmount',
        invoiceCurrency: 'invoiceCurrency',
        quantity: 'quantity',
        netMass: 'grossMass',
      },
    ],
  };

  const kds: MsgFetchKeyDataSetResponse = {
    creator: 'baumann',
    goodsPassportId: 'goodsPassportId',
    timestamp: 'just now',
    data: JSON.stringify(keyDataSetDto),
  };

  const msgFetchProcessResponse: MsgFetchProcessResponse = {
    processId: '1234',
    documents: [],
    keyData: [kds],
  };

  const msgFetchTokenHistoryResponse: MsgFetchTokenHistoryResponse = {
    TokenHistory: {
      creator: 'baumann',
      id: 'id',
      history: [
        {
          creator: 'baumann',
          id: 'id',
          tokenType: 'tokenType',
          timestamp: 'just now',
          changeMessage: 'changeMessage',
          valid: true,
          info: { data: JSON.stringify(keyDataSetDto) },
          segmentId: 'segmantId',
        },
      ],
    },
  };

  const msgFetchAllTokenResponse: MsgFetchAllTokenResponse = {
    Token: [
      {
        creator: 'baumann',
        id: 'id',
        tokenType: 'KeyDataSet',
        timestamp: 'just now',
        changeMessage: 'changeMessage',
        valid: true,
        info: { data: JSON.stringify(keyDataSetDto) },
        segmentId: 'segmantId',
      },
    ],
  };

  const mqBroker: DynamicModule = new RMQBroker().getRMQBroker();

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TendermintService, KeyDataSetService],
      imports: [mqBroker, HttpModule],
    }).compile();

    keyDataSetService = module.get<KeyDataSetService>(KeyDataSetService);
    tendermintService = module.get<TendermintService>(TendermintService);
    historyService = module.get<ClientProxy>('HISTORY_SERVICE');
  });

  it('should be defined', () => {
    expect(keyDataSetService).toBeDefined();
  });

  it('findKeyDataSet', async () => {
    const findKeyDataSetDto: FindKeyDataSetDto = {
      goodsPassportId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };
    const processDTO: ProcessDTO = {
      processId: '1234',
      documents: [],
      keydataset: [keyDataSetDto],
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(msgFetchProcessResponse));

    expect(
      await keyDataSetService.findKeyDataSet(findKeyDataSetDto)
    ).toStrictEqual(processDTO);
  });

  it('findKeyDataSet throws exception', async () => {
    const findKeyDataSetDto: FindKeyDataSetDto = {
      goodsPassportId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(undefined));

    const action = async () => {
      await keyDataSetService.findKeyDataSet(findKeyDataSetDto);
    };

    await expect(action()).rejects.toThrow();
  });

  it('createKeyDataSet', async () => {
    const createKeyDataSetDto: CreateKeyDataSetDto = {
      keyDataSetDto: keyDataSetDto,
      processId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };
    const processDTO: ProcessDTO = {
      processId: '1234',
      documents: [],
      keydataset: [keyDataSetDto],
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(msgFetchProcessResponse));
    jest.spyOn(historyService, 'send').mockReturnValue(of(true));

    expect(
      await keyDataSetService.createKeyDataSet(createKeyDataSetDto)
    ).toStrictEqual(processDTO);
  });

  it('createKeyDataSet throws exception', async () => {
    const createKeyDataSetDto: CreateKeyDataSetDto = {
      keyDataSetDto: keyDataSetDto,
      processId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(undefined));

    const action = async () => {
      await keyDataSetService.createKeyDataSet(createKeyDataSetDto);
    };

    await expect(action()).rejects.toThrow();
  });

  it('updateKeyDataSet', async () => {
    const updateKeyDataSetDto: UpdateKeyDataSetDto = {
      keyDataSetDto: keyDataSetDto,
      goodsPassportId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(msgFetchProcessResponse));
    jest.spyOn(historyService, 'send').mockReturnValue(of(true));

    const processDtoAnswer = await keyDataSetService.updateKeyDataSet(
      updateKeyDataSetDto
    );

    expect(processDtoAnswer.processId).toBe('1234');
  });

  it('updateKeyDataSet throws exception', async () => {
    const updateKeyDataSetDto: UpdateKeyDataSetDto = {
      keyDataSetDto: keyDataSetDto,
      goodsPassportId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(undefined));

    const action = async () => {
      await keyDataSetService.updateKeyDataSet(updateKeyDataSetDto);
    };

    await expect(action()).rejects.toThrow();
  });

  it('findAllKeyDataSets', async () => {
    const mnemonic =
      'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble';

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(msgFetchAllTokenResponse));

    const keyDataSetDtos = await keyDataSetService.findAllKeyDataSets(mnemonic);
    expect(keyDataSetDtos[0].creator).toStrictEqual('baumann');
  });

  it('findKeyDataSetHistory', async () => {
    const findKeyDataSetDto: FindKeyDataSetDto = {
      goodsPassportId: '1234',
      mnemonic:
        'marriage dust prize wolf damage eagle eyebrow belt before brass drastic host quantum shadow hundred loan trip more tragic recycle mind quiz rich noble',
    };

    jest.spyOn(tendermintService, 'signAndBroadcast').mockReturnValue(null);
    jest
      .spyOn(tendermintService, 'decodeResult')
      .mockReturnValue(Promise.resolve(msgFetchTokenHistoryResponse));

    const keyDataSetDtos = await keyDataSetService.findKeyDataSetHistory(
      findKeyDataSetDto
    );
    expect(keyDataSetDtos[0].creator).toEqual('baumann');
  });
});
