/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {generateGoodsPassportId} from "./util";

describe('generateGoodsPassportId', () => {
  it('should return the expected hash digest', () => {
    const identificationNumber = 'DE537400371045831';
    const invoiceNumber = '123123';
    const expectedHash = 'a4f5dfc77d562a8c';

    const result = generateGoodsPassportId(identificationNumber, invoiceNumber);

    expect(result)
      .toStrictEqual(expectedHash);
  });

  it('should throw an error when identificationNumber is empty', () => {
    const identificationNumber = '';
    const invoiceNumber = '123123';

    expect(() => generateGoodsPassportId(identificationNumber, invoiceNumber))
      .toThrow('identificationNumber is required');
  });

  it('should throw an error when invoiceNumber is empty', () => {
    const identificationNumber = 'DE537400371045831';
    const invoiceNumber = '';

    expect(() => generateGoodsPassportId(identificationNumber, invoiceNumber))
      .toThrow('invoiceNumber is required');
  });
});
