//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package cli

import (
	"io/ioutil"
	"os"
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/utils"
)

var _ = strconv.Itoa(0)

func CmdCreateTokenFromFile() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create-token-from-file [file]",
		Short: "Creates a new token from the content of a file",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			openedJsonFile, jsonErr := os.Open(args[0])
			if jsonErr != nil {
				return jsonErr
			}
			defer openedJsonFile.Close()

			dataAsBytes, readErr := ioutil.ReadAll(openedJsonFile)
			if readErr != nil {
				return readErr
			}

			goodsPassportId, creationErr := utils.GenerateGoodsPassportIdFromJson(dataAsBytes)
			if creationErr != nil {
				return creationErr
			}

			msg := types.NewMsgCreateKeyDataSet(
				clientCtx.GetFromAddress().String(),
				"",
				string(dataAsBytes),
				goodsPassportId,
			)
			if err := msg.ValidateBasic(); err != nil {
				return err
			}

			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
