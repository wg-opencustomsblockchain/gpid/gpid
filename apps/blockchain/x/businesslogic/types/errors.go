// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package types

// DONTCOVER

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// x/tokenManager module sentinel errors
// TODO-MP: consider distinguishing between our custom error codes and those provided by the SDK (e.g. prefix with '100')
var (
	ErrNoMapping                    = sdkerrors.Register(ModuleName, 1, "document token mapping does not exist")
	ErrSegmentNotFound              = sdkerrors.Register(ModuleName, 2, "segment not found")
	ErrInvalidDocumentType          = sdkerrors.Register(ModuleName, 3, "invalid document type")
	ErrInvalidDocumentId            = sdkerrors.Register(ModuleName, 4, "invalid document id")
	ErrGoodsPassportIdExists        = sdkerrors.Register(ModuleName, 5, "goodsPassportId already exists in this segment")
	ErrGoodsPassportIdDiffers       = sdkerrors.Register(ModuleName, 6, "goodsPassportId differs from identificationNumber and invoiceNumber")
	ErrGoodsPassportIdSegment       = sdkerrors.Register(ModuleName, 7, "goodsPassportId could not be found in any segment")
	ErrIdentificationNumberRequired = sdkerrors.Register(ModuleName, 8, "identificationNumber is required")
	ErrInvoiceNumberRequired        = sdkerrors.Register(ModuleName, 9, "invoiceNumber is required")
	ErrJsonInvalid                  = sdkerrors.Register(ModuleName, 10, "json is invalid")
)
