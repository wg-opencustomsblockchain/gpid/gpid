//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateKeyDataSet{}

func NewMsgCreateKeyDataSet(creator string, processId string, data string, goodsPassportId string) *MsgCreateKeyDataSet {
	return &MsgCreateKeyDataSet{
		Creator:         creator,
		ProcessId:       processId,
		Data:            data,
		GoodsPassportId: goodsPassportId,
	}
}

func (msg *MsgCreateKeyDataSet) Route() string {
	return RouterKey
}

func (msg *MsgCreateKeyDataSet) Type() string {
	return "CreateKeyDataSet"
}

func (msg *MsgCreateKeyDataSet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateKeyDataSet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateKeyDataSet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchProcess{}

func NewMsgFetchProcess() *MsgFetchProcess {
	return &MsgFetchProcess{}
}

func (msg *MsgFetchProcess) Route() string {
	return RouterKey
}

func (msg *MsgFetchProcess) Type() string {
	return "FetchProcess"
}

func (msg *MsgFetchProcess) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchProcess) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchProcess) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchKeyDataSet{}

func NewMsgFetchKeyDataSet() *MsgFetchKeyDataSet {
	return &MsgFetchKeyDataSet{}
}

func (msg *MsgFetchKeyDataSet) Route() string {
	return RouterKey
}

func (msg *MsgFetchKeyDataSet) Type() string {
	return "FetchKeyDataSet"
}

func (msg *MsgFetchKeyDataSet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchKeyDataSet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchKeyDataSet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateKeyDataSet{}

func NewMsgUpdateKeyDataSet() *MsgUpdateKeyDataSet {
	return &MsgUpdateKeyDataSet{}
}

func (msg *MsgUpdateKeyDataSet) Route() string {
	return RouterKey
}

func (msg *MsgUpdateKeyDataSet) Type() string {
	return "UpdateKeyDataSet"
}

func (msg *MsgUpdateKeyDataSet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateKeyDataSet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateKeyDataSet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchTokenHistory{}

func NewMsgFetchTokenHistory() *MsgFetchTokenHistory {
	return &MsgFetchTokenHistory{}
}

func (msg *MsgFetchTokenHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchTokenHistory) Type() string {
	return "FetchTokenHistory"
}

func (msg *MsgFetchTokenHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchTokenHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchTokenHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchAllToken{}

func NewMsgFetchAllToken() *MsgFetchAllToken {
	return &MsgFetchAllToken{}
}

func (msg *MsgFetchAllToken) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllToken) Type() string {
	return "FetchAllToken"
}

func (msg *MsgFetchAllToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
