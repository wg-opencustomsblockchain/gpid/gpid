//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package utils

import (
	"testing"
)

func Test_generateGoodsPassportIdFromJson(t *testing.T) {
	type args struct {
		json []byte
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "valid argument",
			args:    args{[]byte("{\"header\": {\"seller\": {\"identification\": {\"identificationNumber\": \"DE537400371045832\"}}, \"invoice\": {\"invoiceNumber\": \"123123\"}}}")},
			want:    "a4f5dfc77d562a8c",
			wantErr: false,
		},
		{
			name:    "empty argument",
			args:    args{[]byte("{}")},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenerateGoodsPassportIdFromJson(tt.args.json)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateGoodsPassportIdFromJson() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GenerateGoodsPassportIdFromJson() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_generateGoodsPassportId(t *testing.T) {
	type args struct {
		identificationNumber string
		invoiceNumber        string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "valid arguments",
			args:    args{"DE537400371045831", "123123"},
			want:    "a4f5dfc77d562a8c",
			wantErr: false,
		},
		{
			name:    "first argument empty",
			args:    args{"", "123123"},
			want:    "",
			wantErr: true,
		},
		{
			name:    "second argument empty",
			args:    args{"DE537400371045831", ""},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := generateGoodsPassportId(tt.args.identificationNumber, tt.args.invoiceNumber)
			if (err != nil) != tt.wantErr {
				t.Errorf("generateGoodsPassportId() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("generateGoodsPassportId() got = %v, want %v", got, tt.want)
			}
		})
	}
}
