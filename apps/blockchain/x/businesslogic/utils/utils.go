//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

type Data struct {
	Header struct {
		Seller struct {
			Identification struct {
				IdentificationNumber string `json:"identificationNumber"`
			} `json:"identification"`
		} `json:"seller"`
		Invoice struct {
			InvoiceNumber string `json:"invoiceNumber"`
		} `json:"invoice"`
	} `json:"header"`
}

func GenerateGoodsPassportIdFromJson(dataFromMsg []byte) (string, error) {
	var data Data
	jsonErr := json.Unmarshal(dataFromMsg, &data)
	if jsonErr != nil {
		return "", types.ErrJsonInvalid
	}

	identificationNumber := data.Header.Seller.Identification.IdentificationNumber
	invoiceNumber := data.Header.Invoice.InvoiceNumber

	goodsPassportId, generationError := generateGoodsPassportId(identificationNumber, invoiceNumber)
	if generationError != nil {
		return "", generationError
	}
	return goodsPassportId, nil
}

func generateGoodsPassportId(identificationNumber string, invoiceNumber string) (string, error) {
	if len(identificationNumber) == 0 {
		return "", types.ErrIdentificationNumberRequired
	}

	if len(invoiceNumber) == 0 {
		return "", types.ErrInvoiceNumberRequired
	}

	concatenatedNumbers := fmt.Sprintf("%s_%s", identificationNumber, invoiceNumber)
	hashValue := sha256.Sum256([]byte(concatenatedNumbers))
	hashValueAsString := hex.EncodeToString(hashValue[:])
	return hashValueAsString[0:16], nil
}
