//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	"strings"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func Test_msgServer_createToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "Successfull Tx Token Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"}},
			wantErr: false,
		},
		{
			name:    "Successfull Tx HashToken Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
			got, err := k.CreateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", got, "id not nil")
				return
			}
		})
	}
}

// TODO: add MsgIdResponse to updateToken Msg
func Test_msgServer_updateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgUpdateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:    "Successfull Tx Token Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgUpdateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:    "Successfull Tx HashToken Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgUpdateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"})

			if tt.args.msg.ModuleRef == "Token" {
				_, err := k.UpdateToken(tt.args.goCtx, &types.MsgUpdateToken{Creator: tt.args.msg.Creator, TokenRefId: strings.Split(id0.Id, "/")[1], TokenType: tt.args.msg.TokenType, ChangeMessage: tt.args.msg.ChangeMessage, SegmentId: tt.args.msg.SegmentId, ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.UpdateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.UpdateToken(tt.args.goCtx, &types.MsgUpdateToken{Creator: tt.args.msg.Creator, TokenRefId: strings.Split(id1.Id, "/")[1], TokenType: tt.args.msg.TokenType, ChangeMessage: tt.args.msg.ChangeMessage, SegmentId: tt.args.msg.SegmentId, ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.UpdateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}

		})
	}
}
func Test_msgServer_activateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgActivateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:    "Successfull Tx Token Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgActivateToken{Creator: creatorA, SegmentId: "0", ModuleRef: "Token"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:    "Successfull Tx HashToken Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgActivateToken{Creator: creatorA, SegmentId: "0", ModuleRef: "HashToken"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"})
			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{Creator: creatorA, Id: id0.Id})
			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{Creator: creatorA, Id: id1.Id})
			if tt.args.msg.ModuleRef == "Token" {
				_, err := k.ActivateToken(tt.args.goCtx, &types.MsgActivateToken{Creator: tt.args.msg.Creator, Id: id0.Id, SegmentId: "0", ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.ActivateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.ActivateToken(tt.args.goCtx, &types.MsgActivateToken{Creator: tt.args.msg.Creator, Id: id1.Id, SegmentId: "0", ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.ActivateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
		})
	}
}

func Test_msgServer_deactivateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgDeactivateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:    "Successfull Tx ",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgDeactivateToken{Creator: creatorA}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"})

			_, tokenErr := k.DeactivateToken(tt.args.goCtx, &types.MsgDeactivateToken{Creator: tt.args.msg.Creator, Id: id0.Id})
			if (tokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.DeactivateToken() error = %v, wantErr %v", tokenErr, tt.wantErr)
				return
			}
			_, hashTokenErr := k.DeactivateToken(tt.args.goCtx, &types.MsgDeactivateToken{Creator: tt.args.msg.Creator, Id: id1.Id})
			if (hashTokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.DeactivateToken() error = %v, wantErr %v", hashTokenErr, tt.wantErr)
				return
			}

		})
	}
}

func Test_msgServer_moveTokenToWallet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgMoveTokenToWallet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:    "Successfull Tx",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgMoveTokenToWallet{Creator: creatorA, SourceSegmentId: "0", TargetSegmentId: "1"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet1"))
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet2"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "Token"})
			// id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "0", ModuleRef: "HashToken"})

			_, tokenErr := k.MoveTokenToWallet(tt.args.goCtx, &types.MsgMoveTokenToWallet{Creator: tt.args.msg.Creator, TokenRefId: strings.Split(id0.Id, "/")[1], SourceSegmentId: tt.args.msg.SourceSegmentId, TargetSegmentId: tt.args.msg.TargetSegmentId})
			if (tokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.MoveTokenToWallet() error = %v, wantErr %v", tokenErr, tt.wantErr)
				return
			}
		})
	}
}

func Test_msgServer_cloneToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorA := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCloneToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:    "Successfull Tx Token Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgCloneToken{Creator: creatorA, WalletId: "0", ModuleRef: "Token"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:    "Successfull Tx HashToken Module",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgCloneToken{Creator: creatorA, WalletId: "0", ModuleRef: "HashToken"}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet1"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "1", ModuleRef: "Token"})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{Creator: creatorA, TokenType: "type", ChangeMessage: "notChanged", SegmentId: "1", ModuleRef: "HashToken"})

			if tt.args.msg.ModuleRef == "Token" {
				_, err := k.CloneToken(tt.args.goCtx, &types.MsgCloneToken{Creator: tt.args.msg.Creator, TokenId: id0.Id, WalletId: tt.args.msg.WalletId, ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.CloneToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.CloneToken(tt.args.goCtx, &types.MsgCloneToken{Creator: tt.args.msg.Creator, TokenId: id1.Id, WalletId: tt.args.msg.WalletId, ModuleRef: tt.args.msg.ModuleRef})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.CloneToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}

		})
	}
}
