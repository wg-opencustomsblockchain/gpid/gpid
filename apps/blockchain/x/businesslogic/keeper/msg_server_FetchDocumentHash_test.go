//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func Test_msgServer_FetchDocumentHash(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	creatorB := "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchDocumentHash
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.QueryGetDocumentHashResponse
		wantErr bool
	}{
		{
			name:    "Tx Permission denied",
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchDocumentHash{Creator: creatorB}},
			want:    &types.QueryGetDocumentHashResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			_, err := k.FetchDocumentHash(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchDocumentHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
