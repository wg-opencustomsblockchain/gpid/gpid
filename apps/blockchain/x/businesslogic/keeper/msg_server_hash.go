//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	"encoding/json"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"math/rand"
	"sort"
	"strings"

	//auhtorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	HashTokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

const (
	IdAlphabet       = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	DocumentPrefix   = ""
	DocumentIdLength = 10
	SegmentPrefix    = "P_"
	SegmentIdLength  = 10

	WalletId = "0"
)

func CreateRandom(alphabet string, length int, prefix string) string {
	out := prefix

	for i := 0; i < length; i++ {
		out = out + string(alphabet[rand.Intn(len(alphabet))-1])
	}

	return out
}

func (k msgServer) StoreDocumentHash(goCtx context.Context, msg *types.MsgCreateHashToken) (*types.MsgFetchProcessResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	//// authorization
	//authorizationKeeper := k.authorizationKeeper
	//check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	//if err != nil || !check {
	//	return &types.MsgIdResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	//}

	walletResponse, errWallet := k.walletKeeper.FetchWallet(goCtx, walletTypes.NewMsgFetchGetWallet(msg.Creator, WalletId))
	if errWallet != nil {
		return nil, errWallet
	}
	segments := walletResponse.Wallet.SegmentIds
	sort.Strings(segments)

	var segmentId string
	for {
		segmentId = CreateRandom(IdAlphabet, SegmentIdLength, SegmentPrefix)
		idx := sort.SearchStrings(segments, segmentId)
		if idx >= len(segments) || segments[idx] != segmentId {
			break
		}
	}

	_, errSegment := k.walletKeeper.CreateSegmentWithId(goCtx, walletTypes.NewMsgCreateSegmentWithId(msg.Creator, segmentId, "", "", WalletId))
	if errSegment != nil {
		return nil, errSegment
	}

	var documentId string
	for {
		documentId = CreateRandom(IdAlphabet, DocumentIdLength, DocumentPrefix)
		_, exist := k.GetDocumentTokenMapper(ctx, documentId)
		if !exist {
			break
		}
	}

	// Create new TokenRef
	//tokenRefId := uuid.New().String()
	//tokenId := WalletId + "/" + tokenRefId
	NewTokenCp := types.TokenCopies{
		Creator: msg.Creator,
		Index:   documentId,
		Tokens:  []string{documentId},
	}
	k.SetTokenCopies(ctx, NewTokenCp)

	k.CreateDocumentTokenMapper(goCtx, &types.MsgCreateDocumentTokenMapper{
		Creator:    msg.Creator,
		DocumentId: documentId,
		TokenId:    documentId,
	})
	k.walletKeeper.CreateTokenRef(goCtx, walletTypes.NewMsgCreateTokenRef(msg.Creator, documentId, HashTokenTypes.ModuleName, segmentId))

	info := types.HashTokenMetadata{
		DocumentType: msg.DocumentType,
	}
	infoJson, jsonError := json.Marshal(info)
	if jsonError != nil {
		return nil, jsonError
	}

	msgCreateToken := HashTokenTypes.NewMsgCreateToken(msg.Creator, documentId, HashTokenTypes.ModuleName, msg.ChangeMessage, segmentId)
	msgCreateToken.Document = documentId
	msgCreateToken.Hash = msg.Hash
	msgCreateToken.HashFunction = msg.HashFunction
	msgCreateToken.Metadata = string(infoJson)
	msgCreateToken.Timestamp = GetTimestamp()
	k.hashtokenKeeper.CreateToken(goCtx, msgCreateToken)

	k.SetDocumentSegmentMapping(ctx, documentId, segmentId)

	return k.FetchProcessInternal(goCtx, msg.Creator, segmentId)
}

func (k msgServer) AttachDocumentHash(goCtx context.Context, msg *types.MsgAttachHashToken) (*types.MsgFetchProcessResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	//// authorization
	//authorizationKeeper := k.authorizationKeeper
	//check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	//if err != nil || !check {
	//	return &types.MsgIdResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	//}

	var segmentId string
	if msg.TargetId[0:2] == SegmentPrefix {
		segmentId = msg.TargetId
	} else {
		split := strings.Split(msg.TargetId, "_")
		documentSegment, errDocSegment := k.GetDocumentSegment(ctx, split[0])
		if !errDocSegment {
			return nil, types.ErrSegmentNotFound
		}
		segmentId = documentSegment
	}

	walletResponse, errWallet := k.walletKeeper.FetchWallet(goCtx, walletTypes.NewMsgFetchGetWallet(msg.Creator, WalletId))
	if errWallet != nil {
		return nil, errWallet
	}
	segments := walletResponse.Wallet.SegmentIds
	sort.Strings(segments)

	idx := sort.SearchStrings(segments, segmentId)
	if idx >= len(segments) || segments[idx] != segmentId {
		return nil, types.ErrSegmentNotFound
	}

	var documentId string
	for {
		documentId = CreateRandom(IdAlphabet, DocumentIdLength, "")
		_, exist := k.GetDocumentTokenMapper(ctx, documentId)
		if !exist {
			break
		}
	}

	NewTokenCp := types.TokenCopies{
		Creator: msg.Creator,
		Index:   documentId,
		Tokens:  []string{documentId},
	}
	k.SetTokenCopies(ctx, NewTokenCp)

	k.CreateDocumentTokenMapper(goCtx, &types.MsgCreateDocumentTokenMapper{
		Creator:    msg.Creator,
		DocumentId: documentId,
		TokenId:    documentId,
	})
	k.walletKeeper.CreateTokenRef(goCtx, walletTypes.NewMsgCreateTokenRef(msg.Creator, documentId, HashTokenTypes.ModuleName, segmentId))

	info := types.HashTokenMetadata{
		DocumentType: msg.DocumentType,
	}
	infoJson, jsonError := json.Marshal(info)
	if jsonError != nil {
		return nil, jsonError
	}

	msgCreateToken := HashTokenTypes.NewMsgCreateToken(msg.Creator, documentId, HashTokenTypes.ModuleName, msg.ChangeMessage, segmentId)
	msgCreateToken.Document = documentId
	msgCreateToken.Hash = msg.Hash
	msgCreateToken.HashFunction = msg.HashFunction
	msgCreateToken.Metadata = string(infoJson)
	msgCreateToken.Timestamp = GetTimestamp()
	k.hashtokenKeeper.CreateToken(goCtx, msgCreateToken)

	k.SetDocumentSegmentMapping(ctx, documentId, segmentId)

	return k.FetchProcessInternal(goCtx, msg.Creator, segmentId)
}

func (k msgServer) FetchProcessInternal(goCtx context.Context, creator string, segmentId string) (*types.MsgFetchProcessResponse, error) {
	segment, errSegment := k.walletKeeper.FetchSegment(goCtx, &walletTypes.MsgFetchGetSegment{
		Creator: creator,
		Id:      segmentId,
	})
	if errSegment != nil {
		return nil, errSegment
	}

	out := &types.MsgFetchProcessResponse{
		ProcessId: segmentId,
	}

	for _, ref := range segment.Segment.TokenRefs {
		if ref.ModuleRef == HashTokenTypes.ModuleName {
			hashToken, errHashToken := k.FetchHashToken(goCtx /*WalletId + "/" + */, ref.Id)
			if errHashToken != nil {
				return nil, errHashToken
			}

			var info types.HashTokenMetadata
			errJson := json.Unmarshal([]byte(hashToken.Metadata), &info)
			if errJson != nil {
				return nil, errJson
			}

			out.Documents = append(out.Documents, &types.MsgFetchDocumentHashResponse{
				Id:           ref.Id,
				ProcessId:    segmentId,
				Creator:      hashToken.Creator,
				Hash:         hashToken.Hash,
				HashFunction: hashToken.HashFunction,
				DocumentType: info.DocumentType,
				Timestamp:    hashToken.Timestamp,
			})
		} else if ref.ModuleRef == KeyDataSetTokenType {
			token, errToken := k.tokenKeeper.FetchToken(goCtx, &tokenTypes.MsgFetchToken{
				Creator: creator,
				Id:      ref.Id,
			})
			if errToken != nil {
				return nil, errToken
			}
			out.KeyData = append(out.KeyData, &types.MsgFetchKeyDataSetResponse{
				GoodsPassportId: token.Token.Id,
				Data:            token.Token.Info.Data,
				Creator:         token.Token.Creator,
				Timestamp:       token.Token.Timestamp,
			})
		}
	}

	return out, nil
}

func (k msgServer) FetchDocumentHash(goCtx context.Context, msg *types.MsgFetchDocumentHash) (*types.MsgFetchDocumentHashResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	//// Permission check
	//authorizationKeeper := k.authorizationKeeper
	//check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicGetDocumentHash)
	//if err != nil || !check {
	//	return &types.QueryGetDocumentHashtyer Response{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicGetDocumentHash)
	//}

	res, err := k.DocumentHash(goCtx, &types.QueryGetDocumentHashRequest{
		Id: msg.Id,
	})
	if err != nil {
		return &types.MsgFetchDocumentHashResponse{}, types.ErrInvalidDocumentId
	}

	segmentId, segmentExists := k.GetDocumentSegment(ctx, msg.Id)
	if !segmentExists {
		return nil, types.ErrSegmentNotFound
	}

	var info types.HashTokenMetadata
	errJson := json.Unmarshal([]byte(res.Metadata), &info)
	if errJson != nil {
		return &types.MsgFetchDocumentHashResponse{}, errJson
	}

	if msg.DocumentType != info.DocumentType {
		return &types.MsgFetchDocumentHashResponse{}, types.ErrInvalidDocumentType
	}

	return &types.MsgFetchDocumentHashResponse{
		Id:           msg.Id,
		ProcessId:    segmentId,
		Creator:      res.Creator,
		Hash:         res.Hash,
		HashFunction: res.HashFunction,
		DocumentType: info.DocumentType,
		Timestamp:    res.Timestamp,
	}, nil
}

func (k msgServer) FetchProcess(goCtx context.Context, msg *types.MsgFetchProcess) (*types.MsgFetchProcessResponse, error) {
	return k.FetchProcessInternal(goCtx, msg.Creator, msg.Id)
}
