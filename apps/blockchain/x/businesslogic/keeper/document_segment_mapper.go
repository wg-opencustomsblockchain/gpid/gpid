// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

// SetDocumentSegmentMapping set a specific segment for a document
func (k Keeper) SetDocumentSegmentMapping(ctx sdk.Context, document string, segment string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DocumentSegmentMapperKey))
	b := k.cdc.MustMarshal(&types.DocumentSegmentMapper{
		DocumentId: document,
		SegmentId:  segment,
	})
	store.Set(types.KeyPrefix(document), b)
}

// GetDocumentSegment returns a document's segment
func (k Keeper) GetDocumentSegment(ctx sdk.Context, document string) (string, bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DocumentSegmentMapperKey))
	b := store.Get(types.KeyPrefix(document))
	if b == nil {
		return "", false
	}
	var val types.DocumentSegmentMapper
	k.cdc.MustUnmarshal(b, &val)
	return val.SegmentId, true
}
