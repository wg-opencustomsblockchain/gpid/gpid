//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) TokensBySegmentId(c context.Context, req *types.QueryAllTokenByIdRequest) (*types.QueryAllTokenResponse, error) {
	// Keepers
	walletKeeper := k.walletKeeper
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper

	segment, error := walletKeeper.Segment(c, &walletTypes.QueryGetSegmentRequest{Id: req.Id})
	if error != nil {
		return &types.QueryAllTokenResponse{}, error
	}

	// iterate through segment tokenRefs and get specific tokens
	var tokens []*tokenTypes.Token
	var hashTokens []*hashtokenTypes.Token
	for i := 0; i < len(segment.Segment.TokenRefs); i++ {
		switch segment.Segment.TokenRefs[i].ModuleRef {
		case tokenTypes.ModuleName:
			token, _ := tokenKeeper.Token(c, &tokenTypes.QueryGetTokenRequest{Id: segment.Segment.WalletId + "/" + segment.Segment.TokenRefs[i].Id})
			tokens = append(tokens, token.Token)
		case hashtokenTypes.ModuleName:
			token, _ := hashTokenKeeper.Token(c, &hashtokenTypes.QueryGetTokenRequest{Id: segment.Segment.WalletId + "/" + segment.Segment.TokenRefs[i].Id})
			hashTokens = append(hashTokens, token.Token)
		}
	}

	return &types.QueryAllTokenResponse{Token: tokens, HashTokens: hashTokens}, nil
}

func (k Keeper) TokensByWalletId(c context.Context, req *types.QueryAllTokenByIdRequest) (*types.QueryAllTokenResponse, error) {
	// Keepers
	walletKeeper := k.walletKeeper
	walletResponse, error := walletKeeper.Wallet(c, &walletTypes.QueryGetWalletRequest{Id: req.Id})

	// if wallet does not exists
	if error != nil {
		return &types.QueryAllTokenResponse{}, error
	}

	// iterate thorugh all segments in wallet and get TokensBySegmentId
	var tokens []*tokenTypes.Token
	var hashtokens []*hashtokenTypes.Token
	for i := 0; i < len(walletResponse.Wallet.SegmentIds); i++ {
		tokensBySegmendIdResponse, _ := k.TokensBySegmentId(c, &types.QueryAllTokenByIdRequest{Id: walletResponse.Wallet.SegmentIds[i]})
		tokens = append(tokens, tokensBySegmendIdResponse.Token...)
		hashtokens = append(hashtokens, tokensBySegmendIdResponse.HashTokens...)
	}

	return &types.QueryAllTokenResponse{Token: tokens, HashTokens: hashtokens}, nil
}
