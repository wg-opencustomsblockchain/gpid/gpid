//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwallettypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func createTestData(t *testing.T) (types.MsgServer, context.Context, *types.MsgCreateKeyDataSet) {
	keeper, ctx := setupKeeper(t)
	msgServer := NewMsgServerImpl(*keeper)
	goCtx := sdk.WrapSDKContext(ctx)

	keeper.walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "wallet"))
	msgCreateKeyDataSet := types.NewMsgCreateKeyDataSet(creatorA, "0", "hash-value", "10")
	return msgServer, goCtx, msgCreateKeyDataSet
}

func Test_CreateKeyDataSet_1KeyDataSet(t *testing.T) {
	msgServer, goCtx, msgCreateKeyDataSet := createTestData(t)

	keyDataSet, err := msgServer.CreateKeyDataSet(goCtx, msgCreateKeyDataSet)
	assert.Nil(t, err)
	assert.NotNil(t, keyDataSet)
	assert.Equal(t, msgCreateKeyDataSet.GoodsPassportId, keyDataSet.KeyData[0].GoodsPassportId)
	assert.Equal(t, msgCreateKeyDataSet.Data, keyDataSet.KeyData[0].Data)
	assert.Equal(t, msgCreateKeyDataSet.Creator, keyDataSet.KeyData[0].Creator)
}

func Test_CreateKeyDataSet_2KeyDataSets(t *testing.T) {
	msgServer, goCtx, msgCreateKeyDataSet := createTestData(t)

	keyDataSet1, err1 := msgServer.CreateKeyDataSet(goCtx, msgCreateKeyDataSet)
	assert.Nil(t, err1)
	assert.NotNil(t, keyDataSet1)
	assert.Equal(t, 1, len(keyDataSet1.KeyData))
	assert.Equal(t, msgCreateKeyDataSet.GoodsPassportId, keyDataSet1.KeyData[0].GoodsPassportId)
	assert.Equal(t, msgCreateKeyDataSet.Data, keyDataSet1.KeyData[0].Data)
	assert.Equal(t, msgCreateKeyDataSet.Creator, keyDataSet1.KeyData[0].Creator)

	msgCreateKeyDataSet.GoodsPassportId = "20"
	keyDataSet2, err2 := msgServer.CreateKeyDataSet(goCtx, msgCreateKeyDataSet)
	assert.Nil(t, err2)
	assert.NotNil(t, keyDataSet2)
	assert.Equal(t, 2, len(keyDataSet2.KeyData))
	assert.Equal(t, msgCreateKeyDataSet.GoodsPassportId, keyDataSet2.KeyData[1].GoodsPassportId)
	assert.Equal(t, msgCreateKeyDataSet.Data, keyDataSet2.KeyData[1].Data)
	assert.Equal(t, msgCreateKeyDataSet.Creator, keyDataSet2.KeyData[1].Creator)
}

func Test_CreateKeyDataSet_DuplicateId(t *testing.T) {
	msgServer, goCtx, msgCreateKeyDataSet := createTestData(t)

	keyDataSet1, err1 := msgServer.CreateKeyDataSet(goCtx, msgCreateKeyDataSet)
	assert.Nil(t, err1)
	assert.NotNil(t, keyDataSet1)

	keyDataSet2, err2 := msgServer.CreateKeyDataSet(goCtx, msgCreateKeyDataSet)
	assert.NotNil(t, err2)
	assert.Nil(t, keyDataSet2)
}
