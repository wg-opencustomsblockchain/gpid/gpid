//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func (k msgServer) FetchAllSegment(goCtx context.Context, msg *types.MsgFetchAllSegment) (*walletTypes.MsgFetchAllSegmentResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQuerySegmentAll)
	if err != nil || !check {
		return &walletTypes.MsgFetchAllSegmentResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQuerySegmentAll)
	}
	res, error := k.walletKeeper.FetchSegmentAll(goCtx, &walletTypes.MsgFetchAllSegment{
		Creator: msg.Creator,
	})

	if error != nil {
		return &walletTypes.MsgFetchAllSegmentResponse{}, error
	}

	return res, nil
}
