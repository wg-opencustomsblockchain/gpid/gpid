//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	"testing"

	tokenwallettypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func Test_msgServer_storeDocumentHash(t *testing.T) {
	keeper, ctx := setupKeeper(t)

	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateHashToken
	}

	k := msgServer{
		Keeper: *keeper,
	}

	const changeMessage = "ChangeMessage"
	const hash = "Hash"
	const hashFunction = "HashFunction"
	const documentType = "DocumentType"

	keeper.walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))

	hashA, errHashA := k.StoreDocumentHash(goCtx, &types.MsgCreateHashToken{
		Creator:       creatorA,
		ChangeMessage: changeMessage,
		Hash:          hash,
		HashFunction:  hashFunction,
		DocumentType:  documentType,
	})
	if errHashA != nil {
		t.Errorf("msgServer.StoreDocumentHash() error = %v", errHashA)
		return
	}
	if hashA == nil {
		t.Errorf("msgServer.StoreDocumentHash() hashA = %v", hashA)
		return
	}

	hashB, errHashB := k.AttachDocumentHash(goCtx, &types.MsgAttachHashToken{
		Creator:       creatorA,
		TargetId:      hashA.ProcessId,
		ChangeMessage: changeMessage,
		Hash:          hash,
		HashFunction:  hashFunction,
		DocumentType:  documentType,
	})
	if errHashB != nil {
		t.Errorf("msgServer.AttachDocumentHash() error = %v", errHashB)
		return
	}
	if hashB == nil {
		t.Errorf("msgServer.AttachDocumentHash() hashA = %v", hashB)
		return
	}

	hashC, errHashC := k.AttachDocumentHash(goCtx, &types.MsgAttachHashToken{
		Creator:       creatorA,
		TargetId:      hashA.ProcessId,
		ChangeMessage: changeMessage,
		Hash:          hash,
		HashFunction:  hashFunction,
		DocumentType:  documentType,
	})
	if errHashC != nil {
		t.Errorf("msgServer.AttachDocumentHash() error = %v", errHashC)
		return
	}
	if hashC == nil {
		t.Errorf("msgServer.AttachDocumentHash() hashA = %v", hashC)
		return
	}
}
