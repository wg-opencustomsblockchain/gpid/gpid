//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k msgServer) FetchAllSegmentHistory(goCtx context.Context, msg *types.MsgFetchAllSegmentHistory) (*walletTypes.MsgFetchAllSegmentHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQuerySegmentHistory)
	if err != nil || !check {
		return &walletTypes.MsgFetchAllSegmentHistoryResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQuerySegmentHistory)
	}
	res, error := k.walletKeeper.FetchSegmentHistoryAll(goCtx, &walletTypes.MsgFetchAllSegmentHistory{
		Creator: msg.Creator,
	})

	if error != nil {
		return &walletTypes.MsgFetchAllSegmentHistoryResponse{}, error
	}

	return res, nil
}
