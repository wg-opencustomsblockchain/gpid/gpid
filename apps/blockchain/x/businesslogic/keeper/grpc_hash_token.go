// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	HashTokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

func (k Keeper) DocumentHash(c context.Context, req *types.QueryGetDocumentHashRequest) (*types.QueryGetDocumentHashResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)

	mapping, exist := k.GetDocumentTokenMapper(ctx, req.Id)
	if !exist {
		return &types.QueryGetDocumentHashResponse{}, sdkerrors.Wrap(types.ErrNoMapping, "no mapping exist")
	}

	return k.FetchHashToken(c, mapping.TokenId)
}

func (k Keeper) FetchHashToken(goCtx context.Context, id string) (*types.QueryGetDocumentHashResponse, error) {
	response, err := k.hashtokenKeeper.FetchToken(goCtx, &HashTokenTypes.MsgFetchToken{Id: id})
	if err != nil {
		return &types.QueryGetDocumentHashResponse{}, err
	}

	var token HashTokenTypes.Token = *response.Token
	return &types.QueryGetDocumentHashResponse{
		Document:     token.Info.Document,
		Hash:         token.Info.Hash,
		HashFunction: token.Info.HashFunction,
		Metadata:     token.Info.Metadata,
		Creator:      token.Creator,
		Timestamp:    token.Timestamp,
	}, nil
}

func (k Keeper) DocumentHashHistory(c context.Context, req *types.QueryGetDocumentHashHistoryRequest) (*types.QueryGetDocumentHashHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(c)
	hashTokenKeeper := k.hashtokenKeeper

	id, exist := k.GetDocumentTokenMapper(ctx, req.Id)
	if !exist {
		return &types.QueryGetDocumentHashHistoryResponse{}, sdkerrors.Wrap(types.ErrNoMapping, "no mapping exists")
	}

	response, err := hashTokenKeeper.FetchToken(c, &HashTokenTypes.MsgFetchToken{Id: id.TokenId})
	if err != nil {
		return &types.QueryGetDocumentHashHistoryResponse{}, err
	}
	var token HashTokenTypes.Token = *response.Token
	out := types.QueryGetDocumentHashHistoryResponse{
		History: []*types.QueryGetDocumentHashResponse{
			{
				Hash:         token.Info.Hash,
				HashFunction: token.Info.HashFunction,
				Metadata:     token.Info.Metadata,
				Creator:      token.Creator,
				Timestamp:    token.Timestamp,
			},
		},
	}

	history, errHistory := hashTokenKeeper.FetchTokenHistory(c, &HashTokenTypes.MsgFetchTokenHistory{Id: id.TokenId})
	if errHistory != nil {
		return &types.QueryGetDocumentHashHistoryResponse{}, err
	}

	if history == nil {
		return &out, nil
	}

	for i := len(history.TokenHistory.History) - 1; i >= 0; i-- {
		token := history.TokenHistory.History[i]

		out.History = append(out.History, &types.QueryGetDocumentHashResponse{
			Hash:         token.Info.Hash,
			HashFunction: token.Info.HashFunction,
			Metadata:     token.Info.Metadata,
			Creator:      token.Creator,
			Timestamp:    token.Timestamp,
		})
	}

	return &out, nil
}
