//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

//func (k msgServer) FetchDocumentHash(goCtx context.Context, msg *types.MsgFetchDocumentHash) (*types.QueryGetDocumentHashResponse, error) {
//	ctx := sdk.UnwrapSDKContext(goCtx)
//
//	//// Permission check
//	//authorizationKeeper := k.authorizationKeeper
//	//check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicGetDocumentHash)
//	//if err != nil || !check {
//	//	return &types.QueryGetDocumentHashResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicGetDocumentHash)
//	//}
//
//	res, err := k.DocumentHash(goCtx, &types.QueryGetDocumentHashRequest{
//		Id: msg.Id,
//	})
//
//	if err != nil {
//		return &types.QueryGetDocumentHashResponse{}, err
//	} else {
//		return res, nil
//	}
//
//}
