//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func (k msgServer) CreateWalletWithId(goCtx context.Context, msg *types.MsgCreateWalletWithId) (*walletTypes.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, err := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletCreateWallet)
	if err != nil || !check {
		return &walletTypes.MsgIdResponse{}, sdkerrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletCreateWallet)
	}
	res, error := k.walletKeeper.CreateWalletWithId(goCtx, &walletTypes.MsgCreateWalletWithId{
		Creator: msg.Creator,
		Name:    msg.Name,
		Id:      msg.Id,
	})

	if error != nil {
		return &walletTypes.MsgIdResponse{}, error
	}

	return res, nil
}
