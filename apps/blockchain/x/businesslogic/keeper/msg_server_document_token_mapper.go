// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"context"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// No permission check here because its a function used internally
func (k msgServer) CreateDocumentTokenMapper(goCtx context.Context, msg *types.MsgCreateDocumentTokenMapper) (*types.MsgCreateDocumentTokenMapperResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Check if the value already exists
	_, isFound := k.GetDocumentTokenMapper(ctx, msg.DocumentId)
	if isFound {
		return nil, sdkerrors.Wrap(sdkerrors.ErrInvalidRequest, fmt.Sprintf("index %v already set", msg.DocumentId))
	}

	k.SetDocumentTokenMapper(
		ctx,
		types.DocumentTokenMapper{
			DocumentId: msg.DocumentId,
			Creator:    msg.Creator,
			TokenId:    msg.TokenId,
		},
	)
	return &types.MsgCreateDocumentTokenMapperResponse{}, nil
}

// No permission check here because its a function used internally
func (k msgServer) UpdateDocumentTokenMapper(goCtx context.Context, msg *types.MsgUpdateDocumentTokenMapper) (*types.MsgUpdateDocumentTokenMapperResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Check if the value exists
	valFound, isFound := k.GetDocumentTokenMapper(ctx, msg.DocumentId)
	if !isFound {
		return nil, sdkerrors.Wrap(sdkerrors.ErrKeyNotFound, fmt.Sprintf("index %v not set", msg.DocumentId))
	}

	// Checks if the the msg sender is the same as the current owner
	if msg.Creator != valFound.Creator {
		return nil, sdkerrors.Wrap(sdkerrors.ErrUnauthorized, "incorrect owner")
	}

	var documentTokenMapper = types.DocumentTokenMapper{
		DocumentId: msg.DocumentId,
		Creator:    msg.Creator,
		TokenId:    msg.TokenId,
	}

	k.SetDocumentTokenMapper(ctx, documentTokenMapper)

	return &types.MsgUpdateDocumentTokenMapperResponse{}, nil
}
