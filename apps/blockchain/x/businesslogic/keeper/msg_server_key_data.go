//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"
	"sort"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/utils"
	TokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	walletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

const (
	KeyDataSetPrefix    = "GP_"
	KeyDataSetLength    = 10
	KeyDataSetTokenType = "KeyDataSet"
)

func (k msgServer) CreateKeyDataSet(goCtx context.Context, msg *types.MsgCreateKeyDataSet) (*types.MsgFetchProcessResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// authorization
	// authorizationKeeper := k.authorizationKeeper
	// check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// if err != nil || !check {
	//	return &types.MsgIdResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// }

	documentId := msg.GoodsPassportId

	tokenExists := k.tokenKeeper.HasToken(ctx, documentId)
	if tokenExists {
		return &types.MsgFetchProcessResponse{}, types.ErrGoodsPassportIdExists
	}

	walletResponse, errWallet := k.walletKeeper.FetchWallet(goCtx, walletTypes.NewMsgFetchGetWallet(msg.Creator, WalletId))
	if errWallet != nil {
		return nil, errWallet
	}
	segments := walletResponse.Wallet.SegmentIds
	sort.Strings(segments)

	segmentId := msg.ProcessId
	if segmentId != "" {
		idx := sort.SearchStrings(segments, segmentId)
		if idx >= len(segments) || segments[idx] != segmentId {
			return nil, types.ErrSegmentNotFound
		}
	} else {
		for {
			segmentId = CreateRandom(IdAlphabet, SegmentIdLength, SegmentPrefix)
			idx := sort.SearchStrings(segments, segmentId)
			if idx >= len(segments) || segments[idx] != segmentId {
				_, errSegment := k.walletKeeper.CreateSegmentWithId(goCtx, walletTypes.NewMsgCreateSegmentWithId(msg.Creator, segmentId, "", "", WalletId))
				if errSegment != nil {
					return nil, errSegment
				}

				break
			}
		}
	}

	NewTokenCp := types.TokenCopies{
		Creator: msg.Creator,
		Index:   documentId,
		Tokens:  []string{documentId},
	}
	k.SetTokenCopies(ctx, NewTokenCp)

	k.CreateDocumentTokenMapper(goCtx, &types.MsgCreateDocumentTokenMapper{
		Creator:    msg.Creator,
		DocumentId: documentId,
		TokenId:    documentId,
	})
	k.walletKeeper.CreateTokenRef(goCtx, walletTypes.NewMsgCreateTokenRef(msg.Creator, documentId, KeyDataSetTokenType, segmentId))

	msgCreateToken := TokenTypes.NewMsgCreateToken(msg.Creator, documentId, KeyDataSetTokenType, "Created", segmentId)
	msgCreateToken.Timestamp = GetTimestamp()
	k.tokenKeeper.CreateToken(goCtx, msgCreateToken)
	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: msg.Creator,
		TokenId: documentId,
		Data:    msg.Data,
	})

	k.SetDocumentSegmentMapping(ctx, documentId, segmentId)

	return k.FetchProcessInternal(goCtx, msg.Creator, segmentId)
}

func (k msgServer) UpdateKeyDataSet(goCtx context.Context, msg *types.MsgUpdateKeyDataSet) (*types.MsgFetchProcessResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// // authorization
	// authorizationKeeper := k.authorizationKeeper
	// check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// if err != nil || !check {
	//	return &types.MsgIdResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// }

	goodsPassportId, err := utils.GenerateGoodsPassportIdFromJson([]byte(msg.Data))
	if err != nil {
		return nil, err
	}
	if goodsPassportId != msg.Id {
		return nil, types.ErrGoodsPassportIdDiffers
	}

	mapping, exist := k.GetDocumentTokenMapper(ctx, msg.Id)
	if !exist {
		return nil, types.ErrInvalidDocumentId
	}

	segmentId, segmentExists := k.GetDocumentSegment(ctx, msg.Id)
	if !segmentExists {
		return nil, types.ErrGoodsPassportIdSegment
	}

	k.tokenKeeper.UpdateTokenInformation(goCtx, &TokenTypes.MsgUpdateTokenInformation{
		Creator: msg.Creator,
		TokenId: mapping.TokenId,
		Data:    msg.Data,
	})

	return k.FetchProcessInternal(goCtx, msg.Creator, segmentId)
}

func (k msgServer) FetchKeyDataSet(goCtx context.Context, msg *types.MsgFetchKeyDataSet) (*types.MsgFetchProcessResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// // authorization
	// authorizationKeeper := k.authorizationKeeper
	// check, err := authorizationKeeper.HasRole(ctx, msg.Creator, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// if err != nil || !check {
	//	return &types.MsgIdResponse{}, sdkerrors.Wrap(auhtorizationTypes.ErrNoPermission, auhtorizationTypes.BusinessLogicStoreDocumentHash)
	// }

	fmt.Printf("### SegmentId=%s\n", msg.Id)
	segmentId, exists := k.GetDocumentSegment(ctx, msg.Id)
	if !exists {
		return nil, types.ErrGoodsPassportIdSegment
	}

	return k.FetchProcessInternal(goCtx, msg.Creator, segmentId)
}

func (k msgServer) FetchAllToken(goCtx context.Context, msg *types.MsgFetchAllToken) (*types.MsgFetchAllTokenResponse, error) {
	response, error := k.tokenKeeper.FetchAllToken(goCtx, &TokenTypes.MsgFetchAllToken{
		Creator: msg.Creator,
	})
	if error != nil {
		return nil, error
	}

	return &types.MsgFetchAllTokenResponse{
		Token: response.Token,
	}, nil
}

func (k msgServer) FetchTokenHistory(goCtx context.Context, msg *types.MsgFetchTokenHistory) (*types.MsgFetchTokenHistoryResponse, error) {
	response, error := k.tokenKeeper.FetchTokenHistory(goCtx, &TokenTypes.MsgFetchTokenHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if error != nil {
		return nil, error
	}

	return &types.MsgFetchTokenHistoryResponse{
		TokenHistory: response.TokenHistory,
	}, nil
}
