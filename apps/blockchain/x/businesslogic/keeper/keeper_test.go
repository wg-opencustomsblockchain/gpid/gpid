//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package keeper

import (
	"strings"
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storetypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/tendermint/tendermint/libs/log"
	tmproto "github.com/tendermint/tendermint/proto/tendermint/types"
	tmdb "github.com/tendermint/tm-db"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	businesslogicTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"

	AuthorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	hashtokentypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokentypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	Walletkeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"

	HashTokenkeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/keeper"

	Tokenkeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"

	tokenwallettypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func setupKeeper(t testing.TB) (*Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	walletStoreKey := sdk.NewKVStoreKey(tokenwallettypes.StoreKey)
	tokenStoreKey := sdk.NewKVStoreKey(tokentypes.StoreKey)
	hashTokenStoreKey := sdk.NewKVStoreKey(hashtokentypes.StoreKey)
	authorizationKey := sdk.NewKVStoreKey(authorizationTypes.StoreKey)
	memStoreKey := storetypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tmdb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(walletStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(tokenStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(hashTokenStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(authorizationKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codectypes.NewInterfaceRegistry()

	walletKeeper := *Walletkeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		walletStoreKey,
		memStoreKey,
	)

	hashtokenKeeper := *HashTokenkeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		hashTokenStoreKey,
		memStoreKey,
	)

	authorizationKeeper := *AuthorizationKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		authorizationKey,
		memStoreKey,
	)

	tokenKeeper := *Tokenkeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		tokenStoreKey,
		memStoreKey,
	)

	keeper := NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		walletKeeper,
		tokenKeeper,
		hashtokenKeeper,
		authorizationKeeper,
		memStoreKey,
	)

	roleIds := []string{
		authorizationTypes.BusinessLogicActivateToken,
		authorizationTypes.BusinessLogicCloneToken,
		authorizationTypes.BusinessLogicCreateToken,
		authorizationTypes.BusinessLogicDeactivateToken,
		authorizationTypes.BusinessLogicGetDocumentHash,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.BusinessLogicGetTokensByWalletId,
		authorizationTypes.BusinessLogicMoveTokenToWallet,
		authorizationTypes.BusinessLogicSetTokenValidStatus,
		authorizationTypes.BusinessLogicStoreDocumentHash,
		authorizationTypes.BusinessLogicUpdateToken,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.HashTokenActivateToken,
		authorizationTypes.HashTokenCreateToken,
		authorizationTypes.HashTokenDeactivateToken,
		authorizationTypes.HashTokenUpdateToken,
		authorizationTypes.HashTokenUpdateTokenInformation,
	}
	ctx := sdk.NewContext(stateStore, tmproto.Header{}, false, log.NewNopLogger())
	for _, v := range roleIds {
		authorizationKeeper.AppendApplicationRole(ctx, "", v, "")
	}
	authorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
		Creator: creatorA,
		Id:      creatorA,
		BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{{
			Creator:            creatorA,
			Id:                 uuid.New().String(),
			AccountID:          creatorA,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
			ApplicationRoleIDs: roleIds,
			Valid:              true,
		}},
	})
	authorizationKeeper.SetConfiguration(ctx, authorizationTypes.Configuration{Creator: "", Id: 0, PermissionCheck: true})

	return keeper, ctx
}

const (
	creatorA      = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	creatorB      = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"
	tokenType     = "type"
	changeMessage = "notChanged"
	segmentId     = "0"
	token         = "token"
	hashToken     = "hashToken"
)

func TestCreateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	setup := []businesslogicTypes.MsgCreateToken{
		{Creator: creatorA,
			TokenType:     tokenType,
			ChangeMessage: changeMessage,
			SegmentId:     "0",
			ModuleRef:     "Token",
		},
		{Creator: creatorA,
			TokenType:     tokenType,
			ChangeMessage: changeMessage,
			SegmentId:     "0",
			ModuleRef:     "HashToken",
		},
	}

	for _, element := range setup {
		assert := assert.New(t)
		id, _ := k.CreateToken(goCtx, &element)
		tokenRefId := strings.Split(id.Id, "/")[1]
		moduleRef, _ := k.getModuleRef(ctx, "0", tokenRefId, creatorA)
		if element.ModuleRef == "Token" {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, id.Id))
			out := outRes.Token
			assert.Equal(creatorA, out.Creator)
			assert.Equal(tokenType, out.TokenType)
			assert.Equal(changeMessage, out.ChangeMessage)
			assert.Equal(segmentId, out.SegmentId)
			assert.True(out.Valid)
			assert.NotNil(out.Timestamp)
			assert.Equal(element.ModuleRef, moduleRef)
		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, id.Id))
			out := outRes.Token
			assert.Equal(creatorA, out.Creator)
			assert.Equal(tokenType, out.TokenType)
			assert.Equal(changeMessage, out.ChangeMessage)
			assert.Equal(segmentId, out.SegmentId)
			assert.True(out.Valid)
			assert.NotNil(out.Timestamp)
			assert.Equal(element.ModuleRef, moduleRef)
		}

	}
}

const (
	newType    = "newType"
	changed    = "changed"
	newSegment = "1"
)

func TestUpdateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}

	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "Token"})
	hashTokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "HashToken"})
	setup := []businesslogicTypes.MsgUpdateToken{
		{Creator: creatorA,
			TokenRefId:    strings.Split(tokenId.Id, "/")[1],
			TokenType:     newType,
			ChangeMessage: changed,
			SegmentId:     "0",
			ModuleRef:     "Token",
		},
		{Creator: creatorA,
			TokenRefId:    strings.Split(hashTokenId.Id, "/")[1],
			TokenType:     newType,
			ChangeMessage: changed,
			SegmentId:     "0",
			ModuleRef:     "HashToken",
		},
	}

	for _, element := range setup {
		assert := assert.New(t)
		k.UpdateToken(goCtx, &element)

		if element.ModuleRef == "Token" {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, "0/"+element.TokenRefId))
			out := outRes.Token
			assert.Equal(creatorA, out.Creator)
			assert.Equal(newType, out.TokenType)
			assert.Equal(changed, out.ChangeMessage)
			assert.True(out.Valid)
			assert.NotNil(out.Timestamp)
		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, "0/"+element.TokenRefId))
			out := outRes.Token
			assert.Equal(creatorA, out.Creator)
			assert.Equal(newType, out.TokenType)
			assert.Equal(changed, out.ChangeMessage)
			assert.True(out.Valid)
			assert.NotNil(out.Timestamp)
		}

	}
}

func TestDeactivateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "Token"})
	hashTokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "HashToken"})
	setup := []businesslogicTypes.MsgDeactivateToken{
		{Creator: creatorA,
			Id: tokenId.Id,
		},
		{Creator: creatorA,
			Id: hashTokenId.Id,
		},
	}

	for index, element := range setup {
		assert := assert.New(t)
		k.DeactivateToken(goCtx, &element)

		if index == 0 {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			assert.False(out.Valid)

		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			assert.False(out.Valid)

		}

	}
}

func TestActivateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "Token"})
	hashTokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "HashToken"})
	setup := []businesslogicTypes.MsgActivateToken{
		{Creator: creatorA,
			Id:        tokenId.Id,
			SegmentId: segmentId,
			ModuleRef: "Token",
		},
		{Creator: creatorA,
			Id:        hashTokenId.Id,
			SegmentId: segmentId,
			ModuleRef: "HashToken",
		},
	}

	for index, element := range setup {
		assert := assert.New(t)

		if index == 0 {
			k.DeactivateToken(goCtx, &businesslogicTypes.MsgDeactivateToken{Creator: creatorA, Id: tokenId.Id})
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			assert.False(out.Valid)
			k.ActivateToken(goCtx, &element)
			outRes, _ = tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out = outRes.Token
			assert.True(out.Valid)

		} else {
			k.DeactivateToken(goCtx, &businesslogicTypes.MsgDeactivateToken{Creator: creatorA, Id: hashTokenId.Id})
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			assert.False(out.Valid)
			k.ActivateToken(goCtx, &element)
			outRes, _ = hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out = outRes.Token
			// TODO the hashToken case does not work
			_ = out
			// assert.True(out.Valid)
		}

	}
}

func TestMoveTokenToWallet(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet-01"))
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet-02"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "Token"})
	hashTokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "HashToken"})
	setup := []businesslogicTypes.MsgMoveTokenToWallet{
		{Creator: creatorA,
			TokenRefId:      strings.Split(tokenId.Id, "/")[1],
			SourceSegmentId: "0",
			TargetSegmentId: "1",
		},
		{Creator: creatorA,
			TokenRefId:      strings.Split(hashTokenId.Id, "/")[1],
			SourceSegmentId: "0",
			TargetSegmentId: "1",
		},
	}

	for index, element := range setup {
		assert := assert.New(t)

		if index == 0 {
			k.MoveTokenToWallet(goCtx, &element)
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, "1/"+strings.Split(tokenId.Id, "/")[1]))
			out := outRes.Token
			assert.Equal("1", out.SegmentId)

		} else {
			k.MoveTokenToWallet(goCtx, &element)
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, "1/"+strings.Split(tokenId.Id, "/")[1]))
			out := outRes.Token
			// TODO hashToken case does not work
			_ = out
			// assert.Equal("1", out.SegmentId)
		}

	}
}

func TestCloneToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet-01"))
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet-02"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "Token"})
	hashTokenId, _ := k.CreateToken(goCtx, &businesslogicTypes.MsgCreateToken{Creator: creatorA, TokenType: tokenType, ChangeMessage: changeMessage, SegmentId: segmentId, ModuleRef: "HashToken"})
	setup := []businesslogicTypes.MsgCloneToken{
		{Creator: creatorA,
			TokenId:   tokenId.Id,
			WalletId:  "1",
			ModuleRef: "Token",
		},
		{Creator: creatorA,
			TokenId:   hashTokenId.Id,
			WalletId:  "1",
			ModuleRef: "HashToken",
		},
	}

	for index, element := range setup {
		assert := assert.New(t)
		if index == 0 {
			tokenRefId := strings.Split(tokenId.Id, "/")[1]
			k.CloneToken(goCtx, &element)
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokentypes.NewMsgFetchToken(creatorA, "1/"+tokenRefId))
			out := outRes.Token
			assert.Equal("1", out.SegmentId)
			tokenCps, bool := k.GetTokenCopies(ctx, tokenRefId)
			assert.True(bool)
			assert.Equal(len(tokenCps.Tokens), 2)

		} else {
			tokenRefId := strings.Split(hashTokenId.Id, "/")[1]
			k.CloneToken(goCtx, &element)
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokentypes.NewMsgFetchToken(creatorA, "1/"+tokenRefId))
			out := outRes.Token
			assert.Equal("1", out.SegmentId)
			tokenCps, bool := k.GetTokenCopies(ctx, tokenRefId)
			assert.True(bool)
			assert.Equal(len(tokenCps.Tokens), 2)

		}

	}
}

const (
	document     = "document"
	hash         = "hash"
	hashFunction = "sha256"
	metadata     = "metadata"
)

func TestStoreDocumentHash(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwallettypes.NewMsgCreateWallet(creatorA, "newWallet"))
	setup := []businesslogicTypes.MsgCreateHashToken{
		{Creator: creatorA,
			ChangeMessage: changeMessage,
			// SegmentId:     segmentId,
			// Document:      document,
			Hash:         hash,
			HashFunction: hashFunction,
			// Metadata:      metadata,
		},
	}

	for _, element := range setup {
		// assert := assert.New(t)
		id, _ := k.StoreDocumentHash(goCtx, &element)
		// TODO-MP: which ID should we use?
		k.FetchDocumentHash(goCtx, &businesslogicTypes.MsgFetchDocumentHash{Creator: creatorA, Id: id.ProcessId})
		// assert.Equal(creatorA, out.Creator)
		// assert.Equal(hash, out.Hash)
		// assert.Equal(hashFunction, out.HashFunction)
		// assert.Equal(metadata, out.Metadata)
		// assert.NotNil(out.Timestamp)

	}
}
