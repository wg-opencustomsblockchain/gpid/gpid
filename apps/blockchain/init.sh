#!/bin/bash

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

# turn on bash's job control
set -m

# Start the primary process and put it in the background
TokenManagerd start --log_level "error" &
# Start the helper process
# the my_helper_process might need to know how to wait on the
# primary process to start before it does its work and returns
sleep 20s #Wait for tendermint demon start

# create new account
TokenManagerd keys list | grep user >/dev/null || printf '%s\n' "$USER_MNEMONIC" | TokenManagerd keys add user --recover || exit 1
yes | TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show user -a)" 1token

# create new account
TokenManagerd keys list | grep cct >/dev/null || printf '%s\n' "$CCT_MNEMONIC" | TokenManagerd keys add cct --recover || exit 1
yes | TokenManagerd tx bank send "$(TokenManagerd keys show alice -a)" "$(TokenManagerd keys show cct -a)" 1token

for file in "$(dirname $0)"/testfiles/*; do
  yes | TokenManagerd tx BusinessLogic create-token-from-file "$file" --gas 100000000 --from user
done

# now we bring the primary process back into the foreground
# and leave it there
fg %1
