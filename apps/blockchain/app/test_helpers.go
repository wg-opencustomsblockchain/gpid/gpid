//  Copyright Open Logistics Foundation
//
//  Licensed under the Open Logistics Foundation License 1.3.
//  For details on the licensing terms, see the LICENSE file.
//  SPDX-License-Identifier: OLFL-1.3
package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/tendermint/starport/starport/pkg/cosmoscmd"

	"github.com/tendermint/tendermint/libs/log"
	tmproto "github.com/tendermint/tendermint/proto/tendermint/types"
	dbm "github.com/tendermint/tm-db"

	AuthorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	AuthorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	HashTokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/keeper"
	HashTokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	TokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	TokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	WalletKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	WalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"

	sdk "github.com/cosmos/cosmos-sdk/types"
	abci "github.com/tendermint/tendermint/abci/types"
	tmtypes "github.com/tendermint/tendermint/types"
)

const (
	CreatorA = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	CreatorB = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"

	TokenCreator = "Example Token Creator"
	TokenId1     = "T0"
	TokenId2     = "T1"
	TokenType    = "Example Token Type"

	WalletId1  = "W0"
	WalletId2  = "W1"
	WalletName = "Example Wallet Name"

	SegmentId1  = "S0"
	SegmentId2  = "S1"
	SegmentName = "Example Segment Name"
	SegmentInfo = "Example Segment Info"

	Description  = "description"
	Timestamp    = "2000-04-01T08:59:07+00:00"
	Changed      = "changed"
	NotChanged   = "not changed"
	Document     = "document"
	Hash         = "hash"
	HashFunction = "sha256"
	Metadata     = "metadata"

	SuccessfulTx    = "Successful Tx"
	UnsuccessfulTx  = "Unsuccessful Tx"
	SegmentNotFound = "Segment not found"
	WalletNotFound  = "Wallet not found"
	EmptyRequest    = "Empty Request"
	ValidRequest    = "Valid Request"
)

var defaultConsensusParams = &abci.ConsensusParams{
	Block: &abci.BlockParams{
		MaxBytes: 200000,
		MaxGas:   2000000,
	},
	Evidence: &tmproto.EvidenceParams{
		MaxAgeNumBlocks: 302400,
		MaxAgeDuration:  504 * time.Hour,
	},
	Validator: &tmproto.ValidatorParams{
		PubKeyTypes: []string{
			tmtypes.ABCIPubKeyTypeEd25519,
		},
	},
}

// CreateTestInput returns a testable app and the sdk context
func CreateTestInput() (*App, sdk.Context) {

	app := Setup(false)
	ctx := app.BaseApp.NewContext(false, tmproto.Header{})

	appCodec := app.AppCodec()

	app.WalletKeeper = *WalletKeeper.NewKeeper(
		appCodec,
		app.GetKey(WalletTypes.StoreKey),
		app.GetMemKey(WalletTypes.MemStoreKey),
	)

	app.TokenKeeper = *TokenKeeper.NewKeeper(
		appCodec,
		app.GetKey(TokenTypes.StoreKey),
		app.GetMemKey(TokenTypes.MemStoreKey),
	)

	app.HashtokenKeeper = *HashTokenKeeper.NewKeeper(
		appCodec,
		app.GetKey(HashTokenTypes.StoreKey),
		app.GetMemKey(HashTokenTypes.MemStoreKey),
	)

	app.AuthorizationKeeper = *AuthorizationKeeper.NewKeeper(
		appCodec,
		app.GetKey(AuthorizationTypes.StoreKey),
		app.GetMemKey(AuthorizationTypes.MemStoreKey),
	)

	return &app, ctx
}

// Setup returns an initialiazed app
func Setup(isCheckTx bool) App {
	db := dbm.NewMemDB()
	app := NewApp(log.NewNopLogger(), db, nil, true, map[int64]bool{}, DefaultNodeHome, 5, cosmoscmd.MakeEncodingConfig(ModuleBasics), EmptyAppOptions{})
	if !isCheckTx {
		// init chain must be called to stop deliverState from being nil
		genesisState := NewDefaultGenesisState(app.appCodec)
		stateBytes, err := json.MarshalIndent(genesisState, "", " ")
		if err != nil {
			panic(err)
		}

		// Initialize the chain
		app.InitChain(
			abci.RequestInitChain{
				Validators:      []abci.ValidatorUpdate{},
				ConsensusParams: defaultConsensusParams,
				AppStateBytes:   stateBytes,
			},
		)

	}
	return *app
}

// TestAddr creates an address from a bech string
func TestAddr(addr string, bech string) (sdk.AccAddress, error) {
	res, err := sdk.AccAddressFromHex(addr)
	if err != nil {
		return nil, err
	}
	bechexpected := res.String()
	if bech != bechexpected {
		return nil, fmt.Errorf("bech encoding doesn't match reference")
	}

	bechres, err := sdk.AccAddressFromBech32(bech)
	if err != nil {
		return nil, err
	}
	if !bytes.Equal(bechres, res) {
		return nil, err
	}

	return res, nil
}

// createIncrementalAccounts creates a number of sdk accAddresses
func createIncrementalAccounts(accNum int) []sdk.AccAddress {
	var addresses []sdk.AccAddress
	var buffer bytes.Buffer

	for i := 100; i < (accNum + 100); i++ {
		numString := strconv.Itoa(i)
		buffer.WriteString("A58856F0FD53BF058B4909A21AEC019107BA6") // base address string

		buffer.WriteString(numString)
		res, _ := sdk.AccAddressFromHex(buffer.String())
		bech := res.String()
		addr, _ := TestAddr(buffer.String(), bech)

		addresses = append(addresses, addr)
		buffer.Reset()
	}
	return addresses
}

// EmptyAppOptions is a stub implementing AppOptions
type EmptyAppOptions struct{}

// Get implements AppOptions
func (ao EmptyAppOptions) Get(o string) interface{} {
	return nil
}
