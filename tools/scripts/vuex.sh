#!/bin/bash

#
# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3
#

pushd apps/blockchain && 
starport generate vuex && 
cp -r vue/src/store/generated/silicon-economy/base/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic ../../libs/generated/src/lib &&
find ../../libs/generated/src/lib -type f -name '*.ts' -exec sed -i 's/client = await SigningStargateClient.offline( wallet, { registry });//g' {} +
popd
