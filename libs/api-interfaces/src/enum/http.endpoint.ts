/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available HTTP Endpoints.
 */
export enum HTTPEndpoint {
  CREATOR_NAME = 'creator/:creatorid',
  CREATOR_PARAM_ID = ':creatorid',

  HASH_CONTROLLER = 'hash',
  HASH_VERIFY = ':document/verify',
  HASH_UPLOAD = '',
  HASH_ADD = ':document',
  
  HASH_PARAM_ID = ':document',
  
  KEYDATASET_CONTROLLER = 'keydataset',
  KEYDATASET_CREATE = '',
  KEYDATASET_FIND_ALL = '',
  KEYDATASET_HISTORY = ':goodspassportid/history',
  KEYDATASET_FIND = ':goodspassportid',
  KEYDATASET_UPDATE = ':goodspassportid',
  
  KEYDATASET_PARAM_ID = ':goodspassportid',
}
  