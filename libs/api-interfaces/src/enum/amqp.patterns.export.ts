/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Available AMQP Endpoints for this Module.
 */
export enum AMQPPatterns {
  PREFIX = 'v1/',
  TOKEN_CREATE = 'blockchain/create',
  TOKEN_FIND = 'blockchain/find',
  TOKEN_ADD = 'blockchain/add',
  TOKEN_HISTORY = 'blockchain/history',
  KEYDATASET_CREATE = 'blockchain/keydataset-create',
  KEYDATASET_FIND = 'blockchain/keydataset-find',
  KEYDATASET_HISTORY = 'blockchain/keydataset-history',
  KEYDATASET_UPDATE = 'blockchain/keydataset-update',
  KEYDATASET_ALL = 'blockchain/keydataset-all',
}

export enum HistoryMqPattern {
  PREFIX = 'v1/',
  GENERATE_HISTORY = 'generate/export',
  GET_HISTORY = 'get',
}
