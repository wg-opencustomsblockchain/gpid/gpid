/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Collection of the GPID API Information.
 * TENDERMINT_TYPE_URL: The base URL under which the API is served.
 * MODULE: Name of the corresponding Module in the Chain GO Code.
 * ENDPOINTS: The available Endpoints for the Module
 */
export enum HashTokenTypes {
  TENDERMINT_TYPE_URL_BUSINESSLOGIC = '/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic',
  MODULE = 'hashtoken',
  ENDPOINT_TOKEN = 'token',
}
