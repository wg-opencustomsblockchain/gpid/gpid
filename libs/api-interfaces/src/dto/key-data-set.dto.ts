/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface AddressDto {
  streetAndNumber: string;
  city: string;
  postcode: string;
  country: string;
}

export interface IdentificationDto {
  identificationNumber: string;
}

export interface BuyerSellerDto {
  identification: IdentificationDto;
  name: string;
  address: AddressDto;
}

export interface InvoiceDto {
  invoiceNumber: string;
  invoiceDate: string;
}

export interface HeaderDto {
  seller: BuyerSellerDto;
  buyer: BuyerSellerDto;
  invoice: InvoiceDto;
}

export interface CommodityCodeDto {
  harmonizedSystemSubHeadingCode: string;
}

export interface GoodsItemDto {
  sequenceNumber: string;
  commodityCode: CommodityCodeDto;
  descriptionOfGoods: string;
  countryOfOrigin: string;
  countryOfPreferentialOrigin: string;
  invoiceAmount: string;
  invoiceCurrency: string;
  quantity: string;
  netMass: string;
}

export interface KeyDataSetDto {
  goodsPassportId?: string;
  creator?: string;
  creatorName?: string;
  timestamp?: string;

  header: HeaderDto;
  goodsItem: GoodsItemDto[]
}
