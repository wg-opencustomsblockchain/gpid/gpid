/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {GoodsItemDto, HeaderDto, KeyDataSetDto} from "./key-data-set.dto";

export class KeyDataSet implements KeyDataSetDto {
  goodsItem!: GoodsItemDto[];
  header!: HeaderDto;

}
