/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { KeyDataSetDto } from './key-data-set.dto';

export interface CreateKeyDataSetDto {
  keyDataSetDto: KeyDataSetDto;
  processId: string;
  mnemonic?: string;
}
