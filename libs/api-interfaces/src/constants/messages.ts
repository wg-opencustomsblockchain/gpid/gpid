/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * List of System messages.
 * Includes both console output and API responses on error.
 */
// Remove the messages that we don't need and maybe update it a bit #CodeReview
export enum SystemMessage {
  APPLICATION_STARTUP = 'Automatically Generating a new Ewallet and a Corresponding Segment.',
  APPLICATION_STARTUP_FINISHED = 'Startup Successful.',
  WALLET_GENERATION = 'Generating a new wallet with the ID= ',
  SEGMENT_GENERATION = 'Generating a new Segment with the ID= ',
  APPLICATION_ADDRESS = 'Performing request to= ',
  APPLICATION_PAYLOAD = 'With the following Payload= ',
  APPLICATION_RESPONSE = 'Ewallet Response= ',

  CONFIG_UPDATE_WALLET = 'Updated the target Ewallet in Config to= ',
  CONFIG_UPDATE_SEGMENT = 'Updated the target Segment in Config to= ',

  ERROR_NOT_FOUND = 'No token found with that ID.',
  ERROR_NOT_FOUND_ALL = 'No token under that address.',
  ERROR_NOT_FOUND_UPDATE = "Id doesn't match with any recorded Token.",
  ERROR_SAVE = 'Saving failed. Your Input might be faulty.',
  INVALID_CREDENTIALS = 'There is an Error with your wallet. Check if your config matches the Tendermint instance.',
  ERROR_CANNOT_PROCESS = "Can't properly process the tendermint transaction: ",
}
