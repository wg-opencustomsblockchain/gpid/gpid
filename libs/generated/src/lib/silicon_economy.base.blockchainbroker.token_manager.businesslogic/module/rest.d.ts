export declare type BusinesslogicMsgCreateDocumentTokenMapperResponse = object;
export interface BusinesslogicMsgFetchDocumentHashResponse {
    id?: string;
    processId?: string;
    creator?: string;
    hash?: string;
    hashFunction?: string;
    documentType?: string;
    timestamp?: string;
}
export interface BusinesslogicMsgFetchProcessResponse {
    processId?: string;
    documents?: BusinesslogicMsgFetchDocumentHashResponse[];
}
export declare type BusinesslogicMsgUpdateDocumentTokenMapperResponse = object;
export interface BusinesslogicQueryAllTokenResponse {
    Token?: TokenManagertokenToken[];
    HashTokens?: TokenManagerhashtokenToken[];
}
export interface BusinesslogicQueryGetDocumentHashHistoryResponse {
    history?: BusinesslogicQueryGetDocumentHashResponse[];
}
export interface BusinesslogicQueryGetDocumentHashResponse {
    document?: string;
    hash?: string;
    hashFunction?: string;
    metadata?: string;
    creator?: string;
    timestamp?: string;
}
export interface ProtobufAny {
    "@type"?: string;
}
export interface RpcStatus {
    /** @format int32 */
    code?: number;
    message?: string;
    details?: ProtobufAny[];
}
export declare type TokenManagerbusinesslogicMsgEmptyResponse = object;
export interface TokenManagerbusinesslogicMsgIdResponse {
    id?: string;
}
export interface TokenManagerhashtokenInfo {
    document?: string;
    hash?: string;
    hashFunction?: string;
    metadata?: string;
}
export interface TokenManagerhashtokenToken {
    creator?: string;
    id?: string;
    tokenType?: string;
    timestamp?: string;
    changeMessage?: string;
    valid?: boolean;
    info?: TokenManagerhashtokenInfo;
    segmentId?: string;
}
export interface TokenManagertokenInfo {
    data?: string;
}
export interface TokenManagertokenToken {
    creator?: string;
    id?: string;
    tokenType?: string;
    timestamp?: string;
    changeMessage?: string;
    valid?: boolean;
    info?: TokenManagertokenInfo;
    segmentId?: string;
}
export declare type TokenManagertokenwalletMsgEmptyResponse = object;
export interface TokenManagertokenwalletMsgIdResponse {
    id?: string;
}
export interface TokenwalletMsgFetchAllSegmentHistoryResponse {
    SegmentHistory?: TokenwalletSegmentHistory[];
    /**
     * PageResponse is to be embedded in gRPC response messages where the
     * corresponding request message has used PageRequest.
     *
     *  message SomeResponse {
     *          repeated Bar results = 1;
     *          PageResponse page = 2;
     *  }
     */
    pagination?: V1Beta1PageResponse;
}
export interface TokenwalletMsgFetchAllSegmentResponse {
    Segment?: TokenwalletSegment[];
    /**
     * PageResponse is to be embedded in gRPC response messages where the
     * corresponding request message has used PageRequest.
     *
     *  message SomeResponse {
     *          repeated Bar results = 1;
     *          PageResponse page = 2;
     *  }
     */
    pagination?: V1Beta1PageResponse;
}
export interface TokenwalletMsgFetchAllTokenHistoryGlobalResponse {
    TokenHistoryGlobal?: TokenwalletTokenHistoryGlobal[];
    /**
     * PageResponse is to be embedded in gRPC response messages where the
     * corresponding request message has used PageRequest.
     *
     *  message SomeResponse {
     *          repeated Bar results = 1;
     *          PageResponse page = 2;
     *  }
     */
    pagination?: V1Beta1PageResponse;
}
export interface TokenwalletMsgFetchAllWalletHistoryResponse {
    WalletHistory?: TokenwalletWalletHistory[];
    /**
     * PageResponse is to be embedded in gRPC response messages where the
     * corresponding request message has used PageRequest.
     *
     *  message SomeResponse {
     *          repeated Bar results = 1;
     *          PageResponse page = 2;
     *  }
     */
    pagination?: V1Beta1PageResponse;
}
export interface TokenwalletMsgFetchAllWalletResponse {
    Wallet?: TokenwalletWallet[];
    /**
     * PageResponse is to be embedded in gRPC response messages where the
     * corresponding request message has used PageRequest.
     *
     *  message SomeResponse {
     *          repeated Bar results = 1;
     *          PageResponse page = 2;
     *  }
     */
    pagination?: V1Beta1PageResponse;
}
export interface TokenwalletMsgFetchGetSegmentHistoryResponse {
    SegmentHistory?: TokenwalletSegmentHistory;
}
export interface TokenwalletMsgFetchGetSegmentResponse {
    Segment?: TokenwalletSegment;
}
export interface TokenwalletMsgFetchGetTokenHistoryGlobalResponse {
    TokenHistoryGlobal?: TokenwalletTokenHistoryGlobal;
}
export interface TokenwalletMsgFetchGetWalletHistoryResponse {
    WalletHistory?: TokenwalletWalletHistory;
}
export interface TokenwalletMsgFetchGetWalletResponse {
    Wallet?: TokenwalletWallet;
}
export interface TokenwalletSegment {
    creator?: string;
    id?: string;
    name?: string;
    timestamp?: string;
    info?: string;
    walletId?: string;
    tokenRefs?: TokenwalletTokenRef[];
}
export interface TokenwalletSegmentHistory {
    creator?: string;
    id?: string;
    history?: TokenwalletSegment[];
}
export interface TokenwalletTokenHistoryGlobal {
    creator?: string;
    id?: string;
    walletId?: string[];
}
export interface TokenwalletTokenRef {
    id?: string;
    moduleRef?: string;
    valid?: boolean;
}
export interface TokenwalletWallet {
    creator?: string;
    id?: string;
    name?: string;
    timestamp?: string;
    segmentIds?: string[];
    walletAccounts?: TokenwalletWalletAccount[];
}
export interface TokenwalletWalletAccount {
    address?: string;
    active?: boolean;
}
export interface TokenwalletWalletHistory {
    creator?: string;
    id?: string;
    history?: TokenwalletWallet[];
}
/**
* message SomeRequest {
         Foo some_parameter = 1;
         PageRequest pagination = 2;
 }
*/
export interface V1Beta1PageRequest {
    /**
     * key is a value returned in PageResponse.next_key to begin
     * querying the next page most efficiently. Only one of offset or key
     * should be set.
     * @format byte
     */
    key?: string;
    /**
     * offset is a numeric offset that can be used when key is unavailable.
     * It is less efficient than using key. Only one of offset or key should
     * be set.
     * @format uint64
     */
    offset?: string;
    /**
     * limit is the total number of results to be returned in the result page.
     * If left empty it will default to a value to be set by each app.
     * @format uint64
     */
    limit?: string;
    /**
     * count_total is set to true  to indicate that the result set should include
     * a count of the total number of items available for pagination in UIs.
     * count_total is only respected when offset is used. It is ignored when key
     * is set.
     */
    countTotal?: boolean;
    /**
     * reverse is set to true if results are to be returned in the descending order.
     *
     * Since: cosmos-sdk 0.43
     */
    reverse?: boolean;
}
/**
* PageResponse is to be embedded in gRPC response messages where the
corresponding request message has used PageRequest.

 message SomeResponse {
         repeated Bar results = 1;
         PageResponse page = 2;
 }
*/
export interface V1Beta1PageResponse {
    /** @format byte */
    nextKey?: string;
    /** @format uint64 */
    total?: string;
}
export declare type QueryParamsType = Record<string | number, any>;
export declare type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;
export interface FullRequestParams extends Omit<RequestInit, "body"> {
    /** set parameter to `true` for call `securityWorker` for this request */
    secure?: boolean;
    /** request path */
    path: string;
    /** content type of request body */
    type?: ContentType;
    /** query params */
    query?: QueryParamsType;
    /** format of response (i.e. response.json() -> format: "json") */
    format?: keyof Omit<Body, "body" | "bodyUsed">;
    /** request body */
    body?: unknown;
    /** base url */
    baseUrl?: string;
    /** request cancellation token */
    cancelToken?: CancelToken;
}
export declare type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;
export interface ApiConfig<SecurityDataType = unknown> {
    baseUrl?: string;
    baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
    securityWorker?: (securityData: SecurityDataType) => RequestParams | void;
}
export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
    data: D;
    error: E;
}
declare type CancelToken = Symbol | string | number;
export declare enum ContentType {
    Json = "application/json",
    FormData = "multipart/form-data",
    UrlEncoded = "application/x-www-form-urlencoded"
}
export declare class HttpClient<SecurityDataType = unknown> {
    baseUrl: string;
    private securityData;
    private securityWorker;
    private abortControllers;
    private baseApiParams;
    constructor(apiConfig?: ApiConfig<SecurityDataType>);
    setSecurityData: (data: SecurityDataType) => void;
    private addQueryParam;
    protected toQueryString(rawQuery?: QueryParamsType): string;
    protected addQueryParams(rawQuery?: QueryParamsType): string;
    private contentFormatters;
    private mergeRequestParams;
    private createAbortSignal;
    abortRequest: (cancelToken: CancelToken) => void;
    request: <T = any, E = any>({ body, secure, path, type, query, format, baseUrl, cancelToken, ...params }: FullRequestParams) => Promise<HttpResponse<T, E>>;
}
/**
 * @title businesslogic/document_segment_mapper.proto
 * @version version not set
 */
export declare class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
}
export {};
