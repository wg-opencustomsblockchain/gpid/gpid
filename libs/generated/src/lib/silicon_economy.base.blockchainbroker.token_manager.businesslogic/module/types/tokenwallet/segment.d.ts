/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet';

export interface Segment {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  info: string;
  walletId: string;
  tokenRefs: TokenRef[];
}
export interface TokenRef {
  id: string;
  moduleRef: string;
  valid: boolean;
}
export declare const Segment: {
  encode(message: Segment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): Segment;
  fromJSON(object: any): Segment;
  toJSON(message: Segment): unknown;
  fromPartial(object: DeepPartial<Segment>): Segment;
};
export declare const TokenRef: {
  encode(message: TokenRef, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): TokenRef;
  fromJSON(object: any): TokenRef;
  toJSON(message: TokenRef): unknown;
  fromPartial(object: DeepPartial<TokenRef>): TokenRef;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
