/* eslint-disable */
import { Reader, Writer } from 'protobufjs/minimal';
import { Segment } from '../tokenwallet/segment';
import { Wallet } from '../tokenwallet/wallet';
import { TokenHistoryGlobal } from '../tokenwallet/tokenHistoryGlobal';
import { PageRequest, PageResponse, } from '../cosmos/base/query/v1beta1/pagination';
import { SegmentHistory } from '../tokenwallet/segmentHistory';
import { WalletHistory } from '../tokenwallet/walletHistory';
export const protobufPackage = 'silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet';
const baseMsgRemoveTokenRefFromSegment = {
    creator: '',
    tokenRefId: '',
    segmentId: '',
};
export const MsgRemoveTokenRefFromSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.segmentId !== '') {
            writer.uint32(26).string(message.segmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgRemoveTokenRefFromSegment,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 3:
                    message.segmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgRemoveTokenRefFromSegment,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgRemoveTokenRefFromSegment,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
};
const baseMsgMoveTokenToSegment = {
    creator: '',
    tokenRefId: '',
    sourceSegmentId: '',
    targetSegmentId: '',
};
export const MsgMoveTokenToSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.sourceSegmentId !== '') {
            writer.uint32(26).string(message.sourceSegmentId);
        }
        if (message.targetSegmentId !== '') {
            writer.uint32(34).string(message.targetSegmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMoveTokenToSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 3:
                    message.sourceSegmentId = reader.string();
                    break;
                case 4:
                    message.targetSegmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMoveTokenToSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = String(object.sourceSegmentId);
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = String(object.targetSegmentId);
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.sourceSegmentId !== undefined &&
            (obj.sourceSegmentId = message.sourceSegmentId);
        message.targetSegmentId !== undefined &&
            (obj.targetSegmentId = message.targetSegmentId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMoveTokenToSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = object.sourceSegmentId;
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = object.targetSegmentId;
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
};
const baseMsgMoveTokenToWallet = {
    creator: '',
    tokenRefId: '',
    sourceSegmentId: '',
    targetWalletId: '',
};
export const MsgMoveTokenToWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.sourceSegmentId !== '') {
            writer.uint32(26).string(message.sourceSegmentId);
        }
        if (message.targetWalletId !== '') {
            writer.uint32(34).string(message.targetWalletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMoveTokenToWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 3:
                    message.sourceSegmentId = reader.string();
                    break;
                case 4:
                    message.targetWalletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMoveTokenToWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = String(object.sourceSegmentId);
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetWalletId !== undefined && object.targetWalletId !== null) {
            message.targetWalletId = String(object.targetWalletId);
        }
        else {
            message.targetWalletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.sourceSegmentId !== undefined &&
            (obj.sourceSegmentId = message.sourceSegmentId);
        message.targetWalletId !== undefined &&
            (obj.targetWalletId = message.targetWalletId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMoveTokenToWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = object.sourceSegmentId;
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetWalletId !== undefined && object.targetWalletId !== null) {
            message.targetWalletId = object.targetWalletId;
        }
        else {
            message.targetWalletId = '';
        }
        return message;
    },
};
const baseMsgCreateTokenHistoryGlobal = {
    creator: '',
    tokenId: '',
    walletIds: '',
};
export const MsgCreateTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenId !== '') {
            writer.uint32(18).string(message.tokenId);
        }
        for (const v of message.walletIds) {
            writer.uint32(26).string(v);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgCreateTokenHistoryGlobal,
        };
        message.walletIds = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenId = reader.string();
                    break;
                case 3:
                    message.walletIds.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgCreateTokenHistoryGlobal,
        };
        message.walletIds = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = String(object.tokenId);
        }
        else {
            message.tokenId = '';
        }
        if (object.walletIds !== undefined && object.walletIds !== null) {
            for (const e of object.walletIds) {
                message.walletIds.push(String(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenId !== undefined && (obj.tokenId = message.tokenId);
        if (message.walletIds) {
            obj.walletIds = message.walletIds.map((e) => e);
        }
        else {
            obj.walletIds = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgCreateTokenHistoryGlobal,
        };
        message.walletIds = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = object.tokenId;
        }
        else {
            message.tokenId = '';
        }
        if (object.walletIds !== undefined && object.walletIds !== null) {
            for (const e of object.walletIds) {
                message.walletIds.push(e);
            }
        }
        return message;
    },
};
const baseMsgUpdateTokenHistoryGlobal = { creator: '', id: '' };
export const MsgUpdateTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgUpdateTokenHistoryGlobal,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgUpdateTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgUpdateTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateSegmentHistory = { creator: '', id: '' };
export const MsgCreateSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        for (const v of message.history) {
            Segment.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgCreateSegmentHistory,
        };
        message.history = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.history.push(Segment.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgCreateSegmentHistory,
        };
        message.history = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(Segment.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        if (message.history) {
            obj.history = message.history.map((e) => e ? Segment.toJSON(e) : undefined);
        }
        else {
            obj.history = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgCreateSegmentHistory,
        };
        message.history = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(Segment.fromPartial(e));
            }
        }
        return message;
    },
};
const baseMsgUpdateSegmentHistory = { creator: '', id: '' };
export const MsgUpdateSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgUpdateSegmentHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgUpdateSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgUpdateSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateWalletHistory = { creator: '', id: '' };
export const MsgCreateWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        for (const v of message.history) {
            Wallet.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateWalletHistory };
        message.history = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.history.push(Wallet.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateWalletHistory };
        message.history = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(Wallet.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        if (message.history) {
            obj.history = message.history.map((e) => e ? Wallet.toJSON(e) : undefined);
        }
        else {
            obj.history = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateWalletHistory };
        message.history = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(Wallet.fromPartial(e));
            }
        }
        return message;
    },
};
const baseMsgUpdateWalletHistory = { creator: '', id: '' };
export const MsgUpdateWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateWalletHistory };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateWalletHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateWalletHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateSegment = {
    creator: '',
    name: '',
    info: '',
    walletId: '',
};
export const MsgCreateSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.name !== '') {
            writer.uint32(18).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(26).string(message.info);
        }
        if (message.walletId !== '') {
            writer.uint32(34).string(message.walletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                case 3:
                    message.info = reader.string();
                    break;
                case 4:
                    message.walletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        return message;
    },
};
const baseMsgCreateSegmentWithId = {
    creator: '',
    Id: '',
    name: '',
    info: '',
    walletId: '',
};
export const MsgCreateSegmentWithId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.Id !== '') {
            writer.uint32(18).string(message.Id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(34).string(message.info);
        }
        if (message.walletId !== '') {
            writer.uint32(42).string(message.walletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateSegmentWithId };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.Id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                case 4:
                    message.info = reader.string();
                    break;
                case 5:
                    message.walletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateSegmentWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.Id !== undefined && object.Id !== null) {
            message.Id = String(object.Id);
        }
        else {
            message.Id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.Id !== undefined && (obj.Id = message.Id);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateSegmentWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.Id !== undefined && object.Id !== null) {
            message.Id = object.Id;
        }
        else {
            message.Id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        return message;
    },
};
const baseMsgUpdateSegment = {
    creator: '',
    id: '',
    name: '',
    info: '',
};
export const MsgUpdateSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(34).string(message.info);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                case 4:
                    message.info = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        return message;
    },
};
const baseMsgCreateWallet = { creator: '', name: '' };
export const MsgCreateWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.name !== '') {
            writer.uint32(18).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgCreateWalletWithId = { creator: '', Id: '', name: '' };
export const MsgCreateWalletWithId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.Id !== '') {
            writer.uint32(18).string(message.Id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateWalletWithId };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.Id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateWalletWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.Id !== undefined && object.Id !== null) {
            message.Id = String(object.Id);
        }
        else {
            message.Id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.Id !== undefined && (obj.Id = message.Id);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateWalletWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.Id !== undefined && object.Id !== null) {
            message.Id = object.Id;
        }
        else {
            message.Id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgAssignCosmosAddressToWallet = {
    creator: '',
    cosmosAddress: '',
    walletId: '',
};
export const MsgAssignCosmosAddressToWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.cosmosAddress !== '') {
            writer.uint32(18).string(message.cosmosAddress);
        }
        if (message.walletId !== '') {
            writer.uint32(26).string(message.walletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgAssignCosmosAddressToWallet,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.cosmosAddress = reader.string();
                    break;
                case 3:
                    message.walletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgAssignCosmosAddressToWallet,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.cosmosAddress !== undefined && object.cosmosAddress !== null) {
            message.cosmosAddress = String(object.cosmosAddress);
        }
        else {
            message.cosmosAddress = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.cosmosAddress !== undefined &&
            (obj.cosmosAddress = message.cosmosAddress);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgAssignCosmosAddressToWallet,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.cosmosAddress !== undefined && object.cosmosAddress !== null) {
            message.cosmosAddress = object.cosmosAddress;
        }
        else {
            message.cosmosAddress = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        return message;
    },
};
const baseMsgUpdateWallet = { creator: '', id: '', name: '' };
export const MsgUpdateWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgCreateTokenRef = {
    creator: '',
    id: '',
    moduleRef: '',
    segmentId: '',
};
export const MsgCreateTokenRef = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.moduleRef !== '') {
            writer.uint32(26).string(message.moduleRef);
        }
        if (message.segmentId !== '') {
            writer.uint32(34).string(message.segmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateTokenRef };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.moduleRef = reader.string();
                    break;
                case 4:
                    message.segmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateTokenRef };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = String(object.moduleRef);
        }
        else {
            message.moduleRef = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateTokenRef };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = object.moduleRef;
        }
        else {
            message.moduleRef = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
};
const baseMsgEmptyResponse = {};
export const MsgEmptyResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgEmptyResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgEmptyResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgEmptyResponse };
        return message;
    },
};
const baseMsgIdResponse = { id: '' };
export const MsgIdResponse = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgIdResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgIdResponse };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgIdResponse };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetTokenHistoryGlobal = { creator: '', id: '' };
export const MsgFetchGetTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetTokenHistoryGlobalResponse = {};
export const MsgFetchGetTokenHistoryGlobalResponse = {
    encode(message, writer = Writer.create()) {
        if (message.TokenHistoryGlobal !== undefined) {
            TokenHistoryGlobal.encode(message.TokenHistoryGlobal, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobalResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.TokenHistoryGlobal = TokenHistoryGlobal.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobalResponse,
        };
        if (object.TokenHistoryGlobal !== undefined &&
            object.TokenHistoryGlobal !== null) {
            message.TokenHistoryGlobal = TokenHistoryGlobal.fromJSON(object.TokenHistoryGlobal);
        }
        else {
            message.TokenHistoryGlobal = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.TokenHistoryGlobal !== undefined &&
            (obj.TokenHistoryGlobal = message.TokenHistoryGlobal
                ? TokenHistoryGlobal.toJSON(message.TokenHistoryGlobal)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobalResponse,
        };
        if (object.TokenHistoryGlobal !== undefined &&
            object.TokenHistoryGlobal !== null) {
            message.TokenHistoryGlobal = TokenHistoryGlobal.fromPartial(object.TokenHistoryGlobal);
        }
        else {
            message.TokenHistoryGlobal = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllTokenHistoryGlobal = { creator: '' };
export const MsgFetchAllTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllTokenHistoryGlobalResponse = {};
export const MsgFetchAllTokenHistoryGlobalResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.TokenHistoryGlobal) {
            TokenHistoryGlobal.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobalResponse,
        };
        message.TokenHistoryGlobal = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.TokenHistoryGlobal.push(TokenHistoryGlobal.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobalResponse,
        };
        message.TokenHistoryGlobal = [];
        if (object.TokenHistoryGlobal !== undefined &&
            object.TokenHistoryGlobal !== null) {
            for (const e of object.TokenHistoryGlobal) {
                message.TokenHistoryGlobal.push(TokenHistoryGlobal.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.TokenHistoryGlobal) {
            obj.TokenHistoryGlobal = message.TokenHistoryGlobal.map((e) => e ? TokenHistoryGlobal.toJSON(e) : undefined);
        }
        else {
            obj.TokenHistoryGlobal = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobalResponse,
        };
        message.TokenHistoryGlobal = [];
        if (object.TokenHistoryGlobal !== undefined &&
            object.TokenHistoryGlobal !== null) {
            for (const e of object.TokenHistoryGlobal) {
                message.TokenHistoryGlobal.push(TokenHistoryGlobal.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchGetSegmentHistory = { creator: '', id: '' };
export const MsgFetchGetSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetSegmentHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetSegmentHistoryResponse = {};
export const MsgFetchGetSegmentHistoryResponse = {
    encode(message, writer = Writer.create()) {
        if (message.SegmentHistory !== undefined) {
            SegmentHistory.encode(message.SegmentHistory, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetSegmentHistoryResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.SegmentHistory = SegmentHistory.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetSegmentHistoryResponse,
        };
        if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
            message.SegmentHistory = SegmentHistory.fromJSON(object.SegmentHistory);
        }
        else {
            message.SegmentHistory = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.SegmentHistory !== undefined &&
            (obj.SegmentHistory = message.SegmentHistory
                ? SegmentHistory.toJSON(message.SegmentHistory)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetSegmentHistoryResponse,
        };
        if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
            message.SegmentHistory = SegmentHistory.fromPartial(object.SegmentHistory);
        }
        else {
            message.SegmentHistory = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllSegmentHistory = { creator: '' };
export const MsgFetchAllSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllSegmentHistoryResponse = {};
export const MsgFetchAllSegmentHistoryResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.SegmentHistory) {
            SegmentHistory.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllSegmentHistoryResponse,
        };
        message.SegmentHistory = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.SegmentHistory.push(SegmentHistory.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistoryResponse,
        };
        message.SegmentHistory = [];
        if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
            for (const e of object.SegmentHistory) {
                message.SegmentHistory.push(SegmentHistory.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.SegmentHistory) {
            obj.SegmentHistory = message.SegmentHistory.map((e) => e ? SegmentHistory.toJSON(e) : undefined);
        }
        else {
            obj.SegmentHistory = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistoryResponse,
        };
        message.SegmentHistory = [];
        if (object.SegmentHistory !== undefined && object.SegmentHistory !== null) {
            for (const e of object.SegmentHistory) {
                message.SegmentHistory.push(SegmentHistory.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchGetSegment = { creator: '', id: '' };
export const MsgFetchGetSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchGetSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchGetSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchGetSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetSegmentResponse = {};
export const MsgFetchGetSegmentResponse = {
    encode(message, writer = Writer.create()) {
        if (message.Segment !== undefined) {
            Segment.encode(message.Segment, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetSegmentResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Segment = Segment.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetSegmentResponse,
        };
        if (object.Segment !== undefined && object.Segment !== null) {
            message.Segment = Segment.fromJSON(object.Segment);
        }
        else {
            message.Segment = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.Segment !== undefined &&
            (obj.Segment = message.Segment
                ? Segment.toJSON(message.Segment)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetSegmentResponse,
        };
        if (object.Segment !== undefined && object.Segment !== null) {
            message.Segment = Segment.fromPartial(object.Segment);
        }
        else {
            message.Segment = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllSegment = { creator: '' };
export const MsgFetchAllSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchAllSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchAllSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchAllSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllSegmentResponse = {};
export const MsgFetchAllSegmentResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.Segment) {
            Segment.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllSegmentResponse,
        };
        message.Segment = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Segment.push(Segment.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllSegmentResponse,
        };
        message.Segment = [];
        if (object.Segment !== undefined && object.Segment !== null) {
            for (const e of object.Segment) {
                message.Segment.push(Segment.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.Segment) {
            obj.Segment = message.Segment.map((e) => e ? Segment.toJSON(e) : undefined);
        }
        else {
            obj.Segment = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllSegmentResponse,
        };
        message.Segment = [];
        if (object.Segment !== undefined && object.Segment !== null) {
            for (const e of object.Segment) {
                message.Segment.push(Segment.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchGetWalletHistory = { creator: '', id: '' };
export const MsgFetchGetWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetWalletHistoryResponse = {};
export const MsgFetchGetWalletHistoryResponse = {
    encode(message, writer = Writer.create()) {
        if (message.WalletHistory !== undefined) {
            WalletHistory.encode(message.WalletHistory, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetWalletHistoryResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.WalletHistory = WalletHistory.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetWalletHistoryResponse,
        };
        if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
            message.WalletHistory = WalletHistory.fromJSON(object.WalletHistory);
        }
        else {
            message.WalletHistory = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.WalletHistory !== undefined &&
            (obj.WalletHistory = message.WalletHistory
                ? WalletHistory.toJSON(message.WalletHistory)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetWalletHistoryResponse,
        };
        if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
            message.WalletHistory = WalletHistory.fromPartial(object.WalletHistory);
        }
        else {
            message.WalletHistory = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllWalletHistory = { creator: '' };
export const MsgFetchAllWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllWalletHistoryResponse = {};
export const MsgFetchAllWalletHistoryResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.WalletHistory) {
            WalletHistory.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllWalletHistoryResponse,
        };
        message.WalletHistory = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.WalletHistory.push(WalletHistory.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllWalletHistoryResponse,
        };
        message.WalletHistory = [];
        if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
            for (const e of object.WalletHistory) {
                message.WalletHistory.push(WalletHistory.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.WalletHistory) {
            obj.WalletHistory = message.WalletHistory.map((e) => e ? WalletHistory.toJSON(e) : undefined);
        }
        else {
            obj.WalletHistory = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllWalletHistoryResponse,
        };
        message.WalletHistory = [];
        if (object.WalletHistory !== undefined && object.WalletHistory !== null) {
            for (const e of object.WalletHistory) {
                message.WalletHistory.push(WalletHistory.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchGetWallet = { creator: '', id: '' };
export const MsgFetchGetWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchGetWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchGetWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchGetWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetWalletResponse = {};
export const MsgFetchGetWalletResponse = {
    encode(message, writer = Writer.create()) {
        if (message.Wallet !== undefined) {
            Wallet.encode(message.Wallet, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetWalletResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Wallet = Wallet.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetWalletResponse,
        };
        if (object.Wallet !== undefined && object.Wallet !== null) {
            message.Wallet = Wallet.fromJSON(object.Wallet);
        }
        else {
            message.Wallet = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.Wallet !== undefined &&
            (obj.Wallet = message.Wallet ? Wallet.toJSON(message.Wallet) : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetWalletResponse,
        };
        if (object.Wallet !== undefined && object.Wallet !== null) {
            message.Wallet = Wallet.fromPartial(object.Wallet);
        }
        else {
            message.Wallet = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllWallet = { creator: '' };
export const MsgFetchAllWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchAllWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchAllWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchAllWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllWalletResponse = {};
export const MsgFetchAllWalletResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.Wallet) {
            Wallet.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllWalletResponse,
        };
        message.Wallet = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Wallet.push(Wallet.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllWalletResponse,
        };
        message.Wallet = [];
        if (object.Wallet !== undefined && object.Wallet !== null) {
            for (const e of object.Wallet) {
                message.Wallet.push(Wallet.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.Wallet) {
            obj.Wallet = message.Wallet.map((e) => e ? Wallet.toJSON(e) : undefined);
        }
        else {
            obj.Wallet = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllWalletResponse,
        };
        message.Wallet = [];
        if (object.Wallet !== undefined && object.Wallet !== null) {
            for (const e of object.Wallet) {
                message.Wallet.push(Wallet.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
export class MsgClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    CreateSegment(request) {
        const data = MsgCreateSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'CreateSegment', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    CreateSegmentWithId(request) {
        const data = MsgCreateSegmentWithId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'CreateSegmentWithId', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    UpdateSegment(request) {
        const data = MsgUpdateSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'UpdateSegment', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    CreateWallet(request) {
        const data = MsgCreateWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'CreateWallet', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    CreateWalletWithId(request) {
        const data = MsgCreateWalletWithId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'CreateWalletWithId', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    UpdateWallet(request) {
        const data = MsgUpdateWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'UpdateWallet', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    AssignCosmosAddressToWallet(request) {
        const data = MsgAssignCosmosAddressToWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'AssignCosmosAddressToWallet', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    CreateTokenRef(request) {
        const data = MsgCreateTokenRef.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'CreateTokenRef', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    MoveTokenToSegment(request) {
        const data = MsgMoveTokenToSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'MoveTokenToSegment', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    MoveTokenToWallet(request) {
        const data = MsgMoveTokenToWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'MoveTokenToWallet', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    RemoveTokenRefFromSegment(request) {
        const data = MsgRemoveTokenRefFromSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'RemoveTokenRefFromSegment', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    FetchTokenHistoryGlobal(request) {
        const data = MsgFetchGetTokenHistoryGlobal.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchTokenHistoryGlobal', data);
        return promise.then((data) => MsgFetchGetTokenHistoryGlobalResponse.decode(new Reader(data)));
    }
    FetchTokenHistoryGlobalAll(request) {
        const data = MsgFetchAllTokenHistoryGlobal.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchTokenHistoryGlobalAll', data);
        return promise.then((data) => MsgFetchAllTokenHistoryGlobalResponse.decode(new Reader(data)));
    }
    FetchSegmentHistory(request) {
        const data = MsgFetchGetSegmentHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchSegmentHistory', data);
        return promise.then((data) => MsgFetchGetSegmentHistoryResponse.decode(new Reader(data)));
    }
    FetchSegmentHistoryAll(request) {
        const data = MsgFetchAllSegmentHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchSegmentHistoryAll', data);
        return promise.then((data) => MsgFetchAllSegmentHistoryResponse.decode(new Reader(data)));
    }
    FetchSegment(request) {
        const data = MsgFetchGetSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchSegment', data);
        return promise.then((data) => MsgFetchGetSegmentResponse.decode(new Reader(data)));
    }
    FetchSegmentAll(request) {
        const data = MsgFetchAllSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchSegmentAll', data);
        return promise.then((data) => MsgFetchAllSegmentResponse.decode(new Reader(data)));
    }
    FetchWalletHistory(request) {
        const data = MsgFetchGetWalletHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchWalletHistory', data);
        return promise.then((data) => MsgFetchGetWalletHistoryResponse.decode(new Reader(data)));
    }
    FetchWalletHistoryAll(request) {
        const data = MsgFetchAllWalletHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchWalletHistoryAll', data);
        return promise.then((data) => MsgFetchAllWalletHistoryResponse.decode(new Reader(data)));
    }
    FetchWallet(request) {
        const data = MsgFetchGetWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchWallet', data);
        return promise.then((data) => MsgFetchGetWalletResponse.decode(new Reader(data)));
    }
    FetchWalletAll(request) {
        const data = MsgFetchAllWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet.Msg', 'FetchWalletAll', data);
        return promise.then((data) => MsgFetchAllWalletResponse.decode(new Reader(data)));
    }
}
