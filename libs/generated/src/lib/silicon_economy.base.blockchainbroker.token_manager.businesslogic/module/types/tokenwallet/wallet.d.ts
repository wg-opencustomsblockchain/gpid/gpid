/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules..tokenwallet';
export interface Wallet {
  creator: string;
  id: string;
  name: string;
  timestamp: string;
  segmentIds: string[];
  walletAccounts: WalletAccount[];
}
export interface WalletAccount {
  address: string;
  active: boolean;
}
export declare const Wallet: {
  encode(message: Wallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): Wallet;
  fromJSON(object: any): Wallet;
  toJSON(message: Wallet): unknown;
  fromPartial(object: DeepPartial<Wallet>): Wallet;
};
export declare const WalletAccount: {
  encode(message: WalletAccount, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): WalletAccount;
  fromJSON(object: any): WalletAccount;
  toJSON(message: WalletAccount): unknown;
  fromPartial(object: DeepPartial<WalletAccount>): WalletAccount;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
