/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Reader, Writer } from 'protobufjs/minimal';
import {
  MsgFetchAllSegmentHistoryResponse,
  MsgFetchGetWalletResponse,
  MsgFetchGetSegmentResponse,
  MsgFetchAllSegmentResponse,
  MsgEmptyResponse as MsgEmptyResponse1,
  MsgIdResponse as MsgIdResponse2,
  MsgFetchGetWalletHistoryResponse,
  MsgFetchAllTokenHistoryGlobalResponse,
  MsgFetchGetTokenHistoryGlobalResponse,
  MsgFetchAllWalletHistoryResponse,
  MsgFetchAllWalletResponse,
  MsgFetchGetSegmentHistoryResponse,
} from '../tokenwallet/tx';
import { QueryAllTokenResponse } from '../businesslogic/query';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgFetchAllSegmentHistory {
  creator: string;
}
export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}
export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}
export interface MsgFetchAllSegment {
  creator: string;
}
export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}
export interface MsgCreateWallet {
  creator: string;
  name: string;
}
export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}
export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}
export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}
export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
}
export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}
export interface MsgCreateWalletWithId {
  creator: string;
  id: string;
  name: string;
}
export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}
export interface MsgFetchAllWalletHistory {
  creator: string;
}
export interface MsgFetchAllWallet {
  creator: string;
}
export interface MsgFetchSegmentHistory {
  creator: string;
  id: string;
}
export interface MsgCreateSegmentWithId {
  creator: string;
  id: string;
  name: string;
  info: string;
  walletId: string;
}
export interface MsgFetchTokensByWalletId {
  creator: string;
  id: string;
}
export interface MsgFetchTokensBySegmentId {
  creator: string;
  id: string;
}
export interface MsgCreateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}
export interface MsgCreateTokenCopiesResponse {}
export interface MsgUpdateTokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}
export interface MsgUpdateTokenCopiesResponse {}
export interface MsgDeleteTokenCopies {
  creator: string;
  index: string;
}
export interface MsgDeleteTokenCopiesResponse {}
export interface MsgCreateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}
export interface MsgCreateDocumentTokenMapperResponse {}
export interface MsgUpdateDocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}
export interface MsgUpdateDocumentTokenMapperResponse {}
export interface MsgDeleteDocumentTokenMapper {
  creator: string;
  index: string;
}
export interface MsgDeleteDocumentTokenMapperResponse {}
export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}
export interface MsgCreateToken {
  creator: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}
export interface MsgUpdateToken {
  creator: string;
  tokenRefId: string;
  tokenType: string;
  changeMessage: string;
  segmentId: string;
  moduleRef: string;
}
export interface MsgIdResponse {
  id: string;
}
export interface MsgActivateToken {
  creator: string;
  id: string;
  segmentId: string;
  moduleRef: string;
}
export interface MsgDeactivateToken {
  creator: string;
  id: string;
}
export interface HashTokenMetadata {
  documentType: string;
}
export interface MsgCloneToken {
  creator: string;
  tokenId: string;
  walletId: string;
  moduleRef: string;
}
export interface MsgRevertToGenesis {
  creator: string;
}
export interface MsgEmptyResponse {}
export interface MsgCreateHashToken {
  creator: string;
  changeMessage: string;
  hash: string;
  hashFunction: string;
  documentType: string;
}
export interface MsgAttachHashToken {
  creator: string;
  targetId: string;
  changeMessage: string;
  hash: string;
  hashFunction: string;
  documentType: string;
}
export interface MsgFetchDocumentHash {
  creator: string;
  id: string;
  documentType: string;
  timestamp: string;
}
export interface MsgFetchDocumentHashResponse {
  id: string;
  processId: string;
  creator: string;
  hash: string;
  hashFunction: string;
  documentType: string;
  timestamp: string;
}
export interface MsgFetchProcessResponse {
  processId: string;
  documents: MsgFetchDocumentHashResponse[];
}
export declare const MsgFetchAllSegmentHistory: {
  encode(message: MsgFetchAllSegmentHistory, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistory;
  fromJSON(object: any): MsgFetchAllSegmentHistory;
  toJSON(message: MsgFetchAllSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistory>
  ): MsgFetchAllSegmentHistory;
};
export declare const MsgFetchGetWallet: {
  encode(message: MsgFetchGetWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWallet;
  fromJSON(object: any): MsgFetchGetWallet;
  toJSON(message: MsgFetchGetWallet): unknown;
  fromPartial(object: DeepPartial<MsgFetchGetWallet>): MsgFetchGetWallet;
};
export declare const MsgFetchGetSegment: {
  encode(message: MsgFetchGetSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetSegment;
  fromJSON(object: any): MsgFetchGetSegment;
  toJSON(message: MsgFetchGetSegment): unknown;
  fromPartial(object: DeepPartial<MsgFetchGetSegment>): MsgFetchGetSegment;
};
export declare const MsgFetchAllSegment: {
  encode(message: MsgFetchAllSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllSegment;
  fromJSON(object: any): MsgFetchAllSegment;
  toJSON(message: MsgFetchAllSegment): unknown;
  fromPartial(object: DeepPartial<MsgFetchAllSegment>): MsgFetchAllSegment;
};
export declare const MsgUpdateWallet: {
  encode(message: MsgUpdateWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWallet;
  fromJSON(object: any): MsgUpdateWallet;
  toJSON(message: MsgUpdateWallet): unknown;
  fromPartial(object: DeepPartial<MsgUpdateWallet>): MsgUpdateWallet;
};
export declare const MsgCreateWallet: {
  encode(message: MsgCreateWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateWallet;
  fromJSON(object: any): MsgCreateWallet;
  toJSON(message: MsgCreateWallet): unknown;
  fromPartial(object: DeepPartial<MsgCreateWallet>): MsgCreateWallet;
};
export declare const MsgUpdateSegment: {
  encode(message: MsgUpdateSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegment;
  fromJSON(object: any): MsgUpdateSegment;
  toJSON(message: MsgUpdateSegment): unknown;
  fromPartial(object: DeepPartial<MsgUpdateSegment>): MsgUpdateSegment;
};
export declare const MsgFetchGetWalletHistory: {
  encode(message: MsgFetchGetWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWalletHistory;
  fromJSON(object: any): MsgFetchGetWalletHistory;
  toJSON(message: MsgFetchGetWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistory>
  ): MsgFetchGetWalletHistory;
};
export declare const MsgCreateSegment: {
  encode(message: MsgCreateSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegment;
  fromJSON(object: any): MsgCreateSegment;
  toJSON(message: MsgCreateSegment): unknown;
  fromPartial(object: DeepPartial<MsgCreateSegment>): MsgCreateSegment;
};
export declare const MsgFetchAllTokenHistoryGlobal: {
  encode(message: MsgFetchAllTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobal;
  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal;
  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobal>
  ): MsgFetchAllTokenHistoryGlobal;
};
export declare const MsgFetchGetTokenHistoryGlobal: {
  encode(message: MsgFetchGetTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobal;
  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal;
  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobal>
  ): MsgFetchGetTokenHistoryGlobal;
};
export declare const MsgCreateWalletWithId: {
  encode(message: MsgCreateWalletWithId, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletWithId;
  fromJSON(object: any): MsgCreateWalletWithId;
  toJSON(message: MsgCreateWalletWithId): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateWalletWithId>
  ): MsgCreateWalletWithId;
};
export declare const MsgMoveTokenToSegment: {
  encode(message: MsgMoveTokenToSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToSegment;
  fromJSON(object: any): MsgMoveTokenToSegment;
  toJSON(message: MsgMoveTokenToSegment): unknown;
  fromPartial(
    object: DeepPartial<MsgMoveTokenToSegment>
  ): MsgMoveTokenToSegment;
};
export declare const MsgFetchAllWalletHistory: {
  encode(message: MsgFetchAllWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWalletHistory;
  fromJSON(object: any): MsgFetchAllWalletHistory;
  toJSON(message: MsgFetchAllWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistory>
  ): MsgFetchAllWalletHistory;
};
export declare const MsgFetchAllWallet: {
  encode(message: MsgFetchAllWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWallet;
  fromJSON(object: any): MsgFetchAllWallet;
  toJSON(message: MsgFetchAllWallet): unknown;
  fromPartial(object: DeepPartial<MsgFetchAllWallet>): MsgFetchAllWallet;
};
export declare const MsgFetchSegmentHistory: {
  encode(message: MsgFetchSegmentHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchSegmentHistory;
  fromJSON(object: any): MsgFetchSegmentHistory;
  toJSON(message: MsgFetchSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchSegmentHistory>
  ): MsgFetchSegmentHistory;
};
export declare const MsgCreateSegmentWithId: {
  encode(message: MsgCreateSegmentWithId, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentWithId;
  fromJSON(object: any): MsgCreateSegmentWithId;
  toJSON(message: MsgCreateSegmentWithId): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateSegmentWithId>
  ): MsgCreateSegmentWithId;
};
export declare const MsgFetchTokensByWalletId: {
  encode(message: MsgFetchTokensByWalletId, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchTokensByWalletId;
  fromJSON(object: any): MsgFetchTokensByWalletId;
  toJSON(message: MsgFetchTokensByWalletId): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchTokensByWalletId>
  ): MsgFetchTokensByWalletId;
};
export declare const MsgFetchTokensBySegmentId: {
  encode(message: MsgFetchTokensBySegmentId, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchTokensBySegmentId;
  fromJSON(object: any): MsgFetchTokensBySegmentId;
  toJSON(message: MsgFetchTokensBySegmentId): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchTokensBySegmentId>
  ): MsgFetchTokensBySegmentId;
};
export declare const MsgCreateTokenCopies: {
  encode(message: MsgCreateTokenCopies, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateTokenCopies;
  fromJSON(object: any): MsgCreateTokenCopies;
  toJSON(message: MsgCreateTokenCopies): unknown;
  fromPartial(object: DeepPartial<MsgCreateTokenCopies>): MsgCreateTokenCopies;
};
export declare const MsgCreateTokenCopiesResponse: {
  encode(_: MsgCreateTokenCopiesResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateTokenCopiesResponse;
  fromJSON(_: any): MsgCreateTokenCopiesResponse;
  toJSON(_: MsgCreateTokenCopiesResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgCreateTokenCopiesResponse>
  ): MsgCreateTokenCopiesResponse;
};
export declare const MsgUpdateTokenCopies: {
  encode(message: MsgUpdateTokenCopies, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateTokenCopies;
  fromJSON(object: any): MsgUpdateTokenCopies;
  toJSON(message: MsgUpdateTokenCopies): unknown;
  fromPartial(object: DeepPartial<MsgUpdateTokenCopies>): MsgUpdateTokenCopies;
};
export declare const MsgUpdateTokenCopiesResponse: {
  encode(_: MsgUpdateTokenCopiesResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenCopiesResponse;
  fromJSON(_: any): MsgUpdateTokenCopiesResponse;
  toJSON(_: MsgUpdateTokenCopiesResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgUpdateTokenCopiesResponse>
  ): MsgUpdateTokenCopiesResponse;
};
export declare const MsgDeleteTokenCopies: {
  encode(message: MsgDeleteTokenCopies, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgDeleteTokenCopies;
  fromJSON(object: any): MsgDeleteTokenCopies;
  toJSON(message: MsgDeleteTokenCopies): unknown;
  fromPartial(object: DeepPartial<MsgDeleteTokenCopies>): MsgDeleteTokenCopies;
};
export declare const MsgDeleteTokenCopiesResponse: {
  encode(_: MsgDeleteTokenCopiesResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteTokenCopiesResponse;
  fromJSON(_: any): MsgDeleteTokenCopiesResponse;
  toJSON(_: MsgDeleteTokenCopiesResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgDeleteTokenCopiesResponse>
  ): MsgDeleteTokenCopiesResponse;
};
export declare const MsgCreateDocumentTokenMapper: {
  encode(message: MsgCreateDocumentTokenMapper, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateDocumentTokenMapper;
  fromJSON(object: any): MsgCreateDocumentTokenMapper;
  toJSON(message: MsgCreateDocumentTokenMapper): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateDocumentTokenMapper>
  ): MsgCreateDocumentTokenMapper;
};
export declare const MsgCreateDocumentTokenMapperResponse: {
  encode(_: MsgCreateDocumentTokenMapperResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateDocumentTokenMapperResponse;
  fromJSON(_: any): MsgCreateDocumentTokenMapperResponse;
  toJSON(_: MsgCreateDocumentTokenMapperResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgCreateDocumentTokenMapperResponse>
  ): MsgCreateDocumentTokenMapperResponse;
};
export declare const MsgUpdateDocumentTokenMapper: {
  encode(message: MsgUpdateDocumentTokenMapper, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateDocumentTokenMapper;
  fromJSON(object: any): MsgUpdateDocumentTokenMapper;
  toJSON(message: MsgUpdateDocumentTokenMapper): unknown;
  fromPartial(
    object: DeepPartial<MsgUpdateDocumentTokenMapper>
  ): MsgUpdateDocumentTokenMapper;
};
export declare const MsgUpdateDocumentTokenMapperResponse: {
  encode(_: MsgUpdateDocumentTokenMapperResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateDocumentTokenMapperResponse;
  fromJSON(_: any): MsgUpdateDocumentTokenMapperResponse;
  toJSON(_: MsgUpdateDocumentTokenMapperResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgUpdateDocumentTokenMapperResponse>
  ): MsgUpdateDocumentTokenMapperResponse;
};
export declare const MsgDeleteDocumentTokenMapper: {
  encode(message: MsgDeleteDocumentTokenMapper, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteDocumentTokenMapper;
  fromJSON(object: any): MsgDeleteDocumentTokenMapper;
  toJSON(message: MsgDeleteDocumentTokenMapper): unknown;
  fromPartial(
    object: DeepPartial<MsgDeleteDocumentTokenMapper>
  ): MsgDeleteDocumentTokenMapper;
};
export declare const MsgDeleteDocumentTokenMapperResponse: {
  encode(_: MsgDeleteDocumentTokenMapperResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgDeleteDocumentTokenMapperResponse;
  fromJSON(_: any): MsgDeleteDocumentTokenMapperResponse;
  toJSON(_: MsgDeleteDocumentTokenMapperResponse): unknown;
  fromPartial(
    _: DeepPartial<MsgDeleteDocumentTokenMapperResponse>
  ): MsgDeleteDocumentTokenMapperResponse;
};
export declare const MsgMoveTokenToWallet: {
  encode(message: MsgMoveTokenToWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToWallet;
  fromJSON(object: any): MsgMoveTokenToWallet;
  toJSON(message: MsgMoveTokenToWallet): unknown;
  fromPartial(object: DeepPartial<MsgMoveTokenToWallet>): MsgMoveTokenToWallet;
};
export declare const MsgCreateToken: {
  encode(message: MsgCreateToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateToken;
  fromJSON(object: any): MsgCreateToken;
  toJSON(message: MsgCreateToken): unknown;
  fromPartial(object: DeepPartial<MsgCreateToken>): MsgCreateToken;
};
export declare const MsgUpdateToken: {
  encode(message: MsgUpdateToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateToken;
  fromJSON(object: any): MsgUpdateToken;
  toJSON(message: MsgUpdateToken): unknown;
  fromPartial(object: DeepPartial<MsgUpdateToken>): MsgUpdateToken;
};
export declare const MsgIdResponse: {
  encode(message: MsgIdResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgIdResponse;
  fromJSON(object: any): MsgIdResponse;
  toJSON(message: MsgIdResponse): unknown;
  fromPartial(object: DeepPartial<MsgIdResponse>): MsgIdResponse;
};
export declare const MsgActivateToken: {
  encode(message: MsgActivateToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgActivateToken;
  fromJSON(object: any): MsgActivateToken;
  toJSON(message: MsgActivateToken): unknown;
  fromPartial(object: DeepPartial<MsgActivateToken>): MsgActivateToken;
};
export declare const MsgDeactivateToken: {
  encode(message: MsgDeactivateToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgDeactivateToken;
  fromJSON(object: any): MsgDeactivateToken;
  toJSON(message: MsgDeactivateToken): unknown;
  fromPartial(object: DeepPartial<MsgDeactivateToken>): MsgDeactivateToken;
};
export declare const HashTokenMetadata: {
  encode(message: HashTokenMetadata, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): HashTokenMetadata;
  fromJSON(object: any): HashTokenMetadata;
  toJSON(message: HashTokenMetadata): unknown;
  fromPartial(object: DeepPartial<HashTokenMetadata>): HashTokenMetadata;
};
export declare const MsgCloneToken: {
  encode(message: MsgCloneToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCloneToken;
  fromJSON(object: any): MsgCloneToken;
  toJSON(message: MsgCloneToken): unknown;
  fromPartial(object: DeepPartial<MsgCloneToken>): MsgCloneToken;
};
export declare const MsgRevertToGenesis: {
  encode(message: MsgRevertToGenesis, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgRevertToGenesis;
  fromJSON(object: any): MsgRevertToGenesis;
  toJSON(message: MsgRevertToGenesis): unknown;
  fromPartial(object: DeepPartial<MsgRevertToGenesis>): MsgRevertToGenesis;
};
export declare const MsgEmptyResponse: {
  encode(_: MsgEmptyResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgEmptyResponse;
  fromJSON(_: any): MsgEmptyResponse;
  toJSON(_: MsgEmptyResponse): unknown;
  fromPartial(_: DeepPartial<MsgEmptyResponse>): MsgEmptyResponse;
};
export declare const MsgCreateHashToken: {
  encode(message: MsgCreateHashToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateHashToken;
  fromJSON(object: any): MsgCreateHashToken;
  toJSON(message: MsgCreateHashToken): unknown;
  fromPartial(object: DeepPartial<MsgCreateHashToken>): MsgCreateHashToken;
};
export declare const MsgAttachHashToken: {
  encode(message: MsgAttachHashToken, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgAttachHashToken;
  fromJSON(object: any): MsgAttachHashToken;
  toJSON(message: MsgAttachHashToken): unknown;
  fromPartial(object: DeepPartial<MsgAttachHashToken>): MsgAttachHashToken;
};
export declare const MsgFetchDocumentHash: {
  encode(message: MsgFetchDocumentHash, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchDocumentHash;
  fromJSON(object: any): MsgFetchDocumentHash;
  toJSON(message: MsgFetchDocumentHash): unknown;
  fromPartial(object: DeepPartial<MsgFetchDocumentHash>): MsgFetchDocumentHash;
};
export declare const MsgFetchDocumentHashResponse: {
  encode(message: MsgFetchDocumentHashResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchDocumentHashResponse;
  fromJSON(object: any): MsgFetchDocumentHashResponse;
  toJSON(message: MsgFetchDocumentHashResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchDocumentHashResponse>
  ): MsgFetchDocumentHashResponse;
};
export declare const MsgFetchProcessResponse: {
  encode(message: MsgFetchProcessResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchProcessResponse;
  fromJSON(object: any): MsgFetchProcessResponse;
  toJSON(message: MsgFetchProcessResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchProcessResponse>
  ): MsgFetchProcessResponse;
};
/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  FetchAllSegmentHistory(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;
  FetchGetWallet(
    request: MsgFetchGetWallet
  ): Promise<MsgFetchGetWalletResponse>;
  FetchGetSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;
  FetchAllSegment(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse1>;
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse2>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse1>;
  FetchGetWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse2>;
  FetchAllTokenHistoryGlobal(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;
  FetchGetTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse2>;
  MoveTokenToSegment(
    request: MsgMoveTokenToSegment
  ): Promise<MsgEmptyResponse1>;
  FetchAllWalletHistory(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;
  FetchAllWallet(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;
  FetchSegmentHistory(
    request: MsgFetchSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse2>;
  FetchTokensByWalletId(
    request: MsgFetchTokensByWalletId
  ): Promise<QueryAllTokenResponse>;
  FetchTokensBySegmentId(
    request: MsgFetchTokensBySegmentId
  ): Promise<QueryAllTokenResponse>;
  CreateDocumentTokenMapper(
    request: MsgCreateDocumentTokenMapper
  ): Promise<MsgCreateDocumentTokenMapperResponse>;
  UpdateDocumentTokenMapper(
    request: MsgUpdateDocumentTokenMapper
  ): Promise<MsgUpdateDocumentTokenMapperResponse>;
  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse>;
  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse>;
  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;
  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse>;
  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse>;
  CloneToken(request: MsgCloneToken): Promise<MsgEmptyResponse>;
  RevertModulesToGenesis(
    request: MsgRevertToGenesis
  ): Promise<MsgEmptyResponse>;
  StoreDocumentHash(
    request: MsgCreateHashToken
  ): Promise<MsgFetchProcessResponse>;
  AttachDocumentHash(
    request: MsgAttachHashToken
  ): Promise<MsgFetchProcessResponse>;
  FetchDocumentHash(
    request: MsgFetchDocumentHash
  ): Promise<MsgFetchDocumentHashResponse>;
}
export declare class MsgClientImpl implements Msg {
  private readonly rpc;
  constructor(rpc: Rpc);
  FetchAllSegmentHistory(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;
  FetchGetWallet(
    request: MsgFetchGetWallet
  ): Promise<MsgFetchGetWalletResponse>;
  FetchGetSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;
  FetchAllSegment(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse1>;
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse2>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse1>;
  FetchGetWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse2>;
  FetchAllTokenHistoryGlobal(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;
  FetchGetTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse2>;
  MoveTokenToSegment(
    request: MsgMoveTokenToSegment
  ): Promise<MsgEmptyResponse1>;
  FetchAllWalletHistory(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;
  FetchAllWallet(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;
  FetchSegmentHistory(
    request: MsgFetchSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse2>;
  FetchTokensByWalletId(
    request: MsgFetchTokensByWalletId
  ): Promise<QueryAllTokenResponse>;
  FetchTokensBySegmentId(
    request: MsgFetchTokensBySegmentId
  ): Promise<QueryAllTokenResponse>;
  CreateDocumentTokenMapper(
    request: MsgCreateDocumentTokenMapper
  ): Promise<MsgCreateDocumentTokenMapperResponse>;
  UpdateDocumentTokenMapper(
    request: MsgUpdateDocumentTokenMapper
  ): Promise<MsgUpdateDocumentTokenMapperResponse>;
  CreateToken(request: MsgCreateToken): Promise<MsgIdResponse>;
  UpdateToken(request: MsgUpdateToken): Promise<MsgEmptyResponse>;
  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;
  ActivateToken(request: MsgActivateToken): Promise<MsgEmptyResponse>;
  DeactivateToken(request: MsgDeactivateToken): Promise<MsgEmptyResponse>;
  CloneToken(request: MsgCloneToken): Promise<MsgEmptyResponse>;
  RevertModulesToGenesis(
    request: MsgRevertToGenesis
  ): Promise<MsgEmptyResponse>;
  StoreDocumentHash(
    request: MsgCreateHashToken
  ): Promise<MsgFetchProcessResponse>;
  AttachDocumentHash(
    request: MsgAttachHashToken
  ): Promise<MsgFetchProcessResponse>;
  FetchDocumentHash(
    request: MsgFetchDocumentHash
  ): Promise<MsgFetchDocumentHashResponse>;
}
interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
