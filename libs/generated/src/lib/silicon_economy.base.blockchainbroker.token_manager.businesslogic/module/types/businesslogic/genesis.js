/* eslint-disable */
import { TokenCopies } from '../businesslogic/token_copies';
import { DocumentTokenMapper } from '../businesslogic/document_token_mapper';
import { Writer, Reader } from 'protobufjs/minimal';
export const protobufPackage = 'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';
const baseGenesisState = {};
export const GenesisState = {
    encode(message, writer = Writer.create()) {
        for (const v of message.tokenCopiesList) {
            TokenCopies.encode(v, writer.uint32(18).fork()).ldelim();
        }
        for (const v of message.documentTokenMapperList) {
            DocumentTokenMapper.encode(v, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseGenesisState };
        message.tokenCopiesList = [];
        message.documentTokenMapperList = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 2:
                    message.tokenCopiesList.push(TokenCopies.decode(reader, reader.uint32()));
                    break;
                case 1:
                    message.documentTokenMapperList.push(DocumentTokenMapper.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseGenesisState };
        message.tokenCopiesList = [];
        message.documentTokenMapperList = [];
        if (object.tokenCopiesList !== undefined &&
            object.tokenCopiesList !== null) {
            for (const e of object.tokenCopiesList) {
                message.tokenCopiesList.push(TokenCopies.fromJSON(e));
            }
        }
        if (object.documentTokenMapperList !== undefined &&
            object.documentTokenMapperList !== null) {
            for (const e of object.documentTokenMapperList) {
                message.documentTokenMapperList.push(DocumentTokenMapper.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.tokenCopiesList) {
            obj.tokenCopiesList = message.tokenCopiesList.map((e) => e ? TokenCopies.toJSON(e) : undefined);
        }
        else {
            obj.tokenCopiesList = [];
        }
        if (message.documentTokenMapperList) {
            obj.documentTokenMapperList = message.documentTokenMapperList.map((e) => e ? DocumentTokenMapper.toJSON(e) : undefined);
        }
        else {
            obj.documentTokenMapperList = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseGenesisState };
        message.tokenCopiesList = [];
        message.documentTokenMapperList = [];
        if (object.tokenCopiesList !== undefined &&
            object.tokenCopiesList !== null) {
            for (const e of object.tokenCopiesList) {
                message.tokenCopiesList.push(TokenCopies.fromPartial(e));
            }
        }
        if (object.documentTokenMapperList !== undefined &&
            object.documentTokenMapperList !== null) {
            for (const e of object.documentTokenMapperList) {
                message.documentTokenMapperList.push(DocumentTokenMapper.fromPartial(e));
            }
        }
        return message;
    },
};
