/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Reader, Writer } from 'protobufjs/minimal';
import { TokenCopies } from '../businesslogic/token_copies';
import {
  PageRequest,
  PageResponse,
} from '../cosmos/base/query/v1beta1/pagination';
import { DocumentTokenMapper } from '../businesslogic/document_token_mapper';
import { Token } from '../token/token';
import { Token as Token1 } from '../hashtoken/token';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

/** this line is used by starport scaffolding # 3 */
export interface QueryGetTokenCopiesRequest {
  index: string;
}
export interface QueryGetTokenCopiesResponse {
  TokenCopies: TokenCopies | undefined;
}
export interface QueryAllTokenCopiesRequest {
  pagination: PageRequest | undefined;
}
export interface QueryAllTokenCopiesResponse {
  TokenCopies: TokenCopies[];
  pagination: PageResponse | undefined;
}
export interface QueryGetDocumentTokenMapperRequest {
  index: string;
}
export interface QueryGetDocumentTokenMapperResponse {
  DocumentTokenMapper: DocumentTokenMapper | undefined;
}
export interface QueryAllDocumentTokenMapperRequest {
  pagination: PageRequest | undefined;
}
export interface QueryAllDocumentTokenMapperResponse {
  DocumentTokenMapper: DocumentTokenMapper[];
  pagination: PageResponse | undefined;
}
export interface QueryGetDocumentHashRequest {
  id: string;
}
export interface QueryGetDocumentHashResponse {
  document: string;
  hash: string;
  hashFunction: string;
  metadata: string;
  creator: string;
  timestamp: string;
}
export interface QueryAllTokenByIdRequest {
  id: string;
  pagination: PageRequest | undefined;
}
export interface QueryAllTokenResponse {
  Token: Token[];
  HashTokens: Token1[];
}
export interface QueryGetDocumentHashHistoryRequest {
  id: string;
}
export interface QueryGetDocumentHashHistoryResponse {
  history: QueryGetDocumentHashResponse[];
}
export declare const QueryGetTokenCopiesRequest: {
  encode(message: QueryGetTokenCopiesRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetTokenCopiesRequest;
  fromJSON(object: any): QueryGetTokenCopiesRequest;
  toJSON(message: QueryGetTokenCopiesRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryGetTokenCopiesRequest>
  ): QueryGetTokenCopiesRequest;
};
export declare const QueryGetTokenCopiesResponse: {
  encode(message: QueryGetTokenCopiesResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetTokenCopiesResponse;
  fromJSON(object: any): QueryGetTokenCopiesResponse;
  toJSON(message: QueryGetTokenCopiesResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryGetTokenCopiesResponse>
  ): QueryGetTokenCopiesResponse;
};
export declare const QueryAllTokenCopiesRequest: {
  encode(message: QueryAllTokenCopiesRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllTokenCopiesRequest;
  fromJSON(object: any): QueryAllTokenCopiesRequest;
  toJSON(message: QueryAllTokenCopiesRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryAllTokenCopiesRequest>
  ): QueryAllTokenCopiesRequest;
};
export declare const QueryAllTokenCopiesResponse: {
  encode(message: QueryAllTokenCopiesResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllTokenCopiesResponse;
  fromJSON(object: any): QueryAllTokenCopiesResponse;
  toJSON(message: QueryAllTokenCopiesResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryAllTokenCopiesResponse>
  ): QueryAllTokenCopiesResponse;
};
export declare const QueryGetDocumentTokenMapperRequest: {
  encode(message: QueryGetDocumentTokenMapperRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentTokenMapperRequest;
  fromJSON(object: any): QueryGetDocumentTokenMapperRequest;
  toJSON(message: QueryGetDocumentTokenMapperRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentTokenMapperRequest>
  ): QueryGetDocumentTokenMapperRequest;
};
export declare const QueryGetDocumentTokenMapperResponse: {
  encode(message: QueryGetDocumentTokenMapperResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentTokenMapperResponse;
  fromJSON(object: any): QueryGetDocumentTokenMapperResponse;
  toJSON(message: QueryGetDocumentTokenMapperResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentTokenMapperResponse>
  ): QueryGetDocumentTokenMapperResponse;
};
export declare const QueryAllDocumentTokenMapperRequest: {
  encode(message: QueryAllDocumentTokenMapperRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllDocumentTokenMapperRequest;
  fromJSON(object: any): QueryAllDocumentTokenMapperRequest;
  toJSON(message: QueryAllDocumentTokenMapperRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryAllDocumentTokenMapperRequest>
  ): QueryAllDocumentTokenMapperRequest;
};
export declare const QueryAllDocumentTokenMapperResponse: {
  encode(message: QueryAllDocumentTokenMapperResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryAllDocumentTokenMapperResponse;
  fromJSON(object: any): QueryAllDocumentTokenMapperResponse;
  toJSON(message: QueryAllDocumentTokenMapperResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryAllDocumentTokenMapperResponse>
  ): QueryAllDocumentTokenMapperResponse;
};
export declare const QueryGetDocumentHashRequest: {
  encode(message: QueryGetDocumentHashRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashRequest;
  fromJSON(object: any): QueryGetDocumentHashRequest;
  toJSON(message: QueryGetDocumentHashRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentHashRequest>
  ): QueryGetDocumentHashRequest;
};
export declare const QueryGetDocumentHashResponse: {
  encode(message: QueryGetDocumentHashResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashResponse;
  fromJSON(object: any): QueryGetDocumentHashResponse;
  toJSON(message: QueryGetDocumentHashResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentHashResponse>
  ): QueryGetDocumentHashResponse;
};
export declare const QueryAllTokenByIdRequest: {
  encode(message: QueryAllTokenByIdRequest, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): QueryAllTokenByIdRequest;
  fromJSON(object: any): QueryAllTokenByIdRequest;
  toJSON(message: QueryAllTokenByIdRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryAllTokenByIdRequest>
  ): QueryAllTokenByIdRequest;
};
export declare const QueryAllTokenResponse: {
  encode(message: QueryAllTokenResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): QueryAllTokenResponse;
  fromJSON(object: any): QueryAllTokenResponse;
  toJSON(message: QueryAllTokenResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryAllTokenResponse>
  ): QueryAllTokenResponse;
};
export declare const QueryGetDocumentHashHistoryRequest: {
  encode(message: QueryGetDocumentHashHistoryRequest, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashHistoryRequest;
  fromJSON(object: any): QueryGetDocumentHashHistoryRequest;
  toJSON(message: QueryGetDocumentHashHistoryRequest): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentHashHistoryRequest>
  ): QueryGetDocumentHashHistoryRequest;
};
export declare const QueryGetDocumentHashHistoryResponse: {
  encode(message: QueryGetDocumentHashHistoryResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): QueryGetDocumentHashHistoryResponse;
  fromJSON(object: any): QueryGetDocumentHashHistoryResponse;
  toJSON(message: QueryGetDocumentHashHistoryResponse): unknown;
  fromPartial(
    object: DeepPartial<QueryGetDocumentHashHistoryResponse>
  ): QueryGetDocumentHashHistoryResponse;
};
/** Query defines the gRPC querier service. */
export interface Query {
  /** this line is used by starport scaffolding # 2 */
  DocumentHash(
    request: QueryGetDocumentHashRequest
  ): Promise<QueryGetDocumentHashResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/hash/{id}/history"; */
  DocumentHashHistory(
    request: QueryGetDocumentHashHistoryRequest
  ): Promise<QueryGetDocumentHashHistoryResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/tokensBySegmentId/{id}"; */
  TokensBySegmentId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;
  /** option (google.api.http).get = "/silicon-economy/base/blockchainbroker/token-manager/TokenManager/BusinessLogic/tokensByWalletId/{id}"; */
  TokensByWalletId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;
}
export declare class QueryClientImpl implements Query {
  private readonly rpc;
  constructor(rpc: Rpc);
  DocumentHash(
    request: QueryGetDocumentHashRequest
  ): Promise<QueryGetDocumentHashResponse>;
  DocumentHashHistory(
    request: QueryGetDocumentHashHistoryRequest
  ): Promise<QueryGetDocumentHashHistoryResponse>;
  TokensBySegmentId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;
  TokensByWalletId(
    request: QueryAllTokenByIdRequest
  ): Promise<QueryAllTokenResponse>;
}
interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
