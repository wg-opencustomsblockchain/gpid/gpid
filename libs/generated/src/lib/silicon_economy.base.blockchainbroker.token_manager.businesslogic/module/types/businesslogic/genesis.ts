/* eslint-disable */
import { TokenCopies } from '../businesslogic/token_copies';
import { DocumentTokenMapper } from '../businesslogic/document_token_mapper';
import { Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** GenesisState defines the capability module's genesis state. */
export interface GenesisState {
  /** this line is used by starport scaffolding # genesis/proto/state */
  tokenCopiesList: TokenCopies[];
  /** this line is used by starport scaffolding # genesis/proto/stateField */
  documentTokenMapperList: DocumentTokenMapper[];
}

const baseGenesisState: object = {};

export const GenesisState = {
  encode(message: GenesisState, writer: Writer = Writer.create()): Writer {
    for (const v of message.tokenCopiesList) {
      TokenCopies.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.documentTokenMapperList) {
      DocumentTokenMapper.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGenesisState } as GenesisState;
    message.tokenCopiesList = [];
    message.documentTokenMapperList = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.tokenCopiesList.push(
            TokenCopies.decode(reader, reader.uint32())
          );
          break;
        case 1:
          message.documentTokenMapperList.push(
            DocumentTokenMapper.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.tokenCopiesList = [];
    message.documentTokenMapperList = [];
    if (
      object.tokenCopiesList !== undefined &&
      object.tokenCopiesList !== null
    ) {
      for (const e of object.tokenCopiesList) {
        message.tokenCopiesList.push(TokenCopies.fromJSON(e));
      }
    }
    if (
      object.documentTokenMapperList !== undefined &&
      object.documentTokenMapperList !== null
    ) {
      for (const e of object.documentTokenMapperList) {
        message.documentTokenMapperList.push(DocumentTokenMapper.fromJSON(e));
      }
    }
    return message;
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    if (message.tokenCopiesList) {
      obj.tokenCopiesList = message.tokenCopiesList.map((e) =>
        e ? TokenCopies.toJSON(e) : undefined
      );
    } else {
      obj.tokenCopiesList = [];
    }
    if (message.documentTokenMapperList) {
      obj.documentTokenMapperList = message.documentTokenMapperList.map((e) =>
        e ? DocumentTokenMapper.toJSON(e) : undefined
      );
    } else {
      obj.documentTokenMapperList = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<GenesisState>): GenesisState {
    const message = { ...baseGenesisState } as GenesisState;
    message.tokenCopiesList = [];
    message.documentTokenMapperList = [];
    if (
      object.tokenCopiesList !== undefined &&
      object.tokenCopiesList !== null
    ) {
      for (const e of object.tokenCopiesList) {
        message.tokenCopiesList.push(TokenCopies.fromPartial(e));
      }
    }
    if (
      object.documentTokenMapperList !== undefined &&
      object.documentTokenMapperList !== null
    ) {
      for (const e of object.documentTokenMapperList) {
        message.documentTokenMapperList.push(
          DocumentTokenMapper.fromPartial(e)
        );
      }
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;
