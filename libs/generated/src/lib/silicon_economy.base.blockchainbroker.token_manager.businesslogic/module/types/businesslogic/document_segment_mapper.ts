/* eslint-disable */
import { Writer, Reader } from 'protobufjs/minimal';

export const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface DocumentSegmentMapper {
  documentId: string;
  segmentId: string;
}

const baseDocumentSegmentMapper: object = { documentId: '', segmentId: '' };

export const DocumentSegmentMapper = {
  encode(
    message: DocumentSegmentMapper,
    writer: Writer = Writer.create()
  ): Writer {
    if (message.documentId !== '') {
      writer.uint32(10).string(message.documentId);
    }
    if (message.segmentId !== '') {
      writer.uint32(18).string(message.segmentId);
    }
    return writer;
  },

  decode(input: Reader | Uint8Array, length?: number): DocumentSegmentMapper {
    const reader = input instanceof Uint8Array ? new Reader(input) : input;
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDocumentSegmentMapper } as DocumentSegmentMapper;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.documentId = reader.string();
          break;
        case 2:
          message.segmentId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DocumentSegmentMapper {
    const message = { ...baseDocumentSegmentMapper } as DocumentSegmentMapper;
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = String(object.documentId);
    } else {
      message.documentId = '';
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = String(object.segmentId);
    } else {
      message.segmentId = '';
    }
    return message;
  },

  toJSON(message: DocumentSegmentMapper): unknown {
    const obj: any = {};
    message.documentId !== undefined && (obj.documentId = message.documentId);
    message.segmentId !== undefined && (obj.segmentId = message.segmentId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<DocumentSegmentMapper>
  ): DocumentSegmentMapper {
    const message = { ...baseDocumentSegmentMapper } as DocumentSegmentMapper;
    if (object.documentId !== undefined && object.documentId !== null) {
      message.documentId = object.documentId;
    } else {
      message.documentId = '';
    }
    if (object.segmentId !== undefined && object.segmentId !== null) {
      message.segmentId = object.segmentId;
    } else {
      message.segmentId = '';
    }
    return message;
  },
};

type Builtin = Date | Function | Uint8Array | string | number | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;
