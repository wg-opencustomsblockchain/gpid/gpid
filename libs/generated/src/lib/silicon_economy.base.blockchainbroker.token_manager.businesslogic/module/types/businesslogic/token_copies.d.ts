/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

export interface TokenCopies {
  creator: string;
  index: string;
  tokens: string[];
}
export declare const TokenCopies: {
  encode(message: TokenCopies, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): TokenCopies;
  fromJSON(object: any): TokenCopies;
  toJSON(message: TokenCopies): unknown;
  fromPartial(object: DeepPartial<TokenCopies>): TokenCopies;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
