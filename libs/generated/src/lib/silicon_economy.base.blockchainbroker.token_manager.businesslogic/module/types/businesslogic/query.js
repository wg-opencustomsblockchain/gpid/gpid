/* eslint-disable */
import { Reader, Writer } from 'protobufjs/minimal';
import { TokenCopies } from '../businesslogic/token_copies';
import { PageRequest, PageResponse, } from '../cosmos/base/query/v1beta1/pagination';
import { DocumentTokenMapper } from '../businesslogic/document_token_mapper';
import { Token } from '../token/token';
import { Token as Token1 } from '../hashtoken/token';
export const protobufPackage = 'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';
const baseQueryGetTokenCopiesRequest = { index: '' };
export const QueryGetTokenCopiesRequest = {
    encode(message, writer = Writer.create()) {
        if (message.index !== '') {
            writer.uint32(10).string(message.index);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetTokenCopiesRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.index = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetTokenCopiesRequest,
        };
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.index !== undefined && (obj.index = message.index);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetTokenCopiesRequest,
        };
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        return message;
    },
};
const baseQueryGetTokenCopiesResponse = {};
export const QueryGetTokenCopiesResponse = {
    encode(message, writer = Writer.create()) {
        if (message.TokenCopies !== undefined) {
            TokenCopies.encode(message.TokenCopies, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetTokenCopiesResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.TokenCopies = TokenCopies.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetTokenCopiesResponse,
        };
        if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
            message.TokenCopies = TokenCopies.fromJSON(object.TokenCopies);
        }
        else {
            message.TokenCopies = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.TokenCopies !== undefined &&
            (obj.TokenCopies = message.TokenCopies
                ? TokenCopies.toJSON(message.TokenCopies)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetTokenCopiesResponse,
        };
        if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
            message.TokenCopies = TokenCopies.fromPartial(object.TokenCopies);
        }
        else {
            message.TokenCopies = undefined;
        }
        return message;
    },
};
const baseQueryAllTokenCopiesRequest = {};
export const QueryAllTokenCopiesRequest = {
    encode(message, writer = Writer.create()) {
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAllTokenCopiesRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAllTokenCopiesRequest,
        };
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAllTokenCopiesRequest,
        };
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseQueryAllTokenCopiesResponse = {};
export const QueryAllTokenCopiesResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.TokenCopies) {
            TokenCopies.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAllTokenCopiesResponse,
        };
        message.TokenCopies = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.TokenCopies.push(TokenCopies.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAllTokenCopiesResponse,
        };
        message.TokenCopies = [];
        if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
            for (const e of object.TokenCopies) {
                message.TokenCopies.push(TokenCopies.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.TokenCopies) {
            obj.TokenCopies = message.TokenCopies.map((e) => e ? TokenCopies.toJSON(e) : undefined);
        }
        else {
            obj.TokenCopies = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAllTokenCopiesResponse,
        };
        message.TokenCopies = [];
        if (object.TokenCopies !== undefined && object.TokenCopies !== null) {
            for (const e of object.TokenCopies) {
                message.TokenCopies.push(TokenCopies.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseQueryGetDocumentTokenMapperRequest = { index: '' };
export const QueryGetDocumentTokenMapperRequest = {
    encode(message, writer = Writer.create()) {
        if (message.index !== '') {
            writer.uint32(10).string(message.index);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentTokenMapperRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.index = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentTokenMapperRequest,
        };
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.index !== undefined && (obj.index = message.index);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentTokenMapperRequest,
        };
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        return message;
    },
};
const baseQueryGetDocumentTokenMapperResponse = {};
export const QueryGetDocumentTokenMapperResponse = {
    encode(message, writer = Writer.create()) {
        if (message.DocumentTokenMapper !== undefined) {
            DocumentTokenMapper.encode(message.DocumentTokenMapper, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentTokenMapperResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.DocumentTokenMapper = DocumentTokenMapper.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentTokenMapperResponse,
        };
        if (object.DocumentTokenMapper !== undefined &&
            object.DocumentTokenMapper !== null) {
            message.DocumentTokenMapper = DocumentTokenMapper.fromJSON(object.DocumentTokenMapper);
        }
        else {
            message.DocumentTokenMapper = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.DocumentTokenMapper !== undefined &&
            (obj.DocumentTokenMapper = message.DocumentTokenMapper
                ? DocumentTokenMapper.toJSON(message.DocumentTokenMapper)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentTokenMapperResponse,
        };
        if (object.DocumentTokenMapper !== undefined &&
            object.DocumentTokenMapper !== null) {
            message.DocumentTokenMapper = DocumentTokenMapper.fromPartial(object.DocumentTokenMapper);
        }
        else {
            message.DocumentTokenMapper = undefined;
        }
        return message;
    },
};
const baseQueryAllDocumentTokenMapperRequest = {};
export const QueryAllDocumentTokenMapperRequest = {
    encode(message, writer = Writer.create()) {
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAllDocumentTokenMapperRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAllDocumentTokenMapperRequest,
        };
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAllDocumentTokenMapperRequest,
        };
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseQueryAllDocumentTokenMapperResponse = {};
export const QueryAllDocumentTokenMapperResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.DocumentTokenMapper) {
            DocumentTokenMapper.encode(v, writer.uint32(10).fork()).ldelim();
        }
        if (message.pagination !== undefined) {
            PageResponse.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAllDocumentTokenMapperResponse,
        };
        message.DocumentTokenMapper = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.DocumentTokenMapper.push(DocumentTokenMapper.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.pagination = PageResponse.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAllDocumentTokenMapperResponse,
        };
        message.DocumentTokenMapper = [];
        if (object.DocumentTokenMapper !== undefined &&
            object.DocumentTokenMapper !== null) {
            for (const e of object.DocumentTokenMapper) {
                message.DocumentTokenMapper.push(DocumentTokenMapper.fromJSON(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.DocumentTokenMapper) {
            obj.DocumentTokenMapper = message.DocumentTokenMapper.map((e) => e ? DocumentTokenMapper.toJSON(e) : undefined);
        }
        else {
            obj.DocumentTokenMapper = [];
        }
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageResponse.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAllDocumentTokenMapperResponse,
        };
        message.DocumentTokenMapper = [];
        if (object.DocumentTokenMapper !== undefined &&
            object.DocumentTokenMapper !== null) {
            for (const e of object.DocumentTokenMapper) {
                message.DocumentTokenMapper.push(DocumentTokenMapper.fromPartial(e));
            }
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageResponse.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseQueryGetDocumentHashRequest = { id: '' };
export const QueryGetDocumentHashRequest = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentHashRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentHashRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentHashRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseQueryGetDocumentHashResponse = {
    document: '',
    hash: '',
    hashFunction: '',
    metadata: '',
    creator: '',
    timestamp: '',
};
export const QueryGetDocumentHashResponse = {
    encode(message, writer = Writer.create()) {
        if (message.document !== '') {
            writer.uint32(10).string(message.document);
        }
        if (message.hash !== '') {
            writer.uint32(18).string(message.hash);
        }
        if (message.hashFunction !== '') {
            writer.uint32(26).string(message.hashFunction);
        }
        if (message.metadata !== '') {
            writer.uint32(34).string(message.metadata);
        }
        if (message.creator !== '') {
            writer.uint32(42).string(message.creator);
        }
        if (message.timestamp !== '') {
            writer.uint32(50).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentHashResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.document = reader.string();
                    break;
                case 2:
                    message.hash = reader.string();
                    break;
                case 3:
                    message.hashFunction = reader.string();
                    break;
                case 4:
                    message.metadata = reader.string();
                    break;
                case 5:
                    message.creator = reader.string();
                    break;
                case 6:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentHashResponse,
        };
        if (object.document !== undefined && object.document !== null) {
            message.document = String(object.document);
        }
        else {
            message.document = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = String(object.hashFunction);
        }
        else {
            message.hashFunction = '';
        }
        if (object.metadata !== undefined && object.metadata !== null) {
            message.metadata = String(object.metadata);
        }
        else {
            message.metadata = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.document !== undefined && (obj.document = message.document);
        message.hash !== undefined && (obj.hash = message.hash);
        message.hashFunction !== undefined &&
            (obj.hashFunction = message.hashFunction);
        message.metadata !== undefined && (obj.metadata = message.metadata);
        message.creator !== undefined && (obj.creator = message.creator);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentHashResponse,
        };
        if (object.document !== undefined && object.document !== null) {
            message.document = object.document;
        }
        else {
            message.document = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = object.hashFunction;
        }
        else {
            message.hashFunction = '';
        }
        if (object.metadata !== undefined && object.metadata !== null) {
            message.metadata = object.metadata;
        }
        else {
            message.metadata = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseQueryAllTokenByIdRequest = { id: '' };
export const QueryAllTokenByIdRequest = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        if (message.pagination !== undefined) {
            PageRequest.encode(message.pagination, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryAllTokenByIdRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                case 2:
                    message.pagination = PageRequest.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryAllTokenByIdRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromJSON(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.pagination !== undefined &&
            (obj.pagination = message.pagination
                ? PageRequest.toJSON(message.pagination)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryAllTokenByIdRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.pagination !== undefined && object.pagination !== null) {
            message.pagination = PageRequest.fromPartial(object.pagination);
        }
        else {
            message.pagination = undefined;
        }
        return message;
    },
};
const baseQueryAllTokenResponse = {};
export const QueryAllTokenResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.Token) {
            Token.encode(v, writer.uint32(10).fork()).ldelim();
        }
        for (const v of message.HashTokens) {
            Token1.encode(v, writer.uint32(18).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseQueryAllTokenResponse };
        message.Token = [];
        message.HashTokens = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Token.push(Token.decode(reader, reader.uint32()));
                    break;
                case 2:
                    message.HashTokens.push(Token1.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseQueryAllTokenResponse };
        message.Token = [];
        message.HashTokens = [];
        if (object.Token !== undefined && object.Token !== null) {
            for (const e of object.Token) {
                message.Token.push(Token.fromJSON(e));
            }
        }
        if (object.HashTokens !== undefined && object.HashTokens !== null) {
            for (const e of object.HashTokens) {
                message.HashTokens.push(Token1.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.Token) {
            obj.Token = message.Token.map((e) => (e ? Token.toJSON(e) : undefined));
        }
        else {
            obj.Token = [];
        }
        if (message.HashTokens) {
            obj.HashTokens = message.HashTokens.map((e) => e ? Token1.toJSON(e) : undefined);
        }
        else {
            obj.HashTokens = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseQueryAllTokenResponse };
        message.Token = [];
        message.HashTokens = [];
        if (object.Token !== undefined && object.Token !== null) {
            for (const e of object.Token) {
                message.Token.push(Token.fromPartial(e));
            }
        }
        if (object.HashTokens !== undefined && object.HashTokens !== null) {
            for (const e of object.HashTokens) {
                message.HashTokens.push(Token1.fromPartial(e));
            }
        }
        return message;
    },
};
const baseQueryGetDocumentHashHistoryRequest = { id: '' };
export const QueryGetDocumentHashHistoryRequest = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentHashHistoryRequest,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentHashHistoryRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentHashHistoryRequest,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseQueryGetDocumentHashHistoryResponse = {};
export const QueryGetDocumentHashHistoryResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.history) {
            QueryGetDocumentHashResponse.encode(v, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseQueryGetDocumentHashHistoryResponse,
        };
        message.history = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.history.push(QueryGetDocumentHashResponse.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseQueryGetDocumentHashHistoryResponse,
        };
        message.history = [];
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(QueryGetDocumentHashResponse.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.history) {
            obj.history = message.history.map((e) => e ? QueryGetDocumentHashResponse.toJSON(e) : undefined);
        }
        else {
            obj.history = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseQueryGetDocumentHashHistoryResponse,
        };
        message.history = [];
        if (object.history !== undefined && object.history !== null) {
            for (const e of object.history) {
                message.history.push(QueryGetDocumentHashResponse.fromPartial(e));
            }
        }
        return message;
    },
};
export class QueryClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    DocumentHash(request) {
        const data = QueryGetDocumentHashRequest.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query', 'DocumentHash', data);
        return promise.then((data) => QueryGetDocumentHashResponse.decode(new Reader(data)));
    }
    DocumentHashHistory(request) {
        const data = QueryGetDocumentHashHistoryRequest.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query', 'DocumentHashHistory', data);
        return promise.then((data) => QueryGetDocumentHashHistoryResponse.decode(new Reader(data)));
    }
    TokensBySegmentId(request) {
        const data = QueryAllTokenByIdRequest.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query', 'TokensBySegmentId', data);
        return promise.then((data) => QueryAllTokenResponse.decode(new Reader(data)));
    }
    TokensByWalletId(request) {
        const data = QueryAllTokenByIdRequest.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Query', 'TokensByWalletId', data);
        return promise.then((data) => QueryAllTokenResponse.decode(new Reader(data)));
    }
}
