// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.
import { SigningStargateClient } from "@cosmjs/stargate";
import { Registry } from "@cosmjs/proto-signing";
import { Api } from "./rest";
import { MsgFetchGetWallet } from "./types/businesslogic/tx";
import { MsgActivateToken } from "./types/businesslogic/tx";
import { MsgRevertToGenesis } from "./types/businesslogic/tx";
import { MsgFetchDocumentHash } from "./types/businesslogic/tx";
import { MsgFetchAllTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgFetchProcess } from "./types/businesslogic/tx";
import { MsgFetchAllWallet } from "./types/businesslogic/tx";
import { MsgCreateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgFetchGetSegment } from "./types/businesslogic/tx";
import { MsgUpdateWallet } from "./types/businesslogic/tx";
import { MsgFetchGetWalletHistory } from "./types/businesslogic/tx";
import { MsgCreateSegment } from "./types/businesslogic/tx";
import { MsgDeleteDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgUpdateToken } from "./types/businesslogic/tx";
import { MsgFetchTokensByWalletId } from "./types/businesslogic/tx";
import { MsgFetchTokensBySegmentId } from "./types/businesslogic/tx";
import { MsgCreateKeyDataSet } from "./types/businesslogic/tx";
import { MsgCreateTokenCopies } from "./types/businesslogic/tx";
import { MsgMoveTokenToSegment } from "./types/businesslogic/tx";
import { MsgCreateHashToken } from "./types/businesslogic/tx";
import { MsgAttachHashToken } from "./types/businesslogic/tx";
import { MsgCreateSegmentWithId } from "./types/businesslogic/tx";
import { MsgCloneToken } from "./types/businesslogic/tx";
import { MsgCreateToken } from "./types/businesslogic/tx";
import { MsgDeleteTokenCopies } from "./types/businesslogic/tx";
import { MsgCreateWallet } from "./types/businesslogic/tx";
import { MsgDeactivateToken } from "./types/businesslogic/tx";
import { MsgFetchGetTokenHistoryGlobal } from "./types/businesslogic/tx";
import { MsgUpdateTokenCopies } from "./types/businesslogic/tx";
import { MsgFetchTokenHistory } from "./types/businesslogic/tx";
import { MsgUpdateKeyDataSet } from "./types/businesslogic/tx";
import { MsgFetchAllWalletHistory } from "./types/businesslogic/tx";
import { MsgUpdateSegment } from "./types/businesslogic/tx";
import { MsgFetchAllToken } from "./types/businesslogic/tx";
import { MsgCreateWalletWithId } from "./types/businesslogic/tx";
import { MsgUpdateDocumentTokenMapper } from "./types/businesslogic/tx";
import { MsgFetchKeyDataSet } from "./types/businesslogic/tx";
import { MsgFetchSegmentHistory } from "./types/businesslogic/tx";
import { MsgMoveTokenToWallet } from "./types/businesslogic/tx";
import { MsgFetchAllSegmentHistory } from "./types/businesslogic/tx";
import { MsgFetchAllSegment } from "./types/businesslogic/tx";
const types = [
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWallet", MsgFetchGetWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgActivateToken", MsgActivateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgRevertToGenesis", MsgRevertToGenesis],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchDocumentHash", MsgFetchDocumentHash],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobal", MsgFetchAllTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchProcess", MsgFetchProcess],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWallet", MsgFetchAllWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateDocumentTokenMapper", MsgCreateDocumentTokenMapper],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegment", MsgFetchGetSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateWallet", MsgUpdateWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistory", MsgFetchGetWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegment", MsgCreateSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeleteDocumentTokenMapper", MsgDeleteDocumentTokenMapper],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateToken", MsgUpdateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensByWalletId", MsgFetchTokensByWalletId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensBySegmentId", MsgFetchTokensBySegmentId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateKeyDataSet", MsgCreateKeyDataSet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateTokenCopies", MsgCreateTokenCopies],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToSegment", MsgMoveTokenToSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateHashToken", MsgCreateHashToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgAttachHashToken", MsgAttachHashToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegmentWithId", MsgCreateSegmentWithId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCloneToken", MsgCloneToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateToken", MsgCreateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeleteTokenCopies", MsgDeleteTokenCopies],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWallet", MsgCreateWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeactivateToken", MsgDeactivateToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobal", MsgFetchGetTokenHistoryGlobal],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateTokenCopies", MsgUpdateTokenCopies],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistory", MsgFetchTokenHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateKeyDataSet", MsgUpdateKeyDataSet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistory", MsgFetchAllWalletHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateSegment", MsgUpdateSegment],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllToken", MsgFetchAllToken],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWalletWithId", MsgCreateWalletWithId],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateDocumentTokenMapper", MsgUpdateDocumentTokenMapper],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchKeyDataSet", MsgFetchKeyDataSet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchSegmentHistory", MsgFetchSegmentHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToWallet", MsgMoveTokenToWallet],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistory", MsgFetchAllSegmentHistory],
    ["/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegment", MsgFetchAllSegment],
];
export const MissingWalletError = new Error("wallet is required");
export const registry = new Registry(types);
const defaultFee = {
    amount: [],
    gas: "200000",
};
const txClient = async (wallet, { addr: addr } = { addr: "http://localhost:26657" }) => {
    if (!wallet)
        throw MissingWalletError;
    let client;
    if (addr) {
        client = await SigningStargateClient.connectWithSigner(addr, wallet, { registry });
    }
    else {
        client = await SigningStargateClient.offline(wallet, { registry });
    }
    const { address } = (await wallet.getAccounts())[0];
    return {
        signAndBroadcast: (msgs, { fee, memo } = { fee: defaultFee, memo: "" }) => client.signAndBroadcast(address, msgs, fee, memo),
        msgFetchGetWallet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWallet", value: MsgFetchGetWallet.fromPartial(data) }),
        msgActivateToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgActivateToken", value: MsgActivateToken.fromPartial(data) }),
        msgRevertToGenesis: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgRevertToGenesis", value: MsgRevertToGenesis.fromPartial(data) }),
        msgFetchDocumentHash: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchDocumentHash", value: MsgFetchDocumentHash.fromPartial(data) }),
        msgFetchAllTokenHistoryGlobal: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllTokenHistoryGlobal", value: MsgFetchAllTokenHistoryGlobal.fromPartial(data) }),
        msgFetchProcess: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchProcess", value: MsgFetchProcess.fromPartial(data) }),
        msgFetchAllWallet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWallet", value: MsgFetchAllWallet.fromPartial(data) }),
        msgCreateDocumentTokenMapper: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateDocumentTokenMapper", value: MsgCreateDocumentTokenMapper.fromPartial(data) }),
        msgFetchGetSegment: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetSegment", value: MsgFetchGetSegment.fromPartial(data) }),
        msgUpdateWallet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateWallet", value: MsgUpdateWallet.fromPartial(data) }),
        msgFetchGetWalletHistory: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetWalletHistory", value: MsgFetchGetWalletHistory.fromPartial(data) }),
        msgCreateSegment: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegment", value: MsgCreateSegment.fromPartial(data) }),
        msgDeleteDocumentTokenMapper: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeleteDocumentTokenMapper", value: MsgDeleteDocumentTokenMapper.fromPartial(data) }),
        msgUpdateToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateToken", value: MsgUpdateToken.fromPartial(data) }),
        msgFetchTokensByWalletId: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensByWalletId", value: MsgFetchTokensByWalletId.fromPartial(data) }),
        msgFetchTokensBySegmentId: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokensBySegmentId", value: MsgFetchTokensBySegmentId.fromPartial(data) }),
        msgCreateKeyDataSet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateKeyDataSet", value: MsgCreateKeyDataSet.fromPartial(data) }),
        msgCreateTokenCopies: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateTokenCopies", value: MsgCreateTokenCopies.fromPartial(data) }),
        msgMoveTokenToSegment: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToSegment", value: MsgMoveTokenToSegment.fromPartial(data) }),
        msgCreateHashToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateHashToken", value: MsgCreateHashToken.fromPartial(data) }),
        msgAttachHashToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgAttachHashToken", value: MsgAttachHashToken.fromPartial(data) }),
        msgCreateSegmentWithId: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateSegmentWithId", value: MsgCreateSegmentWithId.fromPartial(data) }),
        msgCloneToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCloneToken", value: MsgCloneToken.fromPartial(data) }),
        msgCreateToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateToken", value: MsgCreateToken.fromPartial(data) }),
        msgDeleteTokenCopies: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeleteTokenCopies", value: MsgDeleteTokenCopies.fromPartial(data) }),
        msgCreateWallet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWallet", value: MsgCreateWallet.fromPartial(data) }),
        msgDeactivateToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgDeactivateToken", value: MsgDeactivateToken.fromPartial(data) }),
        msgFetchGetTokenHistoryGlobal: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchGetTokenHistoryGlobal", value: MsgFetchGetTokenHistoryGlobal.fromPartial(data) }),
        msgUpdateTokenCopies: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateTokenCopies", value: MsgUpdateTokenCopies.fromPartial(data) }),
        msgFetchTokenHistory: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchTokenHistory", value: MsgFetchTokenHistory.fromPartial(data) }),
        msgUpdateKeyDataSet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateKeyDataSet", value: MsgUpdateKeyDataSet.fromPartial(data) }),
        msgFetchAllWalletHistory: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllWalletHistory", value: MsgFetchAllWalletHistory.fromPartial(data) }),
        msgUpdateSegment: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateSegment", value: MsgUpdateSegment.fromPartial(data) }),
        msgFetchAllToken: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllToken", value: MsgFetchAllToken.fromPartial(data) }),
        msgCreateWalletWithId: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgCreateWalletWithId", value: MsgCreateWalletWithId.fromPartial(data) }),
        msgUpdateDocumentTokenMapper: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgUpdateDocumentTokenMapper", value: MsgUpdateDocumentTokenMapper.fromPartial(data) }),
        msgFetchKeyDataSet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchKeyDataSet", value: MsgFetchKeyDataSet.fromPartial(data) }),
        msgFetchSegmentHistory: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchSegmentHistory", value: MsgFetchSegmentHistory.fromPartial(data) }),
        msgMoveTokenToWallet: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgMoveTokenToWallet", value: MsgMoveTokenToWallet.fromPartial(data) }),
        msgFetchAllSegmentHistory: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegmentHistory", value: MsgFetchAllSegmentHistory.fromPartial(data) }),
        msgFetchAllSegment: (data) => ({ typeUrl: "/silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.MsgFetchAllSegment", value: MsgFetchAllSegment.fromPartial(data) }),
    };
};
const queryClient = async ({ addr: addr } = { addr: "http://localhost:1317" }) => {
    return new Api({ baseUrl: addr });
};
export { txClient, queryClient, };
