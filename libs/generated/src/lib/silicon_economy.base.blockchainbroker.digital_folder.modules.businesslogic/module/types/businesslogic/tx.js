/* eslint-disable */
import { Reader, Writer } from 'protobufjs/minimal';
import { TokenHistory } from '../token/tokenHistory';
import { Token } from '../token/token';
import { MsgFetchAllSegmentHistoryResponse, MsgFetchGetWalletResponse, MsgFetchGetSegmentResponse, MsgFetchAllSegmentResponse, MsgEmptyResponse as MsgEmptyResponse1, MsgIdResponse as MsgIdResponse2, MsgFetchGetWalletHistoryResponse, MsgFetchAllTokenHistoryGlobalResponse, MsgFetchGetTokenHistoryGlobalResponse, MsgFetchAllWalletHistoryResponse, MsgFetchAllWalletResponse, MsgFetchGetSegmentHistoryResponse, } from '../tokenwallet/tx';
import { QueryAllTokenResponse } from '../businesslogic/query';
export const protobufPackage = 'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';
const baseMsgFetchAllSegmentHistory = { creator: '' };
export const MsgFetchAllSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllSegmentHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgFetchGetWallet = { creator: '', id: '' };
export const MsgFetchGetWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchGetWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchGetWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchGetWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchGetSegment = { creator: '', id: '' };
export const MsgFetchGetSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchGetSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchGetSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchGetSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchAllSegment = { creator: '' };
export const MsgFetchAllSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchAllSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchAllSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchAllSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgUpdateWallet = { creator: '', id: '', name: '' };
export const MsgUpdateWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgCreateWallet = { creator: '', name: '' };
export const MsgCreateWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.name !== '') {
            writer.uint32(18).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgUpdateSegment = {
    creator: '',
    id: '',
    name: '',
    info: '',
};
export const MsgUpdateSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(34).string(message.info);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                case 4:
                    message.info = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        return message;
    },
};
const baseMsgFetchGetWalletHistory = { creator: '', id: '' };
export const MsgFetchGetWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateSegment = {
    creator: '',
    name: '',
    info: '',
    walletId: '',
};
export const MsgCreateSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.name !== '') {
            writer.uint32(18).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(26).string(message.info);
        }
        if (message.walletId !== '') {
            writer.uint32(34).string(message.walletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                case 3:
                    message.info = reader.string();
                    break;
                case 4:
                    message.walletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        return message;
    },
};
const baseMsgFetchAllTokenHistoryGlobal = { creator: '' };
export const MsgFetchAllTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgFetchGetTokenHistoryGlobal = { creator: '', id: '' };
export const MsgFetchGetTokenHistoryGlobal = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchGetTokenHistoryGlobal,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateWalletWithId = { creator: '', id: '', name: '' };
export const MsgCreateWalletWithId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateWalletWithId };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateWalletWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateWalletWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        return message;
    },
};
const baseMsgMoveTokenToSegment = {
    creator: '',
    tokenRefId: '',
    sourceSegmentId: '',
    targetSegmentId: '',
};
export const MsgMoveTokenToSegment = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.sourceSegmentId !== '') {
            writer.uint32(26).string(message.sourceSegmentId);
        }
        if (message.targetSegmentId !== '') {
            writer.uint32(34).string(message.targetSegmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMoveTokenToSegment };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 3:
                    message.sourceSegmentId = reader.string();
                    break;
                case 4:
                    message.targetSegmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMoveTokenToSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = String(object.sourceSegmentId);
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = String(object.targetSegmentId);
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.sourceSegmentId !== undefined &&
            (obj.sourceSegmentId = message.sourceSegmentId);
        message.targetSegmentId !== undefined &&
            (obj.targetSegmentId = message.targetSegmentId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMoveTokenToSegment };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = object.sourceSegmentId;
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = object.targetSegmentId;
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
};
const baseMsgFetchAllWalletHistory = { creator: '' };
export const MsgFetchAllWalletHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllWalletHistory,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgFetchAllWallet = { creator: '' };
export const MsgFetchAllWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchAllWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchAllWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchAllWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgFetchSegmentHistory = { creator: '', id: '' };
export const MsgFetchSegmentHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchSegmentHistory };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchSegmentHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchSegmentHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateSegmentWithId = {
    creator: '',
    id: '',
    name: '',
    info: '',
    walletId: '',
};
export const MsgCreateSegmentWithId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.name !== '') {
            writer.uint32(26).string(message.name);
        }
        if (message.info !== '') {
            writer.uint32(34).string(message.info);
        }
        if (message.walletId !== '') {
            writer.uint32(42).string(message.walletId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateSegmentWithId };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.name = reader.string();
                    break;
                case 4:
                    message.info = reader.string();
                    break;
                case 5:
                    message.walletId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateSegmentWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = String(object.name);
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = String(object.info);
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.name !== undefined && (obj.name = message.name);
        message.info !== undefined && (obj.info = message.info);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateSegmentWithId };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.name !== undefined && object.name !== null) {
            message.name = object.name;
        }
        else {
            message.name = '';
        }
        if (object.info !== undefined && object.info !== null) {
            message.info = object.info;
        }
        else {
            message.info = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        return message;
    },
};
const baseMsgFetchTokensByWalletId = { creator: '', id: '' };
export const MsgFetchTokensByWalletId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchTokensByWalletId,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchTokensByWalletId,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchTokensByWalletId,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchTokensBySegmentId = { creator: '', id: '' };
export const MsgFetchTokensBySegmentId = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchTokensBySegmentId,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchTokensBySegmentId,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchTokensBySegmentId,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgCreateTokenCopies = { creator: '', index: '', tokens: '' };
export const MsgCreateTokenCopies = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.index !== '') {
            writer.uint32(18).string(message.index);
        }
        for (const v of message.tokens) {
            writer.uint32(26).string(v);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateTokenCopies };
        message.tokens = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.index = reader.string();
                    break;
                case 3:
                    message.tokens.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateTokenCopies };
        message.tokens = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        if (object.tokens !== undefined && object.tokens !== null) {
            for (const e of object.tokens) {
                message.tokens.push(String(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.index !== undefined && (obj.index = message.index);
        if (message.tokens) {
            obj.tokens = message.tokens.map((e) => e);
        }
        else {
            obj.tokens = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateTokenCopies };
        message.tokens = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        if (object.tokens !== undefined && object.tokens !== null) {
            for (const e of object.tokens) {
                message.tokens.push(e);
            }
        }
        return message;
    },
};
const baseMsgCreateTokenCopiesResponse = {};
export const MsgCreateTokenCopiesResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgCreateTokenCopiesResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgCreateTokenCopiesResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgCreateTokenCopiesResponse,
        };
        return message;
    },
};
const baseMsgUpdateTokenCopies = { creator: '', index: '', tokens: '' };
export const MsgUpdateTokenCopies = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.index !== '') {
            writer.uint32(18).string(message.index);
        }
        for (const v of message.tokens) {
            writer.uint32(26).string(v);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateTokenCopies };
        message.tokens = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.index = reader.string();
                    break;
                case 3:
                    message.tokens.push(reader.string());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateTokenCopies };
        message.tokens = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        if (object.tokens !== undefined && object.tokens !== null) {
            for (const e of object.tokens) {
                message.tokens.push(String(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.index !== undefined && (obj.index = message.index);
        if (message.tokens) {
            obj.tokens = message.tokens.map((e) => e);
        }
        else {
            obj.tokens = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateTokenCopies };
        message.tokens = [];
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        if (object.tokens !== undefined && object.tokens !== null) {
            for (const e of object.tokens) {
                message.tokens.push(e);
            }
        }
        return message;
    },
};
const baseMsgUpdateTokenCopiesResponse = {};
export const MsgUpdateTokenCopiesResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgUpdateTokenCopiesResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgUpdateTokenCopiesResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgUpdateTokenCopiesResponse,
        };
        return message;
    },
};
const baseMsgDeleteTokenCopies = { creator: '', index: '' };
export const MsgDeleteTokenCopies = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.index !== '') {
            writer.uint32(18).string(message.index);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgDeleteTokenCopies };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.index = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgDeleteTokenCopies };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.index !== undefined && (obj.index = message.index);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgDeleteTokenCopies };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        return message;
    },
};
const baseMsgDeleteTokenCopiesResponse = {};
export const MsgDeleteTokenCopiesResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgDeleteTokenCopiesResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgDeleteTokenCopiesResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgDeleteTokenCopiesResponse,
        };
        return message;
    },
};
const baseMsgCreateDocumentTokenMapper = {
    creator: '',
    documentId: '',
    tokenId: '',
};
export const MsgCreateDocumentTokenMapper = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.documentId !== '') {
            writer.uint32(18).string(message.documentId);
        }
        if (message.tokenId !== '') {
            writer.uint32(26).string(message.tokenId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgCreateDocumentTokenMapper,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.documentId = reader.string();
                    break;
                case 3:
                    message.tokenId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgCreateDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = String(object.documentId);
        }
        else {
            message.documentId = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = String(object.tokenId);
        }
        else {
            message.tokenId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.documentId !== undefined && (obj.documentId = message.documentId);
        message.tokenId !== undefined && (obj.tokenId = message.tokenId);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgCreateDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = object.documentId;
        }
        else {
            message.documentId = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = object.tokenId;
        }
        else {
            message.tokenId = '';
        }
        return message;
    },
};
const baseMsgCreateDocumentTokenMapperResponse = {};
export const MsgCreateDocumentTokenMapperResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgCreateDocumentTokenMapperResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgCreateDocumentTokenMapperResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgCreateDocumentTokenMapperResponse,
        };
        return message;
    },
};
const baseMsgUpdateDocumentTokenMapper = {
    creator: '',
    documentId: '',
    tokenId: '',
};
export const MsgUpdateDocumentTokenMapper = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.documentId !== '') {
            writer.uint32(18).string(message.documentId);
        }
        if (message.tokenId !== '') {
            writer.uint32(26).string(message.tokenId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgUpdateDocumentTokenMapper,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.documentId = reader.string();
                    break;
                case 3:
                    message.tokenId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgUpdateDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = String(object.documentId);
        }
        else {
            message.documentId = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = String(object.tokenId);
        }
        else {
            message.tokenId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.documentId !== undefined && (obj.documentId = message.documentId);
        message.tokenId !== undefined && (obj.tokenId = message.tokenId);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgUpdateDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = object.documentId;
        }
        else {
            message.documentId = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = object.tokenId;
        }
        else {
            message.tokenId = '';
        }
        return message;
    },
};
const baseMsgUpdateDocumentTokenMapperResponse = {};
export const MsgUpdateDocumentTokenMapperResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgUpdateDocumentTokenMapperResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgUpdateDocumentTokenMapperResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgUpdateDocumentTokenMapperResponse,
        };
        return message;
    },
};
const baseMsgDeleteDocumentTokenMapper = { creator: '', index: '' };
export const MsgDeleteDocumentTokenMapper = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.index !== '') {
            writer.uint32(18).string(message.index);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgDeleteDocumentTokenMapper,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.index = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgDeleteDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = String(object.index);
        }
        else {
            message.index = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.index !== undefined && (obj.index = message.index);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgDeleteDocumentTokenMapper,
        };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.index !== undefined && object.index !== null) {
            message.index = object.index;
        }
        else {
            message.index = '';
        }
        return message;
    },
};
const baseMsgDeleteDocumentTokenMapperResponse = {};
export const MsgDeleteDocumentTokenMapperResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgDeleteDocumentTokenMapperResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = {
            ...baseMsgDeleteDocumentTokenMapperResponse,
        };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = {
            ...baseMsgDeleteDocumentTokenMapperResponse,
        };
        return message;
    },
};
const baseMsgMoveTokenToWallet = {
    creator: '',
    tokenRefId: '',
    sourceSegmentId: '',
    targetSegmentId: '',
};
export const MsgMoveTokenToWallet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.sourceSegmentId !== '') {
            writer.uint32(26).string(message.sourceSegmentId);
        }
        if (message.targetSegmentId !== '') {
            writer.uint32(34).string(message.targetSegmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgMoveTokenToWallet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 3:
                    message.sourceSegmentId = reader.string();
                    break;
                case 4:
                    message.targetSegmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgMoveTokenToWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = String(object.sourceSegmentId);
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = String(object.targetSegmentId);
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.sourceSegmentId !== undefined &&
            (obj.sourceSegmentId = message.sourceSegmentId);
        message.targetSegmentId !== undefined &&
            (obj.targetSegmentId = message.targetSegmentId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgMoveTokenToWallet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.sourceSegmentId !== undefined &&
            object.sourceSegmentId !== null) {
            message.sourceSegmentId = object.sourceSegmentId;
        }
        else {
            message.sourceSegmentId = '';
        }
        if (object.targetSegmentId !== undefined &&
            object.targetSegmentId !== null) {
            message.targetSegmentId = object.targetSegmentId;
        }
        else {
            message.targetSegmentId = '';
        }
        return message;
    },
};
const baseMsgCreateToken = {
    creator: '',
    tokenType: '',
    changeMessage: '',
    segmentId: '',
    moduleRef: '',
};
export const MsgCreateToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenType !== '') {
            writer.uint32(18).string(message.tokenType);
        }
        if (message.changeMessage !== '') {
            writer.uint32(26).string(message.changeMessage);
        }
        if (message.segmentId !== '') {
            writer.uint32(34).string(message.segmentId);
        }
        if (message.moduleRef !== '') {
            writer.uint32(42).string(message.moduleRef);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenType = reader.string();
                    break;
                case 3:
                    message.changeMessage = reader.string();
                    break;
                case 4:
                    message.segmentId = reader.string();
                    break;
                case 5:
                    message.moduleRef = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenType !== undefined && object.tokenType !== null) {
            message.tokenType = String(object.tokenType);
        }
        else {
            message.tokenType = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = String(object.changeMessage);
        }
        else {
            message.changeMessage = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = String(object.moduleRef);
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenType !== undefined && (obj.tokenType = message.tokenType);
        message.changeMessage !== undefined &&
            (obj.changeMessage = message.changeMessage);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenType !== undefined && object.tokenType !== null) {
            message.tokenType = object.tokenType;
        }
        else {
            message.tokenType = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = object.changeMessage;
        }
        else {
            message.changeMessage = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = object.moduleRef;
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
};
const baseMsgUpdateToken = {
    creator: '',
    tokenRefId: '',
    tokenType: '',
    changeMessage: '',
    segmentId: '',
    moduleRef: '',
};
export const MsgUpdateToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenRefId !== '') {
            writer.uint32(18).string(message.tokenRefId);
        }
        if (message.tokenType !== '') {
            writer.uint32(34).string(message.tokenType);
        }
        if (message.changeMessage !== '') {
            writer.uint32(42).string(message.changeMessage);
        }
        if (message.segmentId !== '') {
            writer.uint32(50).string(message.segmentId);
        }
        if (message.moduleRef !== '') {
            writer.uint32(58).string(message.moduleRef);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenRefId = reader.string();
                    break;
                case 4:
                    message.tokenType = reader.string();
                    break;
                case 5:
                    message.changeMessage = reader.string();
                    break;
                case 6:
                    message.segmentId = reader.string();
                    break;
                case 7:
                    message.moduleRef = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = String(object.tokenRefId);
        }
        else {
            message.tokenRefId = '';
        }
        if (object.tokenType !== undefined && object.tokenType !== null) {
            message.tokenType = String(object.tokenType);
        }
        else {
            message.tokenType = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = String(object.changeMessage);
        }
        else {
            message.changeMessage = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = String(object.moduleRef);
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenRefId !== undefined && (obj.tokenRefId = message.tokenRefId);
        message.tokenType !== undefined && (obj.tokenType = message.tokenType);
        message.changeMessage !== undefined &&
            (obj.changeMessage = message.changeMessage);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenRefId !== undefined && object.tokenRefId !== null) {
            message.tokenRefId = object.tokenRefId;
        }
        else {
            message.tokenRefId = '';
        }
        if (object.tokenType !== undefined && object.tokenType !== null) {
            message.tokenType = object.tokenType;
        }
        else {
            message.tokenType = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = object.changeMessage;
        }
        else {
            message.changeMessage = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = object.moduleRef;
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
};
const baseMsgIdResponse = { id: '' };
export const MsgIdResponse = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgIdResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgIdResponse };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgIdResponse };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgActivateToken = {
    creator: '',
    id: '',
    segmentId: '',
    moduleRef: '',
};
export const MsgActivateToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.segmentId !== '') {
            writer.uint32(26).string(message.segmentId);
        }
        if (message.moduleRef !== '') {
            writer.uint32(34).string(message.moduleRef);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgActivateToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.segmentId = reader.string();
                    break;
                case 4:
                    message.moduleRef = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgActivateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = String(object.moduleRef);
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgActivateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = object.moduleRef;
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
};
const baseMsgDeactivateToken = { creator: '', id: '' };
export const MsgDeactivateToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgDeactivateToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgDeactivateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgDeactivateToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseHashTokenMetadata = { documentType: '' };
export const HashTokenMetadata = {
    encode(message, writer = Writer.create()) {
        if (message.documentType !== '') {
            writer.uint32(10).string(message.documentType);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseHashTokenMetadata };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.documentType = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseHashTokenMetadata };
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = String(object.documentType);
        }
        else {
            message.documentType = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.documentType !== undefined &&
            (obj.documentType = message.documentType);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseHashTokenMetadata };
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = object.documentType;
        }
        else {
            message.documentType = '';
        }
        return message;
    },
};
const baseMsgCloneToken = {
    creator: '',
    tokenId: '',
    walletId: '',
    moduleRef: '',
};
export const MsgCloneToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.tokenId !== '') {
            writer.uint32(18).string(message.tokenId);
        }
        if (message.walletId !== '') {
            writer.uint32(26).string(message.walletId);
        }
        if (message.moduleRef !== '') {
            writer.uint32(34).string(message.moduleRef);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCloneToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.tokenId = reader.string();
                    break;
                case 3:
                    message.walletId = reader.string();
                    break;
                case 4:
                    message.moduleRef = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCloneToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = String(object.tokenId);
        }
        else {
            message.tokenId = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = String(object.walletId);
        }
        else {
            message.walletId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = String(object.moduleRef);
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.tokenId !== undefined && (obj.tokenId = message.tokenId);
        message.walletId !== undefined && (obj.walletId = message.walletId);
        message.moduleRef !== undefined && (obj.moduleRef = message.moduleRef);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCloneToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.tokenId !== undefined && object.tokenId !== null) {
            message.tokenId = object.tokenId;
        }
        else {
            message.tokenId = '';
        }
        if (object.walletId !== undefined && object.walletId !== null) {
            message.walletId = object.walletId;
        }
        else {
            message.walletId = '';
        }
        if (object.moduleRef !== undefined && object.moduleRef !== null) {
            message.moduleRef = object.moduleRef;
        }
        else {
            message.moduleRef = '';
        }
        return message;
    },
};
const baseMsgRevertToGenesis = { creator: '' };
export const MsgRevertToGenesis = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgRevertToGenesis };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgRevertToGenesis };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgRevertToGenesis };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgEmptyResponse = {};
export const MsgEmptyResponse = {
    encode(_, writer = Writer.create()) {
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgEmptyResponse };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(_) {
        const message = { ...baseMsgEmptyResponse };
        return message;
    },
    toJSON(_) {
        const obj = {};
        return obj;
    },
    fromPartial(_) {
        const message = { ...baseMsgEmptyResponse };
        return message;
    },
};
const baseMsgCreateHashToken = {
    creator: '',
    changeMessage: '',
    hash: '',
    hashFunction: '',
    documentType: '',
};
export const MsgCreateHashToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.changeMessage !== '') {
            writer.uint32(18).string(message.changeMessage);
        }
        if (message.hash !== '') {
            writer.uint32(42).string(message.hash);
        }
        if (message.hashFunction !== '') {
            writer.uint32(50).string(message.hashFunction);
        }
        if (message.documentType !== '') {
            writer.uint32(58).string(message.documentType);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateHashToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.changeMessage = reader.string();
                    break;
                case 5:
                    message.hash = reader.string();
                    break;
                case 6:
                    message.hashFunction = reader.string();
                    break;
                case 7:
                    message.documentType = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateHashToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = String(object.changeMessage);
        }
        else {
            message.changeMessage = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = String(object.hashFunction);
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = String(object.documentType);
        }
        else {
            message.documentType = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.changeMessage !== undefined &&
            (obj.changeMessage = message.changeMessage);
        message.hash !== undefined && (obj.hash = message.hash);
        message.hashFunction !== undefined &&
            (obj.hashFunction = message.hashFunction);
        message.documentType !== undefined &&
            (obj.documentType = message.documentType);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateHashToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = object.changeMessage;
        }
        else {
            message.changeMessage = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = object.hashFunction;
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = object.documentType;
        }
        else {
            message.documentType = '';
        }
        return message;
    },
};
const baseMsgAttachHashToken = {
    creator: '',
    targetId: '',
    changeMessage: '',
    hash: '',
    hashFunction: '',
    documentType: '',
};
export const MsgAttachHashToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.targetId !== '') {
            writer.uint32(18).string(message.targetId);
        }
        if (message.changeMessage !== '') {
            writer.uint32(26).string(message.changeMessage);
        }
        if (message.hash !== '') {
            writer.uint32(34).string(message.hash);
        }
        if (message.hashFunction !== '') {
            writer.uint32(42).string(message.hashFunction);
        }
        if (message.documentType !== '') {
            writer.uint32(50).string(message.documentType);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgAttachHashToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.targetId = reader.string();
                    break;
                case 3:
                    message.changeMessage = reader.string();
                    break;
                case 4:
                    message.hash = reader.string();
                    break;
                case 5:
                    message.hashFunction = reader.string();
                    break;
                case 6:
                    message.documentType = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgAttachHashToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.targetId !== undefined && object.targetId !== null) {
            message.targetId = String(object.targetId);
        }
        else {
            message.targetId = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = String(object.changeMessage);
        }
        else {
            message.changeMessage = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = String(object.hashFunction);
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = String(object.documentType);
        }
        else {
            message.documentType = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.targetId !== undefined && (obj.targetId = message.targetId);
        message.changeMessage !== undefined &&
            (obj.changeMessage = message.changeMessage);
        message.hash !== undefined && (obj.hash = message.hash);
        message.hashFunction !== undefined &&
            (obj.hashFunction = message.hashFunction);
        message.documentType !== undefined &&
            (obj.documentType = message.documentType);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgAttachHashToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.targetId !== undefined && object.targetId !== null) {
            message.targetId = object.targetId;
        }
        else {
            message.targetId = '';
        }
        if (object.changeMessage !== undefined && object.changeMessage !== null) {
            message.changeMessage = object.changeMessage;
        }
        else {
            message.changeMessage = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = object.hashFunction;
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = object.documentType;
        }
        else {
            message.documentType = '';
        }
        return message;
    },
};
const baseMsgFetchDocumentHash = {
    creator: '',
    id: '',
    documentType: '',
    timestamp: '',
};
export const MsgFetchDocumentHash = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.documentType !== '') {
            writer.uint32(26).string(message.documentType);
        }
        if (message.timestamp !== '') {
            writer.uint32(34).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchDocumentHash };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.documentType = reader.string();
                    break;
                case 4:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchDocumentHash };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = String(object.documentType);
        }
        else {
            message.documentType = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.documentType !== undefined &&
            (obj.documentType = message.documentType);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchDocumentHash };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = object.documentType;
        }
        else {
            message.documentType = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseMsgFetchDocumentHashResponse = {
    id: '',
    processId: '',
    creator: '',
    hash: '',
    hashFunction: '',
    documentType: '',
    timestamp: '',
};
export const MsgFetchDocumentHashResponse = {
    encode(message, writer = Writer.create()) {
        if (message.id !== '') {
            writer.uint32(10).string(message.id);
        }
        if (message.processId !== '') {
            writer.uint32(18).string(message.processId);
        }
        if (message.creator !== '') {
            writer.uint32(26).string(message.creator);
        }
        if (message.hash !== '') {
            writer.uint32(34).string(message.hash);
        }
        if (message.hashFunction !== '') {
            writer.uint32(42).string(message.hashFunction);
        }
        if (message.documentType !== '') {
            writer.uint32(50).string(message.documentType);
        }
        if (message.timestamp !== '') {
            writer.uint32(58).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchDocumentHashResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.id = reader.string();
                    break;
                case 2:
                    message.processId = reader.string();
                    break;
                case 3:
                    message.creator = reader.string();
                    break;
                case 4:
                    message.hash = reader.string();
                    break;
                case 5:
                    message.hashFunction = reader.string();
                    break;
                case 6:
                    message.documentType = reader.string();
                    break;
                case 7:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchDocumentHashResponse,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = String(object.processId);
        }
        else {
            message.processId = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = String(object.hash);
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = String(object.hashFunction);
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = String(object.documentType);
        }
        else {
            message.documentType = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.id !== undefined && (obj.id = message.id);
        message.processId !== undefined && (obj.processId = message.processId);
        message.creator !== undefined && (obj.creator = message.creator);
        message.hash !== undefined && (obj.hash = message.hash);
        message.hashFunction !== undefined &&
            (obj.hashFunction = message.hashFunction);
        message.documentType !== undefined &&
            (obj.documentType = message.documentType);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchDocumentHashResponse,
        };
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = object.processId;
        }
        else {
            message.processId = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.hash !== undefined && object.hash !== null) {
            message.hash = object.hash;
        }
        else {
            message.hash = '';
        }
        if (object.hashFunction !== undefined && object.hashFunction !== null) {
            message.hashFunction = object.hashFunction;
        }
        else {
            message.hashFunction = '';
        }
        if (object.documentType !== undefined && object.documentType !== null) {
            message.documentType = object.documentType;
        }
        else {
            message.documentType = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseMsgFetchProcessResponse = { processId: '' };
export const MsgFetchProcessResponse = {
    encode(message, writer = Writer.create()) {
        if (message.processId !== '') {
            writer.uint32(10).string(message.processId);
        }
        for (const v of message.documents) {
            MsgFetchDocumentHashResponse.encode(v, writer.uint32(18).fork()).ldelim();
        }
        for (const v of message.keyData) {
            MsgFetchKeyDataSetResponse.encode(v, writer.uint32(26).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchProcessResponse,
        };
        message.documents = [];
        message.keyData = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.processId = reader.string();
                    break;
                case 2:
                    message.documents.push(MsgFetchDocumentHashResponse.decode(reader, reader.uint32()));
                    break;
                case 3:
                    message.keyData.push(MsgFetchKeyDataSetResponse.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchProcessResponse,
        };
        message.documents = [];
        message.keyData = [];
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = String(object.processId);
        }
        else {
            message.processId = '';
        }
        if (object.documents !== undefined && object.documents !== null) {
            for (const e of object.documents) {
                message.documents.push(MsgFetchDocumentHashResponse.fromJSON(e));
            }
        }
        if (object.keyData !== undefined && object.keyData !== null) {
            for (const e of object.keyData) {
                message.keyData.push(MsgFetchKeyDataSetResponse.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.processId !== undefined && (obj.processId = message.processId);
        if (message.documents) {
            obj.documents = message.documents.map((e) => e ? MsgFetchDocumentHashResponse.toJSON(e) : undefined);
        }
        else {
            obj.documents = [];
        }
        if (message.keyData) {
            obj.keyData = message.keyData.map((e) => e ? MsgFetchKeyDataSetResponse.toJSON(e) : undefined);
        }
        else {
            obj.keyData = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchProcessResponse,
        };
        message.documents = [];
        message.keyData = [];
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = object.processId;
        }
        else {
            message.processId = '';
        }
        if (object.documents !== undefined && object.documents !== null) {
            for (const e of object.documents) {
                message.documents.push(MsgFetchDocumentHashResponse.fromPartial(e));
            }
        }
        if (object.keyData !== undefined && object.keyData !== null) {
            for (const e of object.keyData) {
                message.keyData.push(MsgFetchKeyDataSetResponse.fromPartial(e));
            }
        }
        return message;
    },
};
const baseMsgFetchKeyDataSetResponse = {
    goodsPassportId: '',
    data: '',
    creator: '',
    timestamp: '',
};
export const MsgFetchKeyDataSetResponse = {
    encode(message, writer = Writer.create()) {
        if (message.goodsPassportId !== '') {
            writer.uint32(10).string(message.goodsPassportId);
        }
        if (message.data !== '') {
            writer.uint32(26).string(message.data);
        }
        if (message.creator !== '') {
            writer.uint32(42).string(message.creator);
        }
        if (message.timestamp !== '') {
            writer.uint32(50).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchKeyDataSetResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.goodsPassportId = reader.string();
                    break;
                case 3:
                    message.data = reader.string();
                    break;
                case 5:
                    message.creator = reader.string();
                    break;
                case 6:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchKeyDataSetResponse,
        };
        if (object.goodsPassportId !== undefined &&
            object.goodsPassportId !== null) {
            message.goodsPassportId = String(object.goodsPassportId);
        }
        else {
            message.goodsPassportId = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = String(object.data);
        }
        else {
            message.data = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.goodsPassportId !== undefined &&
            (obj.goodsPassportId = message.goodsPassportId);
        message.data !== undefined && (obj.data = message.data);
        message.creator !== undefined && (obj.creator = message.creator);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchKeyDataSetResponse,
        };
        if (object.goodsPassportId !== undefined &&
            object.goodsPassportId !== null) {
            message.goodsPassportId = object.goodsPassportId;
        }
        else {
            message.goodsPassportId = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = object.data;
        }
        else {
            message.data = '';
        }
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseMsgCreateKeyDataSet = {
    creator: '',
    processId: '',
    data: '',
    goodsPassportId: '',
};
export const MsgCreateKeyDataSet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.processId !== '') {
            writer.uint32(18).string(message.processId);
        }
        if (message.data !== '') {
            writer.uint32(26).string(message.data);
        }
        if (message.goodsPassportId !== '') {
            writer.uint32(34).string(message.goodsPassportId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgCreateKeyDataSet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.processId = reader.string();
                    break;
                case 3:
                    message.data = reader.string();
                    break;
                case 4:
                    message.goodsPassportId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgCreateKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = String(object.processId);
        }
        else {
            message.processId = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = String(object.data);
        }
        else {
            message.data = '';
        }
        if (object.goodsPassportId !== undefined &&
            object.goodsPassportId !== null) {
            message.goodsPassportId = String(object.goodsPassportId);
        }
        else {
            message.goodsPassportId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.processId !== undefined && (obj.processId = message.processId);
        message.data !== undefined && (obj.data = message.data);
        message.goodsPassportId !== undefined &&
            (obj.goodsPassportId = message.goodsPassportId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgCreateKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.processId !== undefined && object.processId !== null) {
            message.processId = object.processId;
        }
        else {
            message.processId = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = object.data;
        }
        else {
            message.data = '';
        }
        if (object.goodsPassportId !== undefined &&
            object.goodsPassportId !== null) {
            message.goodsPassportId = object.goodsPassportId;
        }
        else {
            message.goodsPassportId = '';
        }
        return message;
    },
};
const baseMsgUpdateKeyDataSet = { creator: '', id: '', data: '' };
export const MsgUpdateKeyDataSet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.data !== '') {
            writer.uint32(26).string(message.data);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgUpdateKeyDataSet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.data = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgUpdateKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = String(object.data);
        }
        else {
            message.data = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.data !== undefined && (obj.data = message.data);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgUpdateKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.data !== undefined && object.data !== null) {
            message.data = object.data;
        }
        else {
            message.data = '';
        }
        return message;
    },
};
const baseMsgFetchProcess = { creator: '', id: '', timestamp: '' };
export const MsgFetchProcess = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.timestamp !== '') {
            writer.uint32(26).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchProcess };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchProcess };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchProcess };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseMsgFetchKeyDataSet = { creator: '', id: '', timestamp: '' };
export const MsgFetchKeyDataSet = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        if (message.timestamp !== '') {
            writer.uint32(26).string(message.timestamp);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchKeyDataSet };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                case 3:
                    message.timestamp = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = String(object.timestamp);
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        message.timestamp !== undefined && (obj.timestamp = message.timestamp);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchKeyDataSet };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        if (object.timestamp !== undefined && object.timestamp !== null) {
            message.timestamp = object.timestamp;
        }
        else {
            message.timestamp = '';
        }
        return message;
    },
};
const baseMsgFetchTokenHistory = { creator: '', id: '' };
export const MsgFetchTokenHistory = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        if (message.id !== '') {
            writer.uint32(18).string(message.id);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchTokenHistory };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                case 2:
                    message.id = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchTokenHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = String(object.id);
        }
        else {
            message.id = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        message.id !== undefined && (obj.id = message.id);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchTokenHistory };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        if (object.id !== undefined && object.id !== null) {
            message.id = object.id;
        }
        else {
            message.id = '';
        }
        return message;
    },
};
const baseMsgFetchTokenHistoryResponse = {};
export const MsgFetchTokenHistoryResponse = {
    encode(message, writer = Writer.create()) {
        if (message.TokenHistory !== undefined) {
            TokenHistory.encode(message.TokenHistory, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchTokenHistoryResponse,
        };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.TokenHistory = TokenHistory.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchTokenHistoryResponse,
        };
        if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
            message.TokenHistory = TokenHistory.fromJSON(object.TokenHistory);
        }
        else {
            message.TokenHistory = undefined;
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.TokenHistory !== undefined &&
            (obj.TokenHistory = message.TokenHistory
                ? TokenHistory.toJSON(message.TokenHistory)
                : undefined);
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchTokenHistoryResponse,
        };
        if (object.TokenHistory !== undefined && object.TokenHistory !== null) {
            message.TokenHistory = TokenHistory.fromPartial(object.TokenHistory);
        }
        else {
            message.TokenHistory = undefined;
        }
        return message;
    },
};
const baseMsgFetchAllToken = { creator: '' };
export const MsgFetchAllToken = {
    encode(message, writer = Writer.create()) {
        if (message.creator !== '') {
            writer.uint32(10).string(message.creator);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseMsgFetchAllToken };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.creator = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseMsgFetchAllToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = String(object.creator);
        }
        else {
            message.creator = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.creator !== undefined && (obj.creator = message.creator);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseMsgFetchAllToken };
        if (object.creator !== undefined && object.creator !== null) {
            message.creator = object.creator;
        }
        else {
            message.creator = '';
        }
        return message;
    },
};
const baseMsgFetchAllTokenResponse = {};
export const MsgFetchAllTokenResponse = {
    encode(message, writer = Writer.create()) {
        for (const v of message.Token) {
            Token.encode(v, writer.uint32(10).fork()).ldelim();
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = {
            ...baseMsgFetchAllTokenResponse,
        };
        message.Token = [];
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.Token.push(Token.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = {
            ...baseMsgFetchAllTokenResponse,
        };
        message.Token = [];
        if (object.Token !== undefined && object.Token !== null) {
            for (const e of object.Token) {
                message.Token.push(Token.fromJSON(e));
            }
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        if (message.Token) {
            obj.Token = message.Token.map((e) => (e ? Token.toJSON(e) : undefined));
        }
        else {
            obj.Token = [];
        }
        return obj;
    },
    fromPartial(object) {
        const message = {
            ...baseMsgFetchAllTokenResponse,
        };
        message.Token = [];
        if (object.Token !== undefined && object.Token !== null) {
            for (const e of object.Token) {
                message.Token.push(Token.fromPartial(e));
            }
        }
        return message;
    },
};
export class MsgClientImpl {
    constructor(rpc) {
        this.rpc = rpc;
    }
    FetchAllSegmentHistory(request) {
        const data = MsgFetchAllSegmentHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllSegmentHistory', data);
        return promise.then((data) => MsgFetchAllSegmentHistoryResponse.decode(new Reader(data)));
    }
    FetchGetWallet(request) {
        const data = MsgFetchGetWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchGetWallet', data);
        return promise.then((data) => MsgFetchGetWalletResponse.decode(new Reader(data)));
    }
    FetchGetSegment(request) {
        const data = MsgFetchGetSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchGetSegment', data);
        return promise.then((data) => MsgFetchGetSegmentResponse.decode(new Reader(data)));
    }
    FetchAllSegment(request) {
        const data = MsgFetchAllSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllSegment', data);
        return promise.then((data) => MsgFetchAllSegmentResponse.decode(new Reader(data)));
    }
    UpdateWallet(request) {
        const data = MsgUpdateWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'UpdateWallet', data);
        return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
    }
    CreateWallet(request) {
        const data = MsgCreateWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateWallet', data);
        return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
    }
    UpdateSegment(request) {
        const data = MsgUpdateSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'UpdateSegment', data);
        return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
    }
    FetchGetWalletHistory(request) {
        const data = MsgFetchGetWalletHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchGetWalletHistory', data);
        return promise.then((data) => MsgFetchGetWalletHistoryResponse.decode(new Reader(data)));
    }
    CreateSegment(request) {
        const data = MsgCreateSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateSegment', data);
        return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
    }
    FetchAllTokenHistoryGlobal(request) {
        const data = MsgFetchAllTokenHistoryGlobal.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllTokenHistoryGlobal', data);
        return promise.then((data) => MsgFetchAllTokenHistoryGlobalResponse.decode(new Reader(data)));
    }
    FetchGetTokenHistoryGlobal(request) {
        const data = MsgFetchGetTokenHistoryGlobal.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchGetTokenHistoryGlobal', data);
        return promise.then((data) => MsgFetchGetTokenHistoryGlobalResponse.decode(new Reader(data)));
    }
    CreateWalletWithId(request) {
        const data = MsgCreateWalletWithId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateWalletWithId', data);
        return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
    }
    MoveTokenToSegment(request) {
        const data = MsgMoveTokenToSegment.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'MoveTokenToSegment', data);
        return promise.then((data) => MsgEmptyResponse1.decode(new Reader(data)));
    }
    FetchAllWalletHistory(request) {
        const data = MsgFetchAllWalletHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllWalletHistory', data);
        return promise.then((data) => MsgFetchAllWalletHistoryResponse.decode(new Reader(data)));
    }
    FetchAllWallet(request) {
        const data = MsgFetchAllWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllWallet', data);
        return promise.then((data) => MsgFetchAllWalletResponse.decode(new Reader(data)));
    }
    FetchSegmentHistory(request) {
        const data = MsgFetchSegmentHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchSegmentHistory', data);
        return promise.then((data) => MsgFetchGetSegmentHistoryResponse.decode(new Reader(data)));
    }
    CreateSegmentWithId(request) {
        const data = MsgCreateSegmentWithId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateSegmentWithId', data);
        return promise.then((data) => MsgIdResponse2.decode(new Reader(data)));
    }
    FetchTokensByWalletId(request) {
        const data = MsgFetchTokensByWalletId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchTokensByWalletId', data);
        return promise.then((data) => QueryAllTokenResponse.decode(new Reader(data)));
    }
    FetchTokensBySegmentId(request) {
        const data = MsgFetchTokensBySegmentId.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchTokensBySegmentId', data);
        return promise.then((data) => QueryAllTokenResponse.decode(new Reader(data)));
    }
    CreateDocumentTokenMapper(request) {
        const data = MsgCreateDocumentTokenMapper.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateDocumentTokenMapper', data);
        return promise.then((data) => MsgCreateDocumentTokenMapperResponse.decode(new Reader(data)));
    }
    UpdateDocumentTokenMapper(request) {
        const data = MsgUpdateDocumentTokenMapper.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'UpdateDocumentTokenMapper', data);
        return promise.then((data) => MsgUpdateDocumentTokenMapperResponse.decode(new Reader(data)));
    }
    CreateToken(request) {
        const data = MsgCreateToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateToken', data);
        return promise.then((data) => MsgIdResponse.decode(new Reader(data)));
    }
    UpdateToken(request) {
        const data = MsgUpdateToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'UpdateToken', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    MoveTokenToWallet(request) {
        const data = MsgMoveTokenToWallet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'MoveTokenToWallet', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    ActivateToken(request) {
        const data = MsgActivateToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'ActivateToken', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    DeactivateToken(request) {
        const data = MsgDeactivateToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'DeactivateToken', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    CloneToken(request) {
        const data = MsgCloneToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CloneToken', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    RevertModulesToGenesis(request) {
        const data = MsgRevertToGenesis.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'RevertModulesToGenesis', data);
        return promise.then((data) => MsgEmptyResponse.decode(new Reader(data)));
    }
    StoreDocumentHash(request) {
        const data = MsgCreateHashToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'StoreDocumentHash', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    AttachDocumentHash(request) {
        const data = MsgAttachHashToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'AttachDocumentHash', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    FetchDocumentHash(request) {
        const data = MsgFetchDocumentHash.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchDocumentHash', data);
        return promise.then((data) => MsgFetchDocumentHashResponse.decode(new Reader(data)));
    }
    CreateKeyDataSet(request) {
        const data = MsgCreateKeyDataSet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'CreateKeyDataSet', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    UpdateKeyDataSet(request) {
        const data = MsgUpdateKeyDataSet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'UpdateKeyDataSet', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    FetchKeyDataSet(request) {
        const data = MsgFetchKeyDataSet.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchKeyDataSet', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    FetchProcess(request) {
        const data = MsgFetchProcess.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchProcess', data);
        return promise.then((data) => MsgFetchProcessResponse.decode(new Reader(data)));
    }
    FetchAllToken(request) {
        const data = MsgFetchAllToken.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchAllToken', data);
        return promise.then((data) => MsgFetchAllTokenResponse.decode(new Reader(data)));
    }
    FetchTokenHistory(request) {
        const data = MsgFetchTokenHistory.encode(request).finish();
        const promise = this.rpc.request('silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic.Msg', 'FetchTokenHistory', data);
        return promise.then((data) => MsgFetchTokenHistoryResponse.decode(new Reader(data)));
    }
}
