/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

export interface DocumentTokenMapper {
  creator: string;
  documentId: string;
  tokenId: string;
}
export declare const DocumentTokenMapper: {
  encode(message: DocumentTokenMapper, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): DocumentTokenMapper;
  fromJSON(object: any): DocumentTokenMapper;
  toJSON(message: DocumentTokenMapper): unknown;
  fromPartial(object: DeepPartial<DocumentTokenMapper>): DocumentTokenMapper;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
