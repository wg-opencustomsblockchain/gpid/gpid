/* eslint-disable */
import { Writer, Reader } from 'protobufjs/minimal';
export const protobufPackage = 'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';
const baseDocumentSegmentMapper = { documentId: '', segmentId: '' };
export const DocumentSegmentMapper = {
    encode(message, writer = Writer.create()) {
        if (message.documentId !== '') {
            writer.uint32(10).string(message.documentId);
        }
        if (message.segmentId !== '') {
            writer.uint32(18).string(message.segmentId);
        }
        return writer;
    },
    decode(input, length) {
        const reader = input instanceof Uint8Array ? new Reader(input) : input;
        let end = length === undefined ? reader.len : reader.pos + length;
        const message = { ...baseDocumentSegmentMapper };
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
                case 1:
                    message.documentId = reader.string();
                    break;
                case 2:
                    message.segmentId = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
            }
        }
        return message;
    },
    fromJSON(object) {
        const message = { ...baseDocumentSegmentMapper };
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = String(object.documentId);
        }
        else {
            message.documentId = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = String(object.segmentId);
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
    toJSON(message) {
        const obj = {};
        message.documentId !== undefined && (obj.documentId = message.documentId);
        message.segmentId !== undefined && (obj.segmentId = message.segmentId);
        return obj;
    },
    fromPartial(object) {
        const message = { ...baseDocumentSegmentMapper };
        if (object.documentId !== undefined && object.documentId !== null) {
            message.documentId = object.documentId;
        }
        else {
            message.documentId = '';
        }
        if (object.segmentId !== undefined && object.segmentId !== null) {
            message.segmentId = object.segmentId;
        }
        else {
            message.segmentId = '';
        }
        return message;
    },
};
