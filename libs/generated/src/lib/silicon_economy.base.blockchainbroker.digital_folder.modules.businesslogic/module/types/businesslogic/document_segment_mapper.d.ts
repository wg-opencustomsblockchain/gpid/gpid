/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.businesslogic';

export interface DocumentSegmentMapper {
  documentId: string;
  segmentId: string;
}
export declare const DocumentSegmentMapper: {
  encode(message: DocumentSegmentMapper, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): DocumentSegmentMapper;
  fromJSON(object: any): DocumentSegmentMapper;
  toJSON(message: DocumentSegmentMapper): unknown;
  fromPartial(
    object: DeepPartial<DocumentSegmentMapper>
  ): DocumentSegmentMapper;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
