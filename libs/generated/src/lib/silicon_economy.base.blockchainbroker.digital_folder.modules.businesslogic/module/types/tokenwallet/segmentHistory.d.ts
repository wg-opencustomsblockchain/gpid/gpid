/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Segment } from '../tokenwallet/segment';
import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet';
export interface SegmentHistory {
  creator: string;
  id: string;
  history: Segment[];
}
export declare const SegmentHistory: {
  encode(message: SegmentHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): SegmentHistory;
  fromJSON(object: any): SegmentHistory;
  toJSON(message: SegmentHistory): unknown;
  fromPartial(object: DeepPartial<SegmentHistory>): SegmentHistory;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
