/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Wallet } from '../tokenwallet/wallet';
import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet';
export interface WalletHistory {
  creator: string;
  id: string;
  history: Wallet[];
}
export declare const WalletHistory: {
  encode(message: WalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): WalletHistory;
  fromJSON(object: any): WalletHistory;
  toJSON(message: WalletHistory): unknown;
  fromPartial(object: DeepPartial<WalletHistory>): WalletHistory;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
