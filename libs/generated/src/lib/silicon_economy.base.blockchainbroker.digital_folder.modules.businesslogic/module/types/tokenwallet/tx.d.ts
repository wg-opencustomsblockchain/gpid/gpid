/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Reader, Writer } from 'protobufjs/minimal';
import { Segment } from '../tokenwallet/segment';
import { Wallet } from '../tokenwallet/wallet';
import { TokenHistoryGlobal } from '../tokenwallet/tokenHistoryGlobal';
import {
  PageRequest,
  PageResponse,
} from '../cosmos/base/query/v1beta1/pagination';
import { SegmentHistory } from '../tokenwallet/segmentHistory';
import { WalletHistory } from '../tokenwallet/walletHistory';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet';
/** this line is used by starport scaffolding # proto/tx/message */
export interface MsgRemoveTokenRefFromSegment {
  creator: string;
  tokenRefId: string;
  segmentId: string;
}
export interface MsgMoveTokenToSegment {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetSegmentId: string;
}
export interface MsgMoveTokenToWallet {
  creator: string;
  tokenRefId: string;
  sourceSegmentId: string;
  targetWalletId: string;
}
export interface MsgCreateTokenHistoryGlobal {
  creator: string;
  tokenId: string;
  walletIds: string[];
}
export interface MsgUpdateTokenHistoryGlobal {
  creator: string;
  id: string;
}
export interface MsgCreateSegmentHistory {
  creator: string;
  id: string;
  history: Segment[];
}
export interface MsgUpdateSegmentHistory {
  creator: string;
  id: string;
}
export interface MsgCreateWalletHistory {
  creator: string;
  id: string;
  history: Wallet[];
}
export interface MsgUpdateWalletHistory {
  creator: string;
  id: string;
}
export interface MsgCreateSegment {
  creator: string;
  name: string;
  info: string;
  walletId: string;
}
export interface MsgCreateSegmentWithId {
  creator: string;
  Id: string;
  name: string;
  info: string;
  walletId: string;
}
export interface MsgUpdateSegment {
  creator: string;
  id: string;
  name: string;
  info: string;
}
export interface MsgCreateWallet {
  creator: string;
  name: string;
}
export interface MsgCreateWalletWithId {
  creator: string;
  Id: string;
  name: string;
}
export interface MsgAssignCosmosAddressToWallet {
  creator: string;
  cosmosAddress: string;
  walletId: string;
}
export interface MsgUpdateWallet {
  creator: string;
  id: string;
  name: string;
}
export interface MsgCreateTokenRef {
  creator: string;
  id: string;
  moduleRef: string;
  segmentId: string;
}
export interface MsgEmptyResponse {}
export interface MsgIdResponse {
  id: string;
}
/** this line is used by starport scaffolding # 3 */
export interface MsgFetchGetTokenHistoryGlobal {
  creator: string;
  id: string;
}
export interface MsgFetchGetTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal | undefined;
}
export interface MsgFetchAllTokenHistoryGlobal {
  creator: string;
  pagination: PageRequest | undefined;
}
export interface MsgFetchAllTokenHistoryGlobalResponse {
  TokenHistoryGlobal: TokenHistoryGlobal[];
  pagination: PageResponse | undefined;
}
export interface MsgFetchGetSegmentHistory {
  creator: string;
  id: string;
}
export interface MsgFetchGetSegmentHistoryResponse {
  SegmentHistory: SegmentHistory | undefined;
}
export interface MsgFetchAllSegmentHistory {
  creator: string;
  pagination: PageRequest | undefined;
}
export interface MsgFetchAllSegmentHistoryResponse {
  SegmentHistory: SegmentHistory[];
  pagination: PageResponse | undefined;
}
export interface MsgFetchGetSegment {
  creator: string;
  id: string;
}
export interface MsgFetchGetSegmentResponse {
  Segment: Segment | undefined;
}
export interface MsgFetchAllSegment {
  creator: string;
  pagination: PageRequest | undefined;
}
export interface MsgFetchAllSegmentResponse {
  Segment: Segment[];
  pagination: PageResponse | undefined;
}
export interface MsgFetchGetWalletHistory {
  creator: string;
  id: string;
}
export interface MsgFetchGetWalletHistoryResponse {
  WalletHistory: WalletHistory | undefined;
}
export interface MsgFetchAllWalletHistory {
  creator: string;
  pagination: PageRequest | undefined;
}
export interface MsgFetchAllWalletHistoryResponse {
  WalletHistory: WalletHistory[];
  pagination: PageResponse | undefined;
}
export interface MsgFetchGetWallet {
  creator: string;
  id: string;
}
export interface MsgFetchGetWalletResponse {
  Wallet: Wallet | undefined;
}
export interface MsgFetchAllWallet {
  creator: string;
  pagination: PageRequest | undefined;
}
export interface MsgFetchAllWalletResponse {
  Wallet: Wallet[];
  pagination: PageResponse | undefined;
}
export declare const MsgRemoveTokenRefFromSegment: {
  encode(message: MsgRemoveTokenRefFromSegment, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgRemoveTokenRefFromSegment;
  fromJSON(object: any): MsgRemoveTokenRefFromSegment;
  toJSON(message: MsgRemoveTokenRefFromSegment): unknown;
  fromPartial(
    object: DeepPartial<MsgRemoveTokenRefFromSegment>
  ): MsgRemoveTokenRefFromSegment;
};
export declare const MsgMoveTokenToSegment: {
  encode(message: MsgMoveTokenToSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToSegment;
  fromJSON(object: any): MsgMoveTokenToSegment;
  toJSON(message: MsgMoveTokenToSegment): unknown;
  fromPartial(
    object: DeepPartial<MsgMoveTokenToSegment>
  ): MsgMoveTokenToSegment;
};
export declare const MsgMoveTokenToWallet: {
  encode(message: MsgMoveTokenToWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgMoveTokenToWallet;
  fromJSON(object: any): MsgMoveTokenToWallet;
  toJSON(message: MsgMoveTokenToWallet): unknown;
  fromPartial(object: DeepPartial<MsgMoveTokenToWallet>): MsgMoveTokenToWallet;
};
export declare const MsgCreateTokenHistoryGlobal: {
  encode(message: MsgCreateTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgCreateTokenHistoryGlobal;
  fromJSON(object: any): MsgCreateTokenHistoryGlobal;
  toJSON(message: MsgCreateTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateTokenHistoryGlobal>
  ): MsgCreateTokenHistoryGlobal;
};
export declare const MsgUpdateTokenHistoryGlobal: {
  encode(message: MsgUpdateTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgUpdateTokenHistoryGlobal;
  fromJSON(object: any): MsgUpdateTokenHistoryGlobal;
  toJSON(message: MsgUpdateTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgUpdateTokenHistoryGlobal>
  ): MsgUpdateTokenHistoryGlobal;
};
export declare const MsgCreateSegmentHistory: {
  encode(message: MsgCreateSegmentHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentHistory;
  fromJSON(object: any): MsgCreateSegmentHistory;
  toJSON(message: MsgCreateSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateSegmentHistory>
  ): MsgCreateSegmentHistory;
};
export declare const MsgUpdateSegmentHistory: {
  encode(message: MsgUpdateSegmentHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegmentHistory;
  fromJSON(object: any): MsgUpdateSegmentHistory;
  toJSON(message: MsgUpdateSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgUpdateSegmentHistory>
  ): MsgUpdateSegmentHistory;
};
export declare const MsgCreateWalletHistory: {
  encode(message: MsgCreateWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletHistory;
  fromJSON(object: any): MsgCreateWalletHistory;
  toJSON(message: MsgCreateWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateWalletHistory>
  ): MsgCreateWalletHistory;
};
export declare const MsgUpdateWalletHistory: {
  encode(message: MsgUpdateWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWalletHistory;
  fromJSON(object: any): MsgUpdateWalletHistory;
  toJSON(message: MsgUpdateWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgUpdateWalletHistory>
  ): MsgUpdateWalletHistory;
};
export declare const MsgCreateSegment: {
  encode(message: MsgCreateSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegment;
  fromJSON(object: any): MsgCreateSegment;
  toJSON(message: MsgCreateSegment): unknown;
  fromPartial(object: DeepPartial<MsgCreateSegment>): MsgCreateSegment;
};
export declare const MsgCreateSegmentWithId: {
  encode(message: MsgCreateSegmentWithId, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateSegmentWithId;
  fromJSON(object: any): MsgCreateSegmentWithId;
  toJSON(message: MsgCreateSegmentWithId): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateSegmentWithId>
  ): MsgCreateSegmentWithId;
};
export declare const MsgUpdateSegment: {
  encode(message: MsgUpdateSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateSegment;
  fromJSON(object: any): MsgUpdateSegment;
  toJSON(message: MsgUpdateSegment): unknown;
  fromPartial(object: DeepPartial<MsgUpdateSegment>): MsgUpdateSegment;
};
export declare const MsgCreateWallet: {
  encode(message: MsgCreateWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateWallet;
  fromJSON(object: any): MsgCreateWallet;
  toJSON(message: MsgCreateWallet): unknown;
  fromPartial(object: DeepPartial<MsgCreateWallet>): MsgCreateWallet;
};
export declare const MsgCreateWalletWithId: {
  encode(message: MsgCreateWalletWithId, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateWalletWithId;
  fromJSON(object: any): MsgCreateWalletWithId;
  toJSON(message: MsgCreateWalletWithId): unknown;
  fromPartial(
    object: DeepPartial<MsgCreateWalletWithId>
  ): MsgCreateWalletWithId;
};
export declare const MsgAssignCosmosAddressToWallet: {
  encode(message: MsgAssignCosmosAddressToWallet, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgAssignCosmosAddressToWallet;
  fromJSON(object: any): MsgAssignCosmosAddressToWallet;
  toJSON(message: MsgAssignCosmosAddressToWallet): unknown;
  fromPartial(
    object: DeepPartial<MsgAssignCosmosAddressToWallet>
  ): MsgAssignCosmosAddressToWallet;
};
export declare const MsgUpdateWallet: {
  encode(message: MsgUpdateWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgUpdateWallet;
  fromJSON(object: any): MsgUpdateWallet;
  toJSON(message: MsgUpdateWallet): unknown;
  fromPartial(object: DeepPartial<MsgUpdateWallet>): MsgUpdateWallet;
};
export declare const MsgCreateTokenRef: {
  encode(message: MsgCreateTokenRef, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgCreateTokenRef;
  fromJSON(object: any): MsgCreateTokenRef;
  toJSON(message: MsgCreateTokenRef): unknown;
  fromPartial(object: DeepPartial<MsgCreateTokenRef>): MsgCreateTokenRef;
};
export declare const MsgEmptyResponse: {
  encode(_: MsgEmptyResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgEmptyResponse;
  fromJSON(_: any): MsgEmptyResponse;
  toJSON(_: MsgEmptyResponse): unknown;
  fromPartial(_: DeepPartial<MsgEmptyResponse>): MsgEmptyResponse;
};
export declare const MsgIdResponse: {
  encode(message: MsgIdResponse, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgIdResponse;
  fromJSON(object: any): MsgIdResponse;
  toJSON(message: MsgIdResponse): unknown;
  fromPartial(object: DeepPartial<MsgIdResponse>): MsgIdResponse;
};
export declare const MsgFetchGetTokenHistoryGlobal: {
  encode(message: MsgFetchGetTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobal;
  fromJSON(object: any): MsgFetchGetTokenHistoryGlobal;
  toJSON(message: MsgFetchGetTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobal>
  ): MsgFetchGetTokenHistoryGlobal;
};
export declare const MsgFetchGetTokenHistoryGlobalResponse: {
  encode(
    message: MsgFetchGetTokenHistoryGlobalResponse,
    writer?: Writer
  ): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetTokenHistoryGlobalResponse;
  fromJSON(object: any): MsgFetchGetTokenHistoryGlobalResponse;
  toJSON(message: MsgFetchGetTokenHistoryGlobalResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetTokenHistoryGlobalResponse>
  ): MsgFetchGetTokenHistoryGlobalResponse;
};
export declare const MsgFetchAllTokenHistoryGlobal: {
  encode(message: MsgFetchAllTokenHistoryGlobal, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobal;
  fromJSON(object: any): MsgFetchAllTokenHistoryGlobal;
  toJSON(message: MsgFetchAllTokenHistoryGlobal): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobal>
  ): MsgFetchAllTokenHistoryGlobal;
};
export declare const MsgFetchAllTokenHistoryGlobalResponse: {
  encode(
    message: MsgFetchAllTokenHistoryGlobalResponse,
    writer?: Writer
  ): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllTokenHistoryGlobalResponse;
  fromJSON(object: any): MsgFetchAllTokenHistoryGlobalResponse;
  toJSON(message: MsgFetchAllTokenHistoryGlobalResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllTokenHistoryGlobalResponse>
  ): MsgFetchAllTokenHistoryGlobalResponse;
};
export declare const MsgFetchGetSegmentHistory: {
  encode(message: MsgFetchGetSegmentHistory, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentHistory;
  fromJSON(object: any): MsgFetchGetSegmentHistory;
  toJSON(message: MsgFetchGetSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentHistory>
  ): MsgFetchGetSegmentHistory;
};
export declare const MsgFetchGetSegmentHistoryResponse: {
  encode(message: MsgFetchGetSegmentHistoryResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentHistoryResponse;
  fromJSON(object: any): MsgFetchGetSegmentHistoryResponse;
  toJSON(message: MsgFetchGetSegmentHistoryResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentHistoryResponse>
  ): MsgFetchGetSegmentHistoryResponse;
};
export declare const MsgFetchAllSegmentHistory: {
  encode(message: MsgFetchAllSegmentHistory, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistory;
  fromJSON(object: any): MsgFetchAllSegmentHistory;
  toJSON(message: MsgFetchAllSegmentHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistory>
  ): MsgFetchAllSegmentHistory;
};
export declare const MsgFetchAllSegmentHistoryResponse: {
  encode(message: MsgFetchAllSegmentHistoryResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentHistoryResponse;
  fromJSON(object: any): MsgFetchAllSegmentHistoryResponse;
  toJSON(message: MsgFetchAllSegmentHistoryResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentHistoryResponse>
  ): MsgFetchAllSegmentHistoryResponse;
};
export declare const MsgFetchGetSegment: {
  encode(message: MsgFetchGetSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetSegment;
  fromJSON(object: any): MsgFetchGetSegment;
  toJSON(message: MsgFetchGetSegment): unknown;
  fromPartial(object: DeepPartial<MsgFetchGetSegment>): MsgFetchGetSegment;
};
export declare const MsgFetchGetSegmentResponse: {
  encode(message: MsgFetchGetSegmentResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetSegmentResponse;
  fromJSON(object: any): MsgFetchGetSegmentResponse;
  toJSON(message: MsgFetchGetSegmentResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetSegmentResponse>
  ): MsgFetchGetSegmentResponse;
};
export declare const MsgFetchAllSegment: {
  encode(message: MsgFetchAllSegment, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllSegment;
  fromJSON(object: any): MsgFetchAllSegment;
  toJSON(message: MsgFetchAllSegment): unknown;
  fromPartial(object: DeepPartial<MsgFetchAllSegment>): MsgFetchAllSegment;
};
export declare const MsgFetchAllSegmentResponse: {
  encode(message: MsgFetchAllSegmentResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllSegmentResponse;
  fromJSON(object: any): MsgFetchAllSegmentResponse;
  toJSON(message: MsgFetchAllSegmentResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllSegmentResponse>
  ): MsgFetchAllSegmentResponse;
};
export declare const MsgFetchGetWalletHistory: {
  encode(message: MsgFetchGetWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWalletHistory;
  fromJSON(object: any): MsgFetchGetWalletHistory;
  toJSON(message: MsgFetchGetWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistory>
  ): MsgFetchGetWalletHistory;
};
export declare const MsgFetchGetWalletHistoryResponse: {
  encode(message: MsgFetchGetWalletHistoryResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletHistoryResponse;
  fromJSON(object: any): MsgFetchGetWalletHistoryResponse;
  toJSON(message: MsgFetchGetWalletHistoryResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetWalletHistoryResponse>
  ): MsgFetchGetWalletHistoryResponse;
};
export declare const MsgFetchAllWalletHistory: {
  encode(message: MsgFetchAllWalletHistory, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWalletHistory;
  fromJSON(object: any): MsgFetchAllWalletHistory;
  toJSON(message: MsgFetchAllWalletHistory): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistory>
  ): MsgFetchAllWalletHistory;
};
export declare const MsgFetchAllWalletHistoryResponse: {
  encode(message: MsgFetchAllWalletHistoryResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletHistoryResponse;
  fromJSON(object: any): MsgFetchAllWalletHistoryResponse;
  toJSON(message: MsgFetchAllWalletHistoryResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllWalletHistoryResponse>
  ): MsgFetchAllWalletHistoryResponse;
};
export declare const MsgFetchGetWallet: {
  encode(message: MsgFetchGetWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchGetWallet;
  fromJSON(object: any): MsgFetchGetWallet;
  toJSON(message: MsgFetchGetWallet): unknown;
  fromPartial(object: DeepPartial<MsgFetchGetWallet>): MsgFetchGetWallet;
};
export declare const MsgFetchGetWalletResponse: {
  encode(message: MsgFetchGetWalletResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchGetWalletResponse;
  fromJSON(object: any): MsgFetchGetWalletResponse;
  toJSON(message: MsgFetchGetWalletResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchGetWalletResponse>
  ): MsgFetchGetWalletResponse;
};
export declare const MsgFetchAllWallet: {
  encode(message: MsgFetchAllWallet, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): MsgFetchAllWallet;
  fromJSON(object: any): MsgFetchAllWallet;
  toJSON(message: MsgFetchAllWallet): unknown;
  fromPartial(object: DeepPartial<MsgFetchAllWallet>): MsgFetchAllWallet;
};
export declare const MsgFetchAllWalletResponse: {
  encode(message: MsgFetchAllWalletResponse, writer?: Writer): Writer;
  decode(
    input: Reader | Uint8Array,
    length?: number
  ): MsgFetchAllWalletResponse;
  fromJSON(object: any): MsgFetchAllWalletResponse;
  toJSON(message: MsgFetchAllWalletResponse): unknown;
  fromPartial(
    object: DeepPartial<MsgFetchAllWalletResponse>
  ): MsgFetchAllWalletResponse;
};
/** Msg defines the Msg service. */
export interface Msg {
  /** this line is used by starport scaffolding # proto/tx/rpc */
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse>;
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse>;
  AssignCosmosAddressToWallet(
    request: MsgAssignCosmosAddressToWallet
  ): Promise<MsgEmptyResponse>;
  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse>;
  MoveTokenToSegment(request: MsgMoveTokenToSegment): Promise<MsgEmptyResponse>;
  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;
  RemoveTokenRefFromSegment(
    request: MsgRemoveTokenRefFromSegment
  ): Promise<MsgEmptyResponse>;
  /** Query */
  FetchTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;
  FetchTokenHistoryGlobalAll(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;
  FetchSegmentHistory(
    request: MsgFetchGetSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;
  FetchSegmentHistoryAll(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;
  FetchSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;
  FetchSegmentAll(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;
  FetchWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;
  FetchWalletHistoryAll(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;
  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse>;
  FetchWalletAll(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;
}
export declare class MsgClientImpl implements Msg {
  private readonly rpc;
  constructor(rpc: Rpc);
  CreateSegment(request: MsgCreateSegment): Promise<MsgIdResponse>;
  CreateSegmentWithId(request: MsgCreateSegmentWithId): Promise<MsgIdResponse>;
  UpdateSegment(request: MsgUpdateSegment): Promise<MsgEmptyResponse>;
  CreateWallet(request: MsgCreateWallet): Promise<MsgIdResponse>;
  CreateWalletWithId(request: MsgCreateWalletWithId): Promise<MsgIdResponse>;
  UpdateWallet(request: MsgUpdateWallet): Promise<MsgEmptyResponse>;
  AssignCosmosAddressToWallet(
    request: MsgAssignCosmosAddressToWallet
  ): Promise<MsgEmptyResponse>;
  CreateTokenRef(request: MsgCreateTokenRef): Promise<MsgIdResponse>;
  MoveTokenToSegment(request: MsgMoveTokenToSegment): Promise<MsgEmptyResponse>;
  MoveTokenToWallet(request: MsgMoveTokenToWallet): Promise<MsgEmptyResponse>;
  RemoveTokenRefFromSegment(
    request: MsgRemoveTokenRefFromSegment
  ): Promise<MsgEmptyResponse>;
  FetchTokenHistoryGlobal(
    request: MsgFetchGetTokenHistoryGlobal
  ): Promise<MsgFetchGetTokenHistoryGlobalResponse>;
  FetchTokenHistoryGlobalAll(
    request: MsgFetchAllTokenHistoryGlobal
  ): Promise<MsgFetchAllTokenHistoryGlobalResponse>;
  FetchSegmentHistory(
    request: MsgFetchGetSegmentHistory
  ): Promise<MsgFetchGetSegmentHistoryResponse>;
  FetchSegmentHistoryAll(
    request: MsgFetchAllSegmentHistory
  ): Promise<MsgFetchAllSegmentHistoryResponse>;
  FetchSegment(
    request: MsgFetchGetSegment
  ): Promise<MsgFetchGetSegmentResponse>;
  FetchSegmentAll(
    request: MsgFetchAllSegment
  ): Promise<MsgFetchAllSegmentResponse>;
  FetchWalletHistory(
    request: MsgFetchGetWalletHistory
  ): Promise<MsgFetchGetWalletHistoryResponse>;
  FetchWalletHistoryAll(
    request: MsgFetchAllWalletHistory
  ): Promise<MsgFetchAllWalletHistoryResponse>;
  FetchWallet(request: MsgFetchGetWallet): Promise<MsgFetchGetWalletResponse>;
  FetchWalletAll(
    request: MsgFetchAllWallet
  ): Promise<MsgFetchAllWalletResponse>;
}
interface Rpc {
  request(
    service: string,
    method: string,
    data: Uint8Array
  ): Promise<Uint8Array>;
}
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
