/**
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage =
  'silicon_economy.base.blockchainbroker.digital_folder.modules.tokenwallet';
export interface TokenHistoryGlobal {
  creator: string;
  id: string;
  walletId: string[];
}
export declare const TokenHistoryGlobal: {
  encode(message: TokenHistoryGlobal, writer?: Writer): Writer;
  decode(input: Reader | Uint8Array, length?: number): TokenHistoryGlobal;
  fromJSON(object: any): TokenHistoryGlobal;
  toJSON(message: TokenHistoryGlobal): unknown;
  fromPartial(object: DeepPartial<TokenHistoryGlobal>): TokenHistoryGlobal;
};
declare type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | undefined;
export declare type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? {
      [K in keyof T]?: DeepPartial<T[K]>;
    }
  : Partial<T>;
export {};
