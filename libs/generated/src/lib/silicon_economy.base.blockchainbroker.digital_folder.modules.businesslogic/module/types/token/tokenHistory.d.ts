import { Token } from '../token/token';
import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.token";
export interface TokenHistory {
    creator: string;
    id: string;
    history: Token[];
}
export declare const TokenHistory: {
    encode(message: TokenHistory, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): TokenHistory;
    fromJSON(object: any): TokenHistory;
    toJSON(message: TokenHistory): unknown;
    fromPartial(object: DeepPartial<TokenHistory>): TokenHistory;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
