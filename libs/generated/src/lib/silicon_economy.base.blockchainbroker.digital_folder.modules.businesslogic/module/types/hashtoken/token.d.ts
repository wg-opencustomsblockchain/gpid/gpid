import { Writer, Reader } from 'protobufjs/minimal';
export declare const protobufPackage = "silicon_economy.base.blockchainbroker.digital_folder.modules.hashtoken";
export interface Token {
    creator: string;
    id: string;
    tokenType: string;
    timestamp: string;
    changeMessage: string;
    valid: boolean;
    info: Info | undefined;
    segmentId: string;
}
export interface Info {
    document: string;
    hash: string;
    hashFunction: string;
    metadata: string;
}
export declare const Token: {
    encode(message: Token, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Token;
    fromJSON(object: any): Token;
    toJSON(message: Token): unknown;
    fromPartial(object: DeepPartial<Token>): Token;
};
export declare const Info: {
    encode(message: Info, writer?: Writer): Writer;
    decode(input: Reader | Uint8Array, length?: number): Info;
    fromJSON(object: any): Info;
    toJSON(message: Info): unknown;
    fromPartial(object: DeepPartial<Info>): Info;
};
declare type Builtin = Date | Function | Uint8Array | string | number | undefined;
export declare type DeepPartial<T> = T extends Builtin ? T : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>> : T extends {} ? {
    [K in keyof T]?: DeepPartial<T[K]>;
} : Partial<T>;
export {};
