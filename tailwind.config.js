/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./apps/frontend/src/**/*.{html,ts}'],
  theme: {
    extend: {},
  },
  plugins: [require('@tailwindcss/typography'), require('daisyui')],
};
