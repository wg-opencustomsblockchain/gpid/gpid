= Chapter 10: Quality Requirements

*Contents*

* <<10.1 Quality Tree>>
* <<10.2 Quality Scenarios>>

"Quality is the totality of characteristics and values of a software product that relate to its ability to meet stated or implied requirements." [_Software Quality according to https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC 9126; ISO 25010]]_

The most important quality goals have already been described in → https://oe160.iml.fraunhofer.de/wiki/display/IOT/Chapter+1%3A+Introduction+and+Goals#Chapter1:IntroductionandGoals-1.3QualityGoals[Section 1.3].
This section contains all quality requirements as quality tree with scenarios.
We also capture quality requirements with lesser priority, which will not create high risks when they are not fully achieved.
The quality scenarios depict the fundamental quality goals as well as other required quality properties.
They allow the evaluation of decision alternatives.

== 10.1 Quality Tree

We use a tabular form of the quality tree.
The following table lists the classifications (as https://en.wikipedia.org/wiki/ISO/IEC_9126[ISO/IEC9126-1] characteristics and sub-characteristics with some additional terms) and the methods used to improve and to ensure software quality.
_It is based on the Evaluation Matrix._

[cols=",,",options="header",]
|===
|Quality Goal (ISO 9126-1)
|Requirement (for Quality Improvement)
|Test of Requirement (Quality Assurance)

a|
*Reliability*

(Maturity, Fault tolerance/_Robustness_, Recoverability)

a|
* All components of {projectName} are designed to be scalable and redundant.
* If necessary, further instances of the components can be started/stopped by the Cluster in order to be able to react to load peaks.
* Appropriate unit tests exist, especially for error cases and boundary values.
* The architecture is designed such that methods depending on external services can be tested without having the external service available.

a|
* Unit tests (Jest, GoLang) following code package structure
* Using SonarCube during development and during the build process ensures a code coverage of at least 80%.
* All external dependencies are mockable, e.g. for unit tests.

a|
*Efficiency*

(Time behaviour, Resource utilization, _Scalability_)

a|
* All components of {projectName} are designed to be scalable and redundant.
* If necessary, additional instances of the components can be started/stopped by the Cluster to be able to react to load peaks.
* The Blockchain network can be expanded and shrinked based on the number of participants

a|
* No performance tests implemented yet.

a|
*Usability*

(Understandability, Learnability, Operability, Attractiveness, Simplicity)

a|
* Web interfaces are only functions around the management of {projectName}, its configuration, as well as for device management.
* Web interfaces visualize the current state of the {projectName} application, its components and connected devices.
* The {projectName} management frontend needs to appeal the Administrators and for other stakeholders especially in demonstrations.
* For Admins, the effort required to integrate new functionalities must be minimal by documentation, guide and an SDK.
* The GUI Style guide is respected.
* Complete arc42 conformance software documentation and user guides exists.
* Detailed documentation of public interfaces with OpenApi/Swagger.

a|
* Document review

a|
*Functionality*

(Suitability, Accuracy, Interoperability, Security, Completeness)

a|
* All functional requirements are fulfilled and implemented accurately and secure.
* Explicit, object-oriented domain model.
* Complete arc42 documentation, Jira tickets,
* The code is documented completely using JSDoc and Golang conventions.

a|
* Unit and integration tests
* High test coverage as a safety net
* Document Review
* Code Review

a|
*Maintainability*

(Testability, Stability, Analyzability, Changeability)

a|
* {projectName} must be connected to the Silicon Economy infrastructure and Blockchain Networks of project partners.
* {projectName} must be designed in such a way that the crash of one component or one infrastructure does not lead to a crash of the other components.
* {projectName} components are designed and implemented stateless. All states & configurations are stored in a database that is backed up regularly.
* On restart of the {projectName} application, the current configuration is loaded from the database.
* {projectName} development respects all developer guidelines.

a|
* Integration tests
* Test cases for putting into service and starting the application (e.g. DB available, interfaces).
* https://en.wikipedia.org/wiki/ISO/IEC_9126[[.underline]#CI/CDpipeline#], automatic compliance testing, code review, https://en.wikipedia.org/wiki/ISO/IEC_9126[merge requests]
|===

== 10.2 Quality Scenarios

TODO
